FROM php:7.2-cli

# Install requirements: I'll need nginx for serving the site and git and the zlib library for installing Sculpin
RUN apt-get update
RUN apt-get install -y nginx git zlib1g-dev && docker-php-ext-install zip

# Install Composer
COPY composer.phar /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer

# Install Sculpin
RUN git clone https://github.com/sculpin/sculpin.git /usr/lib/sculpin && \
    cd /usr/lib/sculpin && \
    git checkout 2.1 && \
    composer install && \
    ln -s /usr/lib/sculpin/bin/sculpin /usr/local/bin/sculpin

# Copy my custom stuff into the standard Sculpin install
COPY app /usr/lib/sculpin/app
COPY source /usr/lib/sculpin/source
COPY posts /usr/lib/sculpin/source/_posts

# Generate the new static website
RUN cd /usr/lib/sculpin && bin/sculpin generate --env=prod --url=https://skoop.dev

# Symlink the new static site to the document root
RUN rm -rf /var/www/html
RUN ln -s /usr/lib/sculpin/output_prod /var/www/html

# Run Nginx to serve the static site
CMD ["nginx", "-g", "daemon off;"]
