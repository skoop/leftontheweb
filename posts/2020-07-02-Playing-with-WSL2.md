---
layout: post
title: "Playing with WSL2"
date: 2020-07-02 12:00:00
comments: true
categories: 
  - php
  - wsl2
  - windows
social:
    highlight_image: /_posts/images/wsl2.png
    summary: I've been playing with Docker on WSL2, the new Linux integration for Windows, and Microsoft seems to have implemented a game changer for web development.
    
---

Some time ago I got fed up with the performance of Docker on Mac. Especially with the bigger projects I work on, the performance was getting horrible. After trying Windows for a bit, I switched to Windows because performance was better on Windows.

But as I was using Docker for Windows on a project last year, performance was still horrible. I was sometimes waiting for a page to load for several seconds. While Docker for Windows was performing better, it was still not performing the way I wanted to. After a while I decided to switch to Linux, just for the performance.

Linux and I have a love/hate relationship. Technologically it is far superior, especially for power users such as developers. In terms of UX... I hate it. It's better now than it was 10 years ago, but it's still not good. Especially when things go wrong, too often you have to revert to searching the Internet for a solution that requires executing complex commands and manually editting config files. I understand some people love that, but I've had it with that stuff since, I don't know, ages ago. Basically, since I switched to Mac and stuff "just worked".

So when Microsoft announced WSL2 with a full linux kernel and support for Docker, I was excited! This could mean my perfect setup could finally happen: The UX of Windows but with the Docker performance of Linux.

The update came out end of May, but I was in a big project and didn't have time to play with it. Yesterday, I finally set out to try it.

## First impressions

My first impression is quite positive. The installation of WSL2 is easy, the Docker for Windows installer immediately picks up on the fact that WSL2 is available as a backend which makes the setup of my environment a breeze. 

So the first thing I did was clone a git repository to my Windows system, head into bash and do `docker-compose up -d` to start the project. The project builds, executes Composer and runs. But I notice `composer install` is already quite slow and once the project is up and running, the pages still take quite a long time in the browser. And then it hits me... I checked out the repository on my host system, which means they basically have to go through the Windows -> WSL2 mount to linux, and then to Docker. That might slow things down.

## Second try

OK, let's try again, but now let's clone the repository directly in the WSL2 filesystem. After cloning, I again type `docker-compose up -d` and .... whoa! Hang on, the build is done already? Composer ran incredibly fast. Let's try in the browser... whoa! Again, blazingly fast! This is incredible! This is near Linux performance for Docker.

But now my files are in the WSL2 filesystem, and I want to use PHPStorm and SmartGit to edit the files and commit and push to Gitlab. Is there a way to do that? Why, yes of course there is. The smart people at Microsoft have a solution for everything I want.

It took a bit of searching around, but it is actually quite easy to find the path of the files. As it turns out, in your bash shell you can just type `explorer.exe .` and it will open a Windows Explorer window in the directory on your WSL2 filesystem. Seriously, the integration between WSL2 and Windows is amazing. The path will look something like this:

```
\\wsl$\Ubuntu-20.04\home\stefan\php\my-project
```

But that's not all: When I started PHPStorm and I created a new project from existing files, PHPStorm automatically detected the WSL2 filesystem in the new project window. I can just choose between my C-drive and the WSL filesystem. This is excellent, and so easy to set up.

## Concluding

I don't want to make a major conclusion like "Docker with WSL2 is perfect" because I haven't actually done serious development with it, but so far it is looking very good. The performance is amazing, the integration between WSL2 and the Windows operating system is great, and setting up tools such as PHPStorm and SmartGit is a breeze. Yes, this really, truly looks like a game changer for me.