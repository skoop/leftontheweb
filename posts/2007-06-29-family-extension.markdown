---
layout: post
title: "Family extension"
date: 2007-06-29 1:47
comments: true
categories: 
  - personal 
---
Well, well, it looks like Tomas will be getting a little baby brother or sister in January. We had an ultrasound made on tuesday, and it seems everything is just fine in Marjolein's belly. A little baby is growing there! 

Exciting stuff happening, that's for sure. Tomas seems to get it, sort of, that there is a little baby growing inside Marjolein. So far he's quite fine with it :)
