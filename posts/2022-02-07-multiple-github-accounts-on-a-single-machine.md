---
layout: post
title: "Multiple Github accounts on a single machine"
date: 2022-02-07 20:00:00
comments: true
categories: 
  - php
  - github
  - development
  - localhost
social:
    summary: Believe it or not, after over 20 years of PHP development I've now run into the first situation where I need more than a single Github account on the same machine. It was quite easy to solve. Sharing the solution here!
    
---

I've not used Github a lot anymore in recent times because we've switched to [Gitlab](https://gitlab.com/) some time ago. The main reason for using Github at this point is client work. But sometimes, I still need my account for open source reasons. 

For one of the customers I do work for, however, I was asked to create a separate Github account. Now I was running into some issues because the private repositories are now shared with this new account, but by default I'm logged into my own account on the commandline (well, it picks my default key). Searching around I ended up on the site of freeCodeCamp, where [Bivil M Jacob](https://www.freecodecamp.org/news/manage-multiple-github-accounts-the-ssh-way-2dadc30ccaca/) shared the solution that ended up working. The solution: Add a configuration file.

The first three steps are pretty standard and now that hard to understand, but the fourth step was new to me. I had no idea this was possible. So I followed the guidance of Bivil and ended up with my own `.ssh/config` file:

```
# Skoop, - the default config
Host github.com
   HostName github.com
   User git
   IdentityFile ~/.ssh/id_rsa
   
# clientskoop
Host client.github.com    
   HostName github.com
   User git
   IdentityFile ~/.ssh/cf_ed25519
```

I've redacted this example to not include my actual client name. Now, if I want to clone a private repository from my client, all I need to do is:

`git clone git@client.github.com:clientname/clientrepo.git`

In the above command, note the `client.github.com` host name that is the same as the Host I defined in the config file. And lo and behold, when I clone in this way I can clone without a problem. It takes the correct key for this specific repository and I can do everything I want: push, pull, etc.

Sometimes the solution is simpler than you would've imagined.