---
layout: post
title: "Comment spam"
date: 2006-01-28 4:50
comments: true
categories: 
  - textpattern 
---
Though Textpattern is still not a huge target for comment spammers, lately, this website seems to have been discovered by some comment spammers that target textpattern as well. And so began my quest for a blocking system. Before attempting to write one myself, I decided it might be easier to first check if someone already had something in place :)

Through <a href="http://forum.textpattern.com/viewtopic.php?pid=37884#p37884">this post on the textpattern forum</a>, I came to <a href="http://www.thewatchmakerproject.com/journal/141/killing-comment-spam-in-textpattern">this anti-comment spam measure</a>. The only "downside" is that you have to maintain your own blacklist (are there any centralized blacklist services yet for spam words?) on your server (or leech of other people's blacklists, but that's not good and makes commenting slow). I've just implemented this system here and it seems to work quite well, at least with some small testing I've done.

Maybe I'll port over the system to a database-managed blacklist, which might make it slightly easier to maintain.
