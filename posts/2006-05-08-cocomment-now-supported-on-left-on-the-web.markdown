---
layout: post
title: "CoComment now supported on Left on the Web"
date: 2006-05-08 13:43
comments: true
categories: 
---
This weblog is now supporting <a href="http://www.cocomment.com/">CoComment</a> users. <a href="http://weblog.zerokspot.com/">zeroK</a> pointed me to this nice service which you can use to track comments you leave around the world on weblogs. At the moment, unfortunately, only CoComment-supporting weblogs can be used, but they're working on extending it to all weblogs.
