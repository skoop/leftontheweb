---
layout: post
title: "Flamewars (or: why not just pick the best tool for the job?)"
date: 2007-09-24 13:44
comments: true
categories: 
  - technology 
---
In the past few days I've read quite a few weblog posts on <em>language x</em> vs <em>language y</em> and most of the posts were somewhat ranting or flaming against the language not of the author's choice. Quite a few were triggered by the fact that <a href="http://www.oreillynet.com/ruby/blog/2007/09/7_reasons_i_switched_back_to_p_1.html">Derek Sivers moved back to PHP from Ruby on Rails</a>. And just a few hours ago, I also read <a href="http://pixelated-dreams.com/archives/321-The-Pownce-Conundrum.html">The Pownce Conundrum</a>, a piece by Davey Shafik who I usually have in high regards when it comes to knowledge on programming, who singlehandedly seems to bash Python, Ruby on Rails and Java in a single post.

I still don't really understand where these wars come from. Sure, Ruby on Rails was mainly marketing at first, but at it's basis lies a wonderful programming language. Python is not <a href="http://www.google.com/">Google</a>'s language of choice because it's so bad, so why is it wrong to use that? Hell, even Java is still good for a lot of things. So I am a PHP developer. Does that mean I should bash all these languages?

No! I think people should leave all these wars to rest, and instead work on developing whatever they are working on at the time. When starting a project (or even somewhere halfway the project), choose the language that fits best to your project. Projects can even span multiple languages if you ask me? If Java is the best for the backend of your enterprise application, use Java for that. We know for a fact that PHP is usually better for the frontend, so let's use PHP for the frontend. It can all work!

It's even better. Instead of dissing eachother, one could learn from eachother. A very inspiring thought when you get used to it ;). Fabien Potencier from <a href="http://www.symfony-project.com/">symfony</a> mentioned during one of his sessions at <a href="http://www.symfonycamp.com/">SymfonyCamp</a> that he can often get really good ideas from looking at the code of other frameworks. OK, Fabien is also not that positive about Ruby on Rails, but he did get some of the symfony 1.0 structure there (Helpers anyone?). He also does look at what frameworks like Django are doing. And I'm sure most other languages can also easily learn stuff from how PHP works, as PHP is still the language for getting things done quickly.

So why bash eachother? Instead, work together to make your own language of choice better.
