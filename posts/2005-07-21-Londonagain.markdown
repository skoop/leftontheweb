---
layout: post
title: "London again"
date: 2005-07-21 14:08
comments: true
categories: 
  - london 
  - terrorism 
---
It&#39;s all over the Internet by now. More explosions in the London tube. I was not planning on giving it any attention, until coming by this Flickr picture. The picture itself isn&#39;t what caught my attention, but the text with the picture is definately worth a mention. I think it gives a good insight on the feelings of everyone.  <img src="http://photos23.flickr.com/27607830_d8a834da5d.jpg" border="0" width="325" /> <blockquote>At last weeks vigil one of the speakers (don&#39;t know who) read a poem he wrote and from that my favourite quote was &#39;Defiance a four letter word spelled with the middle finger&#39; For the last two weeks Londoners have basically been giving the terrorists the finger -&quot; F&#39;U we ain&#39;t scared and we are not intimidated...&quot; It felt like today they gave us the finger back... and I am not impressed.</blockquote> (<a href="http://www.flickr.com/photos/thetigerduck/27607830/in/photostream/">thetigerduck</a>)
