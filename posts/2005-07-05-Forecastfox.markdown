---
layout: post
title: "Forecastfox"
date: 2005-07-05 5:34
comments: true
categories: 
  - leftontheweb 
---
In the category 'cool and useful', I'll today present you with a nice <a href="http://www.mozilla.org/products/firefox">Firefox</a> extension called <a href="https://addons.mozilla.org/extensions/moreinfo.php?application=firefox&id=398">ForecastFox</a>. Using this nice plugin, you can get your local weather forecast as nice little icons in your firefox Statusbar (or in various other locations in Firefox!). Very cool, and pretty useful if you want to know what kind of weather to expect.
