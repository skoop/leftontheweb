---
layout: post
title: "Hermann Hesse -Siddhartha"
date: 2006-02-02 14:59
comments: true
categories: 
  - books 
---
Something completely different from <a href="https://skoop.dev/article/180/the-bourne-identity">The Bourne Identity</a> and <a href="https://skoop.dev/article/189/american-psycho">American Psycho</a> for me this time. <a href="http://en.wikipedia.org/wiki/Siddhartha_%28novel%29">Siddhartha</a> is seen as a legendary book by many, and I can understand why. I literally finished it in a few days (where one day equals approx. an hour and a half during my travel from and to work). Of course, this was due to the fact that it's only a 152-page book with relatively big letters, but it definitely also had to do with the actual story.

The story itself is definitely interesting. I guess I can read a lot of different stories that are alike, and they will all be interesting, because they offer a certain perspective on life. In this book, you basically follow Siddhartha through life, through his life journey in search of peace and perfection. Finding perfection is far from easy, and Siddhartha experiences this.

Already from an early point in the book one can easily figure out that the message this book is trying to get across is a very buddhist, zen -wisdom- knowledge (a very wise lesson I learned from this book: One can obtain knowledge from others, from teachers, but wisdom can only be obtained by experience).

I guess I had an extra interest in this book due to my (still quite young) interest in zen and buddhism, but I am sure that most people would highly enjoy this book. 

Judging from the author biography at the end of the book, I might also enjoy more works by <a href="http://en.wikipedia.org/wiki/Hermann_Hesse">Hesse</a>, so I might try to find some other Hesse books as well. For now, I'm going to continue with 'Latter Days at <a href="http://en.wikipedia.org/wiki/Colditz_Castle">Colditz</a>', the sequel to 'The Colditz Story' that I read earlier.
