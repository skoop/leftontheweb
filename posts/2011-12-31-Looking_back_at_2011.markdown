---
layout: post
title: "Looking back at 2011"
date: 2011-12-31 19:33
comments: true
categories: 
  - php 
  - symfony 

---
<h2>January</h2>
<p>The biggest thing for me in January was of course the news that I became an official <a href="https://skoop.dev/message/Now_a_Sensio_partner">Sensio training partner</a>. I very much like to deliver training, and being able to deliver the official symfony training is of course awesome! After a slow start early in the year, it took off some more after the release of Symfony2 stable (not surprising).</p>

<p>A blogpost that many people appreciated was my blogpost about <a href="https://skoop.dev/message/Barcodes_and_QR_codes_in_PHP">barcodes and QR codes</a>. I am clearly not the only one that needs to work with those, so the response was quite good. I'm very happy that so many people felt this was a useful blogpost. I've since then moved on to other projects already, so I don't use this anymore myself, but I will be referring to it myself as well if I ever need it again.</p>

<h2>February</h2>
<p>Another popular blogpost (and sparked some discussion on Twitter as well) was my blogpost about the <a href="https://skoop.dev/message/The_future_of_magazines">future of magazines</a>. After getting our first iPad, my eyes opened to the possibilities that devices like this are opening for digital magazines.</p>

<h2>March</h2>
<p>In March, a discussion started about how Drupal could or could not work for government websites. I <a href="https://skoop.dev/message/Drupal_not_good_for_government_websites">gave my opinion on this</a> which gave a nice discussion in the comments and on Twitter. I've said it before and I'll keep saying: Don't discard any tool by default. Each tool has it's own place in the PHP world, and Drupal's spot might be growing for years to come.</p>

<h2>April</h2>
<p>In April, I wrote a HOWTO on using <a href="https://skoop.dev/message/API_documentation_in_Jenkins_with_DocBlox">DocBlox with Jenkins</a>, and though it got no comments aside from spam comments, I've gotten a lot of feedback from people that they found it quite useful, which is always good to hear.</p>

<h2>May</h2>
<p>In May, I started a new series of articles. And though I still have some topics that I want to write about in this series, I already think the <a href="https://skoop.dev/tag/tools">whole series</a> might be useful for people. The first article I wrote in this series was about <a href="https://skoop.dev/message/My_tools_IDE_PHPStorm">PHPStorm</a>, the center of my workday.</p>

<h2>June</h2>
<p>In June, after some controversy about a fork of PHP, I wrote what I thought the <a href="https://skoop.dev/message/What_PHP_needs_well_what_I_think_it_needs">PHP project and the PHP community needs</a>. This was again a contribution to a bigger discussion, and it's good to see that some of the points I and some others tried to make got picked up.</p>

<h2>July</h2>
<p>Looking back like this makes me realize that quite a few of my blogposts were on discussions and controversies. In July, it was about whether conferences ask <a href="https://skoop.dev/message/Make_them_pay">speakers to pay for a ticket</a>, even though they are speaking. Simply said, I think it's OK if they do that, as long as they communicate this upfront. </p>

<h2>October</h2>
<p>In October I decided to open source <a href="https://skoop.dev/message/Open_Sourcing_LinkTuesdaycom">LinkTuesday.com</a>, a website I built for getting a quick overview of the links that are being shared as part of Link Tuesday. Since then, I've already gotten a couple of pull requests, so I believe it has been a good decision to do this.</p>

<p>I also wrote about my <a href="https://skoop.dev/message/A_URL_shortener_in_a_couple_of_hours">first real experience with Silex</a>, the PHP5 microframework based on Symfony. I really liked this a lot, and actually I'm <a href="http://techademy.eu/%EF%BB%BF%EF%BB%BFjanuari-2012-silex-rest/">doing a workshop on Silex</a> in January out of the new Techademy I started with Joshua Thijssen.</p>

<p>And then there was the news that <a href="https://skoop.dev/message/Drupal_just_got_a_whole_lot_more_compatible">Drupal is going to be using some Symfony2 components in their project</a>, which is great for interoperability and will improve both Symfony2 and Drupal.</p>

<h2>November</h2>
<p>New conferences are always fun. I was at the first Dutch PHP Conference, the first PHPBenelux Conference, the first 4Developers conference and in November I was happy to announce that I will be <a href="https://skoop.dev/message/DDay_is_coming_to_Finland">speaking at the new D-Day Conference</a> in Helsinki, Finland.</p>

<p>And of course November was the month of Joshua Thijssen and myself <a href="https://skoop.dev/message/We_just_launched_Techademy">launching Techademy</a>, a new training concept to inspire developers, to let them expand their knowledge on two different topics in a single day.</p>

<h2>December</h2>
<p>And this month, I launched a new small Symfony2 bundle that allows one to easily embed an old symfony 1 project inside a Symfony2 project: <a href="https://skoop.dev/message/Introducing_IngewikkeldWrapperBundle">IngewikkeldWrapperBundle</a>. I'm using it myself now on one project and it works quite nice.</p>

<h2>2011</h2>
<p>2011 was a good year in some aspects, a bad year in others. Basically, like any other year. But I'm looking forward to seeing what 2012 will bring. There's big plans and ideas, let's see if that will get off the ground. Happy new year everyone!</p>
