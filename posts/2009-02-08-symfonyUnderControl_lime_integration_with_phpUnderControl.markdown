---
layout: post
title: "symfonyUnderControl: lime integration with phpUnderControl"
date: 2009-02-08 22:26
comments: true
categories: 
  - continuousintegration 
  - symfony 
  - php 
  - lime 
  - phpunit 
  - symfonyUnderControlPlugin 
  - phpUnderControl 
  - ibuildings 
---
<p>There is a plugin for symfony that enables phpUnit testing for symfony projects. However, this plugin used to be in limbo for quite a while. Besides, even though I see the power of phpUnit and have worked with it on several occassions, I personally prefer the more minimalistic approach of lime for unit testing.</p><p>From the first moment I was introduced to the concept of continuous integration I have been really interested in this. I&#39;ve been playing with several packages for CI and set them up for fun. One thing that kept bothering me was that I could not test my own symfony projects with it.&nbsp;</p><p>I started working on the symfonyUnderControlPlugin a few months ago, and quickly had a working proof of concept. It wasn&#39;t perfect, but it did the job: It outputted xUnit XML to the logs directory of phpUnderControl based on the test results of my unit and functional tests. However, this setup was far from perfect. Time constraints kept me from spending more time on it at that point though.</p><p>Recently, inside <a href="http://www.ibuildings.com/" target="_blank">Ibuildings</a>, a new program was launched. This program enables Ibuildings employees to get allocated a certain amount of time to work on Open Source software. I&#39;ve sent in a proposal to enable me to further work on the symfonyUnderControlPlugin and this was approved! So, last week, I started working on rewriting the plugin to have a better setup and after not even having spent half of my allocated time, I&#39;ve already come to a point where I was able to generate more than the XML my Proof of Concept was generating. I&#39;ve now released this as a first alpha preview release with <a href="http://www.symfony-project.org/plugins/symfonyUnderControlPlugin?tab=plugin_admin#new_release" target="_blank">release number 0.0.1</a> .&nbsp;</p><p>So, thanks to Ibuildings, symfony/lime users can now start trying to integrate their projects with phpUnderControl continuous integration. Keep in mind that this is not production-ready yet, it&#39;s a first alpha release. But I appreciate any feedback I can get on this version: bugs, feature requests, comments, questions. Everything is welcome. </p>
