---
layout: post
title: "It's that time again!"
date: 2004-12-02 14:11
comments: true
categories: 
  - leftontheweb 
---
<h1>Dumb spammer time!</h1>

Sometimes I really have to laugh about spammers. Some spammers are true amateurs that don't even know their own software. Every once in a while, an e-mail comes in such as the one today:

<strong>Subject:</strong> %RND_SUBJECT

Yeah! Very random subject man!

Further down, there is another variable that the spammer entered incorrectly:

%ENCODED_RECIPIENT_LIST

Oh how I can laugh at such spammers. They're too dumb to spam.
