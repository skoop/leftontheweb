---
layout: post
title: "Music to code to"
date: 2006-01-23 13:07
comments: true
categories: 
  - music 
---
Wow, another entry so quick after the previous. I guess there's lots of stuff to tell. <a href="http://weblog.zerokspot.com/">zeroK</a> also had a nice idea. A <a href="http://weblog.zerokspot.com/posts/569/">top 5 of albums to code to</a>. Here's mine, in random order (because it depends on the mood):

Underworld - Everything, Everything
Evanescence - Fallen
Eminem - The Eminem Show
Boards of Canada - Music Has The Right To Children
U.N.K.L.E. - Psyence Fiction
