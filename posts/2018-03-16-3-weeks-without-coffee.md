---
layout: post
title: "3 weeks without coffee"
date: 2018-03-16 15:50:00
comments: true
categories: 
  - coffee
  - addiction
social:
    highlight_image: /_posts/images/coffee.jpg
    highlight_image_credits:
        name: Trevor Bexon
        url: https://www.flickr.com/photos/trevorbexon/15024302808
    summary: Coffee is not necessarily good for the human body, and grabbing a coffee first thing in the morning is a habit that can be broken. Three weeks ago I decided to break the habit and get rid of coffee again.
    
---

Three weeks ago I decided that was going to take a break from coffee. Every once in a while I take a break from certain things, or try to minimize their usage. Some months ago I minimized the amount of soft drinks I was drinking, and 3 weeks ago it was time to quit coffee. I wanted to break the habit and get rid of my [caffeine dependence](https://en.wikipedia.org/wiki/Caffeine_dependence). 

I'd done this before so I knew what to expect, and it was not very different this time around. The first day was fine except for the habit of getting coffee, and I accidentally got coffee when visiting a client, out of habit. "You want coffee?" I got asked, and just like that I said "sure". I didn't realize I had quit coffee until I already finished half of it.
The second and third day I had some headaches and got a lot of urges to get coffee. I resisted the urges, got water or tea instead, and pretty much got off my addiction (or dependence, or habit). From day 4 onward I had pretty much no urge to get coffee anymore.

The effects? I sleep better. I wake up less tired (well, except when I go to bed really late of course). I am also less tense, feel more relaxed. Long story short, it just feels a bit better.

I intend to keep this up for a long time. Now that I'm used to not drinking coffee, it's not that hard anymore, and the urge to get coffee is gone. I'm also considering doing a similar habit-breaking experiment with alcohol. 

If you've got similar experiences with experiments like these, I'd be happy to hear from you.
