---
layout: post
title: "I've had it with the music industry"
date: 2007-03-26 12:46
comments: true
categories: 
  - world 
---
This world is becoming too weird. For years I've been managing <a href="http://www.electronicmusicworld.com/">Electronic Music World</a>, a free online music magazine promoting both indie and commercial electronic music. Publishing news, reviews and the occasional article.

A few weeks ago, though, I received a letter from a US lawyer representing some of the big labels. Basically, they told me to either stop my service, or to ensure that the content that was "meant for the US market" would only be seen by the "US market". It can't get weirder than that, can it? <a href="http://www.electronicmusicworld.com/2007/03/26/emw-electronic-music-world-to-become-us-only">Read the whole story</a>.

I want to keep running my site, and I also want to occasionally post news items with links to digital music streams of new releases on the site. Even of commercial artists. And so I'll be taking some measures to prevent non-US visitors from seeing my content. My measures? IP filtering. I know it's far from "secure", I don't care. The lawyers have accepted the proposal. They feel it's a "reasonable technical effort to ensure the content is only viewed by the target audience." Fine by me.
