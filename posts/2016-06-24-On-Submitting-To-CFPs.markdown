---
layout: post
title: "On submitting to CFPs"
date: 2016-06-24 11:55:00
comments: true
categories: 
  - php 
  - speaking
  - conferences
  - cfp
  
---

One of the first things you do if you want to speak is that you submit to a CFP (Call for Papers) of a conference. Most conferences use this system to get a nice selection of talks out of which to compile the schedule for a conference. The idea is that speakers tell the conference what they want to speak about and the conference can make the selection of speakers and topics for their conference. 

Having been a conference organizer as well as a speaker, I've seen both sides of the CFP process. Because of that, I've made some decisions as a speaker on how I handle CFPs. Since I've discussed this with several people in the past year but it keeps coming up, I've decided to document one of my decisions here.

### Overlap

Today I [tweeted](https://twitter.com/skoop/status/746257618222604288):

  > <pre>#firstworldproblems #speakerproblems call for papers closing before other conferences in same period have announced their speakers</pre>
  
To give this some context: I was looking at the currently open CFPs (thanks to the awesome [CFP Report](https://thecfpreport.com/) newsletter) and noticed that the CFP for [PHP[world]](https://world.phparch.com/) is closing today. Now, I submitted some talks to [ZendCon](http://zendcon.com/), which is in the same period, and I can't really handle two North-American conferences in such a short time period so I need to make a choice. However, ZendCon has not sent out acceptance/rejection e-mails, so I don't know if I get accepted there yet.

Andreas Heigl [responded with](https://twitter.com/heiglandreas/status/746265828631392256):

  > @skoop apply and then see what happens. First one calling wins ;)
  
This is actually what I used to do, and I know other people do this as well. And from the speaker perspective, this makes sense. Having been on the other side, however, I've decided not to do this anymore. Let me explain...

### The CFP process

Most conferences get hundreds of proposals, where they have between 5 and 25 slots to fill, depending on how big the conference is. You may already notice the problem: You'll have to make a very small selection out of a big pile of proposals.

My experience with a CFP is that it is usually pretty easy to discard the first 10% of the proposals. They are one or more of the following:

- Unclear
- Not appropriate for the theme of the conference
- A single talk for an international speaker (too expensive for most conferences)
- Just plain bad

I must admit that the last reason is actually becoming more rare. Speakers are putting much more effort into their proposals and talks than they used to, but it sometimes still happens.

The biggest problem with this is: After removing 10% off the big pile, you still have a pretty big pile of appropriate, high-quality talks left. You'll have another 70-80% of the talks to reject.

The process of going through all these talks is hard. It usually involves a lot of reading, research, trying to find a balance in the offered subjects, deciding on how many international speakers you can afford to fly in and trying to find the right talks to accept for those international speakers (international speakers usually need at least two talks to be accepted to make it financially viable to invite them). 

I've been involved in the CFP team for a lot of conferences. This whole process is usually a team effort of between 2 and 20 people. Imagine the amount of time that goes into creating a balanced schedule out of the CFP. Hours and hours of work.

Now, imagine what happens when you finally send out the acceptance e-mails, and one or maybe even two or three speakers respond with "sorry, I can't make it".

That response means (part of) the above process begins again. I know it happens, but it is not a nice experience as a conference organizer. So...

### My personal rule

As a result of the above, I've set a personal rule only to submit to the CFP of a conference when I know for sure I can make it (emergencies and such are the exception to that rule of course). It seems only fair to conference organizers that I can make this guarantee when I submit to their conference.

### Now what?

I'm not saying all speakers should adhere to this rule, but I hope this blogpost will give a better perspective to new and existing speakers about "The Other Side" of the CFP.

