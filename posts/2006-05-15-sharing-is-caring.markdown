---
layout: post
title: "Sharing is caring?"
date: 2006-05-15 13:36
comments: true
categories: 
  - politics 
---
Privacy has been a problem in the USA for a long time (though for some reasons on some levels the US is more free than other countries). Europe has so far not become the 'Big Brother' state that the USA is on a lot of levels. However, we've now come one step closer.

In Europe, there is a law that requires telecom- and Internetproviders to keep all logs of traffic by users for a couple of months up until a couple of years. Even though the dutch parliament and the dutch communications industry have protested heavily against this, our minister of justice decided to support this, and has also passed this law here in The Netherlands. Though I don't really like the idea (let alone think about the cost for this storage, which is gigantic, and will in the end be calculated into the price I pay for Internet and phone usage) it has now become even scarier.

The European parliament has decided that they have no trouble with <a href="http://euobserver.com/9/21580">giving the USA access to this information</a>.

Now, we all know by now that the US government and it's secret services have absolutely no notion of what the words 'civil rights' mean. So these people having access into the information on with whom I have contact over the telephone and what I do over the Internet, even while it's quite innocent, is scary. Very, very scary.

There was a time where Europe was the balancing good against the evil of the USA. It seems this is not true anymore. Europe is following the USA at it's every whim these days. I can not feel safe anymore talking to friends of the phone, or communicating with friends over the Internet using chat, e-mail or discussion forums.

I will definitely be contacted the representatives in our dutch parliament about this, asking them to clear this up and to forbid our minister of justice to also support this madness. This is truly unbelievable. Last time I checked, we were not part of the undemocratic USA, lead by it's mad dictator Bush. But it seems that Europe is looking into the sky where mr Bush claimed to see a dead bird flying, while Bush is taking over Europe. Maybe it's again time for underground resistance, maybe we should already start organizing to fight the occupational forces which might soon be sent to Europe to 'liberate' Europe.
