---
layout: post
title: "Getting Real"
date: 2008-05-04 20:03
comments: true
categories: 
  - book 
  - review 
  - rubyonrails 
  - symfony 
  - gettingreal 
  - methodology 
  - agile 
---
<p>The subtitle for the book is &quot;<em>The smarter, faster, easier way to build a successful web application.</em>&quot; I am sure this is indeed true. The approach to software development in this book sure is smart, fast and easy. Of course, not everything discussed will apply to every software development project, but it is not meant to do so. The authors realized this and clearly stated so in the introduction. Not everything will work for everyone, or for any project. Similarly, the bigger part of what is described in the book is not new. It&#39;s just a description of how software development at 37Signals is working, and working well.</p><p>Having said all that, they seem to have found a great approach to software development. It is very agile, and looks at software very much from a user perspective. The agile approach was not new to me at all, but their view on the amount of features to implement is very refreshing. Instead of adding features all the time, instead you wonder if a feature is actually necessary, and maybe even scratch planned features.</p><p>Their whole approach seems very design-centric however, which may work for them but will probably cause a lot of planning problems for other companies. It&#39;s not impossible, but may require some drastic change. However, I do think that the plain-html prototype phase could be skipped in favor of a very agile iterative approach (that is also described) using very simple coded versions. Sure, it&#39;s just a bit more work, but may save work in the future as well. A framework such as <a href="http://www.symfony-project.org/" target="_blank">symfony</a>  is excellent for such an approach (as, I am sure, is their own <a href="http://www.rubyonrails.com/" target="_blank">Ruby on Rails</a>). </p><p>All in all, I think this book is a very refreshing look on software development, and even if you can&#39;t apply 80% of the discussed approaches to your own situation, it is still a very recommended read for any software developer. Even for big enterprise applications, I see a lot of things that may be applied to make the project a big success, in less amount of time.</p><p>And for those that don&#39;t want to buy the dead tree version, it is also available <a href="http://getreal.37signals.com/" target="_blank">as PDF</a>  or even for <a href="http://gettingreal.37signals.com/toc.php" target="_blank">free reading online</a> . </p>
