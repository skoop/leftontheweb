---
layout: post
title: "RABCK.com update"
date: 2005-12-11 17:54
comments: true
categories: 
  - books 
---
Time has been a big problem in the past few months. I had enough time to write the basic setup of the <a href="http://www.bookcrossing.com/">BookCrossing</a>-related website <a href="http://www.rabck.com/">RABCK.com</a> but never really had the time to look at some reported "bugs", mostly related to cookies that some browsers had a hard time to accept (or so it seems).

Anyway, in the hope of solving at least some of the problems coming from that, I just added a password reset functionality to the site. Some of the problems may for instance come because the randomly generated password was hard to remember. I still randomly generate passwords, but now people can also reset it to a new randomly generated password ;)

So let's hope this solves most of the problems. For other cookie related problems, I might look back to <a href="https://skoop.dev/article/165/want-a-cookie">an earlier post</a> and conclude that cookies in at least recent versions of Internet Explorer are quite buggy, so it may need some more digging to find a proper solution.
