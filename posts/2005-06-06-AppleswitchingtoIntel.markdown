---
layout: post
title: "Apple switching to Intel"
date: 2005-06-06 4:46
comments: true
categories: 
  - leftontheweb 
---
Shocking news today in the world of <a href="http://www.apple.com/">Apple</a>. Apple is ditching IBM, and <a href="http://news.com.com/Apple+to+ditch+IBM%2C+switch+to+Intel+chips/2100-1006_3-5731398.html?tag=nefd.lede">moving to Intel</a> for their future products. Quite shocking, since the Power PC chip has been the core of Apple computers for, well, ages.
