---
layout: post
title: "Illegal downloader spends more money on digital music"
date: 2005-07-27 16:06
comments: true
categories: 
  - music 
  - download 
---
According to <a href="http://news.bbc.co.uk/1/hi/technology/4718249.stm">this article on BBC</a>, people who get the most of their music through file sharing, are also the biggest spenders when it comes to buying legal music online. This basically means that the music industry is starting to sue their best clients. In what kind of world are we living then?  Again, I will reiterate the fact that I&#39;d love to buy my music through legal online music stores if only these stores were available to me (a linux user). Of course, there is <a href="http://bleep.com/">Bleep</a> and <a href="http://emusic.com/">eMusic</a>, but these don&#39;t give me the occasional top 40 tune that I like.  Time for the music industry to change the way they handle digital music and file sharing, maybe?
