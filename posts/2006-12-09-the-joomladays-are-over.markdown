---
layout: post
title: "The Joomla!Days are over"
date: 2006-12-09 12:07
comments: true
categories: 
  - technology 
---
The Joomla!Days are over, let my Joomla!Days begin. When I started working for <a href="http://www.dop.nu/">my current employer</a> I knew they were working with Joomla! and Mambo a lot, and I wasn't the biggest fan of that CMS. But with 1.5 they have been doing a lot to make it one of the most impressive CMS'es out there by actually thinking it out and doing a lot of proper system design. The framework beneath the new version is well thought-out and well-designed, and even though it is in PHP4, it seems to be a very nice framework with a good approach to development.

The development of the J!Code Joomla IDE also looks nice and I was mightily impressed by Laurens Vanderput's work on SiteMAN, which I think has a lot of potential and will be very useful, for instance for (potential?) clients that don't really know how to work with web interfaces. SiteMAN is a very simple approach to managing your website.

h2.Joomla! versus the rest

Lately I've been working with <a href="http://www.symfony-project.com/">Symfony</a> for application development, and that won't stop. Symfony is perfect for big, complex applications that don't necessarily evolve around content management in the traditional sense. Joomla! however has made the conscious decision to stick with content management, and do that right. So the Joomla! framework is focussed on content management mainly, and they do that very very well. This allows them to really stick to what they do best. A very wise decision.

h2. So what's next for me?

Quite simple. I'm sticking with Symfony for big application development, but I want to find one of my smaller websites that is mainly around, well, weblogging or something like that, and see if I can migrate that to Joomla! 1.5 so that I can learn more about the framework and the CMS. That should keep me occupied for a while. Maybe I can also help the Joomla! community out in one way or another. We'll see what happens. I'm very enthousiastic though about Joomla! after these two days.
