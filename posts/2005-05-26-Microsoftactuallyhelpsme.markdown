---
layout: post
title: "Microsoft actually helps me..."
date: 2005-05-26 6:48
comments: true
categories: 
  - technology 
---
<a href="http://www.tearsong.com/">Marjolein</a> made a new design for her website. And of course, I turned it into HTML. This design uses two stylesheets, one for Internet Explorer, and one for real browsers ;) . Anyway, A simple browser check is easy in PHP, but we're also using <a href="http://www.nucleuscms.org/">Nucleus</a> for her weblog, and in the skinning system Nucleus uses, it's not possible to use PHP.

<a href="http://www.rakaz.nl/">Rakaz</a>, however, had <a href="http://www.rakaz.nl/nucleus/item/24">the solution</a>. It seems Microsoft has built a system called Conditional Comments into their Internet Explorer browser. Using that, you can let IE do something that other browsers won't do (since other browsers will just see it as regular comments). A very interesting concept that is quite useful, because it makes it easier to make work-around for IE non-standards issues.

So now everything works again, the IE stylesheet is loaded when people browsing with IE come by. The code I used is quite simple:

<blockquote>&amp;lt;link rel="stylesheet" href="http://www.tearsong.com/tearsong.css" type="text/css" /&amp;gt;
&amp;lt;!--[if IE]&amp;gt;
&amp;lt;link rel="stylesheet" href="http://www.tearsong.com/tearsong-ie.css" type="text/css" /&amp;gt;
&amp;lt;![endif]--&amp;gt;</blockquote>

This ensures the tearsong.css is loaded for all browsers, but only IE will load the IE-specific CSS.
