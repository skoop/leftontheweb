---
layout: post
title: "The user will always be the weak security link"
date: 2007-02-11 14:40
comments: true
categories: 
  - technology 
---
I'm an occasional user of the <a href="http://www.cocomment.com/">coComment</a> system. That is, I have an account, I mark most of the comments I leave to be tracked by coComment, yet I rarely actually check. I should do more often of course ;)

Usually, I keep good track of which textarea fields I mark to be tracked by coComment. Yet, I seem to slip occasionally. I noticed I did this once, while filling out my <a href="http://last.fm/">Last.fm</a> profile. I am clearly not the only one. Since I slipped, 120 other people have also done the same.

And as I was thinking about this, I was immediately seeing the potential for a security or privacy problem here. In this case, the last.fm profiles are public anyway, so no problems will arise in this situation. But consider the huge amount of web applications out there where you have to sign in to use the application. If I would accidentally mark some private note on such an application to be tracked by coComment, and others have done the same thing before me, then that would open up the information I entered to the world.

Obviously, this is no security hole you can fix. It's a mindset that users need to get used to. But even me, an experienced web user that has an eye on security, even I slip every once in a while it seems. Security is a tricky thing, and the user will always be the weak link.
