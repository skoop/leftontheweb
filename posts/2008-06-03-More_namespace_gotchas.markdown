---
layout: post
title: "More namespace gotchas"
date: 2008-06-03 21:26
comments: true
categories: 
  - xml 
  - namespace 
  - simplexml 
  - xpath 
  - php 
---
<p>My problem with the elements was the opposite of my earlier problem. I was trying to fetch elements that were not namespaced, or so I thought. I had overlooked the fact that there was a default namespace with no prefix in the document.</p><p>It took me a while to find the fact that there was a global namespace, but even that didn&#39;t solve the problem. First, I tried simply getting all namespaces and registering them for Xpath:</p><blockquote><p>$namespaces = $text-&gt;getNamespaces ( true );<br />foreach ( $namespaces as $prefix =&gt; $namespace ) &#123;<br />&nbsp;&nbsp;&nbsp; $text-&gt;registerXpathNamespace ( $prefix, $namespace );<br />&#125; </p></blockquote><p>I thought this should solve the problem, but it didn&#39;t. I still didn&#39;t get anything back from my Xpath query. </p><p>A lot of Googling later, it turned out there is this weird thing with default namespaces. Xpath just can&#39;t handle it without prefix. This article pushed me in the right direction. It&#39;s on DOM and only slightly mentions SimpleXML, but indeed, it&#39;s the same in XML. So what&#39;s the solution?</p><blockquote><p>$namespaces = $text-&gt;getNamespaces ( true );<br />foreach ( $namespaces as $prefix =&gt; $namespace ) &#123;<br />&nbsp;&nbsp;&nbsp; if (empty ( $prefix )) &#123;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $prefix = &#39;default&#39;;<br />&nbsp;&nbsp;&nbsp; &#125;<br />&nbsp;&nbsp;&nbsp; $text-&gt;registerXpathNamespace ( $prefix, $namespace );<br />&#125;</p></blockquote><p>Simple as that. Just check for the empty prefix, fill it, and things will start working. Don&#39;t forget to use your prefix in your Xpath query as well!</p><blockquote><p>$links = $text-&gt;xpath ( &#39;//default:a&#39; );</p></blockquote><p>These are the kinds of things that you encounter once and then always remember, so encounter them here by reading lest you not fall into the same pit ;) </p>
