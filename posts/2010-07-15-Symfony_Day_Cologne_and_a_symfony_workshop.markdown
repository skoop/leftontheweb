---
layout: post
title: "Symfony Day Cologne and a symfony workshop"
date: 2010-07-15 21:30
comments: true
categories: 
  - symfony 
  - php 
  - symfonyday 
  - sfdaycgn 
  - training 
  - workshop 
  - cologne 
---
<p>Just like last year I'll be hosting a full-day beginner workshop on symfony (using symfony 1.4 as the basis) at the Symfony Day Cologne for people interested in learning about symfony or people just starting out with the framework. During this day you will actually build your first symfony application using step-by-step instructions, starting with the concept and the basics and slowly expanding until you have a working application. The full-day workshop runs parallel to the main conference track.</p>

<p>Because both Interlutions and myself can understand though that you might also want to attend both the main conference track and the workshop, there is now an option to do just that: On saturday October 9th I will be doing the same workshop again for those that want to attend both the conference and the workshop. This <a href="http://www.interlutions-schulungen.de/schulung-symfony-02.php" target="_blank">training</a> is part of the new "<a href="http://www.interlutions-schulungen.de/" target="_blank">Interlutions Schulungen</a>" department of Interlutions, who will be offering PHP-related training on a more regular basis in Germany.</p>

<p>I am really excited about both the main conference and the possibility to do the additional workshop on saturday. And remember: if you act fast, you save money, because both Symfony Day and the workshop have an early bird discount you can benefit from!</p>
