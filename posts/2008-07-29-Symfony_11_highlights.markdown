---
layout: post
title: "Symfony 1.1 highlights"
date: 2008-07-29 13:30
comments: true
categories: 
  - symfony 
  - ibuildings 
  - meta 
---
<p>It&#39;s not a full list and does not pretend to be. It is simply my view on why symfony 1.1 is a great successor to symfony 1.0.</p><p><a href="http://www.ibuildings.com/blog/archives/1171-Symfony-1.1.html" target="_blank">Read here</a>. </p>
