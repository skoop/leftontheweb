---
layout: post
title: "Re-launching the dutch PHP usergroup activity"
date: 2008-09-18 19:23
comments: true
categories: 
  - php 
  - usergroup 
  - event 
---
<p>We&#39;re getting more active again by organizing bi-monthly meetings. The first one will take place next <a href="http://www.phpgg.nl/node/864" target="_blank">tuesday, september 23rd</a>, in Amsterdam. We have a room at the Vrije Universiteit where, starting at 7:45PM, dutch (potential) professional PHP developers will be able to meet eachother, attend two presentations, enjoy some drinks, and win cool prizes!</p><p>It feels great to get the usergroup back on the road again, and our momentum is building quickly! This is in a big part to the credit of the energy and inspiration we are getting from <a href="http://felix.phpbelgium.be/blog/" target="_blank">Felix</a>  and <a href="http://www.dragonbe.com/" target="_blank">Mike</a>, thanks guys! During the first meeting we will have a presentation by <a href="http://willy.boerland.com/" target="_blank">Bert Boerland</a>  (Drupal association) on the 42 advantages of <a href="http://www.drupal.org/" target="_blank">Drupal</a>, a presentation which I&#39;m very curious about. Also, I will myself do a presentation on the Power of Refactoring, a presentation I am less curious about obviously but will be interesting nonetheless as it will be very similar (if not the same) as the presentation I&#39;m doing at the <a href="http://www.phpconference.com/" target="_blank">International PHP Conference</a>  in october.</p><p>Our next meeting will be in November, exact date to be announced during our meeting next tuesday. If you&#39;re in the Netherlands, entrance to the meeting is free to both members of the usergroup and non-members! Just register by either put yourself in the <a href="http://upcoming.yahoo.com/event/1071608/" target="_blank">upcoming.org</a>  event or sending a <a href="http://www.phpgg.nl/contact" target="_blank">short e-mail to us</a>  through our website. I can&#39;t wait! </p>
