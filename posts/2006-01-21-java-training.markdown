---
layout: post
title: "Java training"
date: 2006-01-21 5:57
comments: true
categories: 
---
Yesterday was the last of a three day Java introduction course. Or actually, one day of UML, and two days of Java. And I have the feeling that I've wasted a lot of time.

Luckily, my employer organized the training, so it hasn't cost me any money. However, with a deadline quite near, it was a lot of wasted time. The UML training was nice, but I don't expect to start using it any time soon. The first 1.5 days of the Java training were a disappointment. Most of it was OO basics, and though the Java syntax is slightly different, most of it was pretty much the same as PHP(5). Only in the last afternoon did we get some interesting information on what Java can do in terms of application serving, thin client work, and frameworks.

I partly understand why they'd want to move their back-end from PHP to Java (not fully, since PHP is quickly replacing Java as the industry standard for these kinds of things), and for this, I obviously need to gain new skills. I'd gladly learn Java, Java seems quite good and nice. But this course was... boring.
