---
layout: post
title: "Why your current developer is terrible"
date: 2014-03-28 17:30
comments: true
categories: 
  - php 
  - developers 
  - quality
---
Earlier today I got pointed on Facebook to [this article by Shamoon Siddiqui on Medium.com](https://medium.com/p/506a06ae35ea). I would very much recommend this article to both developers and managers. This is an excellent read. It also made me think about my role.

First of all: I've been on both sides of the above story. I've been the new developer hating on the previous developer, and I've been the guy who left hearing stories about the new developer. And to be honest: When you're a new developer looking at an existing codebase, you'll spot a lot of bad things. You'll probably also spot a lot of things that you think are bad things, but are simply different than how you would've solved it. **There is a difference!**

My current role is different. As a consultant, I come into companies with existing teams (or sometimes just a single developer). I'm still in the same role though: I am the "new guy" coming in and spotting weaknesses, errors, problems etc. The difference, however, is that I'm there for this specific reason: I'm usually called in to help improve a codebase, fix a project, etc.

Over the past years of doing this work, I've also found out that there are many reasons why the codebase is, let's say, suboptimal. This has to do with many things, but interestingly, very often it has very little to do with actual technical causes. Just to list a few:

* The developer presented himself or herself as a senior, while they were actually not that senior
* The management was not able to make decisions, causing developers to have to switch between tasks and solutions constantly
* The developer(s) and management don't communicate well
* Sales and delivery are two seperate isolated parts of the company

There are more reasons, but the above are the most common reasons I've encountered so far. Let me dive into all of those a bit more.

## Seniority

I've seen it happen many times: A new developer is hired by a company. Their CV looks impressive: 8 years of experience with PHP. Definitely a senior developer! And since we need to focus on good quality, we need a senior! Then once the developer starts, it turns out the developer isn't all that senior. Tasks take longer than expected, the software contains more bugs than expected, what happened?

Now, the first problem is: What is the definition of a senior developer? To me, a senior developer is not necessarily someone with lots of years experience. You could have 8 years of experience building websites and CMS'es for customers, but when you join a company building enterprise-level e-commerce solutions, you're not a senior. Sure, you know PHP and you've solved the CMS-problem over and over, but what's your experience with payment systems? Invoicing? E-commerce has a completely different set of problems to solve. Some solutions that work for a CMS might not work for E-commerce. Seniors know this. They don't know the solution to all the problems, but they know the solution is not always the same. They communicate well, can explain a problem, and know where to look for a solution. *A senior knows (s)he doesn't know everything*.

When hiring a developer, don't blindly look at how many years of experience the candidate has with PHP (or whatever language you work with). Also have a look at the variation of the projects the developer has worked on. Unless, of course, your company operates in the same business as this developer with 8 years of experience in a specific field. Then you *do* want to hire the developer. Well, if everything else checks out.

## Decisions, focus and quality

Talk to any developer and ask them what is one of the biggest causes of bad code, and they'll tell you it is a lack of focus. Not being able to focus on a single task from start to finish ensures the code that is being delivered is of less quality than it could be. 

One important reason for a lack of focus is not the fact that your developer checks their Facebook or Twitter regularly, or that they go out for some football or play on the gaming console three times a day. No, it's usually that your developer(s) are not protected from distraction caused by the business. 

When I worked at a product-focussed company years ago, we had this issue. On a regular basis, while working on a feature, someone would stand at the desk of a random developer and ask them to "quickly do this" or "fix this bug, it's highly critical!". Because of this, we never made our planning and the amount of bugs in our code was sometimes unacceptable. 

Our team lead then made the best decision ever: He told us to redirect everyone to him. He would then decide on the priority of the request and put it into the planning. This was paradise! Within weeks, people didn't even come to us anymore, but instead went directly to the team lead. We could focus on our tasks, reached our goals, and the quality of our code was higher than ever.

Our team lead, however, got no programming work done anymore. Instead, he would gather requests, determine priority and requirements and ensure all the tasks were ready for us to pick up. Another very important task: He made sure that the request was an actual decision, instead of just an idea that would have to be rolled back again in a month. A lot of bugs are caused by implemented features that have to be rolled back, partially rolled back or changed after being implemented.

Looking at current methodologies, the scrum master role comes to mind. But you shouldn't just name someone scrum master and expect things to work well. Being a scrum master is usually a dedicated task. You can't really expect the scrum master to get much work done aside from being a scrum master. At least not if you want the scrum master to do a good job.

## Communication

Good communication is an art. It is a specialty. As I said above, a true senior developer is able to communicate. Communication isn't easy.

The most important thing in communication when it comes to (technical) projects is to ensure that both sides mean the same thing. This can be really hard, because of very specific terminology being used by developers, customers, managers, etc. They all use different words for the same thing, or sometimes the same word and mean different things. It is important to realize this. Once you know this happens, you can ensure that you all mean the same thing. If a feature request comes in, make sure that the specifications are extensive. Go through the specifications together to ensure you're on the same page. Do anything you can to ensure you mean the same thing, then both sign off on it.

Whatever you do, always try to keep an open mind in communicating with everyone. Make no assumptions and try to clear up any unclarities there are. This makes a lot of sense, and seems common knowledge, but I've seen so many problems arise from not actually doing this to want to write this down. Again. And again. And yes, I've fallen into this trap. 

## Isolation

One of the worst situations I've ever been in (at several different companies, a lot of people seem to make this mistake) was that sales was an isolated team. They'd go out to customers, talk to them, try and find out what they wanted, then "communicate" (yeah, see what I did there?) the requirements to the developers to get an estimation. Then they'd go back to the customer with the estimation and a price. The customer (it is a customer after all) would try to get a lower price, which usually would succeed, and then the project would be given to the developers. Development would take up the specs, build what was in there, deliver it, and the project would (partially) fail. Time and time again.

The cause of this would usually be the isolation of both sales and delivery. Sales did their thing, then delivery did their thing. But they'd hardly ever communicate. This would cause incorrect specifications, incorrect estimations, incorrect end products and unhappy customers, developers and sales.

It might sound scary, but development is teamwork. And the team consists not just of developers, but also of sales. You work together. Talk to your customer together. Write specifications together. You share the praise together and you share the blame together. Sales needs to understand how development works and developers need to understand how sales works.

## So why is my current developer terrible?

Actually, your developer isn't all that terrible. Chances are, the developer is exactly what you could expect of him or her. Your expectations might be wrong though. Or you might need to work on communication, on processes, on your company. Have a good look not just at development, but at your company as a whole. Then you might understand the cause of the problem. Or, of course, hire a consultant to do that. [Here's a good place to find one](http://www.dutchweballiance.nl/).
