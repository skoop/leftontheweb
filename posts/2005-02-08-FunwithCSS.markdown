---
layout: post
title: "Fun with CSS"
date: 2005-02-08 4:31
comments: true
categories: 
  - leftontheweb 
---
Yesterday, I received an e-mail from a client. Some of the text on their website, that was editted with a rich-text textarea editor, had gotten a different font layout as the rest of the site. Of course, this was not what was supposed to happen. Cause of this, I soon found out, was some markup that didn't get filtered by the "Word HTML filter" I built in, since this client pastes a lot of their website text into the site from Word. And so I needed to find out how to overrule style attributes in these Word-generated tags. Thanks to <a href="http://www.zerokspot.com/">zeroK</a> I found out about the <a href="http://www.w3.org/TR/REC-CSS2/cascade.html#important-rules">!important</a> rule in CSS. And so, by adding the following CSS to the site's header:

* &#123; font-family: Verdana, Arial, Helvetica, sans-serif !important; &#125;

I overruled the font-family style attribute added by Word. Now, all text in the site is always the same font-family, as it was meant to be.
