---
layout: post
title: "Solving 'convert: delegate library support not built-in `white' (LCMS) @ warning/profile.c/ProfileImage/853'"
date: 2015-02-25 14:05
comments: true
categories: 
  - php 
  - imagemagick 
  - crop
---

Another quick fix documented here simple because it may help me (or someone else of course) in the future.

## Context

For a project I'm working on right now I need to crop an image using [ImageMagick](http://www.imagemagick.org/). We're using the commandline instead of [Imagick](http://php.net/manual/en/class.imagick.php) because we need to execute a custom command with a lot of extra options for this specific action, since the image will eventually be used for high-quality printing. As I was testing the command, tweaking the parameters, I eventually ended up with an error:

  > convert: delegate library support not built-in `white' (LCMS) @ warning/profile.c/ProfileImage/853
  
## The solution

It turns out this was very simply to solve: I used homebrew to install imagemagick, and I installed it simply by doing:

  > brew install imagemagick
  
Apparently, **this is wrong**. At least when you want to specify your own profile (for instance by using *-profile /path/to/sRGB_IEC61966-2-1_black_scaled.icc*). The solution? Install it --with-little-cms:

  > brew install imagemagick --with-little-cms
  
With thanks to *clee704* on [Github](https://github.com/Homebrew/homebrew/issues/16619#issuecomment-19022238) who came up with this solution.
