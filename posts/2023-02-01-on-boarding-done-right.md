---
layout: post
title: "On-boarding done right"
date: 2023-02-03 14:40:00
comments: true
categories: 
  - php
  - development
  - on-boarding
  - newjob
social:
    highlight_image: /_posts/images/boarding.png
    highlight_image_credits:
        name: George N
        url: https://flickr.com/photos/takoyaki_king/20247788062/
    summary: Every start is important, but especially when you start a new project or a new job. Time to look at what we can do to make on-boarding a success.
    
---

Well begun is half the work. A good start is half the battle. Expressions like these may seem cliche but they are actually true, especially when talking about getting new developers on board. Better yet, in a market where there is more companies looking for developers than there are developers available, making sure that their first impression of your company is good makes it easier to retain those developers. But also from an efficiency point of view, or looking at it from "having fun working", making sure that developers can be easily on-boarded is important, for all parties involved.

And so, I want to talk a bit about on-boarding. Based on recent experiences, but also based on the [Ingewikkeld Session](https://ingewikkeld.dev/sessions) we recently did about this topic. 

## Why should it be good?

Let's first talk a bit about why the on-boarding should be good.

### The first impression

If you are able to hire a developer in the current market: Congratulations! Even with the recent big tech lay-offs, for many companies it is still not easy to hire developers, especially experienced developers. As such, you want those people to have a good first impression of your company and of their new job. Remember: There's a good chance that your new developer has said "no" to one or more other companies to be able to work for you. So you really want them to feel welcome and get the idea that their time at your company will be fun. 

### Get the most out of your new developer

Whether they are a freelancer, external contractor or a new in-house developer, you want them to be up-and-running as soon as possible. As long as they are not up-and-running yet, they mostly cost your company money. When working with external developers companies usually have the expectation that they are up-and-running quite quickly, and those developers are usually used to that. But you have a role in this as well, because if you are not prepared, it'll take them longer to be useful to your company.

### Get that functionality out

Aside from the financial aspect there is also the functional aspect: You hire developers because you have a need to build functionality, and you want to get that functionality available to your customers, whether that is internal users or actual, paying customers. So it is in everyone's benefit to get that new functionality out the door as soon as possible. Getting new developers working on that functionality as soon as possible is essential to do just that. 

## How to make it good?

So, now that we've looked at reasons why we want a good on-boarding, let's look at ways to improve the on-boarding process.

### Make a plan

With one recent customer I worked for, I've experienced one of the best on-boardings I've ever had. There was a plan. Not just a short list of things to do, but a full wiki page personalized for my on-boarding telling me which tools I needed, which people I needed to talk to and what other tasks are important for a good on-boarding. This sounds like a lot of work, but it pays off. I was up-and-running in very little time and I knew exactly which people in the organization I needed to introduce myself to and talk to. And since there was a wiki page with check boxes, I had a good overview of the things I still needed to do and my manager had a good idea of how far along I was with the tasks I had. Honestly, this was a great starting experience for me.

### Get the right tooling

Your tooling matters. And modern tools give you a lot of options of making the on-boarding process very easy. If you have a good setup with Docker and possibly tools such as Make or Ant, setting up your local development environment will be a breeze. Last year I worked for a client where, when I came in on my first day, the only thing I had to do was cloning their Git repository and running `make up`. Sure, that initial setup took a while, but that gave me the opportunity to get a drink and talk to some developers to hear more about the project. By the time I came back to my laptop, my development environment was running. It really is worth the investment to not only create the correct Docker containers, but also make sure that that initial setup automatically loads a database with test fixtures, sets up anything that needs to be set up and already runs certain one-time tasks.

If this is not something you can or want to invest in, then at least make sure that the README.md file in the root of your project contains all commands you need to execute manually to get things set up. 

Whether you automate everything or use the README approach, this is not a one-time investment. This requires regular maintenance as you project gets bigger. Worse than no documentation is incorrect documentation. So make a concious effort to keep the automated scripts and/or the README up-to-date.

### Be available

A new joiner will have questions. Better yet: If a new joiner has little to no questions, I would worry about the new joiner. But of course, to be able to ask these questions and successfully and efficiently on-board in your project/company, they need to be able to get answers to those questions. So, especially in the first couple of days, make sure that someone is always available to answer questions. Always? Yes, in a perfect situation there would always be someone that can help. It does not have to be a single person (although something can be said about simply schedeling "on-boarding new developer" in for a single person so that others can focus on their regular work) but ideally it is always clear who will answer questions.

### Low-hanging fruit

Especially when you work on complex software, you can not expect a new developer to immediately work with the rest of the developers on implementing big new features. It takes a while to get going and understand what a codebase is about. To get people up-and-running soon and digging into the codebase, it is good to have some simple tasks available to pick up in the first few days. Things that may be interesting: Simple changes to existing functionality, writing (unit) tests for currently untested code, simple refactoring tasks. This has multiple advantages: the developer has an easy way of digging into the code to learn how it works, the low-hanging fruit is often things that get overlooked quickly by experienced developers because those focus on the bigger tasks, and it gives a very rewarding feeling to be able to make your first pull request in the first day or first couple of days. 

## The session

If this all sounds interesting to you, I would recommend checking out the Ingewikkeld Session we recently did on this subject. You can watch the Youtube video below, or look for Ingewikkeld Sessions in your favorite podcast app to listen to this session. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/EdK250rbAY4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Having said that, if you like learning about development-related subjects, it's a good idea to have a regular look at the planning for our [Ingewikkeld Sessions](https://ingewikkeld.dev/sessions). And if you really like what we're doing, you can also [support us at Patreon](https://patreon.com/ingewikkeld). If you support us, you get access to the full back catalogue of our live stream recordings. 

## We can help

If you have more questions about on-boarding or you need help setting it up, do get in touch with us at [Ingewikkeld](https://ingewikkeld.dev/). We can help by looking at your current on-boarding process and advising on how to improve it, and we can even help you improve it. 