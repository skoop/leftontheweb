---
layout: post
title: "Speaking at Pfcongres 2010"
date: 2010-03-30 6:34
comments: true
categories: 
  - pfcongres 
  - pfc10 
  - php 
  - conference 
---
In the past years the event has been amazing and I have a full trust that this year won&#39;t be any different. With speakers such as Paul Borgermans, Mike van Riel, Christian Heilmann, Jan-Willem Eshuis, Mihai Corlan, Harrie Verveer, Bram Veenhof and many more on topics such as Database Control, Scrum, Flex, Dependency Injection and DSL the event promising to be awesome again this year. And with this year being the fifth consecutive year for the conference there is a birthday to celebrate!&nbsp;<div><br /></div><div>If you&#39;re into web development (PHP, Javascript, Web standards, Flex, Microsoft, Project methodologies, mobile, documentation) then Pfcongres might be an interesting conference to attend. With a price of only 37.50 EUR (30.00 EUR for Pfz members) it really is a steal. There is no reason for you not to be there! So <a href="http://www.pfcongres.com/tickets" target="_blank">register now</a>, and I&#39;ll see you there!</div>
