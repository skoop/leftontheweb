---
layout: post
title: "A URL shortener in a couple of hours"
date: 2011-10-14 13:06
comments: true
categories: 
  - silex 
  - php 
  - Symfony2 
  - symfony 
  - prototype 
  - microframework 
---
<h2>About Silex</h2>
<p><a href="http://silex.sensiolabs.org/">Silex</a> is one of the many microframeworks we've seen pop up recently. Contrary to most other microframeworks, however, Silex is built on top of an existing open source project: Symfony2. A microframework is a very small framework which basically does nothing more than bootstrap and handle your request and response. Though the microframeworks cater to a very specific need, I really like the approach of not needing a full framework for a project that clearly is simple enough not to need one.</p>

<p>Some things I really like about Silex are:
<ul>
    <li>Built on top of existing open source components</li>
    <li>Packaged in a phar package</li>
    <li>Extensible: There are extensions available to the Silex core, and I can extend it even more</li>
</ul>
</p>

<h2>Sitting down</h2>
<p>This morning, I sat down to have a look at Silex and see how it works. I simply downloaded the phar, set up my basic project, and started working on setting up what I had in mind. Every time I got stuck or needed some info, I just Googled the topic to find the answer.</p>

<p>And this is where the first "problem" arose. I followed the steps in one of the pages I found on the official Silex and ran into an error telling me Silex\Extension\TwigExtension could not be loaded. Thanks to <a href="http://twitter.com/Stof70">@Stof70</a>, however, I found out that actually, I was looking at an old version of the documentation, and Extensions had been renamed to <a href="http://silex.sensiolabs.org/doc/providers/twig.html">ServiceProviders</a>. After changing my code, everything was working again!</p>

<p>Using this approach, it only took me a couple of hours to get a working version of my URL shortener. I did this by a constant refactoring, starting off with a very simple version and then constantly improving this code up to a point where I had what I wanted. Quite nice. Now it's time for some real-life testing :)</p>

<h2>When Silex</h2>
<p>What I've found is that Silex (or any other microframework for that matter) is an excellent approach for small projects where you start with an idea of what you want to build and then improve on your existing code. It is very tempting to put in hackish solutions, which is OK if you keep on improving your code but you have to actually do that and not keep the hackish solution.</p>

<p>Another great situation where you might want to use a microframework is for quick prototyping, if you want to build a quick prototype for a customer to show them what can be done, and have the opportunity of rebuilding it later for the final version, a microframework will give you everything you need for the basic setup. Quite nice! So try it! Just take a couple of hours and an idea of a simple site or app you want to build, and start building. Even if you never use it later on, you'll have learned something new, which is always good.</p>
