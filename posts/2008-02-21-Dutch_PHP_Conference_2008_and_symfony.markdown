---
layout: post
title: "Dutch PHP Conference 2008 and symfony"
date: 2008-02-21 19:59
comments: true
categories: 
  - php 
  - symfony 
  - DPC2008 
  - ibuildings 
  - zend 
  - conference 
  - event 
---
<p>The <a href="http://www.phpconference.nl/" target="_blank">conference</a>  has been prepended this year with an extra day filled with workshops. On friday June 13th, there will be 4 tutorials: One on PHP Unit by Sebastian Bergmann, one on Zend Framework by Matthew Weier O&#39;Phinney, one of Advanced PHP by my colleague Dennis-Jan Broerse, and last but definitely not least, one on symfony by none other than Fabien Potencier and myself! That&#39;s right, we&#39;ll do the tutorial together! I am pretty excited about this as this will not be some conference session, but a full-day tutorial. And I&#39;m possibly even more excited about being able to do this together with the big man behind the symfony framework!</p><p>But aside from these tutorials, the line-up for the main conference on June 14th is awesome as well: Zeev Suraski, Marco Tabini, Sebastian Bergmann, Gaylord Aulke, Lorna Jane Mitchell, Fabien Potencier, Matthew Weier O&#39;Phinney, Ivo Jansch and even more to be announced. That is one hell of a line-up! I am so much looking forward again to this event. </p>
