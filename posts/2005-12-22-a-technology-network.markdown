---
layout: post
title: "A technology network"
date: 2005-12-22 16:25
comments: true
categories: 
  - weblogging 
---
Lately, I've been thinking about weblogging networks. After being introduced to the <a href="http://9rules.com/">9 rules network</a>, I got an idea. What the people at 9 Rules are doing is very good. High quality weblogs centralized on a single site. There are some things I'd want to do differently, and 9 Rules is very wide in terms of topics, covering just about any topic available.

So I've been thinking about starting a new weblog network site, focussing purely on (web) technology-related weblogs. The website of this network would, apart of course from some information on the network, also aggregate the full content of member sites into a single overview of posts. And maybe some other ideas that would come from members of the network of course.

Anyone else interested in maybe working on this? Or people who would be visitors of such a network site that have ideas of things they'd like to see on the site? Naming ideas?
