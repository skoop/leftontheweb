---
layout: post
title: "Update your phpBB!"
date: 2005-03-19 3:53
comments: true
categories: 
  - leftontheweb 
---
Every once in a while, I am shocked by the lack of responsibility of some hosting providers. A lot of hosting providers offer their clients free phpBB installations, either through a control panel or as an added service of their support. However, a lot of these hosting providers do not tell their clients that installation is not all. That phpBB should be regularly updated when new versions are released. Of course, this does not limit itself to phpBB, but really to any web application.

We're currently at phpBB 2.0.13. And today, someone comes to the phpBB support forum because his forum, phpBB 2.0.<strong>4</strong>, was hacked. I <a href="http://www.phpbb.com/phpBB/viewtopic.php?p=1484533#1484533">quote</a>:

<blockquote>My hosting service provided that version of phpBB and I never really saw it necessary to upgrade (until today!!)</blockquote>

Hosting providers should really tell their clients who want to use free software installations that they should be on the lookout for updates. It's also in their own interest, because sometimes, whole servers can be taken down by malicious hackers.
