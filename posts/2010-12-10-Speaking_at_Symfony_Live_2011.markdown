---
layout: post
title: "Speaking at Symfony Live 2011"
date: 2010-12-10 19:23
comments: true
categories: 
  - php
  - symfony
  - sflive2011 
  - paris 
  - sanfrancisco 
  - conferences 
---
<p>This talk is all about not using PHP. I have seen too often that developers or companies decide to use PHP for everything. And even though PHP is a very versatile language that can be used for a huge variation of things, it is not meant for all tasks. Sometimes it is better to go for software written in another language than PHP, simply because that software does a better job.</p>

<p>A variation of software will come by. Things like Solr, Gearman, Memcache, Trac and Jira will be discussed as well as other tools not built in PHP but which will make your life as a developer a whole lot easier. It is important for everyone to realize that you should choose the best tool for the job, and not use a screw when a nail is best suited for the job.</p>

<p>I am really looking forward to both Symfony Live conferences and meeting friends new and old. See you there!</p>
