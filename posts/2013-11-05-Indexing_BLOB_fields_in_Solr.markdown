---
layout: post
title: "Indexing BLOB fields in Solr"
date: 2013-11-05 13:00
comments: true
categories: 
  - mysql 
  - solr 
  - blob
---
Just a quick blogpost today about Solr. If I help someone with this post, good. If I help myself to remember this, good. 

In a project I'm currently working on I'm stuck with a legacy database where I can't change the fields for fear of breaking a legacy application that is using the same data. One of the fields in the database that I need to index with Solr is a MySQL BLOB field. I initially just indexed it as is, but that resulted in odd codes instead of the actual text:

> B@61bf2ef8

Since I use the DataImportHandler instead of writing my own indexing scripts, I can't do the conversion myself, so I needed a simple way to convert the BLOB to a usable text somewhere in the indexing process. Of course, [Google](http://www.google.com/) and [Stack Overflow](http://www.stackoverflow.com/) come to the rescue. Thanks to [this answer](http://stackoverflow.com/a/4221962/391327) to a question about this topic, I know that it is actually quite easy.

~~~
select CONVERT(Text USING utf8) as description FROM Book where EAN='${bookproduct.id}'
~~~

That's it! Now, instead of the odd object codes as above, I actually index the text-version of the BLOB field.
