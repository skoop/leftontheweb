---
layout: post
title: "Windows 98 Quirks"
date: 2005-12-14 16:26
comments: true
categories: 
  - technology 
---
My in-laws have a computer still running Windows 98. Basically, the computer can't really handle anything else. Not a problem, because they just use it for some basic Internet stuff, and to load the pictures they make with their digital camera.

Recently they moved their PC to another room. They asked me to reconnect it, and so I did. I reconnected, and then booted up the computer. It gave a BIOS error about 'auxiliary', so I suspected something related to the mouse or the keyboard. After a bit of waiting though, it still booted fine, or at least up until a certain point, when it started giving error about the Extended Memory Manager, and not being able to locate HIMEM.SYS.

Uhm. Huh? I now suspected something more serious to have happened to Windows or the harddisk. This is not supposed to happen. It should just boot but without being able to use the mouse or the keyboard, whichever one was the problem. Since it ended up on a DOS prompt, I could try the keyboard, and indeed, it didn't work. The chord of the keyboard indeed seemed to have some damage. At least the BIOS error was solved.

When I went there a few days later, with a keyboard I still had lying around at home, and connected the keyboard, then booted up the computer, no BIOS error. That problem solved! Now only for that weird HIMEM.SYS error.

What error?

Windows 98 booted as usual, no problem at all!

Now I know this is a Microsoft product, and so I should be expecting weird behaviour, but how the hell can a broken keyboard cause the OS to misplace files? No way this can be related. But it is.

The problem was solved, but it kept nagging at me. How can this be related. If anyone here knows the answer, please leave a comment, because it truly baffles me that a keyboard problem can influence memory management.
