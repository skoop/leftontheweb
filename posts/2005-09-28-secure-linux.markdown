---
layout: post
title: "Secure Linux"
date: 2005-09-28 9:23
comments: true
categories: 
  - technology 
---
I am not even halfway the article, but I'm already quite impressed with it. <a href="http://linuxhelp.blogspot.com/2005/09/securing-your-computer-running-linux.html">This article</a> gives a really good overview of things you can do to secure your linux computers from those evil crackers (not hackers!) that try to get into your system to use it for all kinds of malicious stuff.
