---
layout: post
title: "TomTom, here I come"
date: 2005-10-05 4:30
comments: true
categories: 
  - personal 
---
Well, it's final. After looking for a job for not even two little weeks, I've been hired by GPS navigation company <a href="http://www.tomtom.com/">TomTom</a>. Things have gone unbelievably fast since I published by CV online.

I am looking forward a lot to starting my work at TomTom, next monday. I think I'll have a wonderful job there, with lots of learning ahead. A big challenge, which is what I like.
