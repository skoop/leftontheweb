---
layout: post
title: "A good read, an alternate view"
date: 2004-12-05 3:53
comments: true
categories: 
  - leftontheweb 
---
I frequent quite a few forums, related to different subjects. <a href="http://www.n5md.com/">n5MD</a> is a very cool independent label where good music comes out, and they also have a forum. Today, a wonderful <a href="http://www.n5md.com/forum/board_entry.php?id=1258&page=0&category=all&order=last_answer&descasc=DESC">topic was posted</a> on that forum. It gives an interesting alternate view on schizophrenia.

Read it, and think about it a bit more.
