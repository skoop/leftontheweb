---
layout: post
title: "Support the global community"
date: 2011-12-29 8:23
comments: true
categories: 
  - php 
  - symfony 
  - kiva 
  - micro-financing 
  - pfcongres 
---
<p>This is the reason I give to charity a lot. But since 2008, I also support entrepreneurs worldwide by micro-financing loans they need for starting or expanding their business. I've been doing this through <a href="https://www.kiva.org/">Kiva</a>. Looking at my stats, since 2008 I've made 11 loans to a variety of countries (Cambodia, Senegal, Philippines, Paraguay, Ukraine, Uganda, Mexico, Bolivia and Kenya) for a variety of different purposes (Restaurant, Farming, Crafts, Food Production/Sales, Food Market, Furniture Making, Musical Performance and Sewing). Funny detail: The currency for Philippines is PHP.</p>

<p>The concept of Kiva is that several users of Kiva support a project by all lending $25 (or more). The entrepreneur gets the money, can use it for the purpose for which it was lent, and then repay it over a certain period with monthly repayments. All lenders get an occasional update on the status of their loan, and as soon as a repayment has been made, you get it back into your Kiva account. Once repaid, you can make another loan to another person.</p>

<p>As I mentioned above, I think we who live in wealthy countries have a responsibility to support those people in their efforts to improve their own life. And that's why, at the end of a beautiful 2011, I want to ask everyone in the PHP community to look beyond the PHP community to our global community. Consider creating a Kiva account, put in $25 (or more of course) and make a loan. Help other people, this time not by answering questions or writing some code, but by lending them your money. And of course, if you do, feel free to join the <a href="http://kiva.org/invitedto/phpeople/by/skoop">PHPeople</a> or <a href="http://kiva.org/invitedto/symfony_team/by/skoop">Symfony team</a>.</p>

<p>Supporting the global community is also why we purchased Kiva gift cards as speaker gifts for <a href="http://pfcongres.com/">PFCongres 2011</a>. The response from speakers to this was quite good, so I'm happy we made this choice. Now I hope other people will make the choice to support Kiva and the global community.</p>

<p>I wish you all a very happy 2012, and hope to meet many of you at your local conference of choice :)</p>
