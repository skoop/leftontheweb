---
layout: post
title: "PostCrossing"
date: 2005-07-26 12:36
comments: true
categories: 
  - postcrossing 
  - fun 
---
After <a href="http://www.bookcrossing.com/">BookCrossing</a>, now there is <a href="http://www.postcrossing.com/">PostCrossing</a>. A nice way to receive a random postcard from all over the world every once in a while. Pretty fun, if you ask me. It doesn&#39;t cost much (I mean, how much does it cost to send a postcard? Not a lot eh?) and it brings some fun when you receive a postcard.
