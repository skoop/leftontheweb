---
layout: post
title: "Dear recruiter"
date: 2021-05-28 12:30:00
comments: true
categories: 
  - php
  - recruiters
social:
    summary: An open letter to all tech recruiters out there on how to work with tech people.
    
---

## Dear recruiter,

This is an open letter to you, the recruiter who focusses on the tech industry. I've had many interesting experiences with recruiters, and I want to share some, to tell you about my positive and negative experiences, in the hopes that you might learn something.

In my social bubble, both online and offline, many developers are not happy with you. Perhaps not you specifically, but recruiters are often seen as annoying, 
rude, lying people. It is therefore especially important for you to make a good first impression when you contact someone. Hopefully, the things I'm going to talk about will help you do that.

### Be clear and honest from the start

I have had many interactions with recruiters that were either very unclear or dishonest, or conveniently left out some information in the early communication about a project. 

One example was an email I got about a very interesting project. I showed interest, sent a CV and had an interview. This means that from my side, I've invested several hours already, hours that for me as a freelancer, are unpaid. Because I see potential in the project.

The customer was interested and I was interested as well. And then the paperwork came about, which included a 90 day payment period. 90 days. That is three months. When I responded to that, I got told "This is standard for this customer". That might be true, but you know that and I don't. And I don't know a lot of freelancers that are OK with or even capable of handling a 90 day payment period. It could've saved me (and you!) hours of work and a disappointment in the final stages of the process. And if this is standard, you knew about it and could've communicated this in your initial email (or quickly thereafter).

More recently, I got an email about an interesting full-remote project. I specifically communicated in our initial communication that the full-remote was an important requirement, and you went ahead and introduced my developer to the customer. When that developer interviewed with the customer, it turned out that because they were recruiting for a new team, there was a 2-day on-site requirement. When I asked about this, you told me you had heard about this already. Why had you not already communicated this to me? This could've saved me, my developer and you a lot of time and frustration.

### Send the right projects

The amount of messages on LinkedIn and email in my inbox from recruiters is pretty big. Which I can accept, because that should mean that we do our work well and you as a recruiter think we could be good potential candidates for the roles your customers have.

Unfortunately, when I wade through the emails, I can delete about 75% of the emails immediately. With my company we focus on PHP development, architecture, consulting and training (and you know about that!), so why do I get so many emails for Python positions, Java positions etc? You are wasting a lot of my time, because yes, I go through all of these because there might be a super cool project in there that we want to work on.

If you send out emails or LinkedIn messages, please make sure you only send messages that actually apply to me. If it's sort of related (like a product owner or scrum master role) that's fine as well, but things that are totally unrelated to our specialty can better be sent to the people that specialize in that.

### Rates

I only rarely receive emails about projects where the rates are communicated in the first email. Most of the emails contain useless wording such as `good rates` or `competitive rates`. Just tell me what you're willing to pay me. I'm happy to discuss my rate and in specific situations adapt my rate to fit with your client's wishes. But, here we go again, it would save you and me a lot of time if you communicate this upfront. Because if your maximum rate is 80% of my minimum rate, we're never going to be able to make a deal. 

### Ask before sending my CV

Unfortunately, I've been in situations where I was contacted by you and I sent you my CV, and while we're still discussing specifics it turns out you've already sent my CV to your client. Later in our discussion it then turns out that we can't agree on specifics, and then I get the `but I've already sent your CV to the client and they're really interested, can't we agree on this because this looks bad on me`. Listen, if you've already sent my CV while we are still discussing specifics, that is not _my_ problem. Also, please consider the damage it does to _my_ image. Because I'm pretty sure you're not going to tell your client that you f*cked up. Which means that this specific client, which I may or may not encounter in the future, may have a negative experience with me, even if they haven't even ever spoken to me.

## That first impression

The above are only a few examples. I have many more where these came from. So let's talk about making a good first impression. And that really already starts with your initial contact:

- Send an email first, with as much information you have on the project: A good description of what the project is, what your client is looking for and why you think I would be/have a good candidate, a maximum rate (possibly accompanied by a preferred rate), a description of the process and any special requirements or other agreements that are important such as payment method, hours per week/month, specific work days, required insurances and anything special that is expected to be included in the rate such as specific hardware or software, background checks, etc)
- Feel free to call me after a couple of days if I haven't responded yet, but give me some time to consider if I am or have a good candidate for the project and whether I can agree with the wishes and requirements
- If any new information comes to light between your email or other contact we have, inform me of that. Don't leave it out for me to find out later. Because I will (have to) find out.

This is just for that good first impression. If my first impression of you is not good, given the amount of bad apples in the recruitment business, it is very likely that you'll quickly make it to my mental blacklist and I'll ignore you in the future.

After a good first impression, make sure to show that you want to continue a good business relationship. I try to do that as well by contacting you if I feel there might be a good opportunity. Don't spoil it by spamming me with projects that you know I'm not a good fit for. If we agree to work together, ensure that you agree to your end of the deal in terms of payments etc. That ensures we might be able to work together for a long time.

## Stand out

In a recruitment world that is dominated by spammers, lying people, frustrating communications and an overall negative attitude, it is quite easy to stand out, it is easy to show that you're serious about doing business with me by showing you don't just care about your own profit margin, but you care about a long-term business relationship. I care deeply about delivering high quality services to my customers and, if we work together, to your customers. Please show me that you also care about that. If you do, I will not only be happy to work with you for a long time, but I'll also be happy to help you find other possible candidates, which may lead to more money and a better network for you. I'd call that a win/win situation. Please care. I do.
