---
layout: post
title: "Pedal Powered VoIP"
date: 2005-06-23 13:15
comments: true
categories: 
  - leftontheweb 
---
Now <a href="http://www.tectonic.co.za/viewr.php?id=489">this is cool</a>. If you have no phone line, no electricity, but you still want to use the phone, what do you do? Of course! Get a computer, modify a bike, and create your own electricity and a wireless connection to phone over IP.
