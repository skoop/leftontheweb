---
layout: post
title: "Upcoming conferences"
date: 2024-06-21 16:50:00
comments: true
categories: 
  - php
  - conferences
  - talks
  - cakefest
  - apiplatformcon
social:
    highlight_image: /_posts/images/apc24.png
    summary: I have two upcoming conferences in the next couple of months. Will you join me in Luxembourg or France?
    
---

Conferences are both fun and useful. Fun because you meet people and useful because you learn stuff. I have two conferences in the upcoming months, and I hope to see you there!

## Cakefest

Last year was my first [Cakefest](https://cakefest.org). It was in Los Angeles, and a lot of fun. While I am not using CakePHP and have not used CakePHP in a while it was good to meet the community and share some information that might be useful.

I am honored that they think I did good enough a job to be invited back again this year. And even better: I don't have to fly out, I am actually driving to Esch-sur-Alzette in Luxembourg. The [schedule](https://cakefest.org/schedule) is looking good! I will be doing my talk on sustainable open source contributions there.

[Tickets are available](https://cakefest.org/tickets) for the in-person conference, or you could even get a free ticket for the online stream of the conference. But in-person is usually more fun, so if you have the opportunity, join us!

## API Platform Conference

Another conference that I got invited back to is [API Platform Conference](https://api-platform.com/con/2024). Again in driving distance for me, and I had a lot of fun last time, so I'm absolutely looking forward again to being there. The conference is in Lille, in the North of France. 

In Lille, I will be doing my Domain-Driven Design basics talk, which focusses on what I think is the most important part of DDD: Understanding the problem. Lille is a beautiful city, the [schedule](https://api-platform.com/con/2024/schedule/) is amazing... I'm really looking forward to it.

This conference also has [both in-person and online tickets](https://api-platform.com/con/2024/#pricing). If you have the opportunity, I'd love to see you there!

