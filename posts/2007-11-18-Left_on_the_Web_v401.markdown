---
layout: post
title: "Left on the Web v4.0.1"
date: 2007-11-18 12:42
comments: true
categories:  - leftontheweb 
---
<p>I just deployed version 4.0.1 of Left on the Web to the server. It&#39;s a minor update, a bugfix release. Just wanted to share some findings:</p><ul><li>Caching of Flickr and Ma.gnolia has been improved. I am using the sfFileCache class of symfony to do the caching, and I was not manually setting the lifetime of the cache (assuming a reasonable default value was set). But it seems there was no time out on the cache, it just kept on living. Thinking back, this actually makes sense: Let it live until the cache gets cleared if you don&#39;t specify a lifetime. But my change has been now to specify a lifetime, so that you actually see some new photos and links pop up.</li><li>To save a bit of bandwidth, I have changed the header image from a single header.php that streams from a directory, to a little css magic in the header. I&#39;ve done this because the header.php could not be cached (else I would not get the wanted random header effect) which causes quite some bandwidth usage. With the css magic, I now inject a random image filename from the same directory into a css rule in the header of each page. This will allow each image to be cached by the browser without losing the random header.</li></ul>There&#39;s some bigger chances that I want to implement, but they will come some where in the near future.
