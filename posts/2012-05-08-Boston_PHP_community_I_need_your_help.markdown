---
layout: post
title: "Boston PHP community, I need your help!"
date: 2012-05-08 13:57
comments: true
categories: 
  - php 
  - symfony 
  - conferences 
  - northeastphp 
---
<p>Usually this means I have to cancel my speaking at the conference, but I would really love to be part of this conference. So, I'm looking for one or more companies in the Boston area that are interested in hiring me (or possibly me and <a href="http://www.adayinthelifeof.nl/">Joshua Thijssen</a>, my business partner in <a href="http://techademy.eu/">Techademy</a>) for training, consulting or a couple of days of development. The way I figure it, if I can sell two or three days worth of work in the Boston area, that will fund my trip and hotel for the NorthEast PHP conference.</p>

<p>Another option of course would be a company that is looking for some community cred in the NorthEast area to sponsor me (and Josh?) to come over. If we can get at least part of the flight covered by a sponsor, that will already help us big time. And yes, I will give you credit during my talk as well on twitter, facebook and my blog. How cool is that?</p>

<p>If you're interested in hiring me (and Josh?), please get in touch! Please, dear NorthEast PHP community, help me fly over to Boston and meet you all!</p>
