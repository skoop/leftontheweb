---
layout: post
title: "Flickr Video"
date: 2008-04-09 18:49
comments: true
categories: 
  - flickr 
  - photo 
  - video 
  - youtube 
---
<p>Actually, I have mixed feelings about it. I am happy because the way they implemented it is very clean, very minimal. A very simple approach to video hosting, integrating perfectly with the already present photo hosting. When embedding video&#39;s from Flickr, the embedded player is also excellent in it&#39;s simplicity. It does what it is supposed to do, and does it well. This has always been the Flickr approach to photo hosting, and they extended this perfectly towards video.</p><p>However...</p><ul><li>Flickr was good for it&#39;s photo hosting because it focussed on just that. Now that they added video this implies they&#39;ll have to split their focus on two seperate services. Even though interruptions of service were very rare on Flickr, it is to be seen if they can keep that up with the addition of video. An extra service to maintain, more data to store on disk. This is something only the future will tell.</li><li>The video hosting is currently limited to 90 seconds or 150Mb of video. This isn&#39;t a lot. The great thing about Flickr for photos is that it attracts both <a href="http://www.flickr.com/photos/skoop/" target="_blank">amateur photographers</a>  and <a href="http://www.flickr.com/photos/rebba/" target="_blank">serious photographer</a>. With such low limits, for now most video&#39;s uploaded to Flickr will be <a href="http://www.youtube.com/watch?v=k6ILY_eM7Vk&amp;feature=related" target="_blank">stupid stuff</a>  you find on <a href="http://www.youtube.com/" target="_blank">YouTube</a>. <br /></li></ul>I guess for now, let&#39;s see how it goes. I&#39;ve uploaded <a href="http://www.flickr.com/photos/skoop/2400752164/" target="_blank">two short</a> <a href="http://www.flickr.com/photos/skoop/2400756084/" target="_blank"> screen recordings</a>  I used in a presentation last saturday and they work quite well. And even though there is some <a href="http://www.flickr.com/groups/no_video_on_flickr/" target="_blank">resistance against the video service</a>, for now I&#39;ll stay positive about it all. Though I sure hope they&#39;ll set higher limits as soon as their new service has shown stability.
