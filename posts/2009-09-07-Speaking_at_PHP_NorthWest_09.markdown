---
layout: post
title: "Speaking at PHP NorthWest 09"
date: 2009-09-07 16:06
comments: true
categories: 
  - php 
  - phpnw 
  - symfony 
  - zendframework 
  - ezcomponents 
  - pear 
  - frameworks 
  - conferences 
---
Last years speaker schedule was quite nice and this year again the organization has come up with a great list of speakers. The list includes (amongst others) Derick Rethans, Scott MacVicar, Michelangelo van Dam and Lorna Mitchell. I am very much looking forward to attending some of the sessions.<br /><br />My own talk is a brand new talk on integrating symfony and <a href="http://framework.zend.com/" target="_blank">Zend Framework</a>. People who know me a bit will be aware that even though I am mostly a symfony advocate (and involved in that project), I very much like the Zend Framework as well because of it&#39;s ease of use of individual components. Many people currently still believe though that you will have to make a choice for a single framework. In my session, I hope to convince people that you should not pick either symfony or Zend Framework, but you should go for the best of both worlds. This means integrating Zend Framework components into your symfony projects, but it can also mean integrating symfony components into your Zend Framework project. And though in my talk I will be limiting myself to these two frameworks, there is no need to stop there. There&#39;s some excellent other frameworks (<a href="http://ezcomponents.org/" target="_blank">ezComponents</a>, <a href="http://pear.php.net/" target="_blank">PEAR</a>, and much more) that you can use to your advantage.<br /><br />So if you&#39;re in the area, you should definitely come to PHPNW09. And even if you&#39;re not in the area, it is definitely worth your time and money. The conference is not very expensive either, and plane or train tickets to Manchester are also quite affordable. So there&#39;s no excuses anymore! I hope to see you there!<br />
