---
layout: post
title: "Job Openings at Dutch Open Projects!"
date: 2007-04-16 5:03
comments: true
categories: 
  - technology 
---
Due to expanding business Dutch Open Projects BV (DOP) has multiple positions for:

<strong>Ambitious PHP Developer</strong>

As a PHP Developer you are working in a variety of self-steering Project teams. You will take an active part in developing, building and implementing custom built applications for a variety of customers and a diversity of products and services. Customer satisfaction is key in doing our business right the first time and we expect you to have the same mind setting.
Within the application product range we offer for example websites, components and custom built (web based) applications. Also DOP makes use of a full suite of Open Source/Open Standards Software such as CRM, administrative/logistics tools and a full range of web shop applications. All with custom built front-end interfaces, databases en external connections as required. Finally maintenance and expanding of existing (<a href="http://en.wikipedia.org/wiki/Application_service_providing">ASP</a>) applications is part of the job description. This is a position in our Leusden-based office.

<em>What are we looking for?</em>

* Intellectual and working capacity on Bachelor level. Demonstrable by completed or nearly completed degree on Bachelor's level or acquired by traceable (self study/hobby/working) experience;
* You are able to showing some of your (self study/hobby/working) combined PHP/MySQL/(X)HTML/CSS projects;
* You are keen on the Open Source Community. Possibly you are already an active member of Open Source Communities?;
* Candidates with an understanding of one or more of the following techniques: Linux, Kubuntu, Subversion, AJAX, Joomla!, Symfony, PHP5, Zend Studio, Table-less lay-lout and Tux;
* Experience with PostgreSQL or Oracle is a plus;


<em>What do we offer?</em>

* An informal working culture;
* Professional self-steering project teams;
* Development with Zend Studio;
* Possibility of acquiring your Zend (PHP5) certificate;
* Growth and guidance to becoming a genuine PHP Guru;
* Working in a forest environment with our own swimming pool;
* Remunerate according to knowledge and level of experience.

Are you feeling appealed to working in this professional but informal Open Source/Open Standards Organization with a bunch of geek colleagues and interesting products? Please contact us. We will gladly have an appointment with you giving you the possibility to talk to your future team members.

<hr noshade />

Due to expanding business Dutch Open Projects BV (DOP) has multiple positions for:

<strong>Experienced PHP Developer</strong>

As an experienced PHP Developer you are working in a variety of self-steering Project teams. You will take an active part in developing, building and implementing custom built applications for a variety of customers and a diversity of products and services. Customer satisfaction is key in doing our business right the first time and we expect you to having the same mind setting. As experienced PHP Developer you are co-responsible for project scope including budget, adhering to deadlines and quality of produced PHP code and functionality. You are able to motivate your junior Developers and give them guidance and are able to answering their queries.
You are accustomed to translate Functional Design to proper solutions and functionality.
Within the application product range we offer for example websites, components and custom built (web based) applications. Also DOP makes use of a full suite of Open Source/Open Standards Software such as CRM, administrative/logistics tools and a full range of web shop applications. All with custom build front-end interfaces, databases en external connections as required. Finally maintenance and expanding of existing (ASP) applications is part of the job description. This is a position in our Leusden-based office.

<em>What are we looking for?</em>

* Intellectual and working capacity on Bachelor or Academic level. Demonstrable by a related Bachelor's or Academic degree or sound working experience. Your resume shows a clear track-record.
* You are a proven PHP Guru in code, Functional Design, CSS and MySQL;
* You are keen on the Open Source Community. Possibly you are already an active member of Open Source Communities?;
* You have a sound experience with one or more of the following techniques: Linux, Kubuntu, Subversion, AJAX, Joomla!, Symfony, PHP5, Zend Studio, Factory, Singleton, Agile Development, deployment, Table-less lay-lout and Tux;
* Experience with PostgreSQL or Oracle is a plus;


<em>What do we offer?</em>

* An informal working culture;
* Professional self-steering project teams;
* Development with Zend Studio;
* Possibility of acquiring your Zend (PHP5) certificate;
* Guidance and means to your personal development to becoming for example a (Agile) Project leader or Information Analyst.
* Working in a forest environment with our own swimming pool;
* Remunerate according to knowledge and level of experience.

Are you feeling appealed to working in this professional but informal Open Source/Open Standards Organization with a bunch of geek colleagues and interesting products? Please contact us. We will gladly have an appointment with you giving you the possibility to talk to your future team members.
