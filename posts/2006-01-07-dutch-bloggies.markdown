---
layout: post
title: "Dutch Bloggies"
date: 2006-01-07 5:42
comments: true
categories: 
  - personal 
---
The <a href="http://www.dutchbloggies.nl/">Dutch Bloggies</a> are the dutch version of the international weblog awards (Bloggies).

After some technical problems earlier, I've finally been able to nominate my favorite weblogs. Mind you, they're all dutch (though not all dutch-language) weblogs. Since I don't mind posting my nominations, here they are:

<strong>Best weblog</strong>
<a href="http://www.mijnkopthee.nl/">Mijn Kop Thee</a>
<a href="http://xiffy.nl/weblog">Xiffy</a>

<strong>Best written logs</strong>
<a href="http://marcelhesseling.nl">Marcel Hesseling</a>
<a href="http://www.rommelhok.com">Rommelhok</a>

<strong>Best design</strong>
<a href="http://www.i-marco.nl/weblog">The Net Is Dead</a>

<strong>Best lifelog</strong>
<a href="http://www.jnnk.nl">Jnnk</a>

<strong>Best collective log</strong>
<a href="http://www.mijnkopthee.nl">Mijn Kop Thee</a>

<strong>Best newcomer</strong>
<a href="http://daanvanderburgh.web-log.nl">Denk soms na</a>

<strong>Best music log</strong>
<a href="http://www.fileunder.nl">File Under</a>
<a href="http://www.myownmusicindustry.nl">My Own Music Industry</a>
<a href="http://thegonewait.blogspot.com">The Gone Wait</a>

<strong>Best political log</strong>
<a href="http://daanvanderburgh.web-log.nl">Denk soms na</a>
<a href="http://robertgiesberts.nl/index.php">Robert Giesberts' Webboek</a>
<a href="http://www.janmarijnissen.nl/weblog">Jan Marijnissen</a>

<strong>Best photolog</strong>
<a href="http://www.yoda.nl">Yoda</a>
<a href="http://milov.nl/">Milov</a>

<strong>Best themelog</strong>
<a href="http://annevankesteren.nl">Anne van Kesteren</a>

I might've forgotten something, but I have some time left to fill in any blanks :)
