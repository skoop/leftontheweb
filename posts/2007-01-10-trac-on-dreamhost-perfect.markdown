---
layout: post
title: "Trac on Dreamhost: Perfect"
date: 2007-01-10 14:43
comments: true
categories: 
  - technology 
---
Installing <a href="http://trac.edgewall.org/">Trac</a> has been described as "hell", "horrendous", and other such wonderfully positive words. Especially on <a href="http://www.dreamhost.com/r.cgi?116006">DreamHost</a>, as I've understood from some people.

But one of the best things of DreamHost is their community. DreamHost manages an extensive <a href="http://wiki.dreamhost.com/">wiki</a>, full of useful information, for instance on the topic of <a href="http://wiki.dreamhost.com/index.php/Trac">installing Trac</a>. But aside from information, the DreamHost community does more!

Enter <a href="http://wiki.dreamhost.com/index.php/DreamTracInstall">DreamTracInstall</a>. A community effort to automate the installation of Trac on DreamHost. Using a simple shell script, you can automate the complete installation of Trac into your DreamHost account, including such useful Trac plugins as TracWebAdmin.

And so now, for a new project that I'll soon post about a bit in this spot, I have a nice and shining Trac installation! Lovely!
