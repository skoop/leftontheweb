---
layout: post
title: "Back2Buzzics"
date: 2006-09-13 4:43
comments: true
categories: 
  - music 
---
It has been a long time since I have seriously done some music, let alone finished a track for public consumption. Well, (hopefully) good news! I have a new track!

<txp:file_download_link id="3"><txp:file_download_name /></txp:file_download_link>

Download yourself this new track of mine, Back2Buzzics. It's done completely in Buzz (thanks to my new VMware Player which allows me to run Windows inside Linux). It's inspired by oldschool techno wizards such as Robert Armani. Enjoy!

<strong>Update: The song is now also available for listening at <a href="http://www.last.fm/music/Stefan+Koopmanschap/Back2Buzzics">Last.fm</a></strong>
