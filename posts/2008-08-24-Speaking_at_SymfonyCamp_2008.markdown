---
layout: post
title: "Speaking at SymfonyCamp 2008"
date: 2008-08-24 19:45
comments: true
categories: 
  - symfony 
  - php 
  - event 
  - symfonycamp 
---
<p>Having initiated the event last year, and it going on to become the success it was last year, I am very happy that my former employer Dutch Open Projects decided to organize SymfonyCamp again this year. The line-up for this year is quite impressive, with not only symfony founder Fabien Potencier there, but also Dustin Whittle (Yahoo!), Jonathan Wage (Doctrine), Ian Christian and Fabian Lange there. A lot of symfony community big shots, as I&#39;d like to call them.</p><p>I am very happy that my name was added to that list. My talk, titled Debugging with symfony - The Power Of The Web Debug Toolbar, will be zooming in on the web debug toolbar, one of the most underestimated features of the framework. I will be speaking on saturday 13th, but I&#39;ll also be present on friday the 12th of course! I don&#39;t want to miss this. </p>
