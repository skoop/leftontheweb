---
layout: post
title: "Here we go again, two days in a row"
date: 2008-06-28 23:39
comments: true
categories: 
  - live 
  - paradiso 
  - music 
---
<p>Since Paradiso in itself is a venue to always look forward to, and Live simply has this special meaning in my life, it&#39;s a complete bonus to be able to attend two shows by Live in a row there. This time, there&#39;s no release out that triggers them to be here, yet the shows will still be special: Live will be recording a DVD during the shows.</p><p>I can&#39;t really tell you why, but Live has always triggered something in me. And it seems with a lot of dutchies, they become popular here first, and then the rest of the world joined in with Throwing Copper, Secret Samadhi and The Distance To Here mostly. And though their most recent offerings have not always been as good (in my humble opinion) as their earlier offerings, they still rock big time.</p><p>Before any Live concert I attend, I always have one or two songs that I&#39;d love to hear. This time round, it&#39;s <em>Waitress</em> and <em>Stage</em>, both from Throwing Copper, that have my special fancy. I don&#39;t think there&#39;s a big chance of them playing those though, since they are recording a DVD and that DVD most probably will need to contain mostly their hits. Still, I can hope for it of course ;)</p>
