---
layout: post
title: "The Serendipity Machine"
date: 2016-10-28 16:45:00
comments: true
categories: 
  - php 
  - serendipity
  - conference
  - meeting
  - seats2meet
  
---

I recently finished a book that has been in my possession for quite some time already, [The Serendipity Machine](https://www.goodreads.com/book/show/21023471-the-serendipity-machine) by Sebastian Olma. Subtitled _A Disruptive Business Model for Society 3.0_ sounds very buzzword-y and startup-y, but despite the subtitle it was a very good book.

I've been a big fan of the [Seats2Meet](https://seats2meet.com/) concept for quite a long time. The idea of getting a free working place including free lunch in exchange for sharing your knowledge and experience with others really appeals to me. I was first introduced to the concept quite a while ago by a colleague. Despite my business model causing me to work on-site with customers most of the time I've worked at Seats2Meet on several occassions, and with [PHPAmersfoort](http://phpamersfoort.nl/) we also organized meetups at one of their locations.

Some of the things discussed in the book really already describe what happens naturally in the PHP community. Especially things like sharing knowledge is happening a lot. One of the things where I think we can still improve though is the facilitating meetings. On the higher level, I mean to combat the isolation of the "islands" inside the greater PHP community, such as Drupal, Joomla! and all the different framework communities. But on a lower level, I also mean the people attending usergroup meetings and conferences. In a way, initiatives such as FIG provide a way of doing this, but on that smaller, lower level, we fail to do this, perhaps also because we fail to facilitate this. The fact that the majority of PHP developers is pretty introvert does not help, but all the more reason to facilitate these meetings.

_So how do we facilitate this?_ you may ask. This is where we can have a look at how Seats2Meet does this. Their platform, where all visitors of the Seats2Meet locations sign in, allows people to "tag" themselves. In their profile, they list the topics they're expert in. For instance, my profile for Seats2Meet contains the following:

![Tags in Seats2Meet profile](/_posts/images/tags-s2m.png) 

When you've book yourself a seat, you'll see the tags of other people that are there together with you, and you can look for the specific knowledge that you need. This is an active facilitation of meetings between people that have something in common. If at some point during the day while working at Seats2Meet you find yourself in need of some specific knowledge, all you have to do is check their system to see if someone with that knowledge is also at the location you are at. 

## The hallway track

This is where the hallway track at conferences becomes useful. While most people are in a session, staying in the hallway is a great way of meeting other people that made the same choice of a skipping a session. These kinds of random meetings are harder during breaks: There are so many people in the hallway at such a moment that it's hard to really decide who to talk to outside of the people you already know, also because everyone else is also talking to people they know. During a session though, there is little choice. So next time you skip a session, just talk to someone else that made the same choice. Even if you can't help eachother at that moment, you've expanded your network (or value network as Sebastian calls it in the book). 

Conferences may facilitate these meetings by allowing "tagging" of people. Either through an online platform or by allowing people to add "tags" to their name badge. I can't tell you exactly how this should be done, but I do feel this is something that can be done in a better way, to facilitate these "random" meetings. To enhance the serendipity.

If all this sounds good, I would recommend you picking up a copy of The Serendipity Machine. It is an inspiring read, especially for those that are active in the PHP community. And if you're near a Seats2Meet location, book a seat for a day every once in a while. It's free and it's awesome.

The Serendipity Machine (ISBN 978-90-8169, November 2012, Creative Commons BY-NC-ND) is also available as [free PDF download](https://www.seats2meet.com/downloads/The_Serendipity_Machine.pdf).