---
layout: post
title: "JIRA, but then differently?"
date: 2006-11-15 14:12
comments: true
categories: 
  - technology 
---
Ever since working with <a href="http://www.atlassian.com/software/jira/">JIRA</a> at TomTom, I've been looking for a similar tool for my various projects. I've found a lot of tools done in PHP, but nothing so far has been equally impressing and useful as JIRA itself.

Now JIRA has an open source license, but I don't really have an open source project to present to Atlassian as the project for which I'd need JIRA. I just need it for my personal projects. And since $1200 is a bit much for a tool for some personal development, JIRA is not really an option.

So here I am, asking for your help: Is there a tool very similar (preferably some type of true clone) to JIRA? It needs to work in a very similar way, and preferably be in PHP (though any other language is fine, as long as it's webbased). And it should be free, or very cheap. Anyone?
