---
layout: post
title: "Logging in english"
date: 2006-01-18 17:31
comments: true
categories: 
  - weblogging 
---
I recently shut down my dutch web log in favor of this one. Though I've always had fun maintaining my dutch weblog, I feel that it's useless to maintain both. English is my main language on the Internet anyway, and since just about everyone I have contact with that would be remotely interested in what I write on my weblog, I might as well just do it in english.

It does give me one dilemma: Should I write about these typical dutch topics that I'd normally put on my dutch weblog. I'm not sure yet. There's a good chance that I won't. That I'll just leave those topics. Or, if they're related to the region where I live, I might just write an article for <a href="http://www.amersfoortcentraal.nl/">Amersfoort Centraal</a>.

Aside from that one little dilemma, I am happy about my decision. There really was no real need to maintain a seperate dutch weblog.
