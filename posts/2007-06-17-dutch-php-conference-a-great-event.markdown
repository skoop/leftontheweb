---
layout: post
title: "Dutch PHP Conference: A great event!"
date: 2007-06-17 4:51
comments: true
categories: 
  - technology 
---
So yesterday was the <a href="http://www.phpconference.nl/">Dutch PHP Conference</a> in the RAI in Amsterdam. Aside from doing a presentation on Symfony there, I had a <a href="https://skoop.dev/article/344/my-plans-for-the-dutch-php-conference">clean plan</a> on which sessions to attend. I ended up though to change my plans slightly. Instead of the PDO presentation, I wanted to check out the new ATK by attending Ivo's presentation.

The event was very well organized. Even though comparing is a bit unfair, when I compare it to our own PHP Bootcamp of two weeks ago, it was a bit more impersonal. Of course, this was the result of a much bigger attendance, and even with the nearly 300 people there, iBuildings had to turn down another 100-150 people (or so I heard). 

Cal Evans' keynote on mashups was very interesting. Unfortunately, the live demo failed due to a failing wifi connection, but even without the live demo, the presentation was very cool. 

Following Cal was Kevlin Henney, who experienced some technical trouble at the start of his presentation, but easily solved this with a great stand-up comedy act. After about ten minutes, finally the beamer worked again and so the presentation started. Kevlin talked about Object Oriented programming and how PHP differs from others. It was an interesting talk, mainly a funny one with much jokes. Unfortunately, I could not discover a very clear conclusion or direct point to make out in his presentation. It was interesting nonetheless.

After the break, Gaylord Aulke's presentation on the Zend Framework was a bit of a let down, but this seemed to be mainly due to a shortage of time (or simply too much to tell). The main part of the presentation was now spent on presenting MVC and ZF's implementation of this, which was nice but took a bit too long.

The ATK presentation by Ivo Jansch was very interesting. It's very interesting to see how with so little code you can create full applications thanks to the ATK framework. I guess if I dive into it, I might start to like it, but the development which such a framework seemed a bit too abstract for me. I like to see some code and do some coding ;)

Finally, my own presentation on Symfony: I felt it was good. I was not as nervous as I had expected to be, but of course I missed a few things. The room was pretty filled, I guess somewhere around 70 people attending, and the questions I got afterwards were also "good" questions which led me to believe that the information I wanted to get across actually did get across. So yeah, I felt it was a success.

Last but not least, Derick Rethans did a presentation on Test Driven Development. Despite some throat problems he managed to get across a clear message about how Test Driven Development can enhance the quality of delivered code. And hell, if Microsoft is doing it, it must be good, right? ;) Anyway, it was a very nice presentation. I'm not sure yet if I'm going to attempt it in one of projects, but the ideas are clear. One important conclusion to make after the presentation I guess is: You need 100% commitment to using TDD, or else it's probably useless.

After this, we had some drinks at the RAI, and then some of the speakers including me went on to a restaurant just behind the RAI to have dinner. There I was able to talk a bit with Sander van de Graaf, Lukas Kahwe Smith, Derick Rethans and someone else who's name I now forgot (darn! sorry for that). It was a very nice way to close a great day. Thanks everyone!

For those interested, my presentation (slides in dutch) is now available on <a href="http://www.slideshare.net/skoop/symfony-dutch-php-conference">Slideshare</a>.
