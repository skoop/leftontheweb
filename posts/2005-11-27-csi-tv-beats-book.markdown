---
layout: post
title: "CSI: TV beats book"
date: 2005-11-27 4:44
comments: true
categories: 
  - books 
---
CSI is one of my all-time favorite TV shows. Simply awesome. Of course, for a lot of successful things, all kinds of other material is also done. CSI also has books. Being the bookwurm that I am, I recently had the opportunity to get me a CSI-book through <a href="http://www.bookcrossing.com/">BookCrossing</a>. So I got the book, and I finished it this week.

The book was good. It definitely had that CSI-atmosphere to it. However, it can't beat the TV show. By far, the TV show is better. This might, in part, be because the TV show was there first, and the books are based on the TV show, but still, books are most of the times superior. Not this time. The books fail to make use of the full potential of a book, of the power of imagination. Also, there are passages in the book that basically describe what you'd usually see in a TV show where for instance DNA is being processed etc. This works in TV, not in written form. 

I might read some more CSI books, don't get me wrong, it was a nice book, however, this is a perfect example where the TV show is much better than the book.
