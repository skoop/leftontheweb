---
layout: post
title: "Cool dutch PHP events with a taste of Symfony"
date: 2007-05-19 12:10
comments: true
categories: 
  - technology 
---
As I <a href="https://skoop.dev/article/333/and-then-there-were-events-enough">mentioned earlier</a> this year is a good year for PHP events in The Netherlands. The pfCongrez has come and gone, and was a great experience, and now two new events are coming, both of which in one way or the other I am involved in and proud of. So let's give both the spotlight:

<strong>PHP Bootcamp</strong>
<a href="http://www.phpcamp.nl/">PHP Bootcamp</a> is an event we at <a href="http://www.dop.nu/">Dutch Open Projects</a> are organizing on June 2nd. It is a free event and I'm proud to be able to announce to you that we've been able to get some leading people from the world of PHP Frameworks to speak at our event.  Fran
