---
layout: post
title: "Netlabels and reviews"
date: 2006-11-03 12:12
comments: true
categories: 
  - music 
---
For quite a while now, I've been running the <a href="http://www.electronicmusicworld.com/">Electronic Music World</a> magazine. On that website I've been publishing news and articles on electronic music, as well as reviews of cd's and vinyl and the occasional netlabel release.

Recently though, I've been a big fan of the dutch review website <a href="http://muzieklog.rommelhok.com/">Muzieklog.rommelhok</a>. They offer a short 3-5 line review and a rating. This approach to reviewing was very interesting. I never considered this, but I felt this to be a very good way to review music.

Inspired by that site, I've started a new subsite of Electronic Music World on <a href="http://net.electronicmusicworld.com/">net.electronicmusicworld.com</a> where I also use this approach, focussing fully on netlabel/online releases. So far, I really like this approach. I'm currently doubting if I'll return to writing reviews on Electronic Music World. I might turn it into a site dedicated to bringing news instead.
