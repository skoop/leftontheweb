---
layout: post
title: "Collaborating on portals"
date: 2006-05-07 14:06
comments: true
categories: 
  - technology 
---
Maybe it's web2.0, or maybe not. It's a nice concept though. <a href="http://www.zimbio.com/">Zimbio</a> allows users to create a new portal page including links, a forum, a collaborative weblog, and many more features, based on the idea that anyone can add and edit. So basically, a wiki, but with more than just the standard wiki article pages.

I like this idea, I definitely quite like it. It's well executed, with lots of fancy AJAX calls to keep you on the same page. No reloading required.

The only thing I dislike is the unoriginal design. Zimbio clearly seems to want to ride the AJAX/web2.0 bandwagon. With so many sites with similar designs around, this one will have a hard time to stand out between the rest. 
