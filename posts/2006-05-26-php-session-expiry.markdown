---
layout: post
title: "PHP Session Expiry"
date: 2006-05-26 15:14
comments: true
categories: 
  - technology 
---
It's funny. Earlier this week I encountered this problem, and then just now, <a href="http://blog.centresource.com/2006/05/23/php-session-lifetime-an-adventure/">someone else writes about it</a>. Though their problem is exactly opposite to the problem I encountered, the cause and the solution is the same.

So what is the problem? In the php.ini, you can configure the session.gc_maxlifetime setting. This setting controls how long a session may live before the garbage collector kills it because it has expired. Then, in your own script, if you feel the php.ini setting is too short or too long, you can specify your own gc_maxlifetime by using <a href="http://php.net/ini_set">ini_set()</a>. So far, no problem.

The problem we encountered was that our php.ini setting was set to something like 24 minutes, and we wanted 24 hours. So we used ini_set() to set the maxlifetime to that value, and still we got reports where a session would time out much earlier than expected. Not good.

Last week, after a bit of digging, we found the cause of the problem. We have multiple applications running on this server that use sessions. Most use the default php.ini setting, and not this adapted setting. All sessions are saved in files in a directory (/tmp in our case). But when the garbage collector is triggered, it does not discriminate between sessions created using the default php.ini setting and the one we set manually. No problem when the garbage collector is triggered from our script. But a big problem when it's being triggered by the other applications. The garbage collector will go through the /tmp directory and see a lot of session files that have expired. It will delete them. Gone is your session that should've lived for 24 hours.

This is, in my very humble opinion, a big shortcoming in PHP's session handling. There is an easy work-around though: those of your applications that use a custom gc_maxlifetime should also be saving the session files to a different session.save_path. Here also, ini_set() will support you in setting this value. This should of course point to an existing directory which is writable by your php/webserver user. 

Now, the next step of course, would be that php would change their session handling. I can not imagine this being too hard. Each session file would, aside from the already present (serialized) session data, contain a tag that is in some way seperated from the actual session data. This tag contains the timestamp of the session's expiration. When a session is being written, the session handler can easily calculate the moment of expiry for the session based on the current timestamp + active session.gc_maxlifetime. Now, when the garbage collector is triggered, it need only read the first line of each session file and purge the files that have a timestamp lower than the current time. I understand that this is probably slightly more resource-intensive than just reading the timestamp of file, but it would enhance php's error handling in such way that I personally feel this is not a problem. It could even be an optional php.ini setting (session.enhanced_session_lifetime = 1). Of course, I'm not in the PHP group, so I guess for now I'll just have to implement a custom session handler with this functionality. Quite a pain though, since I'll have to do this with all my sites then. Or just implement the above fix of course ;)
