---
layout: post
title: "Identity crisis"
date: 2006-07-29 13:24
comments: true
categories: 
  - phpBB 
---
Hmm... It looks like I am in some kind of identity crisis. Either that, or someone else is in mine ;)

Take <a href="http://www.phpbb.com/phpBB/viewtopic.php?p=2289060">this guy</a>. He is claiming that I am one of the original phpBB developers, and that I've moved on to join the phpBB2 Plus team. I am also the author of some kind of software called the Cracker Tracker. 

I have never done any official phpBB development. I am not even part of the phpBB2 Plus team. And I don't even know what the Cracker Tracker is! Now that is fun. Am I in some kind of identity crisis? ;)
