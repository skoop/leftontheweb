---
layout: post
title: "Left on the Web v4.0.2"
date: 2007-11-25 12:37
comments: true
categories: 
  - leftontheweb 
---
<p>The homepage in itself could&#39;ve warranted a bigger version jump, but I felt it too early to bring a v4.2 and besides this &quot;feature&quot; was planned for 4.0 in the first place. The homepage is a better starter into what this site brings I think that having the weblog on the homepage. It also makes it easier for me to add more functionality (which I have planned already), so that there is a central point to all this functionality. I am quite happy with how it turned out, especially considering I am far from a developer.</p><p>Another notable change is the fact that the excerpts I use on the overview page are not also on the article page itself. The reason for this is twofold: First of all it gives a better flow in the &quot;story&quot; I try to tell (since the main articles I write usually start at the point where the excerpt stopped), and also some feed readers (for instance the Google Reader) seem to only show the title and not the excerpt (thanks Ivo for pointing me to that) so people may miss the excerpt alltogether.</p><p>Anyway, here it is, enjoy it! </p>
