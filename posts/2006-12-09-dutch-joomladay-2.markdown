---
layout: post
title: "Dutch Joomla!Day 2"
date: 2006-12-09 0:18
comments: true
categories: 
  - technology 
---
Second day of the dutch Joomla!Days, it's morning and I'm currently at the Dutch Open Projects stand representing the company. We're currently looking for 4 coders and possibly an account manager, which we hope to get from the Joomla! community. Or at least get some contacts.

Plan for today: 

10.15 - 11.00: Keynote speech by Johan Janssens
12.00 - 13.45: Joomla! 1.5 Framework session by Johan Janssens
14.45 - 16.30: either a session about migration from Joomla! 1.0 to 1.5, or a session about webservices with Joomla! 1.5

As yesterday, I'll update this post whenever I am at a session.

<strong>10:14</strong>: Johan Janssens speaks about the history of Joomla! now. He explains that the Joomla! team is offering services such as the developer network and the extensions site to support the community. They do not want to control the content and judge, simply to offer it as a service to users and developers.

Johan also mentions that Joomla! and Drupal and other projects are working together to make a strong stand against commercial projects. They're not trying to compete. 

It's interesting to see how professional the Joomla! team has been structured, with a core team that coordinates and work groups that have specific tasks. For instance there is a seperate QA workgroup that checks all bugs marked as resolved, and tests each release.

Joomla! is also working on a special version of Eclipse which will be branded as 'J!Code', which integrates Eclipse with the Joomla! documentation to show the tooltips for Eclipse functions. The first build should be available soon.

<strong>12:00</strong>: Johan starts to tell a bit more about J!Code, the Eclipse-based Joomla! IDE, for which <a href="http://forge.joomla.org/sf/projects/jcode">a first release is available</a> (unfortunately just for windows).Then he moves on back toward the framework.

In the framework, all Joomla! libraries start with a J. Johan asks all developers not to start their own libraries with a J to prevent conflicts. The structure of the framework is very clear and logical, with an MVC setup. The MVC setup is slightly extended because there is a difference in Joomla between an extension and an application.

Today, Johan goes deeper into the used design patterns. For people new to design patterns, he explains what the patterns do, but for people who already know it, he uses examples from the framework to explain it, so that it is useful for everyone.

Now, we get a nice series of flowcharts on how the system is executed, what happens where, etc. It is very nice to see that the Observer pattern is nicely implemented in the system, and that security is one of the most important focusses.

The MVC implementation in Joomla! is very much decoupled. The Controler loads the Model and feeds it with the information it needs to execute it's logic, then loads the View. The View then talks to the Model to get the right information, but the View can not set information. The Model does not talk to the Controller or the View. This is a much more flexible MVC setup than some other frameworks such as Cake. Decoupling is very important to Joomla!

Johan gives a clear explanation of how to write a simple component. He does this by starting with an "old-style" component, and then explaining what to do with MVC within a component. It's pretty simple and logical.

A hot issue is backwards compatiblity versus new features and good setup of new features. For now, it should all be backwards compatible, and so some of the features are not yet set up in the best possible way. Approaches to this are open for discussion for newer versions.

<strong>14:48</strong>: Webservices in Joomla! 1.5 is the session that I'm attending now. It's in a very small room, too small for the number of people interested it seems. A lot of moving around, getting extra chairs etc. Clearly a hot topic. Presenter of this session is Laurens Vanderput, one of the new Joomla! developers that joined after participating in the Google Summer of Code.

He starts with a clear description of what webservices are and how they work. He then goes into Service Oriented Architecture, stating that it's a big advantage to use it because clients will only need to know the protocol and data definition, and not how the data gathering is implemented. One of the disadvantages is that it's by default a stateless system, so authentication and authorization needs to be implemented by the client and servers.

He explains some webservice-related concepts such as WSDL, UDDI, WS-Security, WS-ReliableExchange.

As second part, Laurens goes into the different protocols and explains the basics: XML-RPC/JSON-RPC, SOAP and REST. XML-RPC and SOAP are both defined by Microsoft at first. Microsoft then threw XML-RPC away because it was too simple, and yet now XML-RPC is outperforming SOAP both in speed and in number of implementations.

XML-RPC has 8 set datatypes, and that's it. No custom data types are possible. The Blogger API is a good example of a standard API within XML-RPC. The implementation of the client and server can differ, as long as the same API is used, it will work.

SOAP is much more complex, and can also be used over protocols such as SMTP and FTP. The transport encoding is also XML, but it is much more complex because of all the options. This also makes it slow.

REST is completely different from XML-RPC and SOAP. REST does not need a seperate server, it just needs microformats to be implemented in the output. REST is stateless. The purpose is to get data. "The best example of REST is the Internet."

Defining web services in Joomla! 1.5 happens in a plugin, not in an extension. A plugin is located inside the framework, where an extension extends an application. The XML-RPC application is a default implementation, which exposes functionality available in plugins specifically written to be available.

Laurens also gives some nice examples of how to implement the client system, both in PHP and in Java. Laurens was instoppable, even after multiple reminders that he was over his time, he kept on doing things like demo-ing his very impressive <a href="http://www.joomlatools.org/">SiteMAN</a> application.
