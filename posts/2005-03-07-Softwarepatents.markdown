---
layout: post
title: "Software patents"
date: 2005-03-07 6:46
comments: true
categories: 
  - leftontheweb 
---
This can not be happening. Unbelievable. Even though there was massive resistance, the European council has accepted the <a href="http://wiki.ffii.org/Cons050307En" rel="tag">Softwarepatents</a> proposal.

By accepting this proposal, they have signed the death penalty for many small software companies, who may need to start paying high amounts of license money for using certain techniques or even work methods that are patented by big corporations.

I just made up my mind on what to vote when the elections are held here in the Netherlands on whether to accept or reject the European constitution...
