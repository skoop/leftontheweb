---
layout: post
title: "Wannahave"
date: 2005-07-18 14:49
comments: true
categories: 
  - leftontheweb 
---
Now <a href="http://www.artlebedev.com/portfolio/optimus/">this</a> is something I'd like to have. What a beautiful keyboard! And useful as well, being able to map the keys and not even that, also to map whatever the key says! Very cool! I'm sure it's going to be expensive, but if it's not *too* expensive I might actually get one.
