---
layout: post
title: "Questionable decisions @ php.net"
date: 2005-12-01 16:09
comments: true
categories: 
  - technology 
---
I was quite surprised when I saw the <a href="http://www.php.net/ChangeLog-5.php#5.1.1">Changelog</a> for the release of PHP 5.1.1. Especially one specific entry immediately caught my eye:

<em>Native date class is withdrawn to prevent namespace conflict with PEAR's date package.</em>

This is quite a questionable decision, if you ask me. They have altered the programming language to fit an application written in it. I'd think that, usually, you'd have to alter the application for changes in the programming language.
