---
layout: post
title: "Stress"
date: 2006-03-19 1:23
comments: true
categories: 
  - personal 
---
I've had stress before, obviously one will never do a development job where at one point or another, you get a bit stressed. However, the way it works within the company I currently work for, <a href="http://www.tomtom.com/">TomTom</a>, it is all new for me. It is not a short period, a few days, of pressure. The pressure has been on-going for the past few months, first leading up to Cebit obviously, and now towards release.

Last thursday we delivered the first release candidate of our latest project. Supposedly, this will lower the pressure for a bit. We'll see. I could do with some lower pressure. Having high pressure for weeks, months is quite harsh on your body, even though you don't do any physical work.
