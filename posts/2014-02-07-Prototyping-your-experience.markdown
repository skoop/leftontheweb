---
layout: post
title: "Prototyping Your Experience"
date: 2014-02-07 10:20
comments: true
categories: 
  - php 
  - prototyping 
  - frameworks 
  - tribalism
---
Whether in a professional environment or in private, prototyping (or making a proof of concept) is a useful tool. It is something people underestimate though. Frequently I hear people thinking the time spent on a prototype or proof of concept is a waste of time. **NO, IT ISN'T**

Just this morning, I was reading [this article](https://the-pastry-box-project.net/ed-finkler/2014-february-6) by [Ed Finkler](http://twitter.com/funkatron) on Tribalism and I was thinking about it. Ed describes tribalism in the tech-world, and I could totally understand what he was talking about. The fact that terms like "browser-war" and "framework-war" come by in our world. The fact that we always put Drupal against Joomla! against Wordpress is proof that we live in a tribal techworld. And as Ed writes, this is not necessarily a bad thing, but it usually comes with some negative side-effects. 

I've done projects at a lot of web agencies where the company had standardized on a single framework or a single CMS. And with "standardized", I mean, that was the only technology being used, and aside of perhaps some individual libraries, if some solution did not exist for that chosen technology, they'd build it themselves without looking outside of their tribe. I usually question such behaviour, and the response I regularly get is "we can't all know everything about every (framework|CMS)". Of course I agree with this: In the fast-moving world of technology we live in, you can't be an expert in every available technology. The thing is: You don't *need* to be an expert in every technology. You need to know just enough to know when it can be useful.

And so I get to prototyping. Prototyping (or making a proof of concept) is nothing new. It's definitely not limited to software either. Better yet, given the dynamic nature of software and the pretty static nature of other industries, it is even better suited for those other industries; imagine building an airplane without building a prototype first. You'll never know what's wrong with the design of your plane until it crashes with 250 people inside. 

Aside from building a prototype to testdrive your design and see if it is any good, you can use the prototyping approach also for your own good. An important thing to realize is that a prototype is not a production-ready product. It is something you build to prove to yourself that a concept works, or to try out one (or several different) approach(es). And once you're done, you can always decide to throw away the prototype and start all over. 

The thing is, you can use prototyping for the projects you do (and you should definitely consider this approach when you're not sure about a proposed solution), but you can also use prototyping for learning things. Actually, while prototyping, you usually learn anyway. But consider using it for learning a new technology for a change.

## LinkTuesday

Let's take a practical example. When Symfony2 came out, I was deep into symfony 1. However, Symfony2 was completely new and different from symfony 1, so I needed something to try it our and learn the concepts. I decided to build a very simple website that would aggregate all the links posted to twitter with the [#linktuesday-hashtag](https://twitter.com/search?q=%23linktuesday&src=typd). I started my first Symfony2-project and started playing around. Once done, I decided to [open source it](https://github.com/LinkTuesday/Linktuesday.com) and [publish the website](http://linktuesday.com/). But the most important thing of this project was: I learned the basics of Symfony2.

## Gowat.ch

A similar example is [gowat.ch](http://gowat.ch). I wanted to dive into the [Silex microframework](http://silex.sensiolabs.org/). It was a hip thing to do at that time to have your own custom URL shortener, and since a microframework is a good fit for that, I decided to write my own shortener with Silex. As with LinkTuesday, the end result was good enough for me to actually put into production. For me at least. But again, the most important thing was that I now know Silex and knew when to use and when not to use Silex.

## And it goes on...

You might be aware that I'm a big advocate for interoperability and using multiple frameworks in projects wherever it makes sense. I've done several talks on the subject. One of those talks I did with [Enrico Zimuel](http://www.zimuel.it/) of Zend Framework fame. We'd done a talk where we used some simple examples. Last year, we were going to do the talk again at the Forum PHP conference in Paris but we decided we weren't happy with the examples. They were clear, but did not contain practical use cases. So we sat down together to write a prototype of an application that would actually use both frameworks, or in this case it would use Symfony2 and Zend Frameworks Apigility project for RESTful APIs. It took us a day of playing around, but we ended up with a prototype that worked and showed exactly what we wanted: That it is possible (and easy) to use the Doctrine entities from a Symfony2 project to create a RESTful API with Apigility. In the process of doing so, Enrico learned some stuff about the Symfony2 bootstrap (hell, *I* learned some things about the Symfony2 bootstrap), and I learned the basics of Apigility. Enrico even shared this knowledge in [a blogpost](http://www.zimuel.it/create-api-symfony2-apigility/).

## Failure is success

The examples I've given so far are all examples of projects that actually made it to the outside world in one way or another. But there are many projects that I've started that either failed or that I just decided were not good enough. The thing with the prototyping approach is that failure is success. It's not about the end result of the project, it's about the process of building the prototype. It's about using a new technology, about finding out how that technology works, how to solve the problems you encounter with that technology. It's about gaining knowledge on which problems this technology solves, and which problems it doesn't solve. As I mentioned before: The goal is not to become an expert, but to know when you should and when you shouldn't use the technology, so that in a future project you can decide which technology you should use to solve the problem at hand. 

## The business side of things

Coming back to the agencies that said "we can't all know everything about every (framework|CMS)": Absolutely! However, you should be aware more of the world around you! Invest a couple of hours a week or even a couple of hours a month in working with a different technology so that you become more aware of other technologies. You don't have to completely switch to a different technology, you don't even have to master that technology, but just being aware that it exists is already a great asset for your company and your development team(s). You could even have different people look at different technologies and then present their findings to eachother. Or make it a competition: Have different team members build the same proof of concept with different technologies, and see which is the better solution. Enough possibilities to get more knowledge with a limited investment.

## Learning is key

We work in an ecosystem that is ever changing, always evolving. New technologies come in to replace old technologies or to work side by side with the technologies we all love. It may sound cliché, but the snoozers lose in this world. Standing still is not an option. We need to keep learning. Forever.
