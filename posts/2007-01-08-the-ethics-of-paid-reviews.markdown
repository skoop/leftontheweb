---
layout: post
title: "The ethics of paid reviews"
date: 2007-01-08 12:24
comments: true
categories: 
  - weblogging 
---
<a href="http://www.achievo.org/blog">Ivo Jansch</a> <a href="https://skoop.dev/article/307/reviewme-another-way-to-earn-money-with-your-website#c000646">responded</a> to <a href="https://skoop.dev/article/307/reviewme-another-way-to-earn-money-with-your-website">my previous post</a>, in which I discussed my small success using <a href="http://www.reviewme.com/">ReviewMe</a>, about the ethics of paid reviews. An interesting topic, which already prompted me to reply with the length of a regular post to Ivo's response. I've let my thoughts go a bit more on this topic.

Ethics. If there is any topic which is more subjective than anything else, it's probably ethics. Because what is ethical for one, can be highly unethical for someone else. And also, some people can handle certain questionably ethical situations better than others. Now I don't want to start praising myself to heaven, but in this case, I think I'm used to the situation a bit.

The question at hand is: Are paid reviews really reviews, or will the reviewer automatically be a bit biased by the payment (s)he receives for the review. As Ivo mentioned, ??a more than usual number of reviewers will feel 
