---
layout: post
title: "Holiday project: Better wifi and cabled network"
date: 2022-01-07 15:15:00
comments: true
categories: 
  - network
  - wifi
  - home
  - holidays
social:
    summary: My holiday project was to improve our home network. In this blogpost, I explain the things we encountered and what the eventual setup now is
    highlight_image: /_posts/images/network.png
    
---

Almost two years ago we moved to our current house. It was actually right at the start of the current pandemic, because we had some potential issues with moving because at the start it was unclear what the rules of the first lockdown were going to be. Luckily, moving was still an option.

In the old house we had set up our wifi network using a Ubiquity Amplify set with a base station and two mesh points. This worked really well because it was an old house so the wifi signal could pretty easily pass through floors and walls. The house was narrow but long, and we had a good wifi covering throughout the house.

The new house we moved into, however, had some challenges. It's a newly built house, very well isolated and with floor heating. It's great! However, having a set up with a mesh network that relies solely on a wifi signal being passed around quickly turned out to not be reliable. The signal was almost fully blocked by the heating and isolation material, causing great wifi on the ground floor but bad to now coverage on the first and second floor. 

I contacted Ricardo Wijthoef of [Tweeweg Internet](https://tweeweginternet.nl), who was also the man responsible for the wifi on our [WeCamp](https://weca.mp) island. A great person very knowledgable of how to set up wifi and cabled networks. We got together and made a plan of how the network would work, what equipment we needed and how to set it all up. We started with the first floor (since the ground floor had pretty OK coverage with the Ubiquity gear) and getting the cables up to the second floor. Once the day was over we agreed to set a new date soon to finish the rest.

Unfortunately, Ricardo got quite ill and after some time in the hospital an email landed in my inbox some time later with the announcement that Ricardo had passed away. While this was sad for our home network, it was even more sad for the fantastic person that Ricardo was. Always willing to help people, friendly and a funny guy. 

For quite some time after that, I could not be motivated to even think about doing the network without Ricardo, but last year I got to the point that I said to myself: Let's finish this thing.

## The setup

Because getting a good wifi single up two floors is almost impossible, the only solution is is to pull cables up. Since we already had a cable going from the ground floor to the second floor and the first floor was already fully functional, that meant I just needed to get the correct access points for the ground floor and the second floor. Ricardo had recommended [TP-Link Omada](https://www.tp-link.com/nl/business-networking/all-omada/) gear and since we already had installed an [EAP235-Wall](https://www.tp-link.com/nl/business-networking/omada-sdn-access-point/eap235-wall/) on our first floor, I ordered two more. Since these are powered by Power of Ethernet (PoE) I ordered a [TL-SH1005P](https://www.tp-link.com/nl/business-networking/unmanaged-switch/tl-sg1005p/) Switch. This would ensure all access points would have power. To have full control over the network and connect to the modem/router of my fiber provider, I got us a [TL-R605](https://www.tp-link.com/nl/business-networking/omada-sdn-router/er605/) Router.

Now there were two more challenges to solve:

* I wanted most devices in our living room to be connected by cable
* I did not want to have to configure all devices seperately

## Cabled connections

The EAP235-Wall has 3 ethernet ports, which is fantastic because it allows you to have good wifi as well as cabled connections. But our living room has more than 3 devices that I want to have cabled: Our TV, our settop box, our Playstation, our BlueSound and our AppleTV. So I got us an 8-port unmanaged [TL-SG108 switch](https://www.tp-link.com/nl/home-networking/gigabit-port-switch/tl-sg108/) that I connected to the ground floor EAP235-Wall. Problem solved. Once I realised how easy this was, I got another TL-SG108 for the office on the second floor. While wifi is good there, having ethernet there for our computers is even better and those switches are not that expensive. Excellent!

## Configure once

Now the other thing: I wanted an easy way to manage all these TP-Link devices. Luckily, TP-Link has an easy solution for that in the form of the [OC200 Hardware Controller](https://www.tp-link.com/nl/business-networking/omada-sdn-controller/oc200/), a small device powered by PoE that allows a central place to configure all devices using  the Omada Cloud environment. Configure once then simply roll out the configuration changes to all devices in your network. 

## Well, that was easy

After connecting everything the setup was a breeze. It was very easy to connect everything together and get the controller to recognize all the devices. I was a bit shocked at first that the default username/password combination for all devices was `admin`/`admin`, but when you log in to the devices you're forced to change that. Also, the controller will take over the control of all the devices anyway. So that was easily settled.

## Ka-ching

Now, what did all of this cost? A quick overview:

* 3 TP-Link EAP235-Wall access points: 70 euro each
* 1 TP-Link TL-R605 router: 60 euro
* 1 TP-Link OC200 hardware controller: 75 euro
* 2 TP-Link TL-SG108 8-port switches: 30 euro each
* 1 TP-Link TL-SG1005P 5-port PoE switch: 45 euro

That makes a total investment of 450 euro, plus the cables (but Ricardo was nice enough to just give those to us).

## Concluding

I am extremely happy with this setup. The Omada Cloud web interface and the Omada app are really easy to use and gives a lot of control over your network. The wifi connection throughout the house is now fantastic and having the devices that use a lot of data such as the TV, settop box, Playstation, BlueSound and AppleTV on a cabled connection has greatly improved the audio and video quality of those devices. The investment was really worth it. Plus, it was a nice hobby project over the holidays.