---
layout: post
title: "Symfony all around"
date: 2007-06-07 12:00
comments: true
categories: 
  - technology 
---
Last weekend was our PHP Bootcamp. It was a huge success! With about 30 people present, the whole room was filled with PHP enthousiasts. The three presentations by Thomas Weidner, Johan Janssens and Francois Zaninotto were awesome! All three deserve great respect for bringing these presentations. The panel discussion afterwards was a very cool thing as well, and brought many questions from the visitors of the event. All in all, a wonderful afternoon.

Afterwards, we had a BBQ which was also a big success. Everyone had nice conversations and discussions, and Johan even took out his laptop to show some stuff to people. All in all, a huge success and definitely something we'll be repeating.

Now, for me, it's time to finish my presentation for the <a href="http://www.phpconference.nl/">Dutch PHP Conference</a>. Quite exciting, as the conference is completely sold out so it will be crowded!
