---
layout: post
title: "Dutch Joomla!Day 1"
date: 2006-12-08 1:51
comments: true
categories: 
  - technology 
---
<strong>10:48</strong>: I'm sitting here in the lobby. First session has started but I'm not really interested, so I have some time to type the plan for today:

11.15 - 11.50: Joomla! Business Example (United Nations!)
11.50 - 12.30: How does Google use and stimulate Open Source (Google!)
13.30 - 15.00: Joomla! 1.5 Framework (Joomla!)

<strong>11:20</strong>: Philippe Chabot of the United Nations is having a presentation about the CMS for the <a href="http://www.unric.org/">United Nations Regional Information Center for Western Europe</a>. He explains the RIC organization, and why they chose Joomla. They looked at other CMS'es as well, and Vignette seemed nearly perfect but was too expensive. They even tried Microsoft Sharepoint Portal server, but he mentions, very politically correct, that it is "Microsoft Software, which says enough". Now, he talks about the choice between Open Source, Commercial and custom software. He mentions Open Source might be a little less secure, but much more flexible. Custom then was discarded, so the choice would be Open Source versus Commercial. This battle was won by Open Source mainly because of the hassle of support contracts with commercial organizations and products, where in Open Source this is open. The cost of the complete project of this website cost &euro;20.900.

Stats for the current site: 50.000 unique visitors, 80.000 visits. The project was successful enough to also start similar sites for other regions, such as Africa.

Some custom components were created, for instance so that interns can do content entry without having access to the backend. 

<strong>11:52</strong>: Leslie Hawthorn of Google starts her presentation. Google uses a lot of Open Source: Python, C/C++, Linux, Mysql, Subversion, etc. Gubuntu, which is supposed to be some Google Linux Distribution, is simply Ubuntu with some custom scripts for the Google Engineers to work on.

Google uses Open Source for independence, adaptability, secrecy (no licenses mean no one knows how many servers Google has), quality (more eyes mean more people that might find and fix bugs), time-savings and roots. <a href="http://code.google.com/">Code.google.com</a> is the place to be for information about this.

Not everything is released to Open Source, because of several reasons:
- Business reasons
- Regulatory reasons (export to for instance terrorism supporters)
- Other reasons (for instance code that is not useful anymore)

Google supports Open Source to keep competition alive, to reduce the importance of proprietary development on the desktop, to attract employees, as a pre-training (people who worked on Open Source projects will know how to work in a team), and of course it's good PR.

Engineers decide themselves if code goes open source. Little hierarchy means more decisions for engineers. Of course, extra checks are done such as legal checks, quality checks, etc etc. A project is then created on the Google Code website and the code is released.

Google supports Open Source also by donating money to Open Source projects, computer science research, hosting open source conferences & events, and they run the Summer of Code program.

<strong>15:34</strong>: The presentation of Johan Janssens is accompanied by a paper which is also available online, and once I have the link I'll post it here. This session will be not too technical, but a mix between business and technique. It will evolve around philosophy, architecture and framework.

Architecture of the 1.5 framework is based on DRY principles. 

The philosophy is "Joomla is the MacOS of the CMS systems". It's not to compare it with Mac, but that Joomla! has a few layers that are flexible and that not everyone will be exposed to every layer.

Joomla 1.5 framework is, as they say, fully UTF-8 compatible using some nice workarounds for the lack of UTF-8 support in PHP. They use a third-party library for this.

Joomla 1.0.* is basically one file with all the basic functionality: Joomla.php. Joomla 1.5 has a thought-out framework setup. 

Johan goes on to describe the Joomla! CMS and framework structure. Interesting discussions on PHP5, design patterns etc. take place while discussing the new framework setup.

Interesting things for the future: Versioning for content, not just a history, but some kind of branching. A simple workflow implementation. Actual user management done right with ACL.
