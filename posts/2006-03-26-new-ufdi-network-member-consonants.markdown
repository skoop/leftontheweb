---
layout: post
title: "New Ufdi Network member: Consonants"
date: 2006-03-26 11:06
comments: true
categories: 
  - technology 
---
I am proud to announce a new member to the <a href="http://www.ufdi.net/">Ufdi Network</a>! <a href="http://grkvlt.blogspot.com/">Consonants</a> is the weblog of andrew kennedy from the UK, where he writes amongst other things about Java, Unix, Security. Or, as he describes it himself:

<blockquote>the grkvlt irregular publishing mechanism - enterprise java, web development, information security, statistics and probability, gambling, book reviews and technology discussion, together at last!</blockquote>

Welcome to the network Andrew!
