---
layout: post
title: "Digital gaming: back to text-only"
date: 2005-04-21 9:56
comments: true
categories: 
  - leftontheweb 
---
When the Internet was young, or even before the Internet, when computers were young, most of the games you could play on the computer were text-only <acronym title="Role Playing Games">RPGs</acronym>. Soon, simple ASCII graphics took over. Then when the Internet was young, it was <acronym title="Multi User Dungeons">MUDs</acronym> all over. Multi-player textbased RPGs. Of course, soon, graphics got better, computer got more powerful, and everyone was aiming for graphics.

Now, a new game is on the horizon, using a recent technology: Wiki's. <a href="http://www.20by20room.com/2003/11/lexicon_an_rpg.html">Here's the description of Lexicon: An RPG</a>. It sounds like lots of fun. If anyone is interested in playing, I can easily set up a game and we can think of a theme to do this with.
