---
layout: post
title: "Protecting forums against spammers"
date: 2006-11-05 13:33
comments: true
categories: 
  - technology 
---
Spam is a big problem, and everyone knows it. Not just the spam you get in your e-mail, these days spammers also write bots to find discussion forums and post their spam there.

Quite some time ago the Support Team Leader of <a href="http://www.phpbb.com/">phpBB</a>, Techie-Micheal, had an idea about this. He started developing the idea in his head, and he gathered a team of people who shared his ideas. Last month, Micheal contacted me, to see if I was also interested in joining the team. After hearing what the project was about, I immediately said yes. There's some GREAT ideas here.

The project that I'm talking about is <a href="http://unavailable.bbprotection.net/">bbProtection</a>. The site is not yet up, but soon we hope to start with a public beta. I'm very enthousiastic about this project, as I think it has a great potential to block spammers from discussion forums.
