---
layout: post
title: "That little detail in converting services.xml to services.yml"
date: 2014-01-10 14:15
comments: true
categories: 
  - symfony2 
  - services.yml 
  - services.xml
---
Here's a little detail that got me frustrated for a while. In the project I'm currently working on, we've decided to standardize on Yaml for the service configuration over the default XML. Since we already had a couple of bundles developed with XML, I needed to convert those configuration files to adhere to our chosen standard. This took me longer that it needed to, and I couldn't figure out what the problem was.

The problem turned out to be a tiny little detail that you easily miss. I converted all the keys and values of the XML literally when I converted to Yaml. However, there is one key that needs a small change. When you use the repository-class in services.xml, this needs to be updated to repository_class in the Yaml version. Apparently, Yaml doesn't really like the '-' and so wants an underscore.

It took me quite a long time to figure it out. Now you know. Don't make the same mistake! :)
