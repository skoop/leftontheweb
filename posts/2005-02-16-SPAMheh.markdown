---
layout: post
title: "SPAM, heh."
date: 2005-02-16 3:47
comments: true
categories: 
  - leftontheweb 
---
Every once in a while, something really funny happens regarding to spam. Today, I got a funny SPAM mail. I have my <a href="http://www.mozilla.org/products/thunderbird" rel="tag">Thunderbird</a> configured to show plaintext mail if possible. So today I get an e-mail from someone I don't know with the following text:

<blockquote>I feel sorry for people who don't drink. When they wake up in the morning, that's as good as they're going to feel all day. -- Frank Sinatra</blockquote>

I am quite puzzled, but after inspecting the source of the message, I find that it is simply porn SPAM in the HTML part of the message. They just entered a random quote into the plaintext part of the message, probably in the hopes of that way not being stopped by SPAM filters. Thunderbird's SPAM filter indeed didn't catch it as SPAM. And it brought a smile to my face, getting an e-mail like this.
