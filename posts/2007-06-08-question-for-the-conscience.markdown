---
layout: post
title: "Question for the conscience"
date: 2007-06-08 0:47
comments: true
categories: 
  - technology 
---
Who is the better developer?

a) The developer that can do anything by head, will be able to work with any given API and build an application around it
b) The developer who will always be able to find libraries to do what he wants, and "glues" them together into an application
c) both
d) neither
