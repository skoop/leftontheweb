---
layout: post
title: "Speaking at PHPBarcelona"
date: 2009-10-25 14:29
comments: true
categories: 
  - symfony 
  - zendframework 
  - conference 
  - barcelona 
  - php 
---
<p>At the PHPBarcelona conference I&#39;ll be joined by such awesome speakers as Rasmus Lerdorf, Derick Rethans, Lorna Mitchell, Damien Seguy, David Zuelke, Jan Willem Eshuis, Sebastian Bergmann, Fabien Potencier and many more!</p><p>I will be doing my &quot;Integrating symfony and Zend Framework&quot; talk, which I recently did in Tilburg and Manchester. During this talk, I will show some examples of how to use Zend Framework in symfony projects, but also how to use symfony in Zend Framework projects. </p><p>Didn&#39;t have any plans yet for october 30 and 31st? It is still possible to <a href="http://phpconference.es/barcelona-php-conference-2009/registration/" target="_blank">register</a>  for only 50 euro for a two-day ticket or 30 euro for a single-day ticket! I hope to see you in Barcelona!</p>
