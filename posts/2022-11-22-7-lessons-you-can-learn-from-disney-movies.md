---
layout: post
title: "7 Lessons You Can Learn From Disney Movies"
date: 2022-11-22 14:50:00
comments: true
categories: 
  - php
  - symfonycon
  - lessons
  - disney
social:
    highlight_image: /_posts/images/disney-lessons.png
    summary: Disney movies contain lessons. They are life lessons, but they can also be applied to your work as a developer.
    
---

If you're like me and you enjoy Disney movies and Disney shows, you'll most likely have thought at some point while watching a movie:

> Gosh, this reminds me of the time at work when...

All the stuff Disney makes, whether that is Disney itself or Disney Pixar or any of their other ventures, it all contains a lot of lessons and emotions. And that translates well into real life.

Recently, I did this talk at SymfonyCon 2022, in which I shared 7 lessons you can learn from Disney movies, and I'd like to also share those lessons here. Because I know that you can probably relate, at least to some.

## Who are you?

When you start out as a developer you may have an idea of what you want to do or who you are. But along the way you'll have experiences and learn even more about who you are. Or perhaps who you are changes. Your interests may shift, your views on software development may shift, you'll learn new stuff.

During your whole career as a developer, it is important to communicate who you are and what you want. To your colleagues, to your superiors, to the people around you. What's your ambitions? Where do you want to go? What are your talents and what work do you find enjoyable.

I've seen so many developers that got stuck in their jobs and didn't really know how to show it. They tried to hide their true interests and talents because those didn't fit with their current job. 

In Frozen, I see a similar thing happening with Elsa, who hides her uniqueness. Why? Because of an accident she gets scared. She hides herself and instead of focussing on learning what she can do with her skills, she gets stuck. It is not until she comes to the realization that she can solve this problem herself that she learns to fully use her skills, her talents in the right way.

And this also goes for you. During your career, mistakes will happen, you may feel out of place. But instead of sucking it up and just continue the way you are going, you can also embrace who you are and what you like, and try to use that as a superpower. So find out who you are and what your superpowers are, and try to make them work for you instead of against you.

## You are not alone

I already mentioned before that talking to people is a good idea, so let's zoom in on that a bit. Because the communication with people around you makes you realize that you are not alone. There are quite a few support structures around you. Whether that is family, friends, colleagues, your manager, the HR department in your company, the company itself. You don't even have to have all of those available to you. Just one is enough.

And really, sometimes your support structure may disappear. You may lose your job. I hope it doesn't happen, but you may lose your family. Speaking of which, can you remember what happened in the Lion King. Simba losing his father in a quite dramatic way, then leaving. Thinking he is all alone. But he finds support. Rafiki mentors him. And of course his friends Timon and Pumbaa have his back. Of course he has to work hard to try and understand who he is and what he wants, but with a mentor and with the support of his friends, he can make great things happen.

And the same goes for you. Everyone needs a mentor. If I think back, I had a lot of mentors over the years. Not all of them were officially my mentor, but I've learned so much from family, friends and colleagues such as Marjolein, Tomas, Alex, Petra, Ivo, Mike and countless others. Sometimes you have to seek out help, and sometimes it just happens. But the most important message of all is: You are not alone. You are Simba and destined for greatness.

## What is your goal?

When you have the help of a mentor or the support of a friend, use it to set some goals. Because really, without goals, aren't you just going in a random direction? Why don't you just aim for infinity, and beyond? Oh wait, I think there was a Disney movie where that line was used. But that's not really the storyline I wanted to use.

What I wanted to look at though was Hercules. Do you know that movie? As a kid Hercules, son of gods Hera and Zeus, is stripped of his immortality. To get that back, he must become a true hero. And that makes him set out to reach that goal. 

Now I'm not saying you need to become a PHP hero, but what I want to point out was what setting this goal brought him: It gave him purpose, gave him something to reach for, something to fight for. And that also goes for us. In a world of constantly evolving technology, if we don't learn, don't struggle, don't make mistakes while reaching for the goals we set for ourselves, we fall behind. So on a regular basis, set some goals, evaluate your previous goals, don't worry about sometimes not reaching one goal. The journey towards that goal has brought you to where you are now, and where you are now you may be able to set new goals, so it hasn't been for nought. 

## Learn and persevere

And that's one thing to learn anyway. Nothing you do is for nought. Even if it doesn't bring you what you wanted, it may bring you unexpected other lessons. Remember Lightning McQueen from Cars? He makes a mistake that eventually makes him end up in Radiator Springs, where he has to appear in front of a judge and gets sentenced to community service. McQueen is desperate, because he has to make the final race of the Piston Cup in time, but as his time in Radiator Springs progresses he learns to appreciate the town as well as the people in it. He still wants to win the Piston Cup, however, so he practices for that outside of his community service hours. Eventually, he makes sure his community service tasks are done before going to the Piston Cup final. He doesn't win, but the only reason why is because of the sportsmanship he shows in helping one of the other competitors, because of his lessons in Radiator Springs.

For us as developers there's a similar story. Sometimes we make mistakes, sometimes we have to make detours. Sometimes we don't like where we are or where we are going, and we feel pressure or maybe even put pressure on ourselves to get something done. But if we learn to appreciate where we are, the people we inadvertently have around us, and if we keep practicing for where we want to go, we can do a lot and we can learn a lot. And even if something seems impossible, sometimes we need to persevere. We need to keep going, find creative solutions to "impossible" problems, we have to keep trying to get there. And we should not forget to think about the people around us.

And one thing Lightning McQueen learned was that competition is not all there is.

## Is it a competition?

Do you remember Monsters, Inc? A world of monsters that generate energy using the scared screams of human children. The monsters keep track of how much scream they catch and thereby energy they create, as a sort of competition. Sulley is at the top of the list constantly, but Randall wants to go beyond all boundaries to be the top monster. Which triggers all kinds of unwanted situations. Of course, everything is well at the end, but we can learn a big lesson here. In a company, you're a team and together you work towards goals. 

Unfortunately I have experienced several times that developers were very competitive within their own company or even their own team, going as far as hindering other developers in their work so that they could be regarded as the best developer. Well, let me tell you something: If you're not acting in the best interest of the project you're working on, you're not the best developer. The best developer is always the developer that helps their team, and helps the project move forward. 

## Take a chance

Earlier on I mentioned having a goal. And when you've set a goal, sometimes you come to the conclusion that it will not be easy to reach that goal. That you might have to take your chances, take some risk to get to that point. You have to be brave and maybe... bend the rules a bit? 

Remember Mulan? The woman who was adventurous and brave, much to the dismay of her very traditional family. She risks a lot by cutting her hair, pretending to be a man and signing up for the army which eventually leads her into battle. She's wounded and that triggers the outing of her secret. She's kicked out of the army but does not give up and eventually plays an essential role in saving the emperor.

Mulan took enormous risks and I can't really advice you to take risks as big as these, but our work sometimes does involve taking risks. For instance when using a new piece of technology that you don't have experience with yet, but you need something like that for the project you work on. And also in our careers sometimes you just have to give up one job for a chance of a better job. Sometimes you will fail, but failure is OK. Because failure teaches you, and you'll take that experience with you for the next time you face something risky. So, next time you're faced with a big risk, when you have to decide what to do, think of Mulan. Consider your options, and perhaps take a chance.

## Perhaps you need a different perspective

OK, one more lesson to finalize the 7 lessons I wanted to talk about, and for that there's actually 2 movies that I want to talk about.

Let's start with the oldest one so far: Lady and the Tramp. Lady grows up in a very protected and luxurious environment until an incident triggers her to run away and she befriends Tramp, a street dog. The difference between the two could not be bigger. Eventually Lady still believes that they are two different to be friends or even partners, but as the story progresses she learns that while Tramp has a completely different background, they indeed can be friends... and more.

And the other movie that I want to talk about is Up. Remember that one? Carl and Ellie dream of Paradise Falls and save money, but they end up having to spend that money they whole time. As Ellie falls ill and dies, Carl feels he failed on their quest to get to Paradise Falls. The epic scenes of his house flying away carried by balloons is epic, but it isn't until he finds Ellie's scrapbook with photos of their marriage and a note thanking Carl for the adventures they had that Carl realizes that he can have adventures still. He just needed to take a different look at his own situation, to understand that while they didn't make it to Paradise Falls, they still had some beautiful adventures together, and that adventures were still there for the taking. 

Both movies teach us an important lesson of perspective. Sometimes, based on your experiences so far, based on the things that you've learned or seen or done, you feel a certain way about a situation or a piece of code. And while moving forward based on that experience and those lessons you've learned is certainly not a bad thing (it's actually a big part of your work), it can be good to sometimes take a step back and try to look at it from a different point of view. Talk to your colleague. If you're a senior developer: talk to a junior developer. If you're a junior developer, talk to a senior developer. But talk to someone who will look at it from a different perspective. And then (re)consider if you're still on the right path, taking the right approach.

And it doesn't have to be made very big. Remember the rubberducking method? Where you explain a problem to a rubber duck as if the rubber duck would be able to help you with the problem. By having to think about how to explain the problem, you already think in a different way, or that can already help you solve the problem you're facing. By the way, that also works really well with an elePHPant.

## I like this!

That's great! I delivered the above information (plus a bit more) at SymfonyCon 2022 in the Marvel New York hotel in Disneyland Paris recently. If you missed it there, you'll have another opportunity, because I'll also deliver the talk as part of the [Ingewikkeld Sessions](https://ingewikkeld.dev/sessions/) on December 6th. Attending the live stream is free, but of course you [can support us on Patreon](https://patreon.com/ingewikkeld).