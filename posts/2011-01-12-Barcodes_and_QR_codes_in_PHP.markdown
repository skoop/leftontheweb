---
layout: post
title: "Barcodes and QR codes in PHP"
date: 2011-01-12 9:42
comments: true
categories: 
  - php 
  - symfony 
  - zendframework 
  - barcodes 
  - qrcodes 
  - libraries 
---
<h2>Barcodes first</h2>
<p>First, let's have a look at barcodes. For this project, I will be working on some shipping labels. Postal services can be very strict in their demands on the labels (and rightfully so), so it was a nice surprise to see that Zend_Barcode - aside from supporting the regular standards - has <a href="http://framework.zend.com/manual/en/zend.barcode.objects.html">support for several of the postal service standards</a>, like Identcode (used by German postal service), Postnet and Royal Mail. This will surely be really important for my project.</p>

<p>Using Zend_Barcode is really easy. I used <a href="https://github.com/skoop/Stefan-Koopmanschap-s-Toolbox/tree/master/sfZfAutoloader">my autoloader</a> to autoload the Zend library in my project. Obviously, I had to also include the dependencies of Zend_Barcode (Zend_Loader, Zend_Validate, Zend_Exception and Zend_Registry). After that, it's just setting up the right options based on the request, and outputting the barcode:<br />

<script src="https://gist.github.com/775971.js?file=barcode_example.php"></script>

As you can see, it's just a matter of fetching the right options from the request, setting the response content type, and calling the Zend_Barcode factory to create the barcode, then render it. It doesn't get any easier.</p>

<h2>Now for QR</h2>
<p>Now for QR I still had two options left. Usually, when I have two options left, and one is Google, I go for the Google solution. Why? Well, Google usually makes it really easy to use their API, is solid, and is always available. Oh, and *fast*. However, for this project, I decided against this. The main reason for this is that my application may end up running inside a hugely firewalled corporate network with very limited connectivity to the outside world. So I needed something stand-alone for this. The PHP QR Library seemed like a very decent solution and came recommended by people whom I trust a lot on this.</p>

<p>Using PHP QR is possible even simpler than using the Barcode, because it's a single standard so doesn't really have rendering options. So it's just passing along the information that needs to be in the QR code to the library and having it generate the graphic.<br />

<script src="https://gist.github.com/775974.js?file=qr_example.php"></script>

I'm loading the qrlib file here (which includes all the library files), setting the content type and then simply calling QRcode::png() to have the library output a PNG for the passed text. That's it!</p>
