---
layout: post
title: "Get rid of -m"
date: 2016-11-08 21:00:00
comments: true
categories: 
  - php 
  - git
  - commit
  - documentation
  - comment
  
---

I can't remember where I actually first read about this little developer's lifehack, but I've been using this for a while now and really like it: Not using the `-m` option anymore when commiting with git.

## What I used to do...

I use Git nearly exclusively on the command-line. My main reason for that is being burned by a version control integration of an IDE somewhere in the remote past. Since then I don't really trust any version control integration anymore and I want to have the full control over my version control tool of choice, these days mostly Git.

Committing changes had a certain never-changing process:

> git status

To check which files I've touched.

> git diff 

I'd diff each file separately to see if the changes in that file belong to the stuff I currently want to commit.

> git add

I'd add every file that belongs to the current change.

> git commit -m "<comment>"

Then I'd commit the staged changes with a small comment about what the change contains

The main problem with the above flow was that the -m promoted short and simple messages. And tempts a bit too much to write _bugfix_ or _just a little change in the controller_. It is just too tempting. Also, even though I'd just changed on which changes there are in the files I'm committing, I wouldn't have it all in my head anymore.

## Get rid of `-m`

Some time ago I'd read a tip from someone that said "just don't use `-m`. It took some time to process the usefulness of this tip in my head, but eventually I decided to try it. It works!

The last step in my flow now is:

> git commit

This opens up (in my case) vi, where I see a list of the staged files and I can type a commit message. Because I'm not on a command-line anymore but in an editor, I'm less tempted to write only a short commit message. Instead, I can describe what is in the changeset. And since I have the list of changed files there as well, it's easier to remember what I changed and for what reason.

It's a simple lifehack for a developer to write better commit messages, and it works (for me)!