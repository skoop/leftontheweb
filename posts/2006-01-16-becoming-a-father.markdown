---
layout: post
title: "Becoming a father"
date: 2006-01-16 15:17
comments: true
categories: 
  - meta 
---
On <a href="http://www.mcville.net/">mcville</a> I just published an article on becoming a father. I'm quite happy with the result of this article. There will be a second article, a sequel to the current article, just so you know :)

<a href="http://www.mcville.net/article/107/becoming-a-father">Read it here</a>
