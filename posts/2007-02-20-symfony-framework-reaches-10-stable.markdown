---
layout: post
title: "Symfony framework reaches 1.0 stable!"
date: 2007-02-20 3:57
comments: true
categories: 
  - technology 
---
They did it! Symfony framework has has released their <a href="http://www.symfony-project.com/weblog/2007/02/19/symfony-1-0-released.html">stable version 1.0</a>. A nice milestone, congrats to Francois and Fabien!

I've been very impressed with the framework from the moment I first checked it out, somewhere in october or november last year, and I've been working on developing several applications in Symfony since then. If you're looking for a framework for building your PHP application, then I'd highly suggest you check out <a href="http://ww.symfony-project.com/">Symfony</a>
