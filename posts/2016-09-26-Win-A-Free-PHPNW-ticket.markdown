---
layout: post
title: "Win a FREE PHPNW ticket"
date: 2016-09-26 20:00:00
comments: true
categories: 
  - php 
  - speaking
  - conferences
  - phpnw
  - win
  - raffle
  
---

I've said it before and I'll say it again: [PHPNW conference](http://conference.phpnw.org.uk/phpnw16/), every first weekend of October in Manchester, is the best PHP conference I've ever been to. I buy my ticket blindly as soon as they go on sale every year. I've been accepted to speak very regularly in previous years and then raffle off my ticket. I was not expecting to need to do that this year, since I did not get accepted to speak. I was looking forward to experience PHPNW as a delegate this year.

Things have changed though. Unfortunately one of the speakers had to cancel their speaking and I've been asked to be the replacement. Of course I said YES! Speaking at PHPNW is a fantastic experience, and I'll gladly help them in a situation like this. But this also means that I don't need my ticket anymore.

This means, however, that I have a spare ticket. As it has become a tradition by now to raffle my conference ticket once accepted as a speaker, I'm again raffling my PHPNW ticket. The ticket has full access to all events of the main conference, including the hackathon on Friday, the social on Saturday and the Sunday conference day. The only thing is: The conference is THIS WEEKEND!

So: If you want to win my ticket, the only thing you need to do is send me an e-mail: [stefan@ingewikkeld.net](mailto:stefan@ingewikkeld.net). Please only send an e-mail if you can actually make it this weekend. On Wednesday I'll do a random draw of the winner and send them an e-mail.

PHPNW will be awesome again. See you in Manchester!