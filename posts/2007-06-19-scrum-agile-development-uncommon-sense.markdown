---
layout: post
title: "Scrum Agile Development: Uncommon sense"
date: 2007-06-19 3:46
comments: true
categories: 
  - technology 
---
In the <a href="http://kw-agiledevelopment.blogspot.com/2007/06/scrum-agile-development-uncommon-sense.html">most recent article on the agile development weblog</a>, author Kelly Waters makes an excellent point: If you follow the Scrum principles, it will probably feel natural, because it is actually using common sense in development. It is very weird that most development teams don't actually work according to Scrum. And so, obviously, the common sense of most development teams is common and Scrum may need to be called Uncommon Sense to stand out.
