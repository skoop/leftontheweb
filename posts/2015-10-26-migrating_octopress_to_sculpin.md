---
layout: post
title: "Migrating Octopress to Sculpin"
date: 2015-10-26 18:30:00
comments: true
categories: 
  - php 
  - octopress
  - sculpin
---
Just over a week ago I wanted to [publish my previous blogpost](https://skoop.dev/blog/2015/10/23/Creating_a_Bolt_theme_from_a_template/), only to find out that the Gist plugin I was using for the code samples didn't work anymore. Some research showed me that the something changed in the way the Gist API worked that warranted an upgrade of the plugin. So I thought I'd quickly update the plugin to make it work again. Instead, I completely broke my Octopress install. Any attempts to revive it failed big time, partially because of my lack of knowledge regarding Ruby and the gem system.

I considered my options. I don't know much about Ruby or gem, so digging into that, while it would teach me something, would take quite a while. Then of course, I've done some work with [Bolt](http://bolt.cm/) recently, so I could migrate to that. That would still be quite a bit of work because Bolt would have to use a database. Given my [recent adventure with Sculpin](https://skoop.dev/blog/2015/06/05/I_Want_To_Support/) and the fact that Sculpin out of the box nearly supports the format Octopress is using, I decided to migrate to Sculpin instead.

The migrating to Sculpin was actually nearly painless. There were a couple of small issues I ran into, and I'm documenting them here for anyone else attempting to do the same thing.

### Twig code snippets

Since Sculpin is actually using Twig templating engine to generate the pages, having Twig code snippets in your blogposts is going to cause some issues. I solved that by adding the ***[verbatim](http://twig.sensiolabs.org/doc/tags/verbatim.html)*** Twig-tag around the code samples, inside the triple-backtick codeblock seperator. I did look into installing the [codeblock plugin](https://github.com/ramsey/sculpin-codeblock), but decided in the end that it was overkill for what I was trying to do.

### Categories

In Octopress I specified categories as a space-seperated list in my header. So it looked like this:

```
{% verbatim %}
categories: php octopress sculpin
{% endverbatim %}
```

Unfortunately Sculpin did not understand this syntax. Instead, it wanted me to give a yaml-like list:

```
{% verbatim %}
categories:
  - php
  - octopress
  - sculpin
{% endverbatim %}
```

Although this looked like a small change, because I have my full blog archive since 2004 on this site, I had to update all these. For a bit I considered automating this change, but I decided against that and simply went through all of the old articles to update this. Not the most fun job, but it actually made me realize the amount of stuff that is in here. It was fun reading back some of my old blogposts and realize how much I have learned since those days.

### There may be other problems

There may of course be other problems with the migration if you're using other options in Octopress that I wasn't using. And as you can see, at the time of writing this, I haven't actually applied a theme yet to the new Sculpin installation. This is something I still need to work on. For now: It works, I can blog again. Yay for Sculpin! 