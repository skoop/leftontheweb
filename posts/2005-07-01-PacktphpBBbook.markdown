---
layout: post
title: "Packt phpBB book"
date: 2005-07-01 5:24
comments: true
categories: 
---
Packt publishing, quite an interesting publisher with books very specific (open source) software, has released a <a href="http://www.packtpub.com/book/phpbb">book about phpBB</a>. Now that alone is worth a mention. I've had the honor of being a technical reviewer for the book. I received the book about two weeks ago from Packt, but I haven't really had time to do a check of the whole book. However, it does look quite good.

Of course, the coolest thing of the book is the credits page:

<txp:image id="4" />

It is sooooo cool to see your own name in print, in a book!
