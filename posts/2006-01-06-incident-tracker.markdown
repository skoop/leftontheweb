---
layout: post
title: "Incident Tracker"
date: 2006-01-06 14:33
comments: true
categories: 
  - phpBB 
---
In an on-going effort to keep a focus on security not just while coding but also in our support, earlier on we announced a special team dedicated to helping those who got hacked or otherwise affected by security problems.

This team has been active mainly on the background so far, but now we're also active on the foreground, thanks to the launch of our special <a href="http://www.phpbb.com/support/incidents/">Incident Tracker</a>. In this way, we can give some slightly more structured help for those who became victim of abuse of vulnerabilities, also in older versions of phpBB which are known to be vulnerable.

I definitely hope that this will give more people a better feeling about the security efforts of the whole phpBB team. It's not just the developers anymore that have a focus on security.
