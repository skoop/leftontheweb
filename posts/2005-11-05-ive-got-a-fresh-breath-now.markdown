---
layout: post
title: "I've got a fresh breath now!"
date: 2005-11-05 13:17
comments: true
categories: 
  - technology 
---
A month (or maybe two) or so ago a new hype was born on the Internet. A lot of weblogs started posting about <a href="http://www.haveamint.com/">Mint</a>, a new statistics package by <a href="http://www.shauninman.com/">Shaun Inman</a> that presented the gathered data in a nice and easy interface, using the buzzword technique AJAX. Aside from the clear interface, the good <acronym title="Application Programming Interface">API</acronym> for extending the system made it quite interesting for a lot of people. I decided to give it a shot.

<img src="/images/21.png" alt="Mint - screenshot" style="height:177px;width:333px" />

Probably the first information anyone with a website wants to know is: How many visitors do I have. Mint keeps track of this in a nice way. It shows the amount of visitors for several periods, being all-time, for the last day split out in periods of hours, for the past week split out in days, for the past month split out in weeks, and for the past year split out in months.

<img src="/images/22.png" alt="Mint - screenshot" style="height:172px;width:176px" />

Another important thing to know if how people found your site. Referrers are the way to find out about this, and of course Mint displays these. By default, it displays the 'newest unique', which means a page that no one came from before. In this view, it also mentions when the first visit from this page was. You can easily switch to view where you see the latest referrers, including those that people already came from in the past, and in this view you will also see when the visit from that page was. And then there's the repeat option, which shows the amount of visits from the same page. Quite useful information, and thanks to the AJAX list-switching, no reloading is necessary to view any of the lists of information.

<img src="/images/23.png" alt="Mint - screenshot" style="height:214px;width:119px" />

Mint will also help you identify the popular pages on your website. It will keep track of what pages are visited by your visitors, and show you a nice list of the which pages get the most views. A nice additional feature here is to mark a page as 'viewed', which will allow you to keep track of these in a seperate 'viewed' list. This is especially useful if you want to keep track of specific pages within your website. Aside from the popular pages, Mint also has a list for the Most Recent pages, which means which are viewed last, but this is, in my humble opinion, less useful.

<img src="/images/24.png" alt="Mint - screenshot" style="height:159px;width:119px" />

There are a lot of people that earn shitloads of money with Search Engine Optimization, which basically means they try to get your website high up in the ranks of the results of specific keywords. Knowing what keywords people search for they come to your site is quite interesting. And of course, Mint also shows that information. By default it lists the most popular search strings through which people visited your site. Here as well, there is a Most Recent list, which is less useful.

<img src="/images/25.png" alt="Mint - screenshot" style="height:213px;width:114px" />

Earlier, I already mentioned that Mint has an API for extensions. When you buy a license for Mint, you immediately get two extensions as well from Shaun Inman. One of these is the User Agent 007, which gives you insight in the browser, platform, screen resolution and flash version people use to visit your site. This is information very useful for designers who want to know what browsers people use to visit their site, but also of course for tech geeks that want to check out if most of their visitors use standards-compatible browsers ;)

<img src="/images/26.png" alt="Mint - screenshot" style="height:260px;width:200px" />

There are little preferences to set, there is little need for preferences in a statistics package. One of the nicest things you can do, however, is that you can switch the pane order (with panes being the blocks that show certain data). This is done in a very nice drag and drop AJAX interface. Nifty!

<img src="/images/27.png" alt="Mint - screenshot" style="height:141px;width:270px" />

And of course, there are the extensions. Installing extensions is really easy. It's as easy as uploading the files, and clicking the install button in the preferences screen!

Concluding, I really love Mint. Simple as that. It does cost a bit of money, but if you really want to get a good and easy insight into your visitors, then it's money well spent. And it's not like you're spending 100's of dollars to some multi-billion international. Instead, you spend a lousy 30 bucks and in the process support an independent programmer.
