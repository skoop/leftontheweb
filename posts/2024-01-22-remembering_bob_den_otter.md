---
layout: post
title: "Remembering Bob den Otter"
date: 2024-01-22 20:30:00
comments: true
categories: 
  - php
  - bobdenotter
  - boltcms
  - pivot
  - festivals
  - music
social:
    highlight_image: /_posts/images/bobdenotter.png
    highlight_image_credits:
        name: Marjolein van Elteren
        url: https://mastodon.social/@marjolein
    summary: Last week Bob den Otter unexpectedly passed away. I am extremely sad about this loss, and I want to remember him for the awesome stuff he did.
    
---

It was the age before Twitter (can you remember that?) that I started reading blogs (and eventually, writing blogs). One of my favorite Dutch blogs was [Mijn Kop Thee](https://mijnkopthee.nl/). That blog was the blog of Bob den Otter. 

Bob also developed Pivot (and later PivotX), blogging software that a big part of the Dutch blogging community (including myself for quite some time) used. And perhaps also internationally, I have no idea. What I did know is that even in Pivot, while it was fully file-based and no databases were harmed yet, the most important thing was that it was so well-developed. Usable. Intuitive. I loved it.

The first time I can remember meeting Bob was at a meetup I helped organize out of the Dutch PHP usergroup. It was in The Hague, Bob's hometown, and it was on frontend technologies. I have a habit of putting people that I look up to on pedestals and being "afraid" of meeting them, but Bob was so approachable, open and friendly. It felt like there was an immediate connection. 

Since then we have been in contact on a regular basis. I worked for his company [Two Kings](https://twokings.nl/) on a project once and learned a lot from him. About software development, but also about running a business. Bob hated "payment terms". So when you sent him an invoice, he would just pay it immediately. It's something I started doing since then as well. Because it makes absolutely no sense to wait for 30 days to make a payment. 

Another fond memory of Bob is the time when we sat down on Vlieland to work on the website of [Into The Great Wide Open](https://intothegreatwideopen.nl/). Two Kings builds the website of this festival and I volunteered to help with the site. Sitting there with the Two Kings crew working on building the new site was a fantastic experience. Bob built an amazing team, and with that team also built amazing websites.

I mentioned Pivot(X) before, but more recently Bob built and open sourced [Bolt CMS](https://boltcms.io). As before, I love it because it's so intuitive and easy to easy. Defining new data types simply in a configuration file and the system will automatically just use that, that's amazing. With it being based on [Symfony](https://symfony.com), it's also quite easy to extend or alter it's behaviour. I'm really going to miss Bob's vision on software development. He made things easier for everyone, disliked unneeded complexity and just wanted to get the job done well.

What I'm going to miss most though is Bob's passion. It didn't matter what subject. Festivals, music, artisinal web development, climate change, you name it. Every year we would find each other on Vlieland for the Into The Great Wide Open festival. We'd compare our planning (as you can see in the picture above), talk to each other about great new bands, or share a beer in the special beer bar on the festival site. 

Bob passed away on January 18, and I still can't believe it. I've re-read the message notifying me of his passing multiple times just to get that confirmation that indeed, it is true. I don't want to believe it. Without exaggerating, Bob was one of my favourite people on this planet. My heart goes out to his partner and family, to the team at Two Kings, and to all who knew him. 

Bob, wherever you are, thank you for all you did. You inspired me, and I will carry your lessons with me forever.