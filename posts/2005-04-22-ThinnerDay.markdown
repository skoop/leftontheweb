---
layout: post
title: "Thinner Day"
date: 2005-04-22 1:56
comments: true
categories: 
  - leftontheweb 
---
I hereby declare today <a href="http://www.thinnerism.com/">Thinner</a> Day. I'm going to load all the mp3's I have from the Thinner netlabel into a playlist and just play their music for one day. :)

And I suggest you do the same. Because the music released on Thinner is worth it. Bigtime.
