---
layout: post
title: "UTF-8 and MySQL"
date: 2006-09-15 6:08
comments: true
categories: 
  - technology 
---
I am still in the process of moving the sites from my old host to <a href="http://www.dreamhost.com/r.cgi?116006">DreamHost</a>. One of the sites is one I host for <a href="http://www.dreamlin.com/">a fellow artist</a>. After doing the regular MySQL dump, and loading this dump into the new database, all unicode characters were, to say it in proper english, fucked up.

Luckily, MySQL has some options to set the character encoding for the queries. It didn't completely solve the problem, but at least helped. I ended up using the following command to import the database:

<p style="font-family: courier, serif">mysql -usomethingorother -p <strong>--default-character-set=UTF8</strong> --host=databasehostIwantedtoimportto databasename < backup.sql</p>

This solved 99% of the problems. There were still some small problems with specific characters, but overall, everything worked fine. Long live MySQL :)
