---
layout: post
title: "Zend Studio for the whole company"
date: 2007-03-30 8:56
comments: true
categories: 
  - technology 
---
You can say what you want about its competitors, but to me Zend Studio is still THE application for PHP development. Its code highlighting, code completion, wonderful PHPDoc integration, and the great debugging and profiling features when combined with Zend Platform, are simply the best.

Today, we have arranged that all developers of Dutch Open Projects will be using Zend Studio. This is part of my effort to get more structure into our development process. Up until now, everyone was using different tools. Since we have developers using 3 different platforms, using a single tool is hard. Eclipse comes closest for this, but Eclipse, well, let's not talk about my opinion on that.

Now that all our developers will be using the same tool, it will be way more easy for them to help eachother out. Nothing is more annoying than hopping over to a coworkers workstation to find yourself lost in another editor than the one you're used to.

Thanks to the friendly folks at <a href="http://www.ibuildings.nl/">iBuildings</a>, that is a thing of the past :)
