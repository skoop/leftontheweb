---
layout: post
title: "Generating playlists"
date: 2005-02-25 3:49
comments: true
categories: 
  - leftontheweb 
---
When you rip most of your cd collection, you may walk against some trouble with <a href="http://www.xmms.org/" rel="tag">xmms</a> or other music players trying to generate a long playlist of all your music, especially when the music files are located on a network share. In my case, access to this networkshare is pretty slow as well due to the supposedly bad combination of NFS and wireless. Still, you'll want to generate playlists and the easiest way then is to simply load an existing <acronym title="playlist file format"><a href="http://en.wikipedia.org/wiki/M3U" rel="tag">m3u</a></acronym> file.

<a href="http://www.zerokspot.com/">zeroK</a> came to the rescue for me. It's quite easy to create those files on linux. You need a single, simple command:

<pre>find /mp3 -type f > playlist.m3u</pre>

Of course, you need to replace /mp3 with the path to your mp3's. But then you're done. A great and easy way to generate a playlist file of your music.
