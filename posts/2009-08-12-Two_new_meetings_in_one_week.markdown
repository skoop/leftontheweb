---
layout: post
title: "Two new meetings in one week!"
date: 2009-08-12 20:35
comments: true
categories: 
  - php 
  - netherlands 
  - belgium 
  - phpbenelux 
  - events 
---
<p>The <a href="http://www.phpbenelux.eu/nl/node/1178" target="_blank">first meeting</a>  is on august 26th in Houthalen in Belgium. It&#39;s the regular phpBenelux meeting for Belgium, which will take place at the offices of <a href="http://www.inventis.be/" target="_blank">Inventis</a>. There&#39;s two speaker slots, one of which is confirmed already and will be filled by <a href="http://andries.systray.be/blog/" target="_blank">Andries Seutens</a>. He will be talking about Design Patterns. Of course, there will also be room for networking and chatting, and some goodies will be given away! Plus, <a href="http://www.weble.be/" target="_blank">Weble</a>  promises to take care of food, so you&#39;ll be fed with input for both mind and body! So come and join us in Belgium, it promises to be an awesome evening.</p><p>And because we like the combination of mind and body that much, the dutch phpBenelux crew is organizing <a href="http://www.phpbenelux.eu/nl/phpbbq2009" target="_blank">PHP BBQ 2009</a>  on friday august 28th. During this event, which starts at 14.00, we will have <a href="http://www.harrieverveer.com/" target="_blank">Harrie Verveer</a>  speaking on PHP, Flash and Flex. There are also 3 20-minute spots for speakers from the community for which there is currently <a href="http://www.phpbenelux.eu/en/phpbbq2009_cfp" target="_blank">a small CfP</a>  open. So if you want to speak and have a good idea, please do submit your talk! After the talks, the BBQ will be heated up already and the eating and drinking can start. The PHP BBQ 2009 is held at the offices of <a href="http://www.phpbenelux.eu/" target="_blank">Dutch Open Projects</a>  in Leusden, who generously donated their location for this BBQ. Registration for this event is 15 euro to cover food and drink costs, unless we find some other generous sponsors to donate some money. Interested? Contact me for sponsoring options! Interesting in attending? <a href="http://www.phpbenelux.eu/en/aanmelding_bbq2009" target="_blank">Register here</a>! </p>
