---
layout: post
title: "May is conference month!"
date: 2011-04-29 7:44
comments: true
categories: 
  - php 
  - conferences 
  - dpc11 
  - ipc11se 
  - phpday 
---
<h2>PHPDay</h2>
<p>The first conference I'll be attending in May is <a href="http://phpday.it/">PHPDay</a> in Verona, Italy. The conference takes place on May 12 to May 14, though I won't arriving until the 13th. The schedule looks amazing, with workshops by Fabien Potencier, Sebastian Bergmann and Paul Borgermans and talks by (amongst others) Kore Nordmann, Tobias Schlitt, Christian Schaefer, David Zuelke, Thijs Feryn, Daniel Londero, Juozas Kaziukenas, Lorenzo Alberton and many more. My talk at PHPDay will be about Git. The talk, Git for Subversion Users, is an introductory talk into the Git distributed VCS, specifically for people who already have experience with Subversion.</p>

<h2>Dutch PHP Conference</h2>
<p>Then it's time for something a little closer to home. From May 19 to May 21, it's time for <a href="http://phpconference.nl/">Dutch PHP Conference</a> in Amsterdam. This traditionally very high profile conference is the conference that I'm not speaking at this year. But just like last year, I will be hosting the unconference at DPC on the two conference days. I am very excited to be hosting the uncon again, as last year I was impressed with the quality and variety in topics that the uncon offered. If you are coming to Dutch PHP Conference (and you should!), come prepared with something to register into the uncon. More information on the uncon is available <a href="http://www.phpconference.nl/uncon/">here</a> and <a href="http://www.phpconference.nl/home/update-on-dpc-uncon/">here</a>.</p>

<h2>International PHP Conference</h2>
<p>The last conference in May for me is the International PHP Conference, which takes place from May 29 to June 1st in Berlin. It has always been fun to go to the International PHP Conference, and this year I'm even doing a workshop! On Sunday 29th, I'm doing an afternoon workshop on Git for Subversion Users. This workshop is similar to the talk I'm doing at PHPDay, but more extensive and also more practical. During this workshop, people will actually be doing some work with Git themselves. Then on Monday, I'm going to go deeper into Git with my talk, where I'll go into rewriting history with Git, managing Git and Git workflows, reasons for (not) using services like Github and Unfuddle and such. This will be the first time I'm doing this talk and I'm quite excited to be doing it. It's a great topic, and I hope it will be useful to people attending.</p>

<p>If you're in the neighbourhood of one of these conferences, I would highly recommend coming by. I hope to see you at one of these conferences!</p>

