---
layout: post
title: "The adoption of PHP ... 6?"
date: 2007-01-17 0:41
comments: true
categories: 
  - technology 
---
As I <a href="https://skoop.dev/article/302/php5-adoption">wrote</a> <a href="https://skoop.dev/article/309/another-proof-of-little-php5-adoption">before</a> the adoption of PHP5, so long after it has been released is still quite low, I was surprised by <a href="http://blog.agoraproduction.com/index.php?/archives/19-Installing-PHP6-For-beginners.html">David's HOWTO on installing PHP6</a>. I know, this one is aimed at those more tech-savvy people who want to fiddle around, but I truely feel that the PHP Group should focus first on promotion PHP5 more. I don't think a lot of people will adopt PHP6 while they're still working on PHP4.
