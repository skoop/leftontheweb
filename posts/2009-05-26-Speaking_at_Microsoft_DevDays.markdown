---
layout: post
title: "Speaking at Microsoft DevDays"
date: 2009-05-26 20:34
comments: true
categories: 
  - microsoft 
  - devdays 
  - conference 
  - php 
  - phpbenelux 
---
<p>In the past years, many of the big companies have been moving into the PHP space. One of the last but ever since starting the movement also one of the most active is in the PHP space is Microsoft. In less than 2 years time, Microsoft has been moving from the big evil closed-source empire into a company that is seriously dedicating time, money and effort into supporting PHP as one of the most important languages for the platform. By doing this, they&#39;ve not just enabled PHP developers to run their development environments on Windows, but are slowly becoming a more and more favorable platform for deploying PHP applications to. Now, it will be a long time (if ever!) before they become a very serious competitor for linux on the web, but especially in big enterprise organisations where Windows servers are plenty, this opens up serious possibilities for PHP.</p><p>One of the many proofs of Microsoft&#39;s openness to the PHP language is their addition of a 4-talk PHP track to their DevDays conference, a conference which up until now was limited to Microsoft technologies. Now, PHP is one of the technologies represented their, amongst Silverlight, Azure, ASP.NET, C#, Sharepoint and many other topics. </p><p>My talk is titled <strong>5 essential tools for the Windows PHP developer</strong> and will go into the tools that you (may) need to become a serious PHP developer using Windows. The intended audience is people who are coming into PHP (either starting out, or from other technologies) and will give a good introduction into the tools that you need for serious PHP development, ranging from stack installers to IDEs, frameworks and other tools, all with a clear focus on tools that run on Windows of course.</p><p>I am quite excited about speaking at the DevDays (or even being there for that matter). Ever since speaking/attending the 4developers conference in Poland, I&#39;ve come to understand the power of cross-language conferences, because all different languages are usually islands of their own, yet conferences like these allow cross-pollination between the languages.&nbsp;</p><p>Aside from the PHP track, the <a href="http://www.phpbenelux.eu/" target="_blank">phpBenelux</a>  usergroup will also have a stand at the conference, so feel free to come and say hi when you&#39;re at DevDays! </p>
