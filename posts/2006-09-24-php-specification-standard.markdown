---
layout: post
title: "PHP Specification Standard"
date: 2006-09-24 3:59
comments: true
categories: 
  - technology 
---
Standardization. This is one of the things where PHP still falls behind, big time. In a <a href="http://framework.zend.com/wiki/spaces/viewthread.action?key=ZFMLGEN&id=4958">recent thread on the Zend Framework general mailinglist</a>, this topic was touched in an informal proposal to get a module in the framework for Portlets.

In this thread, a reference was made to a <a href="http://jcp.org/en/jsr/detail?id=168">Java specification (JSR)</a>. This is something I really miss in PHP. There is no standard for specification of API's in PHP. This is something we really need! Having standard specifications for API's will really make PHP much better:

<li> There will be set standards that the whole PHP world can use</li>
<li> The community will be creating these standards</li>
<li> Having clear standards will ensure a better adoption in the enterprise world</li>

Now, the question is: who should attempt to start such a system. Obviously writing the specifications is a community thing, but to really get one central specification system up and running, I think a central, credible party should set this up. There's two parties that immediately come to mind:

<li>The PHP Group</li>
<li>Zend</li>

Since lately, Zend has been really active, and is extending it's contacts to include companies like IBM, I think Zend should be the first choice for this. 
