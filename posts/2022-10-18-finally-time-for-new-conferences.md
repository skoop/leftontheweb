---
layout: post
title: "Finally, time for new conferences"
date: 2022-10-18 16:38:00
comments: true
categories: 
  - php
  - conferences
  - sylius
  - syliuscon
  - symfony
  - symfonycon
social:
    highlight_image: /_posts/images/syliuscon2022.png
    summary: Finally we can do some conferences again. PHPDay in May was already a great experience, and in the upcoming months I have two more conferences scheduled as speaker: SyliusCon and SymfonyCon.
    
---

Over 2 years of no conferences was nice when it started, but eventually started to wear on me. I had the luck of being invited to speak at the [PHP Ruhr conference](https://talk.bits.ruhr/event/b49cbfdf-1838-4308-bdc5-9008bed20fd7) in November of last year, which was still awkward because clearly the pandemic was not over yet and there were quite some measures still active. I'm not complaining, they were definitely necessary, but that made the conference quite awkward. The awesome location (the Dortmund stadium) made up for much though. Wow!

Then, May 2022, finally a [PHPDay](https://2022.phpday.it) in-person event again. It reminded me why PHPDay is currently my favorite conference: An excellent location in a beautiful city with nice people and a stellar line-up. OK, and me as well. There were still some active measures but already less, and it felt more normal. And I found out I sorely missed the gathering of peers, talking to old friends and making new friends.

But just one conference in the first half year still felt weird. 

Luckily, I'm going to be making up for that by speaking at two great conferences in the next month. There'll be a bit of overlap in which old friends will be there, but I also look forward to meeting new friends.

## SyliusCon 2022

Over the years I've been involved in a couple of e-commerce projects using [Sylius](https://sylius.com) and it has been a blast. I can not generalize because there's enough pieces of e-commerce software that I have not used yet, but out of all the software I have worked with, Sylius is definitely my favorite. As such, I'm really happy that I got invited to Poland to speak at [SyliusCon](https://sylius.com/conference). My talk will not be about Sylius. Instead, I'll be looking at development and comparing it to situations from Gordon Ramsay's Kitchen Nightmares. Because believe it or not, developers can learn a lot from chef Ramsay.

SyliusCon is next week, October 27. [There are still tickets](https://store.sylius.com/buy-product/syliuscon_ticket)!

## SymfonyCon 2022

Then in November, it's off to Disneyland Paris for [SymfonyCon](https://live.symfony.com/2022-paris-con/). While I work with many frameworks in my job, [Symfony](https://symfony.com/) has been my favorite for years now and I don't foresee that changing any time soon. As such, I am quite happy to be accepted again to speak at SymfonyCon, especially now that it takes place in one of my favorite locations: Disneyland Paris. It is going to be awesome.

At SymfonyCon I'm going a brand new talk, but similar to the one I do at SyliusCon. Instead of looking at chef Ramsay, I'm talking a deep look at Disney movies to see what we as developers can learn from those. I will share 7 lessons I learned from watching Disney movies. See you there?

For SymfonyCon, [get your ticket here](https://live.symfony.com/2022-paris-con/registration/). 