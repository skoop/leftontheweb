---
layout: post
title: "My Tools: Google Calendar"
date: 2011-06-09 14:31
comments: true
categories: 
  - tools 
  - calendar 
  - googlecalendar 
  - google 
---
<h2>The web interface</h2>
<p>Google's <a href="http://calendar.google.com/">web interface</a> is already really useful. It allows me to have a quick overview of my week or even month. What I really like about this is that I can also have external public calendars added to that view. For me, these would be things like Dutch holidays (since I have nothing with religion and most holidays are based on religion, I sometimes don't know about them until the last moment) but also the matches of FC Utrecht, my favorite football (soccer for you Americans) team. Additionally, I can also see the appointments my wife makes because she shared her personal calendar with me. This ensures that we don't both make an appointment at the same time, which would cause issues because there would be no one at home for the kids ;)</p>

<h2>Integration</h2>
<p>The most important reason for choosing Google Calendar over any other solution though is the tight integration with other tools. Since I run a Mac, I want it to integrate with the Calendar app on OSX, which Google does perfectly well. And additionally, I want it to integrate with my iPhone, which again works perfect. The only thing I've not been able to do (but I must admit I haven't really made a big effort here so far) is to have it integrate with the calendar application in my Windows Phone 7 phone. I definitely need to look into getting that to work. Additionally (but again, I haven't looked into this yet), I would want it to integrate with the CRM solution that I soon need to set up. If anyone has tips on which (preferably free and open source) CRM solution has a good Google Calendar integration, I'd like to hear from you!</p>

<h2>Issues</h2>
<p>With me using the calendar on so many different platforms, Google is my central point for all my appointments. And since I basically live off of my calendar, I need it to work, and I need it to work well. So far, in the years that I've been using Google Calendar, I've only once had issues with it where appointments I had disappeared, effectively making me miss some. Obviously, this is rather annoying, but given I've been using Google Calendar for quite some years already, I still have big trust in it.</p>
