---
layout: post
title: "Namespaces in PHP"
date: 2007-12-16 13:54
comments: true
categories: 
  - technology 
---
<p>OK, let me be clear. I don&#39;t think that namespaces will add a lot in terms of functionality, usefulness while programming itself. However, apart from being able to develop your applications using useful functions, you also need to be able to manage your code. And this is where namespaces come in.</p><p>Using namespaces, one will be able to easily group (or &quot;package&quot;) together related pieces of code. It will also ensure that when you&#39;re using different pieces of different libraries you did not develop yourself, that they won&#39;t clash anymore due to similar namings.</p><p>So, even though for day to day code typing it won&#39;t change a lot, it will definitely change a lot for technical project and codebase management. Important enough for me and anyone else doing professional PHP development.&nbsp;</p>
