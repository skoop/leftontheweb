---
layout: post
title: "new Laidback Electronica"
date: 2005-04-15 4:14
comments: true
categories: 
  - leftontheweb 
---
Yesterday was the day of release of two beautiful new EP's on my netlabel <a href="http://www.laidbackelectronica.com/" rel="tag">Laidback Electronica</a>. This time it was the ambient duo Ambient Ashand who brought 4 chilling ambient tracks, and german minimal techno-meister Lomov whose Unterflughydrant is a very inspiring release to dream away to. Both releases are available for free download.
