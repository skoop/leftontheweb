---
layout: post
title: "Some thoughts on recruiting"
date: 2014-03-09 11:00
comments: true
categories: 
  - php 
  - recruiting
---
Recruiters. Many developers hate their guts, and (in most cases) rightfully so. They usually have no clue about technology or about how to communicate with developers. It is also pretty clear most of the times that they're only out for quick wins. I've seen recruiters call a developer they'd placed in a company exactly 6 months after he started to see if he wanted to move on, because 6 months was the period in the contract between the employer and the recruiter. I've seen recruiters tell candidates that the employer wasn't interested, just because the employer didn't want to pay the extremely high recruiter-fee. Hell, my first professional PHP job was like that. I ended up having direct contact with the employer because I didn't believe the story the recruiter was telling me, and landed a pretty cool project. Some of the people I worked with there are still friends.

There is a flipside though: There are recruiters out there that do understand how things work. They know how developers work, communicate, what they feel is important. They are rare, but they do exist. So it would be stupid to just say "don't work with recruiters" to companies or to developers. The thing with working with recruiters is: It takes time. You can't just give a recruiter a profile and wait until they get you developers, or tell them you're looking for a job and wait for the interviews. You have to invest in the relationship with the recruiter, do some research, get to know them, and ensure you've found one of the good ones.

## The role of a recruiter

Many companies I've worked at, with or been in touch with expect a recruiter to look at their huge list of available developers (here's your first mistake, because how many developers are actually available?) and give you the best developers based on a (usually incomplete) profile. But what exactly is it that a recruiter does?

Most recruiters have very little technical knowledge. They're networkers, they create a network amongst developers and companies and try to link one with the other when they get a request. So from the majority of the recruiters, you should not expect that they make a judgement on the seniority of the developer or his skillset.

In an ideal world, the recruiter you work with actually has a development background and keeps his/her development knowledge up to acceptable levels. Ideally, the recruiter should be able to judge the skills and level of any developer they send your way.

## Think about your reasons for hiring a recruiter

Before even contacting a recruiter, you need to ensure you're doing this for the right reasons. Many companies simply expect the wrong things from a recruiter. As I mentioned before, you can't just give them a profile and wait for the developers to come in.

Simply outsourcing getting potential candidates should not be your only reason hiring a recruiter, at least not if you want to save yourselves some time. The time you save by getting in the candidates, you will most probably spend doing interviews with candidates that do not actually fit your profile. The amount of times I (specialized in PHP with very little experience with other languages) got approached by a recruiter for a Java or Python-project is beyond belief. Getting 10 CV's from a recruiter is not a good thing. It's a bad thing. Instead, you'd want to get one or two CV's of developers that actually are what you look for.

Even if you have a good recruiter, don't expect to save much time on the recruitment process. Even if they immediately pass you the right CV's, you'll still need to make the final judgement on whether the developers are good or not, and of course convince them to work for you. You'll still need your own HR-people to handle everything.

## Take back your own recruitment

It may be worth your while to consider taking the recruitment back into your own organization. The most important task for a recruiter is to find the right developers for your organization. This may seems like a task that takes a lot of time, but it is easily combined with other stuff you already do (or should do).

The best representatives of your company when it comes to finding developers are your developers. They can speak to potential developers on the same level, can explain what the challenges are and what the fun aspects are of working for your company. They are also the best people to try and find out if a potential developer would fit your company, both in skills and in personality.

So if you have a position for a new developer, send your existing developers to usergroup meetings and conferences. Let them get in touch with other developers, potential employees. Don't tell them to go out and find people, but tell them to keep their ears and eyes open. They should not try and force the "want to work for us?"-conversation on people, but once the topic comes up, they can of course start talking about it. And they should. The best advertisement for your company is your developers who passionately speak about their work at your company.

## Invest in finding the right recruiter

There are many reasons for not putting the recruitment task on your developers. One of them would be the fact that the job opening is there because the developers already have way too much work.

Whatever your reason is for wanting to hire a recruiter, you need to invest in finding the right one. When you hire the wrong one, there's many things that can go wrong:

* The recruiter might start spamming his whole database. This reflects badly on your company
* The recruiter might come up with completely unfitting developers, spending much of your time on nothing
* The recruiter may not represent your company well, leading to expectations with your potential developer that you can't fulfill.

If you think it through, there may be even more reasons that can go wrong when you've got the wrong recruiter. So invest time in finding the right one: Meet with several recruiters, don't just hire them based on a single phonecall. Meet with them in person and try to find out how they work: How do they find the right developers? Do they just mass-mail, or do they select the right people from their database and approach those people? How do they find out what the level is of a developer?

Another good idea is to have one of your developers talk to the recruiters. Developers are pretty sensitive about recruiters so they usually have a pretty good idea about what type of recruiter they have in front of them after only a short meeting with the recruiter. Learn to trust this gut feeling. It is more powerful than you might think.

Do some research. Bad recruiters are easy to spot. They are the ones that have a lot of job openings on LinkedIn, preferably in the discussions-area of LinkedIn Groups instead of the Jobs area. They may try to start their own PHP usergroup in a city where a popular PHP usergroup already exists, just to lure developers to them. Their names get mentioned on Twitter and Facebook by developers who get easily annoyed by them. There are enough ways to spot the really bad ones. Also, since good recruiters are rare, they are often recommended by developers. You'll find good recruiters actively participating in the PHP community, their company names may be listed as sponsors for usergroups and conferences and they'll actually be at conferences and talk to people.

## Concluding

There is nothing wrong with the concept of recruiters. There are a lot of really bad recruiters though, and you'll want to avoid them. First of all, decide whether you need recruiters in the first place. And once you decide to use recruiters, invest some time in finding the right recruiters.

**Update**: After some feedback from Phil Sturgeon and Ivo Jansch, I've slightly updated the wording of this article.
