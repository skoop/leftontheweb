---
layout: post
title: "Using helpers in actions (Symfony)"
date: 2006-11-30 6:48
comments: true
categories: 
  - technology 
---
We recently had a weird situation here at work where we needed to use the Url helper of Symfony inside the actions.class.php of a module. This helper is available in the view, but not in the actions class by default. 

Of course, we can include this, and this was my first approach. Simply using:

<pre>include_once('symfony/helper/UrlHelper.php');</pre>

worked fine for me. However, when my co-worker updated his code from Subversion, it went BOOM. He got a rather annoying FATAL error about not being able to redeclare a function. It's weird, because it worked in my situation.

The solution is simple though, and was available in the <a href="http://www.symfony-project.com/trac">Symfony trac</a>. Ticket <a href="http://www.symfony-project.com/trac/ticket/899">#899</a> contains the solution, which is more simple than you could imagine:

<pre>sfLoader::loadHelpers(array('Url'));</pre>

Simple!
