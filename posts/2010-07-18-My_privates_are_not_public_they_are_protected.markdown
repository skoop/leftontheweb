---
layout: post
title: "My privates are not public, they are protected"
date: 2010-07-18 19:28
comments: true
categories: 
  - php 
  - symfony 
  - Symfony2 
  - accessmodifiers 
  - protected 
  - private 
  - public 
---
<p>In general, I agree with Lukas. I dislike the way private methods block any possible extension. I much more prefer being able to extend something if a piece of code does not support the use case I have. I agree with pro-private people that it is important to have a good API design and to use that to protect less experienced developers from making mistakes, however one should never assume that the developers using your libraries, especially Open Source libraries, are less than yourself. You as developer of a library (especially Open Source), will never think of every use case possible. People may use your code in ways you have never thought of, and they may still be using it in a very valid use case. So they may also run into problems if you make certain functionality private: this will block them from extending your library and in some situations that may block them from actually using your library in valid use cases!</p>

<p>I definitely am not in favor of simply opening up the complete library to every developer though. By making a clear decision on which methods are public and which methods are protected you will ensure that people simply implementing your library will use the API that you have taken the time designing. And in most cases, this will be enough for those developers, and they will be happy users of your code. However, those that wish to extend your functionality are still able to do so. And yes, this will also open up your library to completely invalid use cases for extending, but this is simply not your problem. You have taken the time and put in the effort to design the API to work in the most common use cases and you have kept the option of extending it for other valid use cases, but there will always be developers that abuse the power you've given them.</p>

<p>I see places though where private (and also final) are valid: When you're working on a specific implementation for a customer, you may want to use private to hide specific functionality for that project, or final to block people from overriding the method even if they may call it from subclasses. In this situation, it actually makes a lot of sense, because you will often have architects or seniors building the basis of a project, and have mediors and juniors finishing the project. And in this case, it *is* the responsibility of the senior/architect when power offered by code is abused by less experienced developers. So unless the senior or architect conciously makes the decision to open up this power, it is smarter to not offer it to the implementing developers. </p>

<p>I want to close down with the only reason I can see at this point where private would make sense in Open Source projects. And this is not really from a technical point of view, but more from the community side of things: When you make methods private or final, people who have an alternate use case will have to contribute their patch to the project if (s)he wants it to be changed in the main library (which in most situations would be preferable for those developers I guess). But I don't think the community point of view should rule above the technical reasons for not using private.</p>
