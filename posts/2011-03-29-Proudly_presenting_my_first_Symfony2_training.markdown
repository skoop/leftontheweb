---
layout: post
title: "Proudly presenting my first Symfony2 training"
date: 2011-03-29 10:12
comments: true
categories: 
  - symfony 
  - Symfony2 
  - php 
  - training 
  - ingewikkeld 
  - dpc11 
---
<p>This first session is <a href="http://stefankoopmanschap.com/getting_started_symfony2_may2011">Getting Started With Symfony2</a>, the beginner-level training as developed by Sensio. It will get you up and running with Symfony2 in two days. This is a great training for people who have never used symfony, or for people who have experience with symfony 1.*, but want to get a good headstart on Symfony2. It does assume you have a good experience with PHP5 and object oriented development with PHP5.2 and (to a certain extent) PHP5.3. The training session will take place on the two days before the Dutch PHP Conference, May 17 and 18.</p>

<p>To celebrate this first official Sensio training I'm doing in The Netherlands, I've arranged for a very special deal: Participants in this Symfony2 training will receive a free conference ticket to the Dutch PHP Conference! And with a discount, you can also purchase access to the DPC tutorial day if you wish to attend that one as well! Important to note though: This special offer is only valid at the minimum of 5 participants for the training session.</p>

<p>A similar session as this one is also being loosely scheduled for August or September, but no date or location has been set for this one yet. If you are interested in this one or <a href="http://trainings.sensiolabs.com/en/catalog/symfony">any other symfony/Symfony2 training</a>, please do get in touch. I can either do the training sessions on-site at your company, or if there is enough interest, schedule a classroom training like the one I'm doing in May.</p>
