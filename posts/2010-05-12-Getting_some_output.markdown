---
layout: post
title: "Getting some output"
date: 2010-05-12 21:02
comments: true
categories: 
  - winphp 
  - php 
  - mediterra 
  - symfony 
  - phpazure 
---
<p>First of all, I didn&#39;t want to reinvent the wheel for some basic stuff. So I did some research and ended up using several libraries:</p><p>&nbsp;</p><ul><li>phpAzure: A library for connecting to Windows Azure services</li><li>symfony request handler: Handling requests</li><li>symfony response handler: Handling responses</li></ul><p>&nbsp;</p><p>Using this, I&#39;ve created a basic system that allows me to easily handle the request with a single front controller, delegate the actual execution to a controller, then pass back (a decorated version of) the content the controller returns to the client. Since that is pretty much what a single call to the application should do, this means I have the basics covered.</p><p>After having the basic setup up and running, it was time to configure IIS to actually start serving content. I was happily surprised that, because I was using the Web Platform Installer, PHP was already fully configured and the only thing I needed to do was change the document root on the default &quot;site&quot; (which is the IIS term for Apache&#39;s VirtualHost) to my local clone of MediTerra, and restart the site. Pointing my browser to &quot;localhost&quot; then did the trick! Now that was easy!</p><p>Of course, the app still doesn&#39;t actually do something so my next task will be to start implementing actual functionality into the controllers to manage the content of Azure Storage. This will be something for my next WinPHP session.&nbsp;</p>
