---
layout: post
title: "Formatting valid xs:dateTime fields with PHP"
date: 2009-09-08 20:01
comments: true
categories: 
  - php 
  - xsd 
  - validation 
  - xml 
  - datetime 
---
<p>After quite some time puzzling on the issue I decided to just fire off my problem to Twitter. </p><p><img src="/images/xsd-datetime-q.png" border="0" alt="Question" width="400" height="192" /> </p><p>Awesome as Twitter is for these kinds of things, I had my answer very soon: </p><p><img src="/images/xsd-datetime-a.png" border="0" alt="Answer" width="400" height="192" /> </p><p>Apparently the DATE_ISO8601 is missing the &quot;:&quot; in the timezone offset, and the solution is very simple:</p><blockquote>date(&#39;c&#39; );<br /></blockquote><p><br />According to the <a href="http://php.net/date" target="_blank">PHP manual</a>, the &#39;c&#39; string also gives the ISO 8601 formatted string. However, this one is correct and working. Thanks to <a href="http://blog.wombert.de/" target="_blank">David Zuelke</a>  for giving me this answer, as it ensured my progress in this specific task :)</p>
