---
layout: post
title: "I'm sorry: An open letter to Elizabeth Naramore"
date: 2013-11-14 16:30
comments: true
categories: 
  - community 
  - TRUCEconf 
  - liz
---
Hi Liz,

I was going to send this letter as an e-mail, but as I was writing the e-mail I felt it necessary to turn this into an open letter. Everybody should see this, and be aware of this. Everyone should stand up to support you. Openly, not just in private.

It is amazing how much crap you're getting for TRUCE conference. I am truly sorry for this. This is not the community I thought I was a part of. It has honestly been a huge surprise how much negativity there is in the world of PHP and tech.

Last weekend at [TrueNorth](http://www.truenorthphp.com/) I attended the [Open Sourcing Mental Illness-talk by Ed Finkler](http://funkatron.com/osmi.html), and one of the most important lessons I took away was to be empathetic and to focus on positivity. The people that go about hatin' clearly have never heard of this. On the other hand, you clearly do know, as TRUCE is a perfect example of this.

For what it's worth, I highly respect you for standing up against inequality and all the problems the tech community has. If only I had the finances, I would immediately book my flight to the US and ensure I'd be there. Unfortunately that won't work, so instead I tried to help you [fund the conference](http://www.gofundme.com/truceconf) with a small donation. Aside from that donation, I would like to really make it clear that I love what you're doing and I highly respect the things you have done, do and hopefully will keep on doing for the PHP and wider tech community. And though I can't really speak for everyone in the PHP community, I know for sure that many agree with this.

Please Liz, don't give in to the hate. That would mean the haters win. And haters should never win. It is the love and passion for solving a problem together what makes the Open Source community so great. Usually we focus on technical problems and coming up with technical solutions, and now we've got a social problem at hand. As many have said in response to the hate-storm after the TRUCE announcement:

    It's better to try and do something than to just sit by and do nothing to solve the problem
    
And so, dear Liz, I want to thank you for trying to do something. TRUCE conference can, at worst, result in a good get together where attendees will start understanding each other just a bit better. In the best situation I can think of, it can spark a movement that will change the tech community to be one that is more inclusive, more empathetic and more understanding. Liz, you ROCK.
