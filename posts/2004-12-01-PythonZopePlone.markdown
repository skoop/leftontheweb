---
layout: post
title: "Python, Zope, Plone"
date: 2004-12-01 4:32
comments: true
categories: 
  - leftontheweb 
---
I really really need to dive into Python. There are things for the web that are very interesting. For instance, Zope, and the Plone application that runs on Zope. Very interesting all that. Of course, PHP is just as interesting and though I've mastered big parts of PHP already there is enough that I could still learn about it. So it's a dilemma: Fully master PHP first, or broaden the horizon by diving into Python, Zope and Plone.

Installing a Zope server with Plone on it is slightly harder than installing Apache with PHP though.

Time to make some considerations.
