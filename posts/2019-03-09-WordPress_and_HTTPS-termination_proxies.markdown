---
layout: post
title: "WordPress and HTTPS-terminating proxies"
date: 2019-03-09 11:15:00
comments: true
categories: 
  - wordpress
  - https
social:
    highlight_image: /_posts/images/redirect-loop.png
    summary: I got confused by a redirect loop when deploying WordPress to our Rancher/Docker setup. It turns out this was related to our proxy
    
---

A blog I am writing for was looking for a new place to host their website. Since [we](http://php.ingewikkeld.net/) have a nice cluster with Rancher up and running, I offered to host the site. It's WordPress, so PHP, so how hard could it be, right?

I spent quite a few hours migrating everything. The initial migration to Docker was not that hard. There is a [great official WordPress image for Docker](https://hub.docker.com/_/wordpress), which makes it extremely easy to set up a new WordPress site in Docker. 

The next thing is handling file uploads. Using the [do-spaces-sync plugin](https://wordpress.org/plugins/do-spaces-sync/) this was easily set up to use [DigitalOcean Spaces](https://www.digitalocean.com/products/spaces/). It took a while to upload all images from the old wp-content/uploads to Spaces, but once that was done, I had it working immediately after setting it up. So far, this whole migration was a breeze.

Until I flipped the switch on the DNS and pointed it to our new hosting. I immediately got caught in an infinite redirect loop, and I had no idea why. I've spent hours turning off plugins, turning them on again. Debugging everything, watching logs. I could not figure it out. In the headers I did find a header saying that the redirect came from WordPress:

`X-Redirect-By: WordPress`

Eventually, I tried explaining the problem in the #wordpress channel in the [PHPNL slack](https://phpnl.nl/) and as I'm typing my explanation something dawns on me...

Our Rancher setup has a load balancer that terminates the HTTPS then forwards an internal request to the container using http. But in WordPress, I have configured the siteurl to be `https://`. So WordPress gets a request using http, figures out it should be using https, and redirects. This causes the infinite redirect loop!

Of course, I wasn't the first to encounter this problem. Once I know what the problem was, searching the Internet quickly gave me the solution. In [Wordpress Codex of course](https://codex.wordpress.org/Administration_Over_SSL). The only thing I needed to do was add a single line to my .htaccess file:

`SetEnvIf X-Forwarded-Proto https HTTPS`

Once I did that, rebuilt my containers and deployed them to Rancher, the problem was solved. All of a sudden, everything worked.