---
layout: post
title: "TestFest 2009 is coming!"
date: 2009-02-04 17:10
comments: true
categories: 
  - php 
  - testfest 
---
<p>The Dutch PHP Usergroup has also registered itself as one of the organizers. We&#39;re currently in early planning stages but we&#39;ve already committed ourselves to organize a TestFest this year together with the Belgian usergroup. Our main aim is to make this year&#39;s event bigger and better and ensure even more tests to be delivered to the PHP team. Of course, we have last year&#39;s success to defend, so we should aim high ;)</p><p>Aside from that, other user groups have already committed to organizing a TestFest and at <a href="http://tek.mtacon.com/" target="_blank">php|tek</a>, TestFest will also be present as part of the <a href="http://tek.mtacon.com/c/s/hackathon" target="_blank">Hack-a-thon</a>  being organized! Matthew Turland and I are the main people responsible for that. If you&#39;re interested in this, please do contact one of us.</p><p>We&#39;re working on getting TestFest into more places as well, so it seems the TestFest virus is spreading already. If you&#39;re representing a usergroup or otherwise want to contribute to TestFest, come hang out in #phptestfest on irc.freenode.net!&nbsp; </p>
