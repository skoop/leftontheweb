---
layout: post
title: "Where do you suggest I start?"
date: 2005-09-20 4:23
comments: true
categories: 
  - weblogging 
---
Over at <a href="http://www.43things.com/">43Things</a>, someone asked <a href="http://www.43things.com/entries/view/236478">where to start if you want to start weblogging</a>. I wrote a short answer which I thought might be nice to also post here, in a slightly different way.

The first thing to do is to consider what kind of weblog you want to start. What will the topic be? What is your target audience? What do you want your weblog to add to the numerous weblogs outthere?

When you've thought of that, you need to consider some things. There are, simply spoken, two types of weblog-systems. The community-based weblogs, think of <a href="http://www.livejournal.com/">Livejournal</a>, <a href="http://www.xanga.com/">Xanga</a>, and such. On these sites you need little to no technical knowledge and your weblog is up and running the minute you register. It also features a huge community of people. This brings in positive and negative sides. You'll have an immediate audience, however, these are not always the types of people you'll want to attract. You'll need little to no technical knowledge, but there is less flexibility in customizing your weblog.

The other type is an individual weblog. You will need to have your own hosting account (possibly with domain name) for that. And you will need to install the weblogging software yourself. Software you could consider are: <a href="http://www.wordpress.org/">Wordpress</a>, <a href="http://www.textpattern.com/">Textpattern</a>, <a href="http://www.pivotlog.net/">Pivotlog</a>, <a href="http://www.nucleuscms.org/">NucleusCMS</a> or one of the many others. This also has positive and negative sides. You'll need at least some basic technical knowledge, but the weblog can be customized to your liking. It'll be harder to find your audience, however you won't "automatically" attract the "bad guys".

I myself started out on <a href="http://www.livejournal.com/user/skoop">livejournal</a>, and then moved away. I now have a <a href="http://www.stefankoopmanschap.nl/weblog/">dutch weblog</a> running using NucleusCMS and an <a href="https://skoop.dev/">english weblog</a> using Textpattern. The audience for these are probably smaller than when I was on livejournal, but the people who read my weblogs now are probably people who are sincerely interested in what I'm writing.
