---
layout: post
title: "GlobIterator: Easy access to specific files"
date: 2012-04-16 14:54
comments: true
categories: 
  - php 
  - globiterator 
  - spl 
  - directoryiterator 
  - filteriterator 
  - iterators 
---
<p>GlobIterator exists since PHP5.3, and basically allows to iterator over anything <a href="http://php.net/glob">glob()</a>able. So instead of having to write my own custom FilterIterator and pass the DirectoryIterator for my directory to that FilterIterator, I can simple instantiate GlobIterator and pass the pattern that I need to it:</p>

<pre>$watchDir = new \GlobIterator($config['incomingPath'].'/*.xml');</pre>

<p>Now I can foreach over $watchDir and do whatever I need to do (in this case parse XML) with only a couple of lines of code! WIN!</p>

<p><strong>Update:</strong> <a href="http://twitter.com/dshafik">Davey</a> rightfully asked "why not use glob() in the first place?". To clarify: I am using GlobIterator because I want to have SplFileInfo objects instead of just plain filenames. I need some of the meta info SplFileInfo offers.</p>
