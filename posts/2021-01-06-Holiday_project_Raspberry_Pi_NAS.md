---
layout: post
title: "Holiday project: Raspberry Pi NAS"
date: 2021-01-06 14:30:00
comments: true
categories: 
  - raspberry pi
  - nas
  - openmediavault
  - bluesound
social:
    highlight_image: /_posts/images/openmediavault.png
    highlight_image_credits:
        name: OpenMediaVault
        url: https://www.openmediavault.org/?page_id=1188
    summary: After recently buying a new BlueSound device we felt the need to expose our full digital music collection so we could easily listen to our music. Time for a NAS. It seemed overkill to go for a full Synology or similar setup, so we decided to do a nice little holiday project: my son and I got ourselves a Raspberry Pi 4, and external USB harddisk and an SD card and went to work.
    
---

After our Sonos died early December, we were in the market for a new device. We did some research, got some advice and eventually decided to go for a [BlueSound Node 2](https://www.bluesound.com/). While going through the features we noticed the option to have your own digital music library playing on it (yeah, I know the Sonos could also do that, but we never set it up) and we felt this was a good trigger to do just that. But getting a very expensive (Sonology or the like) NAS just for that, especially after investing in BlueSound, seemed overkill. But of course there are simpler options. So my son and I set out to create a simple NAS using a Raspberry Pi and an external USB harddisk, and made that our _end of year holiday project_. 

## What we got

My son did the research on what to get and we ordered the following:

* [Raspberry Pi 4, 4GB](https://www.amazon.nl/gp/product/B07TC2BK1X/ref=ppx_yo_dt_b_asin_title_o00_s01?ie=UTF8&psc=1)
* [Raspberry Pi 4 power adapter](https://www.amazon.nl/gp/product/B07TMPC9FG/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)
* [SanDisk 32GB SD card](https://www.amazon.nl/gp/product/B06XWMQ81P/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)
* [GeeekPi case](https://www.amazon.nl/gp/product/B07XRRDQC8/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
* [Seagate 2TB USB3.0 harddisk](https://www.amazon.nl/gp/product/B00TKFEE5S/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)

## Assembling the hardware

When all the hardware was in, it was time to assemble the hardware. And that was surprisingly easy. Not only because my son was the one doing the assembling, but also because the case included a little screwdriver so we didn't have to look for the exact correct screwdriver.

So: SD card in Pi board, Pi board into the case, screw the case closed, connect network cable, power and... oh shit. We don't have a micro-HDMI cable. OK. [Let's order that one and wait for it to arrive](https://www.allekabels.nl/hdmi-micro-kabel/4962/1280780/micro-hdmi-kabel-14-high-speed.html).

Fast-forward a couple of days and the cable arrives: Time to connect the Pi to a screen, keyboard and mouse. We had followed a tutorial on how to install [OpenMediaVault](https://www.openmediavault.org/) but unfortunately the Pi wouldn't boot. The green light blinked 4 times, which means it couldn't find a required file. So we searched around a bit and ended up at the [Installing OMV5 On a Raspberry Pi document](https://github.com/OpenMediaVault-Plugin-Developers/docs/blob/master/Adden-B-Installing_OMV5_on_an%20R-PI.pdf). This was perfect!

## Installing OpenMediaVault

After having found the above document, installation was a breeze:

* Put Raspberry Pi OS on the SD card
* Install updates
* Run the installer script that _Ryecoaaron_ wrote
* Make OMV recognize the USB storage
* Set up a network share

DONE!

## Fun project!

This was a fun little project, and pretty simple. By now, there's gigabytes of music on the NAS already and our BlueSound is already playing the music. This was great. And since I recently [won a Raspberry Pi 400 kit](https://www.raspberrypi.org/products/raspberry-pi-400/?variant=raspberry-pi-400-fr-kit) I'm already thinking about what my next Pi-project will be :)