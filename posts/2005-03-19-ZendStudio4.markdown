---
layout: post
title: "Zend Studio 4"
date: 2005-03-19 10:56
comments: true
categories: 
  - leftontheweb 
---
Last year, I won a license for <a href="http://www.zend.com/store/products/zend-studio/">Zend Studio</a> at the <a href="http://www.phpconference.org/">PHP Conference</a> in Amsterdam. I was quite happy, I never really used an IDE before and thought it was all bloat. Zend Studio definately did proof the opposite.

Today I found out that version 4 of Zend Studio was released recently. It has quite <a href="http://www.zend.com/store/products/zend-studio/whats-new.php">an extensive list of new features</a>. Interesting. So I checked if I could also download this new version with the license I had, expecting I'd have to buy some kind of upgrade package. But no, I could just generate a license key for Zend Studio 4.

They've decided to split Zend Studio up into two different versions. The $99 <a href="http://www.zend.com/store/products/zend-studio/standard.php">Standard version</a> and the $299 <a href="http://www.zend.com/store/products/zend-studio/professional.php">Professional version</a>. And the license I had gave me access to the Professional version!

So yeah, I must say I'm really charmed, once again, by Zend and their Zend Studio IDE.

I've yet to start using the new features, but I'm sure they're just as good as the IDE that I used with version 3.5.
