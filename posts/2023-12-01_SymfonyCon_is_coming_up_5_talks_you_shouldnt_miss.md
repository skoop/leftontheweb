---
layout: post
title: "SymfonyCon is coming up: 5 talks you shouldn't miss"
date: 2023-12-01 11:50:00
comments: true
categories: 
  - php
  - symfony
  - symfonycon
  - conferences
social:
    highlight_image: /_posts/images/sc22.png
    summary: It's almost time for SymfonyCon! Here's 5 talks I'm really looking forward to.
    
---

Next week it's time for [SymfonyCon](https://live.symfony.com/2023-brussels-con/)! I am really looking forward to the conference. To meeting my old friends and make new friends. To the road trip I'm taking by car with a bunch of Dutch Symfony enthousiasts. In this blogpost I want to share with you the talks I really want to go and see. 

## 1. [Sofia Lescano - I did it! I broke production!](https://live.symfony.com/2023-brussels-con/schedule#i-did-it-i-broke-production)
Everyone makes mistakes. And I love it when people own up on their mistakes, and share not just the mistake, but also the lessons learned. Because we learn best by our mistakes. So I'm really looking forward to hearing about the lessons Sofia learned. 
Thursday December 7 at 11:10 in the SensioLabs room.

## 2. [Ondřej Mirtes - Static Analysis Crash Course for Framework Developers](https://live.symfony.com/2023-brussels-con/schedule#static-analysis-crash-course-for-framework-developers)
Static analysis is a very powerful tool, and I still see a lot of companies that don't use the power of running tools like PHPStan automatically. A fresh course is therefore a good idea, and who better to give that course than the PHPStan maestro himself. 
Thursday December 7 at 15:15 in the Symfony room.

## 3. [Guillaume Loulier - Need to search through your data? Heard about Meilisearch?](https://live.symfony.com/2023-brussels-con/schedule#need-to-search-through-your-data-heard-about-meilisearch)
Recently I have worked with Meilisearch for the first time in a project, and I really liked how it worked. As such, I'm looking forward tto hearing a bit more about what it is and how it works. Really looking forward to this one.
Friday December 8 at 10:05 i the SensioLabs room.

## 4. [Ramona Schwering - It's a (testing) trap!](https://live.symfony.com/2023-brussels-con/schedule#it-s-a-testing-trap)
Star Wars reference aside, this will be useful to everyone. Even if you're already very experienced with writing tests (I've done this for quite some time now), I always find that hearing others speak about their experiences and tips and tricks is very useful. As such, I really want to see this talk to see how Ramona does testing.
Friday December 8 at 11:10 in the Symfony room.

## 5. [Rick Kuipers - Symfony is RAD](https://live.symfony.com/2023-brussels-con/schedule#symfony-is-rad)
There's been a lot of talk about how Laravel is the best RAD framework at the moment and I do agree that Laravel is great for RAD. Then again, often I wonder if we couldn't achieve a similar thing if we were using Symfony. Rick will hopefully proof me right. The funny thing is, Rick has been doing this talk at usergroups near me and somehow I've always missed them. So now that he's doing this in Brussels while I'm there, I'll definitely be able to watch it.
Friday December 8 at 14:30 in the SensioLabs room.

Of course your interests may differ form mine. Luckily, SymfonyCon has [an excellent variation in the schedule](https://live.symfony.com/2023-brussels-con/schedule). Will I see you there? [Tickets are still available!](https://live.symfony.com/2023-brussels-con/registration/)