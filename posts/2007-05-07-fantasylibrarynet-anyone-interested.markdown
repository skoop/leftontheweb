---
layout: post
title: "Fantasylibrary.net: Anyone interested?"
date: 2007-05-07 12:59
comments: true
categories: 
  - personal 
---
One of the first things I did when I started learning PHP was developing a site about the Forgotten Realms series by TSR, Inc (later Wizards of the Coast). Eventually, that site evolved into what is now <a href="http://www.fantasylibrary.net/">Fantasylibrary.net</a>, a database of fantasy and sci-fi books. The code is quite old and not everything works anymore. And I've not looked at the pending books, authors and publishers for ages.

Over the past period I've been considering all the sites that I've been working on. FantasyLibrary.net has been a great site to work on, but at this point in time, I'm working on other efforts and FantasyLibrary.net just doesn't fit in, timewise.

So now I'm looking for someone to take over the site. I would hate to see this site go down. Rather, I'd like to pass it on to someone who can make it evolve even more. Who has the time and energy to manage the data, update the code, and make something of it.

So, if you're interested, here's the deal: I'm not out to make big bucks on this. I would appreciate a fair offer for all the effort I've put into the code and the database (even though the code is outdated and really needs an update). I will hand over the full site: The code, the database, the domain. Everything. In exchange for a nice offer and the promise that you're going to take the site to the next level :)

Interested? E-mail me at stefan@fantasylibrary.net with an offer and we'll discuss things.
