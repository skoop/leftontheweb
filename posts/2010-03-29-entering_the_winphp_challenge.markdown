---
layout: post
title: "Entering the WinPHP Challenge"
date: 2010-03-29 17:22
comments: true
categories: 
  - windows 
  - azure 
  - winphp 
  - dpc10 
  - php 
---
<div>It is no secret to people that know me a bit that, despite the fact that I work solely on OSX and Ubuntu, I have some friends in the Microsoft camp. So far though, I never really felt the need or will to start working with things Windows-related myself.&nbsp;</div><div><br /></div><div>When Microsoft announced they&#39;d be running the&nbsp;<a href="http://www.eurowinphp.com/the-challenge/" target="_blank">WinPHP Challenge</a>&nbsp;again this year, I quickly discarded the idea of entering because I don&#39;t use Windows anyway. However, after having seen two talks (one by&nbsp;<a href="http://akrabat.com/" target="_blank">Rob Allen</a>&nbsp;and the other one by&nbsp;<a href="http://www.joshholmes.com/blog/" target="_blank">Josh Holmes</a>) on Windows Azure I got at least a bit interested in how it would be to work with this technology. Having gone over the idea of it a bit more, I decided to take the plunge and make the step out of my comfort zone. And what better way to do that than to enter a challenge. After a bit of thinking, I was able to come up with a nice idea of an application I could create that would not just benefit myself but would be useful to others when open sourced. After some initial thinking on the name, I ended up with all the information needed to register in the challenge.</div><div><br /></div><div>It will be an interesting experience: Having not worked with Windows for some years (4 or 5 I believe), let alone with other Microsoft technologies such as IIS, and not at all with cloud services let alone Windows Azure, I will be forced to really dig into all this to make it happen. I really want to take this step out of my comfort zone though, and see what I can do when I completely switch to a new platform and a new set of technologies. I&#39;ll be blogging about the experience here as well, so you&#39;ll be able to follow what I&#39;m doing.</div><div><br /></div><div>If you&#39;re interested as well: There is still a couple of days left to&nbsp;<a href="http://www.eurowinphp.com/register-here/" target="_blank">enter</a>&nbsp;the challenge. Have a look at the&nbsp;<a href="http://www.eurowinphp.com/" target="_blank">website</a>&nbsp;and take the plunge: There&#39;s some interesting prices attached as well, with the winner getting a trip to the MIX11 conference in Las Vegas!</div>
