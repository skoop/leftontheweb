---
layout: post
title: "New domain"
date: 2019-03-07 14:10:00
comments: true
categories: 
  - dotdev
  - blog
social:
    summary: I'm migrating my blog from my old domain to a brand new domain, skoop.dev
    
---

I've had the domain leftontheweb.com for ages. It's been with me since 2004. However, since I recently got a brand new .dev domain, I decided it was time for a change. Since I can't even remember how I came up with the old name, it's time for a change. A new name that is easy to recognize, easy to remember and easy to link to me.

The new domain name for this blog is:

#skoop.dev

It only makes sense to switch to this domain. Skoop has been my nickname for as long as I have access to the Internet. And since my main occupation is still development, this switch makes sense.

Now, to find interesting topics to blog about again...