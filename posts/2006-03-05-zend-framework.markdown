---
layout: post
title: "Zend Framework"
date: 2006-03-05 11:32
comments: true
categories: 
  - technology 
---
Finally it's there. The <a href="http://framework.zend.com/">Zend Framework</a>. I've taken a quick look and so far it looks really good. It's just a preview release but if everything will be as this preview is, I do think it's going to be a very big and useful part of the code I use in my websites.

Hopefully a more extensive review soon. :)
