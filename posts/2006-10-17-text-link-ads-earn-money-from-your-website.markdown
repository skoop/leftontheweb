---
layout: post
title: "Text Link Ads: Earn money from your website"
date: 2006-10-17 14:31
comments: true
categories: 
  - world 
---
It actually seems to work. A few months ago, I signed up for <a href="http://www.text-link-ads.com/?ref=18513">Text Link Ads</a>, an advertising program for websites that sells simple text links for your website. The only thing you need to do is place a small piece of code on your site. And so I did for <a href="http://www.electronicmusicworld.com/">Electronic Music World</a>.

In the first months, no ads were sold. I was about to give up, when the first advertiser decided to buy a link on my site. After that, things were going very fast. Another ad was sold. Earlier this month, the first advertiser renewed his ad, and a new advertiser came in. And just now, a 4th ad was sold.

I am now in doubt. Should I try this one on my weblog as well? Of course, Electronic Music World has a very clear topic, is a very specific site. My weblog is less focused, and advertisers just need to like the site. So for now, I'll wait a while. Maybe I'll try it in the (near) future.
