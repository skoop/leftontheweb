---
layout: post
title: "Mambo -&gt; Joomla!"
date: 2005-09-06 14:03
comments: true
categories: 
  - joomla 
  - mambo 
  - cms 
---
The old Mambo CMS has a new name. The developers of the old Mambo software had quit their work for Mambo after an argument over the Mambo Foundation that was started. Now, they have presented their new name: <a href="http://www.joomla.org/">Joomla!</a> I wonder where they got that name, it doesn&#39;t sound very intelligent. ;)
