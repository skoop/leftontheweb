---
layout: post
title: "Buy vs. Build on Techportal"
date: 2009-04-15 15:03
comments: true
categories: 
  - meta 
  - ibuildings 
  - php 
  - custom 
  - standard 
  - opensource 
---
In the <a href="http://techportal.ibuildings.com/2009/04/15/buy-vs-build/" target="_blank">blogpost</a>  I write about making the choice between using standard open source software and components on the one hand, and building your own on the other hand. It is a description of how we do things at Ibuildings and will hopefully help others in making this tough decision at the start of any project.
