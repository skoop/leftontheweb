---
layout: post
title: "Trekstor I.beat Censo"
date: 2007-04-17 13:52
comments: true
categories: 
  - technology 
---
I had the luck of getting my hands on this great little mp3 player from Trekstor for a mere 20 euro (yay <a href="http://www.ibood.com/">iBood</a>. It's got 1 gigabyte of memory on which you can store quite a bit of music. More than enough anyway for my tastes, since I'll be able to change tracks every day if I want.

<img src="https://skoop.dev/images/38.jpg" width="280" height="186" align="left" alt="I.Beat Censo" /> This little thing is one of the few mp3 players that openly support Linux. Unfortunately, no ogg support on this one, for that you'll need slightly more expensive players, but alas, for this price, I'm not gonna whine about that. Connecting the player (just as you'd connect a usb stick) indeed immediately offers a nice mount. Unfortunately, so far, I've not been able to get it to work with my amaroK, but that's probably a matter of trying it out a bit. I'll probably get it to work sooner or later. For now, a simple copy command is enough to get the player loaded with some music for testing it.

The USB connection is a USB2 connection, so copying over some music is not a problem at all. It's reasonably fast (as far as USB goes). Soon enough, I have enough music for over an hour of listening, so I unmount it to give it a short test drive.

I copied over some house music to test it out. First thing I notice is that the sound is a bit shallow. Going into the menu, I quickly find the equalizer. One of the settings, Ultra Bass, immediately makes the house sound as it should, so I stick with it.

Since I'm talking about the menu now, let's dig a bit deeper. The menu is easy to access with a seperate menu button. Navigating the menu with the forward and backward button, and OK-ing with the play button, is easy. Aside from browsing the music in two different ways (tracks or folders), the settings, and the voice recorder, there's not much to do. The settings are nice. Aside from the equalizer, you can of course set the play mode in several of the usual settings. But, and this one is quite useful, you can also set the contrast and backlight (the latter is the time until the backlight is automatically turned off, useful for battery-saving!). There is also a sleep timer, for if you want to listen to music while going to sleep. Not sure how this would work with the ear plugs ;) Last option in the main menu is some system statistics, such as free memory.

This whole thing runs on a single AAA battery! Thanks to that and the already quite light design, you'll barely notice you have this machine with you, except for the nice music you're hearing.

I'm quite happy to have had the chance to get this one, especially for this price. What a great little machine!
