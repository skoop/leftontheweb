---
layout: post
title: "BookCrossing RingMix #1"
date: 2005-09-26 3:17
comments: true
categories: 
  - music 
---
Over at the <a href="http://www.bookcrossing.com/forum/15/2730840/9/subj_-De-muziekring">Dutch BookCrossing forum</a>, there was the idea that anyone interested could join the 'MusicRing', where every participant would compile a cd (either mixed or with seperate tracks) and those cd's would, once a month, be passed on to the next participant. This is a perfect way to learn about new music you'd normally not find out about.

My cd is now finished. It's mixed (well, tracks flow into each other, not beatmixed), and contains 16 tracks:

1. Beefcake - Untitled 1
2. Eboman - Sample Jazz
3. Suki Takahasi - The Public School
4. Eminem - Lose Yourself
5. Carl, Tomas Danko, Eminem - I'm sorry Eminem
6. Tobiah - I love your music
7. Daft Punk - One More Time
8. Girls On Top - I wanna dance with numbers
9. Bitcrush - Have you lost your way
10. Mercurial - March of the Lemmings
11. Metaxu - 01091939 Warsaw
12. Urawa - Assembly
13. Somatic Responses - Spatial Awareness
14. Deceptikon - Bossanovastyles
15. New Order - Blue Monday
16. Liquid - Liquid is Liquid (remix)

I'm very happy with the end result. It's a shame there's copyrighted tracks in there, because now I can't publish it on the web...
