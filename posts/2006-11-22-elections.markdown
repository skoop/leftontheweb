---
layout: post
title: "Elections"
date: 2006-11-22 14:08
comments: true
categories: 
  - politics 
---
Elections in The Netherlands is always very interesting. With a majority of the votes having been counted, I guess conclusions can now be made. Even though the biggest party (christian democrats) was in the past government and lost votes, they still got the most votes. Their partner parties however lost a lot of votes and we will not see them back in the government.

These elections were the first where I deviated from my "standard" vote until now. Instead of voting for the Greens, I voted Socialist Party this time. And with me, a lot of others as it seems. The socialist party grew from being the 6th or 7th party in the country to the 3rd party! A good thing, and a sign that the dutch are fed up with the anti-social government of the past years. I am happy about this. 

The most obvious government we will get now, however, might be a very christian government. The Christian Democrats would be combined with the Social Labour and the Christian Union, who are also quite socialist in nature. We'll have to see how this works out, but it doesn't look bad. I'm not happy with the very religious impact, but the fact that a huge part will be socialist is a good sign.
