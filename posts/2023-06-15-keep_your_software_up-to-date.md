---
layout: post
title: "Keep your software up-to-date"
date: 2023-06-15 17:10:00
comments: true
categories: 
  - php
  - framework
  - software
  - upgrade
social:
    highlight_image: /_posts/images/upgrade.jpg
    highlight_image_credits:
        name: Clint Patterson
        url: https://unsplash.com/photos/yGPxCYPS8H4
    summary: PHP and the PHP ecosystem is constantly moving forward. This is something that you should also constantly pay attention to. Because a small step is easier than a big leap.
    
---

In the past year several version of PHP and frameworks within the PHP world have been going into end-of-life status. Either there are only security updates left (PHP 8.0 is not supported anymore since November 26, 2022, but there are still security updates) or they are completely unsupported now (PHP 7.4 does not even get security updates anymore since November 28, 2022). Similar things are happening for older versions of for instance the Symfony framework.

Whether there is a relation with those changes or not, in recent months at Ingewikkeld we've taken on several projects that consisted mostly of upgrading projects. In all situations it is a combination of upgrading PHP (most of the times from PHP 7.4 to PHP 8.1 or PHP 8.2) and upgrading Symfony (from several versions within Symfony 4 to the most recent Symfony versions). 

These are not easy projects. They are quite complex because of everything that is involved. It's not simply "switching to a new PHP version" and then running `composer update`. Depending on your project, there will be different dependencies and dependencies of dependencies to take into account. Your code may contain deprecated features of either PHP or the framework and other libraries that you use. 

Of course, there are many tools that can be used to make this process easier. [Rector](https://getrector.com) is a huge help, and so is [PHPStan](https://phpstan.org). Ingewikkeld's [Muhammad](https://www.linkedin.com/in/moe-jassem/), who has worked on two of the big upgrade projects we've been doing, recently even [did a talk on Rector](https://www.meetup.com/phpamersfoort/events/293485634/). But even with these tools it is not an easy process, and it takes a lot of time. And therefore, also a lot of money.

## Continuous upgrades

One of the main reasons why it is so hard is because we're making a huge leap. A combined leap as well, because we have to upgrade both PHP and the framework and libraries. This makes it extremely complex. So what can we do to solve that? 

Well, what is a lot easier than a big leap forward? Indeed, small steps. So instead of upgrading once every couple of years and doing a huge upgrade that is time consuming and therefore very expensive, make upgrading part of your regular maintenance process. Bugfix-releases and minor releases are usually backwards compatible so doing those upgrades are relatively painless. With major version releases this is a big different. You usually have to schedule some time to do those. But luckily most of the big projects in the PHP world these days have a very transparent release schedule. [Check the roadmap page that Symfony has, for instance](https://symfony.com/releases). [There is also endoflife.date](https://endoflife.date/php) that gives an overview of a lot of languages and frameworks. I linked the PHP page as an example. This release information allows you to schedule your upgrade work months in advance. 

## So I always have to immediately upgrade?

No. First of all, it depends on your situation. There is not one specific way to handle this. And if you are still in your initial development phase of a project, I would actually recommend not to upgrade yet. When you're still in that phase, I would recommend first focussing on building the features that you need. Yes, you can do bugfix upgrades and minor release upgrades, but don't immediately switch to new major versions. Unless, of course, there are specific new features in that new version that will save you time in your development. 

My point is mainly: You apply the "continuous upgrade" mentality mostly to your project when it is already fully in production and you're doing either maintenance or maintenance and development of new features. Schedule the upgrade work as maintenance work.

## Do I get anything out of this?

Yes. Aside from having recent versions of the tools you're working with, PHP upgrades usually bring performance upgrades, which makes your site or application faster, and can also reduce your hosting costs. Frameworks sometimes also get a performance boost. And there's usually bugfixes and sometimes also security fixes in new versions that can help you keep your website or application more secure/less vulnerable. 

## I did not continuously upgrade. Now what?

In that case, you'll have to make the big leap first. And while doing that, plan your new continuous upgrade process. If you need help with the big leap, [Ingewikkeld](https://ingewikkeld.dev/) can help! Feel free to contact us to discuss options.

