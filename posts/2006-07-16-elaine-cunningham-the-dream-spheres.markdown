---
layout: post
title: "Elaine Cunningham - The Dream Spheres"
date: 2006-07-16 15:43
comments: true
categories: 
  - books 
---
It has been a while since I last read something by Elaine Cunningham. It must have been before I encountered <a href="http://www.bookcrossing.com/">BookCrossing</a>. After that, I started reading more than just fantasy and sci-fi ;)

Elaine Cunningham is definitely one of my favorite fantasy authors, together with Tolkien and R.A. Salvatore. Her books in the Forgotten Realms series are easy to read, have good stories and great characters.

In The Dream Spheres we encounter some characters we've seen before. Danilo Thann has featured in a few books already, and so have some of the other characters. Again, this book has an exciting plot, and I seriously until just a few chapters before the end didn't know exactly what was going on. Definitely not as predictable as some books!

Yes, it was nice to finally read some fantasy again from the Forgotten Realms series. I should do that more often.
