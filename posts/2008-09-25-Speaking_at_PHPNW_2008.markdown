---
layout: post
title: "Speaking at PHPNW 2008"
date: 2008-09-25 9:59
comments: true
categories: 
  - php 
  - refactoring 
  - events 
---
<p>Last tuesday at the dutch PHP Usergroup meeting I did this talk the first time, and even though I&#39;m not completely happy with it, most of it went well. I will also do the same presentation at International PHP Conference in Mainz at the end of october, but for those that can not make it there, you have another chance now.</p><p><a href="http://conference.phpnw.org.uk/" target="_blank">PHPNW</a>  is looking to become an excellent conference. It is only one day, so no chance of becoming too tired thanks to late night hacking sessions and such. Also, it is organized not by a commercial party, but by a usergroup. I have nothing against commercial conferences, but for some reason usergroup-driven events usually are a bit more personal and fun. I am very much looking forward to it! </p>
