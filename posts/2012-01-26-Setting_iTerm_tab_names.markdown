---
layout: post
title: "Setting iTerm tab names"
date: 2012-01-26 8:15
comments: true
categories: 
  - iterm2 
  - osx 
  - bash 
  - shellscripting 
  - automation 
---
<p>Being lazy and forgetful, I decided to write a simple bash script to execute this command. What I wanted to be able to do is enter a command with a title, which would then become the tab title. Something like this:</p>
<pre>itermtabname leftontheweb</pre>
<p>This is of course a very simple thing to do, but I have never done any shell scripting. So with a bit of Google Fu I found out how to do this. It is indeed very simple:</p>
<script src="https://gist.github.com/1681644.js"> </script>
<p>I've also added this script to my <a href="https://github.com/skoop/Stefan-Koopmanschap-s-Toolbox/blob/master/shell/itermtabname">toolbox</a>.</p>
