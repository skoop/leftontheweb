---
layout: post
title: "CSS list splitting"
date: 2005-09-27 8:48
comments: true
categories: 
  - technology 
---
Everyone who works with web techniques (specifically markup) and their grandma of course knows about and probably also reads <a href="http://www.alistapart.com/">A List Apart</a>. I am definately one of those. Yesterday, a very interesting article was published on splitting up ordered or unordered lists in a semantically correct way using XHTML and CSS. In <a href="http://www.alistapart.com/articles/multicolumnlists">CSS Swag: Multi-Column Lists</a>, <a href="http://www.alistapart.com/authors/n/paulnovitski">Paul Novitski</a> gives 6 different methods of accomplishing this. Though, in my very humble opinion, only the first one would be most usable in terms of technique (especially from an automated/php point of view), all of them definately look good and will work fine. In all but the first one, though, if this list is generated automatically by for instance php, the php would need to be written specifically for the chosen solution, which in my opinion should be done as little as possible. Logic, content and markup should be split up as much as possible. For now, I guess I won't be splitting up lists yet.
