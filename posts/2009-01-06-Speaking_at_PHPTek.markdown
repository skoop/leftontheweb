---
layout: post
title: "Speaking at PHP|Tek"
date: 2009-01-06 22:16
comments: true
categories: 
  - php 
  - conference 
  - phptek 
  - phptek09 
---
<p><strong>The Power of Refactoring</strong></p><p>&nbsp;I will be revising my refactoring talk again a bit. The base message will remain the same but the execution and order of topics will be changed a bit (no more live coding - live unit test execution stays though) and I&#39;ll be adding a few short things here and there.</p><p><strong>A Guide to Using and Understanding the Community</strong></p><p>I am quite excited to be doing this talk. First of all, it&#39;s the first time I&#39;m doing a joint talk (and I&#39;m quite happy to be doing this with Lorna). Second of all, being the community person that I am, this topic is much closer to me than the refactoring talk. So expect a very interesting talk!</p><p><strong>The North American Community</strong></p><p>Possibly even more exciting is the fact that this is my first time on the other side of the pond, so I&#39;m going to meet a lot of people I&#39;ve so far only spoken to online, or possibly never even spoken to but am very interested in meeting. Amongst those are Elizabeth Naramore, <strike>Cal Evans 2.0</strike> Eli White, Wez Furlong, Chris Shiflett, Chris Cornutt and Ben Ramsey. I could&#39;ve put all speakers on that list of course, but then the list grows so big so quickly ;)</p><p>Anyway, I&#39;m really looking forward to it, so feel free to drop by at <a href="http://tek.mtacon.com/" target="_blank">PHP|tek</a>  in May in Chicago. </p>
