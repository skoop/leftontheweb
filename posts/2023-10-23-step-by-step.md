---
layout: post
title: "Step by step"
date: 2023-10-23 17:15:00
comments: true
categories: 
  - php
  - usergroup
  - session
  - small steps
social:
    highlight_image: /_posts/images/steps.png
    highlight_image_credits:
        name: Leonardo.ai
        url: https://leonardo.ai
    summary: This Thursday Ewout den Ouden and myself are doing a very special session in Apeldoorn for Ode aan de Code!
    
---

If you now think of New Kids On The Block when reading the title of this blogpost, you're old, just like me.

But seriously, this week we're doing something very cool. And "we" in this is my friend and former Ingewikkeld colleague [Ewout](https://phpc.social/@dadaprovider) and myself. We've developed a special session for the [Ode aan de Code](https://www.meetup.com/dijdigital/events/295651691/) meetup in Apeldoorn. The session is about how taking small steps is better than taking a big leap.

Based on several situations related to either software development or the human side of software development we will go through examples of how it is easier to take small steps. And why you really would want to do that. 

But we're not just presenting information. It's going to be an interactive session where the input of all attendees is going to be really important. Without wanting to give away too much, we're actually going to make the attendees the center of attention for this session. Because why just limit ourselves to our own experiences when we can listen to everyone's experiences?

Interested? [RSVP-ing is the first small step to take](https://www.meetup.com/dijdigital/events/295651691/) and join us in Apeldoorn on Thursday for this session. Looking forward to seeing you there!

Note: Out of [Ingewikkeld](https://ingewikkeld.dev/) we can organize similar sessions focussed specifically on your company or team. We can use these sessions not just around a topic such as taking small steps, but also focussed on specific challenges your company or team has. If that sounds good, feel free to [contact us](mailto:info@ingewikkeld.net) to discuss the options.
