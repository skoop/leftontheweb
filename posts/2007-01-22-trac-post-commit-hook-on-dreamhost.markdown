---
layout: post
title: "trac-post-commit-hook on DreamHost"
date: 2007-01-22 6:51
comments: true
categories: 
  - technology 
---
Now that I have <a href="http://trac.edgewall.org/">Trac</a> working on DreamHost, I want to also get the trac-post-commit-hook working, so that commits will automatically show up in Trac. Using the default instructions unfortunately does not work fully, since the installation by DreamTracInstall which I <a href="https://skoop.dev/article/310/trac-on-dreamhost-perfect">wrote about earlier</a> does some specific things (like installing a completely new Python environment) that the default instructions don't take into consideration. It's not that hard though, as the basic steps are the same.

First of all, you'll need <a href="http://trac.edgewall.org/browser/branches/0.9-stable/contrib/trac-post-commit-hook?format=raw">the script</a>. Since DreamTracInstall currently installed 0.9.6, you need to use that version. You put this script in some location on the server. I put mine in /home/skoop/trac_sites/trac_hooks (I know, maybe not the best location, but it keeps my home dir clean). 

Now, using an SSH connection, go to your subversion environment for which you want to install the hook (/home/myusername/svn/myproject/hooks). Copy the post-commit template (post-commit.tmpl) to 'post-commit' (no quotes). Your post-commit script will now be found. Comment out the e-mail alert (unless you want to use it, in that case chance the e-mailaddress). Now, add the following:

<pre>LOG=`/usr/bin/svnlook log -r $REV $REPOS`
AUTHOR=`/usr/bin/svnlook author -r $REV $REPOS`
TRAC_ENV='/home/myusername/trac_sites/myproject/'
HOME='/home/myusername' 
export PYTHONPATH="$HOME/packages/lib/python2.3/site-packages"
export LD_LIBRARY_PATH="$HOME/packages/lib" 
export PATH="$HOME/packages/bin:$PATH"

/home/myusername/packages/bin/python2.3 /home/myusername/trac_sites/trac_hooks/trac-post-commit-hook \
 -p "$TRAC_ENV"  \
 -r "$REV"       \
 -u "$AUTHOR"    \
 -m "$LOG"</pre>

Ensure to edit the paths to your specific situation.

Now, you need to one last thing. You need to ensure that your trac database directory and file are writable by the webserver user. I am sure we're opening up a can of security worms here, but it seems there's no easy way around it, except if you want to somehow start writing proxy scripts which are executed as another user. So go to /home/myusername/trac_sites/myproject/ and chmod -R 777 db

Now you should be all set. Commit something to your repository, with in the commit message something like re #1 (this has to be an existing issue number of course) ... in the Trac web interface, you should now see the commit message with a reference to the changeset listed with the issue you mentioned.

Enjoy!
