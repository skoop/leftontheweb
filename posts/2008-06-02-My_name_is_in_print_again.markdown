---
layout: post
title: "My name is in print again"
date: 2008-06-02 20:16
comments: true
categories: 
  - book 
  - php 
  - recruiting 
---
<p>Back in the days when I worked at Dutch Open Projects, summer 2007, I entered a survey for an author who was writing a book for PHP developers looking for a job. I explained my view on recruiting and what I was looking for when interviewing candidates. Time went on and I actually forgot about it. I moved on to another job, and all of a sudden a new book is being announced which rings a bell.</p><p>It turns out that the book went on to be published by what is currently probably my favorite publisher in the php-book area: <a href="http://www.phparch.com/" target="_blank">PHP|Architect</a>. From the topics covered Michael seems to cover the ground very well and handle all important aspects for people looking for a job. So if you&#39;re new to (or maybe even if you have some experience with) the PHP job market, check out the book. </p>
