---
layout: post
title: "Fun with usernames"
date: 2005-10-30 8:34
comments: true
categories: 
  - music 
---
For some reason, <a href="http://www.last.fm/user/Al_Qaeda/">this username</a> caught my eye. This is the fun you get when you allow users to choose their username. And I actually don't mind. It's a funny way of anonymizing yourself.
