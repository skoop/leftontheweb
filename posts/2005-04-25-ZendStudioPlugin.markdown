---
layout: post
title: "Zend Studio Plugin"
date: 2005-04-25 14:14
comments: true
categories: 
  - leftontheweb 
---
Amongst the tons of plugins for web developers, there is a new one. The <a href="http://www.zend.com/store/products/zend-studio/plug-ins.php">Zend Studio Plugin</a> integrates some Zend Studio functionality with the browser. Of course, only useful if you use Zend Studio for your development, but since I do, I've just installed the plugin.
