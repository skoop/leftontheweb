---
layout: post
title: "New activities"
date: 2007-06-20 5:40
comments: true
categories: 
  - personal 
---
Aside from the development I already do at Dutch Open Projects, and the fact that I am Lead Developer so have a role in helping, stimulating and to some extent steering the rest of the developers, a new activity is being added both to my work and to the Dutch Open Projects portfolio: "consultancy".

One of our new clients is a developer who is planning to write a new piece of software. He has chosen the Symfony framework to build upon, but has little knowledge right now about working with the framework and it's capabilities. And so, we will be helping him in his endeavors. It's a completely new thing for me to do, but being the Symfony advocate that I am, I really like this new branch of work. I definitely hope that we will be doing this for more clients, as that will mean more people will start to use the Symfony framework.
