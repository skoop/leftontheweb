---
layout: post
title: "Upcoming conferences and usergroups"
date: 2017-03-01 15:00:00
comments: true
categories: 
  - php 
  - wordpress
  - joomla
  - conferences
  - events
  
---

It's been a while since I've done a post like this, but given the focus of the upcoming events I thought I'd give a small overview of my upcoming speaker appearances.

## Why?

For 2017 I want to once again focus a bit more on building bridges. I've done this a couple of years ago but limited myself again to mostly generic PHP conferences in the previous couple of years. Others, such as Jenny Wong and Larry Garfield, are trying to build bridges between the generic PHP community and their respective communities (WordPress and Drupal). Inspired by the work some of these people are doing, I wanted to join in again.

## No more generic PHP conferences then?

Of course I'll be at generic PHP conferences and usergroups. But I want to also focus a bit more on what is out there beyond generic PHP and the popular frameworks.

## Events

So let's have a look at the upcoming events where I'll be speaking

### WordPress Meetup Enschede

On Match 14th I'll be speaking at the [WordPress Meetup Enschede](https://www.meetup.com/WordPress-Meetup-Enschede/events/237491814/), a local WordPress usergroup for the Dutch city of Enschede. I'll be doing my 'Level Up Your Team' talk on ways to improve your knowledge.

### Dutch Joomla!Days

On April 2nd I'll travel all the way to Zeist (which, really, is just around the corner from where I live ;) ) for the [Dutch Joomla!Days](https://www.joomladagen.nl/). I've spoken at this event a couple of times already and have always enjoyed being there. This year, I'll be doing two talks: I'll be doing the opening keynote on Sunday on the need to keep learning, and right before the closing keynote I'll be doing my 'Level Up Your Team' talk on ways to improve knowledge. 

### Dutch Web Alliance freelancers meetup

On Monday April 3rd I'll be helping organize and host a [free meetup](https://www.meetup.com/DutchWebAlliance/events/237066788/) for people in The Netherlands who either just started freelancers or want to start freelancing. I'll be one of several Dutch Web Alliance members that will be sharing their knowledge and experience about freelancing.

### PHPKonf

On May 20 I will travel to Turkey for [PHPKonf](http://phpkonf.org/). In Istanbul I will speak about working with components and how you can build your own framework using components with my 'Build your framework like Constructicons' talk.

## More?

If you run a usergroup or conference that focusses not just on generic PHP but on specific applications, such as WordPress, Joomla!, Drupal, Magento or anything else and you are interested in a speaker from the generic PHP community to come give a talk, please do get in touch! For usergroups within the Netherlands, I should be able to travel to you without any issues. For events outside of The Netherlands, I'm OK with a travel reimbursement and a simple hotel. I'd love to make some more connections outside of my PHP bubble, and look at the problems other developers are solving and share with them some of my own knowledge and experience.