---
layout: post
title: "Gravatars on the site"
date: 2005-09-29 6:51
comments: true
categories: 
---
Thanks to <a href="http://johan.galaxen.net/logg/107/textpattern-plugin-glx_gravatar">this fabulous plugin</a>, <a href="http://www.gravatar.com/">Globally Recognized Avatars</a> are now available on this site. Just enter the e-mailaddress you registered with over at gravatar.com in the e-mail field when commenting, and you should see your gravatar being served next to your name.
