---
layout: post
title: "On Priority"
date: 2016-10-10 21:00:00
comments: true
categories: 
  - php 
  - mentalhealth
  - priority
  - geekmentalhelp
  
---

I got a private message on one of the Slack networks I'm on on August 2nd. Mike Bell invited me to write something for [Geek Mental Help](http://geekmentalhelp.com/)-week. As usual, I delayed to the last moment to write that, and the Geek Mental Help-week was last week. I'm publishing this now, a couple of days too late.

For quite a while I was thinking about what to write about. I had quite a few good topics I could possibly write about, but I just hadn't decided yet. I've thrown out all those topics (for now at least) because I am too late, and that actually gave me the best topic.

Last Thursday was a horrible day. I felt my mind being tricked into depression. I specifically use these words because I can not really control when it happens, or what happens. I've had cognitive behavioural therapy to recognize when it happens and to try and counter it, which usually works, but on Thursday I had a really hard time actually doing so.

I had a full-day training session scheduled on Friday, a training session I had already postponed earlier. I could not cancel that. So I set my mind to delivering that on Friday and was able to do so, in a way I feel was good. It went much better than I had anticipated, which made me feel a bit better. It was also incredibly exhausting, and I've found myself to have an extra hard time keeping myself out of depression when I'm tired. I succeeded though, but again, I struggled.

Over the past years I've tried to force myself to have actual weekends more than before. I try to stay away from the computer a bit. During therapy I learned that one of the ways to try and stay out of depression is by staying active, doing things, going out. And so when the idea came up to head into the nearby city to catch Pokémon, that sounded like a good plan. It worked. It kept my mind occupied enough, distracted enough. It was still hard work to keep my head straight though. On Sunday, this resulted in me sleeping quite a bit through the afternoon. Unsurprisingly, I may add.

And now it is Monday. The day started just as bad as the previous days, but somehow as the day progressed, my head cleared. And for the first time in 5 days, I started feeling quite well again. By the end of the day, my head had cleared. What's left is this tired feeling in my head. I'm pretty sure that will clear though.

It's been a long 5 days, but I've survived. And while I never can tell for sure, I'm pretty sure it had to do with me setting priorities. Going out with the family on Saturday, not working on either this blogpost or my keynote for [PFCongres](http://pfcongres.nl/) next Saturday or my talks for[ZendCon](http://zendcon.com/) the week after. And while I'm sorry that this blogpost is a bit too late, it illustrates all too well that it is important to set the right priorities. The great thing is that many people had their priorities right last week. [This list of articles](http://geekmentalhelp.com/) is the proof of that.

