---
layout: post
title: "Angels &amp;#38; Demons, for real?"
date: 2005-08-17 4:54
comments: true
categories: 
---
Only a single day after I finish Dan Brown's Angels & Demons, I get confronted by something more real. Shocking. People who know me a bit know that I'm by far no religious person. I tend to be slightly anti-religious at times. However, I've once spent a week in the community of <a href="http://www.taize.fr/">Taiz