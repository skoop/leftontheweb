---
layout: post
title: "4developers, or: a quick trip to Poland"
date: 2009-03-05 21:28
comments: true
categories: 
  - 4developers 
  - conference 
  - talk 
  - php 
---
<p>The trip will be a very quick trip. I will fly out tomorrow evening, land in Katowice (1.5 hours from Krakow, as I have understood) at 21:30. I will attempt to sleep in the hotel, and then get up early as I&#39;m on second in the Dedicated Languages track (after Ibuildings CTO and overall cool guy <a href="http://www.jansch.nl/2009/03/05/speaking-at-4-developers-conference-poland/" target="_blank">Ivo Jansch</a>) to do my refactoring talk.</p><p>My refactoring talk will be a slightly adapted version of the earlier refactoring talks I did. It will still contain PHP code examples, but will also ensure people know that this practice is not limited to PHP only, but will work in any language when done properly. </p><p>After my talk, I&#39;ll have a few hours to talk to people before having to leave again for my late afternoon flight back home. I am a bit sad about not having time to explore Krakow and surrounding areas, however having done <a href="http://www.phpconference.co.uk/" target="_blank">PHP UK Conference</a>  last weekend and being in Rome for <a href="http://www.phpcon.it/" target="_blank">PHPCon Italia</a>  in two weeks time, I didn&#39;t feel like being away from my wife and kids for too long.</p><p>If you&#39;re from Poland (or just living there) feel free to come over to <a href="http://www.4developers.org.pl/" target="_blank">4developers</a>  on saturday. It will be fun, I promise :) </p>
