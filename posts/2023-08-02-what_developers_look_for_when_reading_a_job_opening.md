---
layout: post
title: "What developers look for when reading a job opening"
date: 2023-08-02 16:55:00
comments: true
categories: 
  - php
  - jobs
  - opening
  - developers
  - vacancy
social:
    highlight_image: /_posts/images/helpwanted.png
    highlight_image_credits:
        name: Nathan Dumlao
        url: https://unsplash.com/photos/xRb4O75uymo
    summary: In the current job market it's hard to find (good) developers. Let's look at some things developers love to see in your vacancy text that will improve your chances of finding a new developer.
    
---

About a month ago a discussion was triggered in the [PHPNL Slack](https://phpnl.nl) on the subject of what people are looking for in a vacancy. The trigger for this discussion was the fact that on a regular basis companies don't disclose a salary indication (an actual amount) in their jobs page. That got me to wonder: What else is good or bad? Perhaps we can learn from this. While probably not scientifically accurate or statistically sound, I did get a lot of responses from a lot of different developers. Let me try to give a summary of the things that were mentioned.

## Salary

First of all, what triggered the discussion in the first place. Mention a salary range or indication. Not mentioning an actual number is for most developers that responded a reason to not even respond to a vacancy. And vague texts such as `conform current market` or `competitive` are even more off-putting than nothing at all. Too many companies use such terms to hide the fact that they simply don't offer a good salary. So be open and clear about what people can expect in terms of salary. 

You should also be open about any bonus systems, 13th month systems or other financial compensations that are part of the job.

## Other benefits

Other benefits can also be important. Do you offer a pension, what costs are reimbursed, do you pay for conferences and training, do you offer paid time for working on open source, etc. Not all developers are primarily focussed on salary, so any other benefits can be deciding factors. Make sure to clearly communicate what developers can and cannot expect at your company.

## Location

Long before we had a global pandemic there were already developers that were mostly working remote, but the pandemic has (finally?) triggered more companies to open their eyes to remote working. Especially a job such as software developer is something that can easily be done remotely. On the other hand, there's a whole group of developers that prefer to work on-site. So make sure to clearly mention whether a developer can or must work on-site, or whether remote working is an option or even required.

Also, if you offer "hybrid" working (partially on-site, partially remote) be clear about how much time is remote, and how much is in-office. 

## Responsibilities

What are the responsibilities of the developer. Are you looking for a pure code monkey, or do you also need someone with other skills: ops, architecture, management, communication? Does this job also involve interviewing potential new developers? Does it involve talking directly to customers? Is it mostly about maintaining an existing application or will the developer also work on new features? Is it a legacy codebase or a greenfield project? Do your developers coach interns? Different developers have different interests, so it's good to manage expectations.

## Working hours

Not everyone is a morning person just like not everyone is an evening person. Mention your work hours, or mention if those work hours are flexible. With the job market becoming more global and developers spanning multiple timezones this is especially important. If you have a daily standup or other meetings that are important that people need to attend, mention their times. That helps developers determine if this is something interesting for them.

Also: Are you looking for 40 hours/week? Or is 36, 32 or even 28 or 24 also an option? 

## Tech stack

What is the tech stack in your company? Also: Which part of that stack will the developer be working with on a regular basis? If you're looking for a backend developer it is not strange that you occassionally need to do some Javascript or CSS work, but if you expect a backend developer to work on the frontend as well on a regular basis, this is important to know. 

Keep in mind that part of the tech stack is the development environment: Does a developer have free choice in what platform they develop on? Do you offer only a limited choice in what laptop and other hardware and software a developer can choose from? These things can be very important for the happiness of a developer, communicate it clearly!

## Methodology

Does the team and/or company use scrum? Kanban? Waterfall? Is everything very structured or very ad-hoc? Some developers thrive in a chaotic environment, others really need structure. Some developers love the predictability of a waterfall approach, others prefer the more flexible scrum or kanban way of working. To make sure you get the right developers to respond to your vacancy, be clear about how you work.

## Duration of the contract

Some companies initially offer a 6- or 12-month contract. Some companies immediately offer a permanent contract after that initial contract, others offer several temporary contracts before going to a permanent contract. Be clear about what a developer can expect. 

## What is the team like?

I'm grouping several different things under this header because they seem related. First of all, there is team composition. Is the team diverse in terms of gender, age, background? You don't have to specify all the details, but some people would rather work in a diverse team while others would prefer a team of similar people. 

Then, culture. What is the culture like within the team and within the company? Is it a formal or informal organization? Is it an organization of individuals or is there a very strong team feeling? These are all things that can improve your chances of a developer responding to your job opening.

## Language

No, I'm not talking about programming language (although, really, do mention which programming language(s) you use) but I'm talking about spoken language. I've come across so many organizations that only want people that speak Dutch, for instance. Mention that! It saves you and the non-Dutch-speaking developers a lot of time (and therefore, money).

## What type of clients do you work for?

Developers have their own principles and therefore don't always want to work for certain types of companies. Or they have a strong interest in specific domains (care, education, industry), so they would apply to a job where the chance of working in that domain is higher. You don't always have to use names (although using names can be a bonus) but at least say what kind of clients you work for. 

## Sustainability and responsible entrepreneurship

Several developers mentioned that they like to know how a company handles things like sustainability and social responsibility. Does your company invest in making sure your ecological footprint is smaller? Do you invest in the (local? worldwide?) community and support NGOs that try to create a better world? 

Similarly, do you offer internships or traineeships? Or do you invest in another way in sharing knowledge and experience? Does your company contribute directly or indirectly to open source? 


## What NOT to do?

Aside from all these things that are good to mention, there's also some things you really don't want to do. 

For instance, do not use terms such as heroes, superstars, rockstars to refer to developers. Most developers are normal humans (yes, really!). 

Don't use things like "we don't have a 9 to 5 mentality". This is a red flag for many developers that signifies that you expect regular unpaid overtime. Even if you mean well, this is a red flag that stops many developers from responding to job openings. As said earlier, specify expectations as to work hours and list the times of important meetings so developers understand whether you are very flexible when it comes to work hours, or whether you're looking for unpaid overtime. 

Don't communicate a salary that is unrealistically low or high. Also: Don't offer a salary below  the range you communicated once you interviewed a developer. Be honest. And that isn't limited to salary.

Do not overestimate yourself. Do not talk about "what an honor it would be to work for us" or "how lucky you are when we pick you". 

Similarly, don't use buzzword bingo terms. Don't talk about "energetic", "kick-ass" or such terms. This makes your text sound untrustworthy. You're trying too hard to make it sound appealing. Instead, be clear and honest. As the saying goes, honesty goes a long way. 

Good luck in your search for developers! I hope this helps!