---
layout: post
title: "Browser speed"
date: 2005-02-12 9:30
comments: true
categories: 
  - leftontheweb 
---
Interesting. A quite extensive <a href="http://www.howtocreate.co.uk/browserSpeed.html">browser speed test</a> for browsers in Linux, Mac and Windows. Very good to see the different browsers put next to eachother. It's not an official test, it's not a scientific test, but a test by someone who wanted to test the speed of the different browsers for several aspects. Which actually leads me to believe in it's results.
