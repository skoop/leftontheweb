---
layout: post
title: "D-Day is coming to Finland"
date: 2011-11-03 14:13
comments: true
categories: 
  - php 
  - symfony 
  - symfonylive 
  - d-day 
  - finland 
  - conferences 
---
<p>The conference, taking place on February 6th in Helsinki, is a new conference focussing on "Design, Development & Digital business". It's not a pure PHP conference, which is what I really like. The <a href="https://twitter.com/#!/DDayConf/statuses/132071287987245056">current list of announced speakers</a> is quite nice already, I'm looking forward to seeing who else will be speaking.</p>

<p>As for my talk: "Don't use a screw when you need a nail" is a talk I've done before twice, and the most important thing people should get from this talk is to not be afraid of tools not written in your favorite language, but look beyond that. For many of those tools you don't need any or just a little bit of knowledge of the language to use it. During my talk, I will mention quite a few tools from different categories that developers could look at.</p>

<p>I realize that it can be quite cold in Finland in February, but I'm really looking forward to this conference. Perhaps I'll see you there?</p>
