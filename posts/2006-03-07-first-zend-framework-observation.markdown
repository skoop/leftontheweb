---
layout: post
title: "First Zend Framework observation"
date: 2006-03-07 17:43
comments: true
categories: 
  - technology 
---
My first observation of the Zend Framework is a negative one. Their wonderful central Zend class allows you to include classes without having to include() or require() them yourselves, and it nicely throws an error if it can't include the file. Much better than getting an error or having to check for the existance of the file yourself. However, you can not include other Zend Framework classes this way.

The naming system for the Zend::loadClass method is (quite logically) set up so that you need to name your file exactly the same as the class inside that file. When the method is called, it will include, for instance, Log.php and look for the existance of a class called Log inside this file. If the file exists, but the class doesn't, it will throw an exception. Very good implementation. However, when I tried this on the built-in Log class in the Zend Framework, I immediately got an exception: The file is called Log.php, however, the class is called Zend_Log. And this is the case for the other classes as well: Zend_Db class is inside Db.php,  Zend_Feed class is inside Feed.php, etc etc. 

Even though the overall functionality of the Zend Framework looks promising, such a big problem in naming policy does offer some worries about the coordination of efforts. I certainly hope this will be fixed in the future.
