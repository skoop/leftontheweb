---
layout: post
title: "phpBB 5 years, yay!"
date: 2005-06-17 6:44
comments: true
categories: 
  - leftontheweb 
---
Today 5 years ago, James Atkinson started a simple <acronym title="Ultimate BulletinBoard">UBB</acronym>-like forum on his own website. The name James gave to this forum was <a href="http://www.phpbb.com/">phpBB</a>.

Happy birthday phpBB!
