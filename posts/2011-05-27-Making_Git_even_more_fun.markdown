---
layout: post
title: "Making Git even more fun"
date: 2011-05-27 14:19
comments: true
categories: 
  - git 
  - game 
  - fun 
---
<p>So I left this alone for a while, until this week. I decided I'd have a look and see what it's all about. And it's actually quite fun. Of course, so far, I've only <a href="http://skoop.github.com/git-achievements/">earned the most common achievements</a> but by just looking at the locked achievements, I see some new ways to do existing things, and I see some stuff I didn't even know about. Pretty cool eh?</p>

<p>Installing git-achievements is just a matter of <A HREF="https://github.com/icefox/git-achievements/blob/gh-pages/README">following the steps in the README</a>. I forked the repository, cloned it to my local machine, and editted my <em>.profile</em> file to include my git-achievements clone into my PATH, and then alias the git-achievements executable to the <em>git</em> command. I have also set the <em>achievement.upload</em> setting, so that <a href="http://skoop.github.com/git-achievements/">I now share my achievements</a>. It's fun, simple and doesn't really interfere much with your normal day to day Git usage.</p>
