---
layout: post
title: "Company and team culture"
date: 2017-07-04 07:20:00
comments: true
categories: 
  - php 
  - culture
  - development
  - company
  - hr

---

I talk to a lot of companies and work at quite a few companies as contractor or consultant. Quite often I hear management complain about how hard it is to get good developers, and also that developers leave again after a while.

Now, there are many reasons why it can be hard to find the right developers. There are also many reasons why developers leave. One thing I've found though is that management does not understand developers, and does not understand what developers are looking for. And while in a lot of companies software is the main way to make money or at least supports the main source of income in quite an important way, the software and the developers who make it are not valued correctly.

For this article I want to focus on an important part of acquiring and keeping developers: Company and team culture.

## Culture

Let's first have a look at what culture is in the context of companies and teams. Culture is about the environment you create within your company and within the development team(s). It is about how comfortable a developer feels while doing their job. In the end, a developer delivers their best work in a workplace where they feel comfortable, where they feel trusted, where they feel respected. 

In the past couple of years I've talked to and worked for companies that have bad (or no) developer culture and I've worked for organizations that have invested a lot in building a great culture. But even the best of those organizations had some weird problems, and the worst of those companies had some good aspects. I want to focus on a couple of these things to illustrate what you can (or should not) do to build a good culture.

### Trust

First and foremost, one thing that I've seen go wrong (and right) in organizations is trust in developers. To create a good culture for developers, it should be clear that management trusts the developer(s) to create a good product. You hire developers because you do not have the expertise, so trust the judgement of developers because of that expertise. Even if you have a background in development and know what you're talking about, if your job is not actually developing software, then trust the judgement of those that do that. It is OK to challenge developers, but make sure that it doesn't turn into "I'd do it like this if I were you". Offer a suggestion instead of telling them what to do: "What about this other approach?" instead of "I think this other approach is better".

Several of the companies I've worked for in the past years understand this. They give full trust in the judgement of developers, even to the point that the development teams are completely autonomous. Some organizations even give trust beyond development decisions to their developers, trusting that they will make the right decisions to build good products for customers but also keep their own team running and profitable.

In other organizations I've seen even relatively simple things like getting a big screen for a CI/status dashboard or a new external keyboard for a laptop to be hard because management did not trust developers enough to just accept this was part of a good work place.

Trust your developers to make the right decisions for your organization from a technical point of view like they trust you to make the right decisions from a business point of view. This is a great basis for a good culture.

### No blame

Mistakes are made in every organization, by everyone who works. Making a mistake that ends up in production can of course have a huge impact on your organization, but rest assured that it has a huge impact on the developer that caused the error. Trying to immediately blame people is counterproductive though. 

The thing is: Quality is not an effort that belongs solely with the developer. Developers are obviously the first people that need to focus on making sure their code adheres to your quality guidelines. They need to test their work to the point where they are confident that their code is working as it should, but the tester should not be the last person in the organization to test code before it goes into production. There should be processes in place for quality assurance. Functionality should be tested also by non-developers (because they look at the software differently), and preferably there should be unit and functional tests that are run automatically on any changes introduced in the code. If possible, there should be a dedicated professional tester. Within smaller organizations this is harder, but there should at least be one non-developer that tests the changes and makes sure things work as they should. 

Having said all this, some things may still go wrong. But instead of blaming the person introducing the issue, look at how to prevent the problem in the future. This is a much more constructive approach and puts focus on team responsibility instead of individual mistakes. Accept mistakes as part of the process, and work to minimize them.

> "Better to trust the man who is frequently in error than the one who is never in doubt." --Eric Sevareid

### Inclusivity

A team is a team. It is a group of people that have to work together to deliver good software. If you've compiled a team of good developers, testers, designers and everyone else you may need to deliver the product that you're working on, make sure that they keep being a team. 

I've worked for an organization some time ago that had their developer culture pretty well set up. There was trust, there were autonomous teams, there was a high focus on quality, there was little blame. It was a great team to be a part of. Because there were several teams, it was deemed important to have a regular meeting with all the teams together to keep them aligned with eachother. A great idea! Sharing knowledge, experience and also decisions between teams is a great way to ensure good, consistent quality in software. The organization deemed the meeting important, yet not important enough to sacrifice development time for the meeting. Because of this, the meeting was scheduled during lunchtime.

Now, working for this company was a joy, and I learned a lot, but it was also quite intense. My head needed the lunch break to get some rest, to relax so that I could take the afternoon without burning out. This may have been a sign that we (or at least I) weren't doing a sustainable pace, but let's keep that out of scope for now. I really needed my lunch to relax. I notified the right people in the organization about this, and I was informed that while the meeting was important, it wasn't required, so I could skip it if I wanted. Over time, however, it turned out that during those meetings quite a few important decisions were either taken or communicated that I never picked up on. This started weighing on my mind: should I start attending these meetings knowing I would be burned out by the end of the workday? I decided that clearly these meetings were very important, so spoke up to the right people again to ask whether the meeting could be moved an hour later, so I could attend. Again, I got a "no". Despite important decisions and announcements, the meeting was not deemed required, so it was fine. And I guess for them it _was_ fine. For me, however, it felt like I was not an important part of the team. Not important enough for being included in these decisions, in hearing other announcements. Given my mental health was already under some pressure due to the intensity of this specific project, this really weighed on me.

Once you've built a team of people, try to take any measure necessary to keep everyone on board. Consider all the input that you get, especially when you get it several times, and try to make it work for everyone involved. The last thing you want is that valuable people feel left out or unheard. Make sure to create an environment where everyone feels included.

### Goals

Another thing that is very important is that you communicate clearly and openly about goals. Only when developers have a good idea of what the goals are for a project will they be able to take the right decisions, technical and otherwise, to create the best solution. I have experienced on several occassions that (project) management only communicated a partial context for a project. The developers then took decisions and set up their solution based on that input. Halfway through the project, the missing part of the context was then communicated, causing those projects to all of a sudden having to make a sharp turn or even a U-turn in terms of approach. At the end of the project (or even before it ends) it's usually the developers that are blamed (see above about blame) for a project either not being delivered on time or containing too many bugs, while often these can be prevented by creating a clear context.

### Communication

Communication is not just about goals and context, it is also about expressing emotion. Whether that emotion is gratitude, happiness, disappointment or anger, it is usually better to communicate than not to communicate. Of course the communication should always happen with respect for the other party, not as a one-way communication, but being able to communicate about any emotion related to the project gives the other party a clear idea of what is going on. 

From the developer point of view it is extremely important to hear about what makes customers (or managers) happy, but also what they don't like. This may sound obvious, but I've found that in a work environment emotions are often hidden, causing a gap between developers and those judging a project. The gap will grow bigger and bigger if nobody states how they feel. Even successful projects and teams can start to feel bad for a developer if they get no feedback on their work, and they'll feel alone and perhaps start looking around for another job where they get the feedback they need. Same goes for negative (constructive) feedback. If you know you messed up but nobody says anything, you'll get the feeling that people are avoiding you. Given most developers look for a place where they feel welcome, where they feel at home, things like this may cause them to look for another job.

### Respect

This may seem obvious, but people like to be treated with respect. Developers are no different. And yet, I've seen so many organizations that do not treat their developers with respect. 

Now even the best empath may have a hard time being empathetic towards a developer, simply because developers are different. They can be nerdy, geeky, have completely different thought patterns and their minds may simply work in a different way from yours. Many developers I know will care less about getting a raise and more about, for instance, getting a good standing desk or an extra monitor for their workplace. Some developers really need a good mechanical keyboard for their work and care more about that than an extra monitor. 

But even if you can't empathize with them, if you don't understand them, they do want to be treated with respect. If you don't listen to their needs, if you don't respect them for their work, there's a good chance they'll move on. They'll find a better place. So please treat developers like you treat any other human (assuming you treat all humans alike, and all humans with respect). 

## Now apply this

It is not easy to create a good company or team culture. It should be really easy though. The key is to respect and trust the people you have working for you to do the job they were hired to do. Developers coming in for an interview have ways of figuring out if a team or company feels right. If they feel you've got your stuff sorted, they'll join your company. And if it turns out you actually got your stuff sorted, they'll stick around. Imagine all the money you don't have to constantly invest into recruiters and recruitment when people stay with you for a long time... if you invest part of that money into your developers, into the team and company culture, you'll have saved yourself a lot of money.

Now people leave. This is a fact. Especially developers will sometimes simply need a new challenge. But even a developer that is leaving can give you good insights into how to further improve your company. Listen to those who leave, and learn from them. 

Good luck building that awesome team and company culture. You can do it.