---
layout: post
title: "Changing jobs"
date: 2007-09-30 12:31
comments: true
categories: 
  - personal 
---
Ivo Jansch from Ibuildings and I have had contact over the years, and I think he mentioned a few times over those years already that he was interested in getting me aboard at Ibuildings. Every time I said no, mainly because the travel time to Vlissingen or Sittard is way too long.

My response to <a href="http://www.khankennels.com/blog/index.php/archives/2007/08/27/what-company-would-you-work-for/">this post</a> prompted Ivo to try again. I received an e-mail offering me to work on the Zend Professional Services, and combine this with my PHP love of my life: Symfony. I would be able to combine my symfony work with a very cool job as Zend Professional Services consultant! Now, how cool does that sound?

But, the travel time was still an objection for me. Luckily, Ivo had a solution this time: I would be able to work from home. Now, you can't offer less travel time than that! 

Over the past weeks, Ivo, Tom and Joni from Ibuildings and myself had two very interesting talks, and we decided we could agree on everything. And so, starting from November 1st I will be joining the ranks of <a href="http://www.ibuildings.nl/">Ibuildings</a>.

This does mean I will be saying goodbye to Dutch Open Projects, a company where I've worked for a year with lots of fun and great achievements, such as the recent SymfonyCamp event. It's sad to say goodbye, but I'm looking forward to something very nice. A great new challenge for me!
