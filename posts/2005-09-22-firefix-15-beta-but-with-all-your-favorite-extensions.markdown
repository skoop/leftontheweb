---
layout: post
title: "Firefox 1.5 beta, but with all your favorite extensions"
date: 2005-09-22 7:30
comments: true
categories: 
  - technology 
---
Earlier on, I <a href="https://skoop.dev/article/136/firefox-15-the-first-beta">posted about the Firefox 1.5 beta</a>, and my main problem being that most of my extensions didn't work with this beta. Luckily, thanks to <a href="http://www.ddblog.org/">Dominik</a>, I now know about the <a href="http://users.blueprintit.co.uk/~dave/web/firefox/buildid/nightly.html">Nightly Tester Tools</a>, an extension which will re-enable all your extensions. Works like a charm! I'm now happily surfing with Firefox 1.5 beta 1! :)
