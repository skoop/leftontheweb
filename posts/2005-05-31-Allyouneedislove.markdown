---
layout: post
title: "All you need is love"
date: 2005-05-31 2:22
comments: true
categories: 
  - leftontheweb 
---
All you need is love. That's today's <a href="http://www.phpbb.com/" rel="tag">phpBB</a>-related theme. Huh? Yeah, really. Because <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=294419">letha found her true love</a> through the phpBB forums. She asked for support, someone helped her, and now they're getting married!

A funny way of meeting your true love, if you ask me. But if it happens, it happens. As this thread proves.
