---
layout: post
title: Regenerative software development
date: 2024-03-18 16:00:00
comments: true
categories: 
  - php
  - software
  - regenerative
  - stress
social:
    highlight_image: /_posts/images/regenerative.png
    highlight_image_credits:
        name: Marjolein van Elteren
    summary: Companies can learn from Extinction Rebellion. Not just about how we should treat our planet, but also how we should treat our people. How we should treat ourselves.
    
---

I have been under a lot of stress. Not just in work, also in my private life. I'm also very worried about the state of the world around us, the community we live in. And I am someone that tends to worry. I have a hard time with the mantra "grant me the serenity to accept the things I cannot change; courage to change the things I can; and wisdom to know the difference." I have always been feeling stress but lately it's been higher than ever.

## Courage to change the things I can

As part of wanting to change the things I can, I have been actively protesting during [Extinction Rebellion](https://rebellion.global) protests in The Netherlands. I've even taken on an active role (Arrestee Support, which is an awesome role to have). And more recently, I've helped start [a local chapter of XR](https://extinctionrebellion.nl/groep/utrechtse-heuvelrug-woudenberg/). If you're in the Utrechtse Heuvelrug or Woudenberg municipalities and you want to join, come join us!

## Regenerate!

Last year, XR blocked a highway in The Netherlands for weeks on end to protest against fossil subsidies. At first everyone was very ready for it. While during the week the group was smaller, during weekends the group grew bigger. But as time went on, the group became smaller. This wasn't because people didn't want to protest anymore. On the contrary. This was because of one of the principles of XR: They are aiming to have a [regenerative culture](https://xr-regenerativecultures.org).

This does not just mean that the way we treat the earth should be in such a way that our planet can recover from the pressure we put onto our planet. This also means that we as humans beings should treat ourselves in such a way that we can recover from the pressure we put on ourselves.

Why? Because you can not be constantly sprinting. So we need to recognize when we run into our boundaries, preferably before we actually run into them, and recover enough so we can get back on our feet and again take action.

## From Extinction Rebellion to software development

I see a similar thing happening in software development. Whether in open source development or in commercial services: We try to keep sprinting. We try to keep the high pace going, hoping we'll get to the finish as fast as possible, just so we can continue with the next race immediately. 

For open source, that means constantly working on the next release. Through specifying what needs to be done, actually building it, testing it, putting the release out then moving on to the next feature on the list.

For commercial software development, that means going from sprint to sprint. Doing your demo, then moving on to the planning of your next sprint and immediately starting it. If you're lucky, you have a retrospective in between. But even the retrospective can be intense, since it is meant for honest feedback. As a software developer, most of your work is done with your head. A retrospective is also done with your head. There is no rest.

## How can we do this?

With [burnout being a major issue amongst developers](https://www.itpro.com/development/software-development/360192/83-of-developers-suffer-from-burnout) we really need to take better care of ourselves. This includes taking care of our boundaries and making sure we don't cross them, at least not for longer periods of time, but also taking care of each other. And identifying where we can improve. 

### Is stress a bad thing?

We often talk about stress as a bad thing. This is because usually we don't start talking about stress until it actually becomes a problem. But by then, it might already be too late. We should realize that there is [good stress and bad stress](https://www.verywellhealth.com/good-stress-vs-bad-stress-7373843), and there is no single definition of what is good and bad. As Mira Miller writes in the linked article: 

> Two people can experience the exact same event—such as a new project with a tight deadline—but one of them might experience it as an opportunity to take on an interesting challenge while the other one might automatically predict that they’ll fail.

So first of all, we need to make sure that we recognize the bad stress in ourselves and each other early enough. If you recognize it early enough, you can prevent issues such as burn-out, but also lower efficiency due to prolonged stress. 

### Make sure you're not "always on"

First, we should all realize that being a software developer does not mean that you're writing code 100% of your working time. And with `we should all`, I mean, all of us. This includes developers, but also project managers, stakeholders and company management. Years ago I did a keynote at several conferences titled [Developers are just like humans](https://speakerdeck.com/skoop/developers-are-just-like-humans-phpnw15). Developers are not just a "human resource". They are first and foremost _human_. 

This also means that we should not feel guilty for taking a break on a regular basis. It is OK to stand near the watercooler talking to someone. And that talking does not necessarily need to be about work. It could also be about what you did last weekend, or your plans for tonight. Or the fact that your kid just starting walking and how funny it was when they fell on their butt when they tried. Or how you won another round of Fortnite last night.

We should also not feel guilty for taking other measures to ensure you're not overloaded. When you're stuck on this annoying bug you can't replicate or how to implement that new architectural pattern you need for your new project, take a step back. Actually, take a lot of steps. Go outside and walk for 30 minutes. You'll feel fresh and there's a chance you'll even find the solution to your problem quickly after you get back. Your head sometimes needs the time to process what you're doing.

### Full sprint?

To retain a sustainable pace in your software development, it could also be useful to not just schedule sprint after sprint. Sure, it's OK to do 2-3 sprints of 2 weeks each right after another. But every once in a while, schedule in a week without sprint. This does not mean you don't have to do work that week, but it's good to be clear of the pressure of a sprint every once in a while. Take the week to do that refactoring you've been postponing because the sprint work was more important. Or step back and (re)consider your current architecture. Does it still work? You could even use the time to thoroughly refine and prepare the stories for the upcoming sprint(s). Most importantly: Don't expect too much of this week. Work will get done, but don't schedule your whole week and expect everything to be fully done. We want to get away from the regular work stress, right?

And I already hear the finance person, the project manager and the management: "But that week costs us a lot of money!". Yes, but as said, the work gets done. And regenerating is very useful to make sure that in the sprints that follow, the developers can again put in the sustainable pace that is expected of them. Besides, that refactoring or thinking about the architecture or writing of missing tests or whatever other thing that is done during that week, that's work that should also be done. By having that done during that one week "off" sprints, your application is better prepared for the future, and you'll save time in the long term.

### Retrospect

In agile we learn that the retrospective is a moment to evaluate our process and discuss possible improvements. I would say that the process is not the only thing that needs evaluation. During the retrospective, also take some time to evaluate the impact work has on each team member. Make sure that everyone is still OK and ready to move on to the next sprint. And you don't even just have to do that during the retrospective. Your daily standup could start with a short check-in for each team member: How are you feeling today? Are you ready for the new day?

### A developer is more than just that human resource

What I mean with this is that a developer is a developer during working hours. But outside of working hours, they also have a life. And things happening outside of working hours can also affect work. It is naive to think it won't, and it's unethical to expect it not to. Developers are not robots, they're human beings.

So, we need to make room for being able to deal with that. It's much more efficient to every once in a while give a developer one or two (paid) day(s) off to handle private matters well than to have a developer that is distracted for a longer period because they have to handle those private issues only after a long and possibly stressful workday. Sure, there are limits to this and if something needs more time then it is OK to see if they can take some days off from their vacation time (I realize this also really depends on the country and how much vacation days someone gets, but my main point is: this can be a conversation. As employer, do what you can but also communicate clearly when what you can do ends).

### Know what the boundaries are

As a developer, it's good to find out what your boundaries are. From experience, I know that you'll cross your boundaries a couple of times because you figure out where they are. But be mindful of when and where you cross your boundaries, and communicate with your team and manager when you cross your boundaries. It is in the benefit of everyone involved to understand where they are and when to take a breath. 

Likewise, just like you keep an eye on yourself, keep an eye on the rest of your team. There are [signs of stress](https://www.mind.org.uk/information-support/types-of-mental-health-problems/stress/signs-and-symptoms-of-stress/) that you can recognize in yourself or others. The best way to prevent burn-out and other stress-related issues such as inefficient work but also physical issues such as carpal tunnel syndrome is to create a culture where it is OK to take a step back for a bit, to go outside for a walk during work hours, to regenerate. 

## Back to XR

And with that, we circle back to Extinction Rebellion. While I did my training for the A12 protests in The Netherlands, the trainer stressed in several ways that as an XR activist, you have a responsibility to yourself. You should clearly recognize when you need to step back. Regenerate. And it is OK to also talk to other about this. 

As a business we can learn from XR. Not just about paying attention to our global footprint, but also to create a culture of regeneration for our team members. Because your team members are more than just people who do work. They're first and foremost human beings. And no one gains by completely burning them out, just like no one gains anything by burning out our planet.

## Regenerative software development

So let's work towards a better, more healthy culture. A culture where there is room to regenerate, that is not focussed on burning people out and then replacing them with someone new. Remember: It is a lot cheaper to retain your existing developers than to hire new ones.