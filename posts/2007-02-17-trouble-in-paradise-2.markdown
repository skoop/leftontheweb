---
layout: post
title: "Trouble in Paradise"
date: 2007-02-17 11:24
comments: true
categories: 
  - technology 
---
In the period of a few hours yesterday, three new PHP frameworks were announced on <a href="http://www.planet-php.org/">Planet PHP</a> and <a href="http://www.phpdeveloper.org/">PHPDeveloper</a>. It was really something I noticed, that this happened in such a short timespan. I was clearly not the only one who noticed. <a href="http://www.gravitonic.com/blog/archives/000359.html">Andrei</a> and <a href="http://killersoft.com/randomstrings/2007/02/16/rails-free-living/">Clay</a> also noticed and wrote about it in pretty clear words. And as Andrei and Clay noticed, this is not necessarily a good thing. This might even be seen as a Bad Thing.

While I never have any problems with developers starting their own framework, I truely believe that it is, in most cases, not a necessary step. Instead of starting your own framework, you could also start working with an existing framework and simply extend the functionality: That is the purpose of a framework. You could even contribute the extensions to the framework, thus ensuring the further development of the framework you're using.

<a href="http://stubbles.org/archives/4-Pavlov.html">Frank from Stubbles</a> decided to "fight back". Even though I agree mostly with Andrei and Clay, Frank has a very good point. One I've been trying to make <a href="https://skoop.dev/article/269/php-specification-standard">earlier</a> as well. Of course, my humble opinion is not probable to be picked up, but with the attention that this "war" is getting, it would be nice if Zend or another leading organization would actually pick up on this. PHP needs specifications for standard functionality. Without it, there will be little unity in the world of PHP. With it, frameworks will be even more usable, and everyone will have an easier time developing.
