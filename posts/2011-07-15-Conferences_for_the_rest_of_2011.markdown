---
layout: post
title: "Conferences for the rest of 2011"
date: 2011-07-15 5:58
comments: true
categories: 
  - conferences 
  - php 
  - symfony 
  - zendcon 
  - symfonyday 
  - phpnw11 
  - pfc11 
---
<h2>PFCongres</h2>
<p>Earlier in the year I stepped back from <a href="http://phpbenelux.eu/">PHPBenelux</a> and joined the ranks of the <a href="http://pfz.nl">PFZ.nl</a> event team. Of course, with PHPBenelux we just had our annual conference, so I was already involved in organizing one, but PFZ.nl did not yet have theirs. That one is now scheduled for September 17. I am not speaking there, however I am helping it organize, so it will be a busy day. :)</p>

<h2>PHPNW11</h2>
<p>The conference that I bought a ticket for before I even knew I was speaking, because I knew for sure I wanted to attend this one regardless. Manchester usergroup PHPNW has their <a href="http://conference.phpnw.org.uk/">annual conference</a> on October 8th and 9th (with a seperate training day before that, but I won't be there). It should be lots of fun (as it always is) and I'm doing not one but two talks there (one on Symfony2, the other on Git). If you're in the neighbourhood (and in this case, Europe is the neighboudhood, it's not *that* expensive to fly to Manchester ;) ) then definitely come to this conference. It's awesome!</p>

<h2>ZendCon</h2>
<p>After 4 or 5 years of submitting proposals, I'm proud to be able to attend and speak at <a href="http://zendcon.com/">ZendCon</a> this year. I will be doing the same Symfony2 talk there that I'm doing at PHPNW just one and a half week before. I am really excited and looking forward to being in Santa Clara with all the other ZendCon-ians. The schedule looks awesome, and I'll meet lots of friends, old and new, at the conference for sure. I will be flying out before the end of the conference though, because of...</p>

<h2>SymfonyDay</h2>
<p>The <a href="http://symfonyday.com/">annual symfony conference</a> in Cologne organized by <a href="http://interlutions.de/">Interlutions</a> will be lots of fun again. This conference is the only reason I was not able to attend PHPNW last year, and the fact that I'm skipping PHPNW means something here. The best of the best of the symfony world will be there, and with a brand new training day the day before the conference, symfonians should try to make it to Cologne on October 20 and 21. I won't make it to the training day because I'm probably somewhere up in the air at that point, but I will make it to the conference, I know this for sure. Whether I'm speaking, I don't know yet, the CfP runs until the end of this month. You can still <a href="http://www.symfonyday.com/en/program.html">submit if you want</a>!</p>

<h2>Taking it easy</h2>
<p>There were lots of other opportunities (OSI Days in India, International PHP Conference in Germany, Australia has a new conference coming up, just to name a few) but I decided earlier in the year that I would take it easy the rest of the year. Easy, in this case, is not that easy, because most conferences are all in a period of about two weeks time, but well, that means I can take it easy before and after that.</p>
