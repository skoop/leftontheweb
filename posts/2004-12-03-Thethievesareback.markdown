---
layout: post
title: "The thieves are back"
date: 2004-12-03 9:23
comments: true
categories: 
  - leftontheweb 
---
A few months ago, through various forums and mailinglists, I was alerted of the presence of a site called JetGroove.com that was selling mp3 downloads of independant artists and labels, without the permission to actually do so. The site currently only has a temporary page telling visitors it's "down for maintainance".

In the meanwhile, a forum that I am responsible for got spammed with a promotional message for a site resembling the old JetGroove so much that it couldn't be a coincidence. It is the exact same site design, and a very very similar catalog of independant music. They only rebranded the site!

So here is my warning. Please, please do NOT buy music from www.audio1.org. Buy the music directly from the labels and artists! They need the money much harder and actually deserve the money you're spending.
