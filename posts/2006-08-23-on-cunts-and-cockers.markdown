---
layout: post
title: "On Cunts and Cock(er)s"
date: 2006-08-23 12:23
comments: true
categories: 
  - music 
---
<a href="http://en.wikipedia.org/wiki/Jarvis_Cocker">Jarvis Cocker</a>, probably best known for his role as lead-singer of the British band <a href="http://en.wikipedia.org/wiki/Pulp_%28band%29">Pulp</a>, can also survive on his own. He's proved that to me by a new single he released, Running The World.

I got the track <a href="http://www.bleep.com/current_item.php?selection=RTRADSCD369_DM">from Bleep</a> for a wonderfully nice price of 1.40 euro and it was worth every penny. A wonderful track about the state of this world, and the fact that those running the world are, as the British can so wonderfully call it, <a href="http://en.wikipedia.org/wiki/Cunt">cunts</a>.

I can highly recommend getting this wonderful track.
