---
layout: post
title: "Introducing IngewikkeldWrapperBundle"
date: 2011-12-21 22:11
comments: true
categories: 
  - symfony 
  - Symfony2 
  - php 
  - bundle 
  - opensource 
  - migration 
---
<p>The IngewikkeldWrapperBundle adds a fallback route: Any URL not caught by your Symfony2 code will be forwarded to your old symfony 1 project. This allows for gradual migration to Symfony2. For an explanation of the thought behind all this, please check <a href="http://www.leaseweblabs.com/2011/12/painless-well-less-painful-migration-to-symfony2/">this post on the Leaseweb Labs blog</a>, where I've guest-posted about this way of migrating from symfony 1 to Symfony 2.</p>

<p>The IngewikkeldWrapperBundle is currently in a proof of concept-phase, but works already for one of my sites. I welcome any feedback and contributions :)</p>
