---
layout: post
title: "What I learned from the Zend/Rogue Wave acquisition (or: why I'm so excited about the Github/Microsoft deal)"
date: 2018-06-06 08:20:00
comments: true
categories: 
  - microsoft
  - github
  - zend
  - roguewave
social:
    highlight_image: /_posts/images/GitHub_Logo.png
    summary: Some years ago when Zend was acquired by Rogue Wave I was openly scared for the future of PHP. I learned from that.
    
---

When [Zend](https://zend.com/) was acquired in 2015 I was openly and vocally scared for the future of Zend (and PHP). I honestly thought that this deal would mean Zend would be absorbed within [Rogue Wave](https://roguewave.com) and that would be the last of it. A friend DM'ed me to warn me that doing this so openly might make it a self fulfilling prophecy, not necessarily because of Rogue Wave but because companies could lose trust in PHP.

Fast-forward three years and PHP is still stronger than even and if anything we're getting more instead of less from Zend: Their products are still going strong and ZendCon has been turned into [ZendCon & Open Enterprise](https://zendcon.com/), broadening the scope of the conference and thereby making it more interesting for developers.

I didn't know Rogue Wave back in those days, which is what made me a bit scared. Basically, I was doing what I always tell people not to be doing: Being scared of the unknown. Getting out of that comfort zone is a good thing (mostly). I shouldn't have reacted in this way.

## About the Github acquisition

Now I'm seeing a lot of people being scared by [Microsoft](https://microsoft.com/) acquiring [Github](https://github.com/), and the funny thing is that I feel no fear. Part of that is probably because I know Microsoft and while they have a past of very bad behaviour when it comes to open source, they've changed a lot in the previous years. Sure, I also make jokes about Windows sometimes, although those jokes are always based on what Windows was years ago, but overall their current leadership seems very aware and open to the concept of open source.

Another reason I'm not scared is that at this point [Github does not seem profitable](https://developers.slashdot.org/story/16/12/16/1639222/building-a-coders-paradise-is-not-profitable-github-lost-66m-in-nine-months-of-2016). A good financial injection from a big company that has no problem investing some money is something like Github is what they may need right now. Sure, things will change after the acquisition has closed, but probably only to make Github profitable again. I trust Microsoft to understand what Github is about and how to run the company.

## Moving to Gitlab

Now, there are enough reasons to move to [Gitlab](https://gitlab.com). For instance their great CI/CD tooling, their tight integration with Docker or one of their many other features. The fact that they run a very transparant and open company (including the Gitlab codebase itself) can be another good reason. My company has mostly migrated to Gitlab already because of our great Gitlab/Docker/Rancher setup. Moving because of the acquisition of Github is probably the worst reason though. Keep in mind that [Gitlab has Google VC](https://www.crunchbase.com/organization/gitlab-com#section-locked-charts) so moving to Gitlab does not mean you're now hosted by an independant Git hosting company.

Having said that, I hope that all those people that migrated their codebases to Gitlab will find out about the awesome features they offer that Github does not offer. You'll have to get used to their interface, but Gitlab is awesome.

## Back to Github

Many people are predicting doom and gloom for Github. Open source repositories should be moved or else..., Microsoft would go and have a look at your private repositories. I see no reason why any of that will happen. Github will still be Github. Of course they will keep your codebase safe and won't look at the contents of your repository: That is their core business. If they would do stuff like that, 99% of their customers would be off their service in no-time.

So to all developers who are scared after the news of the acquisition broke I'd say: Give Microsoft a chance. The company has changed a lot and they deserve a chance to prove what they're worth. So if Github is working for you, there is no need to move away. As said, there are good reasons to move to Gitlab, but please move to Gitlab for the right reasons, not for your fear of Microsoft.