---
layout: post
title: "Community is more than PHP. It is the world"
date: 2013-10-11 10:00
comments: true
categories: 
  - php 
  - community 
  - kiva 
  - microfinance
---
This blogpost has been on my TODO-list for a couple of YEARS now, so I thought it was time to finally write and publish it. It is actually not at PHP, but I hope all you PHP devs do stick with me and read the whole blogpost, as I think it is about a topic we all care a lot about: community.

Community is a term that can be interpreted in many ways. Within the global PHP community we have national communities, regional communities, local communities and communities based on other properties: Which technology you work with, which gender you are, etc. I would like to invite you to look beyond any of these communities, and look at the world around you. Not just the town you live in, not just the country you live in, but at the whole world around us. If you put effort into (or get something out of) the PHP community, why would that not work on a more global scale as well?

I made my first microfinance loan on July 2nd, 2008. I came by [Kiva](http://kiva.org/) through a friend, and decided to invest $25 into [a restaurant in Cambodja that needed expanding](http://www.kiva.org/lend/51659). After about 14 months, the $25 I had put into the loan was back into my account. After the money was back into my account, I immediately made another loan, this time to a [farming group trying to employ the young villagers](http://www.kiva.org/lend/135219). A couple of months later, I decided it would be good to invest more, not keep it with the $25 I had invested, so I put in another $25. This time round, it was [another farmer](http://www.kiva.org/lend/165170), but from the Philippines (currency: PHP!). 

By now, I've made 20 loans, and invested a total of $100. Out of the 20 loans, only one loan failed to fully pay back the loaned amount. I've been lucky so far I guess. Yet I keep on giving out more loans. The thing is: $25 is not much for the average PHP developer. Our rates and wages are pretty high. We can usually spare $25. Or $50. The average Kiva user has invested $43.40, if I understand the numbers correctly. The average PHP developer should be able to get that average up a bit.

With the $100 I've invested, I've now given out a total of $500 in loans. Isn't that amazing? With a little bit of money, I've helped many projects get funded. 

I know there are already many PHP devs on Kiva. There are currently 27 members in the [PHPeople team](http://www.kiva.org/team/phpeople) and 13 in the [Symfony Team](http://www.kiva.org/team/symfony_team). I'm pretty sure there are other teams as well related to PHP. I would like that amount to grow. As a PHP community, let's try and give something back to the whole world, not just our own community. I realize there are many people inside our community that can be helped, but let's also look beyond our language, our country, our neighbourhood. 

I made a loan yesterday [to a lady that wants to start her own business](http://www.kiva.org/lend/613742). Why don't you also [sign up to Kiva](http://kiva.org/) and put in some money. Even if it is only $25, you will help someone realize their dream, help someone improve their own life. For only $25.
