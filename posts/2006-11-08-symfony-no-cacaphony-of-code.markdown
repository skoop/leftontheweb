---
layout: post
title: "Symfony: No cacaphony of code"
date: 2006-11-08 12:04
comments: true
categories: 
  - technology 
---
After giving it multiple quick glances, today a co-worker of mine and me sat down a full day to work through the first part of the <a href="http://www.symfony-project.com/askeet">Symfony tutorial</a> in an attempt to learn to work with <a href="http://www.symfony-project.com/">Symfony</a>. The tutorials are meant to take about one hour each, and that is a good approximate. After some start up problems trying to configure pear and php, we were able to work through the first seven tutorials, which are probably the most important of all 24.

Symfony is a very impressive system. It is more than just a framework, since it takes care of a lot more, including database population, complete <acronym title="Model View Controller">MVC</acronym> approach using the URL, and much, much more. Knowing Symfony will make developing webbased applications so much easier.

So now I'm sitting down to play with it some more, to see what can all be done to get this beautiful system working and to develop new applications with it.
