---
layout: post
title: "A small SimpleXML gotcha (with namespaces)"
date: 2008-05-30 19:07
comments: true
categories: 
  - simplexml 
  - xml 
  - namespaces 
  - php 
---
<p>It was my colleague <a href="http://www.dynom.nl/" target="_blank">Dynom</a>  who saved the day for me. He pointed me towards the second optional parameter of the <a href="http://nl.php.net/manual/en/function.simplexml-element-attributes.php" target="_blank">attributes()</a>  method of the SimpleXMLElement object. Given the following snippet (within a larger snippet):</p><blockquote><p><a href="/weblog/edit/id/something_or_other">a text</a></p></blockquote><p>I was using the first parameter of attributes (the namespace) to let SimpleXML know from which namespace I wanted to get the attributes:</p><blockquote><p>$attributes = $xml-&gt;a-&gt;attributes(&#39;xlink&#39; );<br />echo $attributes[&#39;href&#39;];</p></blockquote><p>This didn&#39;t work. $attributes[&#39;href&#39;] didn&#39;t exist, and var_dump-ing the $attributes array returned an empty array. However, clearly I should learn to read better, because the manual does mention the second parameter $is_prefix, which is a boolean that indicates if the attributes are actually prefixed by the namespace. As, in the above case, this is true, I have to set it to true (it defaults to false). So with this it worked:</p><blockquote><p>$attributes = $xml-&gt;a-&gt;attributes(&#39;xlink&#39;, true);<br /> echo $attributes[&#39;href&#39;];</p></blockquote><p>These are the kinds of details that you can get stumped on for ages, so I hope that people reading this will keep it in mind and remember it when they encounter a similar situation :) </p>
