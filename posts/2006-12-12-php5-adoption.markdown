---
layout: post
title: "PHP5 adoption"
date: 2006-12-12 1:08
comments: true
categories: 
  - technology 
---
Ilia <a href="http://ilia.ws/archives/147-Why-are-you-not-using-PHP-5.html">wonders why people are not using PHP5</a>. A good question, which I even asked of the Joomla! developers a while ago. The answer from Joomla! was predictable and logical: Hosting providers have not adopted PHP5 enough to warrant the switch as of now. Joomla! 2.0 will probably be PHP5, but not earlier.

According to Nexen, only <a href="http://www.nexen.net/chiffres_cles/phpversion/statistiques_de_deploiement_de_php_de_novembre_2006.php">just over 12% of all PHP installations are PHP5</a>. For a PHP version that has been available for over two years already, that is a very bad score. And I truely don't understand why.

Some of the reasons I've heard are:

* Hosting provider does not want to upgrade because it breaks compatibility with PHP3 applications client is using
* PHP4 is stable, why change a winning team?
* Upgrade contains risk

That first reason is scary already, and I've really heard that one, though that was about a year ago. Someone still running on PHP3 really doesn't want to do anything good I guess.

The second reason is the one I can accept, even though there are tons of reasons to go to PHP5. For hosting providers who don't do development, I can perfectly accept this reason. As long as PHP4 is supported and new versions of that come out, there is no need to make the move.

The last reason is stupid. There is little risk in upgrading, especially with the good documentation all around the Internet on upgrading.

For development companies however, there is no excuse anymore. Especially, as Ilia mentions, since PHP 5.2 is more performant as any previous PHP version. That, combined with all the new options in PHP5, such as SimpleXML, the new object model, and you name it, there is no reason to stick to PHP4. With PHP5, PHP made a truely big step from a scripting language towards a real programming language, and now it's time for developers to do the same.

I am trying to contribute to this by trying to get my current employer to do all future development in PHP5. Since we do a lot of hosting of our clients ourselves, this should not be too much of a problem. Since the new Joomla! 1.5 is written with at least forwards compatibility in mind, hopefully we should not have too much trouble with this approach.
