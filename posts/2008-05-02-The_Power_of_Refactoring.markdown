---
layout: post
title: "The Power of Refactoring"
date: 2008-05-02 11:00
comments: true
categories: 
  - meta 
  - refactoring 
  - bestpractices 
---
<a href="http://www.ibuildings.com/blog/archives/921-The-Power-of-Refactoring.html" target="_blank">The Power of Refactoring</a>    hopes to give some insight into why refactoring is actually a very good idea. Feedback is always welcome, either here or in the comments of the actual blog post.
