---
layout: post
title: "PHP5 community effort"
date: 2007-06-29 0:27
comments: true
categories: 
  - technology 
---
It seems, finally, after 3 year and a bit of having PHP5 available, the mass of community developers is starting to see the light. There is a <a href="http://www.hiveminds.co.uk/node/3409">community effort under way</a> to move big projects over to PHP5-only. This may finally be the finishing blow to that endless PHP4 fighting. PHP4 is bad, it's outdated, it should have been dropped ages ago. When most of the big software outthere moves to PHP5-only, hopefully it will quickly be dropped. PHP6 is already on the horizon, so it's time to at least move to PHP5 and make use of all the great features in that.
