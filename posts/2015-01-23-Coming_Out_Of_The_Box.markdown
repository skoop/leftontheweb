---
layout: post
title: "Coming Out Of The Box"
date: 2015-01-23 12:30
comments: true
categories: 
  - php 
  - theartofasking 
  - help 
  - asking
---

Asking for help. Once you start thinking about it, it is really hard to do. It doesn't really matter what you need help with, it is hard once you start wondering whether you should ask.

In [The Art of Asking](https://skoop.dev/blog/2014/12/24/the_art_of_asking/) Amanda Palmer writes about Anthony. Anthony is a professional therapist who helps people in hard times. Anthony is also Palmer's best friend. He helps her a lot, either by listening, by asking questions or by giving advice. But once Anthony himself is in a dire predicament, all of a sudden, it was hard for him. She writes about this:

  > He likes being in control, he loves having answers, he loves fixing and helping people. But he has a really hard time letting people help *him*. Sometimes, when he gets depressed, he shuts down and doesn't like to talk. When that happens, I figure it's time for me to step up, ask him questions, help him through, talk about the problems. Be he clams up and doesn't like talking to anyone about his own problems. He calls it Going Into The Box.
  
Triggered by both this book and events in my own life, I've been thinking a lot about "asking", and have been thinking about how I handle asking, and I've come to an interesting conclusion. Asking is easy if you just do it. Once you start thinking about it, it becomes hard.

I really recognize myself in Amanda Palmer's description of Anthony. I, too, have (had?) a tendency to completely turn inward once I have a hard time. The trigger can be anything from worries over something to stress, but not even my wife will be able to get through to me in such a situation. Why? Because I don't want to ask for help. I don't want to be a nuisance to other people, don't want to bother them with my own problems. Recently though, I've started talking about it to my wife, asked her for help coping with something, and it turns out that once I do that, the problem gets much easier to handle. I can cope.

On the other hand, when you don't really think about it, asking is easy. In the preparations of [WeCamp](http://weca.mp/), we were so excited about our concept and the way things were shaping up that without much hesitation I sent some people an e-mail asking them to be a coach at the event. Being a coach would basically mean "being on an island working for a week, and not getting paid for it". But I didn't think before asking, I just did it. And the response was excellent. Not everyone I approached could to it, for a variety of reasons, but I did not have a hard time asking. Why? Because I did not think about it before asking. I just sent out the e-mails and did it. Because I was really excited about what we were trying to do.

I'm trying to train myself to ask more. Sometimes, this may be annoying but I've found asking works. If people do not have the time to help or even answer your question, they'll just let you know. Most people actually love to help once they're asked. I know I do, which makes it even stranger that I didn't understand that asking would probably result in the same help I give when someone asks me.

Try to be like Anthony when it comes to helping. Try to avoid being like Anthony when it comes to asking help. Come out of that box.
