---
layout: post
title: "Two great evenings"
date: 2008-04-22 21:53
comments: true
categories: 
  - php 
  - calevans 
  - event 
  - ibuildings 
  - sogeti 
---
<p>Yesterday evenings I went for a diner with <a href="http://www.jansch.nl/" target="_blank">Ivo Jansch</a> , <a href="http://www.calevans.com/" target="_blank">Cal Evans</a> , <a href="http://dragonbe.blogspot.com/" target="_blank">Michelangelo van Dam</a>  and <a href="http://www.wolerized.com/" target="_blank">Remi Woler</a> . We went to this small restaurant in Amsterdam called &#39;<a href="http://www.moeders.com/" target="_blank">Moeders</a> &#39;, had a great dinner, lots of awesome geeky php discussions, and an allround great time. After diner we sat down at the hotel Cal and Ivo were staying at to have another drink. I was home late (1.30 am) but it was worth it.</p><p>Today, that same Cal came over to speak at a <a href="http://www.phpseminar.nl/" target="_blank">PHP Seminar</a>  organized by my employer <a href="http://www.ibuildings.nl/" target="_blank">Ibuildings</a>  together with <a href="http://www.sogeti.nl/" target="_blank">Sogeti</a> . During the day, the seminar was mainly aimed at managers and tech people from clients of Ibuildings or Sogeti, but during the evening there was a special tech evening for Ibuildings and Sogeti personnel and a few selected invitees. We first had a nice diner, and then went to watch sessions by Peter Verhage (Ibuildings), Robert van der Linde (Sogeti) and Cal Evans (Zend). All three sessions were very nice, though by far Cal Evans was the most enjoyable (to the defense of Peter and Robert, Cal probably has about 100 times more experience in speaking). Afterwards, we had a drink, and then the evening was over.</p><p>Cal is just an allround cool guy, with the coolest job on the planet. I really enjoyed hanging out with him and listening to his &quot;gardening&quot; session. I think Ibuildings and Sogeti are awesome for getting him over and getting an event like this organized. I&#39;m proud of working for Ibuildings :) </p>
