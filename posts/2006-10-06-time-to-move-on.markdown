---
layout: post
title: "Time to move on"
date: 2006-10-06 5:26
comments: true
categories: 
  - personal 
---
Starting on tuesday, I will not work for <a href="http://www.tomtom.com/">TomTom</a> anymore. Instead, I will be working for a very nice company called <a href="http://www.dop.nu/">Dutch Open Projects</a>. 

Over the past weeks I've transferred as much knowledge as possible to my colleagues, I've either tried to solve issues myself or assigned the issues to colleagues. The result is now visible:

<img src="/images/35.png" alt="No more issues!" style="height:212px;width:439px" />

It seems I am ready to move on.
