---
layout: post
title: "The DPC Uncon"
date: 2010-06-08 19:23
comments: true
categories: 
  - php 
  - dpc10 
  - conference 
  - uncon 
  - phpbenelux 
---
<div>The DPC Uncon is very similar to the uncons found at other PHP conferences. There is little new about this uncon. However, it is a new addition to a conference with such a list of advanced topics that there is no doubt the uncon will also host a variety of interesting topics for people visiting the Dutch PHP Conference as well.&nbsp;</div><div><br /></div><div>We at PHPBenelux are honored to be the hosts of the first DPC Uncon. What this means is that we&#39;ll manage the registrations of speakers into the schedule, and we will also try to make sure that all speakers are at the uncon room in time for their chosen timeslot.</div><div><br /></div><div>We will open registrations for the uncon speaker slots every morning as the doors open somewhere near the entrance/registration booth for the main conference. To give as many people as possible a chance to register, we will only schedule people for that specific day, so on friday you&#39;ll only be able to grab a friday slot, and on saturday you&#39;ll be able to grab a saturday slot. If you have a topic you&#39;re passionate about or an open source project you want to present to the world, then make sure to grab one of the uncon slots. Slots are limited, so make sure to come early if you want one.</div><div><br /></div><div>So if you wonder whether you need to be an experienced speaker for the uncon: No, not at all! An uncon is a great opportunity for less experienced speakers to get some experience at speaking in a conference situation. If you want more information on the how and why of speaking, I suggest <a href="http://www.phpbenelux.eu/en/node/1331" target="_blank">the article I published at the PHPBenelux website about uncons</a>. Also, if you&#39;re still in doubt, just talk to the PHPBenelux crew, or attend the first session in the uncon, which will be an introduction to the hows and whys of speaking at a (un)conference by Michelangelo van Dam and yours truly.</div><div><br /></div><div>I hope to see you all on friday, let&#39;s make the DPC Uncon a great success!</div>
