---
layout: post
title: "Another good thing about this new hosting provider"
date: 2006-08-03 13:12
comments: true
categories: 
  - technology 
---
Nothing negative about my <a href="http://www.doreo.com/">previous host</a>, as they do a good job without a question, but my opinion on <a href="http://www.dreamhost.com/r.cgi?116006">my new hosting provider</a> definitely is better already. Instead of playing a closed card about any downtime, which is common among a lot of hosting providers (again, not my previous hosting provider), DreamHost decided to <a href="http://blog.dreamhost.com/2006/08/01/anatomy-of-an-ongoing-disaster/">give a full explanation of the recent server problems and downtime</a>. Very good! I mean, they're cheap, but usually they don't have these serious problems. And now they encounter serious problems, which are partially to be blamed on themselves, and they openly admit it! Very good!

I think I said it before, but if you're not hosted there, <a href="http://www.dreamhost.com/r.cgi?116006">maybe you should consider switching</a>. And if you let me know, I can even arrange some discount for you!
