---
layout: post
title: "Lessons From ConFoo"
date: 2016-02-27 11:15:00
comments: true
categories: 
  - php 
  - confoo
  - conference
  
---

ConFoo 2016 is over. The past days a huge amount of developers, most of them from Canada but also from the USA, The Netherlands, South Africa, France and quite a few other countries came to Montreal for ConFoo. I know I had a lot of fun, and I surely learned a lot. One of the keynote speakers said on Thursday:

>  “As you learn, share it”

This is something I've been saying as well, but not doing enough of. So as I sit here (local time 4:42AM and awake because at home it's nearly 11AM) I'll share some of the lessons of ConFoo.

## Polyglot conferences are the best

This is an overall lesson and something I've learned before and even say in my Level Up Your Team-talk. Going to a Polyglot conference gives you a fantastic amount of extra information. Just talking to people with different backgrounds will teach you a lot about how other languages solve things, and will give you some additional information on when to choose which tool. While my main weapon of choice is still PHP (and I don't see that changing any time soon), I always feel inspired by seeing how other languages handle things. So get your ass over to [ConFoo](http://confoo.ca), [DomCode](http://conference.domcode.org/), [4Developers](http://2016.4developers.org.pl/en/) or any of those other polyglot conferences. It is certainly worth it.

## We can do so many cool things with technology

On Thursday, I attended the talk "Unlocking Data Trapped in Audio and Video Files" by Keith Casey. Now the stuff he talked about in his talk is not something I've really had to do in any of the projects I've worked on, but the one thing I took away from that talk was: We can do some really cool and crazy things with technology. In his talk, Keith talked about not just converting spoken word into text, but also recognizing emotion, identity and more. We can, in varying forms of correctness, actually do stuff like this already. Isn't that amazing?

I also learned something else: Quite a few companies still don't offer APIs. This is scary.

## Go!

Friday morning started (for me) with a talk by Gemma Anible called "When All You Have Is An Elephpant..." which was, predictable, about choosing the right tool for the job and how PHP is not always that. This is a lesson I learned a long time ago, but what made me come to this talk was that Gemma was going to talk about Go. I've been interested in the Go language for quite some time now, but have never really had a reason to actually look into it, so this talk was a great way of at least getting a bit more information about it.

I have certainly come to the conclusion that I need to check out Go. The examples Gemma gave are quite nice. Though a slightly different syntax, I don't think it would be hard to learn the syntax of Go, but the concepts are certainly different, and it's those concepts that make the language quite interesting, especially for running a lot of stuff concurrently (for instance when writing workers that process stuff in the background). Gemma recommended the [Go Tour](https://tour.golang.org), so I need to check that out soon.

## The power of lightning talks

ConFoo ended with lightning talks. Now the lightning talks I'm used to are usually 10-15 minutes long, but the lightning talks at ConFoo were only 5. I was a bit sceptical at first, but this works extremely well. It is amazing how much information one can put into 5 minutes when challenged to do so, and the talks were a nice variation of funny and serious talks. I am seriously considering doing an event in The Netherlands now purely based on lightning talks. If you're interested in this, [drop me an e-mail](mailto:stefan@ingewikkeld.net).

## Share your lessons

When I decided to try and blog a bit more again, I wanted to try and share more information again. There had been months where I had not blogged, or only blogged once. Looking at [my archive](https://skoop.dev/blog/) I see an increase in blogposts since January 2015, and though there have still been months without blogpost and months with only one blogpost. I will be trying to increase that again. I'm not sharing enough with the world, and I feel I need to improve on that. 

While I try, perhaps you should also try. Blog, speak at usergroups, join community Slack channels, speak at unconferences and conferences, tweet, exchange information and knowledge at the office. Share your lessons. 