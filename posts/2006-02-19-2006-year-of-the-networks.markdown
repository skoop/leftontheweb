---
layout: post
title: "2006: Year of the networks?"
date: 2006-02-19 9:26
comments: true
categories: 
  - technology 
---
We probably all know about <a href="http://www.9rules.com/">9Rules</a>, the weblog network that's been around for a while. There, you can find a wide range of websites on a variety of topics. All are high quality. Network is the main keyword here. The central hub points to the member sites, and from the member sites you can get to the central hub. To find great new content.

I didn't find out about 9rules until somewhere last year, but was quite happy I found it. And it inspired me to do something similar. But slightly different. The main feature I missed on 9rules was a central entrypoint to all content. Well, <a href="https://skoop.dev/article/174/a-technology-network">I posted about this before</a>.

After a while of thinking, playing around, there is something to show for the work. I've not done this alone luckily. After some discussion with <a href="http://www.mcville.net/">Marko</a>, <a href="http://www.filipdewaard.com/">Filip</a> and <a href="http://weblog.zerokspot.com/">Horst</a>, I started some work.  And the result is <a href="http://www.ufdi.net/">the Ufdi Network</a>. 

Right now, the main feature is central aggregation. Content from connected sites is aggregated into the Ufdi website, and subscribing to the Ufdi RSS feed will give you a good overview of this content. In the near future, a few more features might be added.

But the Ufdi Network is not the only new network. Recently, <a href="http://www.i-marco.nl/weblog/archive/2006/02/16/introducing_dutch_directions_t#body">i-marco also introduced a similar idea</a>. Though their focus is on dutch weblogs, the idea seems to be similar.

I personally think this is a good development. As the number of weblogs keeps growing, these networks will help people find the information they are looking for, more than anything else. Networks seem to mostly focus on quality content, and most people are probably looking for this. I definitely hope that both new networks will be successful. And maybe there are more? I'm definitely interested in learning about more networks.
