---
layout: post
title: "Zend Framework, lots of ideas!"
date: 2006-04-03 16:10
comments: true
categories: 
  - technology 
---
There's lots of work and even more thinking about the Zend Framework being done. Proof of that can be found in the first <a href="http://devzone.zend.com/node/view/id/105">Framework roundup</a>. Interesting reading! Especially the part about Authentication and ACL caught my eye. I'll definitely be keeping a good eye on the development in that area. The Framework is very promising.
