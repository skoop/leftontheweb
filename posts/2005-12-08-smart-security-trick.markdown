---
layout: post
title: "Smart security trick"
date: 2005-12-08 1:50
comments: true
categories: 
  - technology 
---
Every single programmer must have had this problem at least once. You have unexpected behaviour and can't find where the problem occurs. Finally, after a lot of searching, you find the problem in an if-condition. Instead of using ==, you used =. Stupid!

Sean Coates posts about a <a href="http://blog.phpdoc.info/archives/28-var-TRUE-or-TRUE-var.html">nifty little trick to prevent this</a>. Instead of testing the value of a variable against a constant, do it the other way round. So, instead of

.bq if($loggedIn == TRUE)

you do it the other way round:

.bq if(TRUE == $loggedIn)

Advantages: if you accidentally type a single '=', your code breaks, since you can't assign a value to a constant using this notation. Also, you won't accidentally overwrite a possible essential variable, allowing easy cracking of your site/cms/whatever.
