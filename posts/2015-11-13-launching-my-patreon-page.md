---
layout: post
title: "Launching my Patreon page"
date: 2015-11-13 12:55:00
comments: true
categories: 
  - php 
  - support
  - patreon
  - theartofasking
---

In June, I [launched](https://skoop.dev/blog/2015/06/05/I_Want_To_Support/) [IWantTo.Support](http://iwantto.support/), a website dedicated to displaying people and projects that you can support. Not just by helping them, but also financially. For a long time I've been thinking about this. While helping a project or developer by coding, documenting or promoting, it sometimes is actually nice to help them by buying them a beer, book or something else they want. 

Now I am at a lot of conferences, so every once in a while I can buy someone a beer. I've also on occasion purchased something off an Amazon wishlist to show my appreciation. But not a lot of projects and developers actually allow simply donating some money. I think more people should give the opportunity of donating some money.

Earlier this year I got introduced to the concept of [Patreon](http://patreon.com/), which is used mostly by artists (yes, it was through [Amanda Palmer](https://www.patreon.com/amandapalmer) that I found out about it). From the moment I heard by it I was intrigued by the approach they take to payments for content and I started wondering [out loud](https://twitter.com/skoop/status/574964982032568320) if we could somehow use the concept for open source contributors.

## So can we?

Yes, we can. And actually, [Rafael already did](https://www.patreon.com/rdohms). You can (and should) support him. His approach is one of monthly payments. Every month, you donate an amount in appreciation of his work for the open source community, which includes the hours he puts into usergroups but also the work he does on [Pronto!](https://chrome.google.com/webstore/detail/pronto/ceppkbmiglldpkelkicajinlfdkbpihh?hl=en), which is clearly mostly for (aspiring) speakers, but still takes quite some time.

The other approach you can take on Patreon is one where you pay per "thing". This makes a lot of sense for open source projects: They could charge people for each release, for instance. And Patreon protects you as supporter against projects that do a lot of releases, because you can choose a maximum amount of money you spend on a project per month. And of course, the project could simply limit the amount of releases that they charge for. This approach would be perfect for libraries, frameworks but also applications. I would happily support some of the projects I've been using in the past years in this way.

## My Patreon

I have also chosen this second approach for [my Patreon page](https://www.patreon.com/skoop). Since I don't release a lot of code, I can't really charge per release of my code. One thing I do on a (sort of) regular basis though is to write blogposts. Whether it is here, on the [DWA site](https://dutchweballiance.nl/category/techblog/) or [elsewhere](http://dev-human.io/~skoop), I do write regularly. And while these articles are released for free, it is something people could support. It is an experiment, I have no idea if this will be successful and if it will be, how successful it will be, but let's hope it will work.

### Donate more, get more

I've also added some rewards! When you start with a single dollar, you'll get an e-mail from me. If you donate more than $25 per thing, I will do a brainstorm session over skype or hangout on any PHP-related topic. If you donate over $30, I'll also send you some actual snailmail stuff like a postcard, perhaps some stickers or anything else I might have lying around. Again, it's all an experiment. I'm also very much open to any suggestions you might have to improve the rewards, so do let me know if you have any ideas about this.

### Recurring support too much?

If recurring support for the same person (in this case: me) is too much (I can understand that) but you want to do a one-off donation, that is possible too. I've added an [about page](https://skoop.dev/about/) that contains a [Paypal.me link](https://www.paypal.me/skoopmanschap) where you can donate.

## Accept donations

So, now that Rafael has taken the step to accept donations and I have taken the step to accept donations, will you? I would love it if more people would take the step of accepting donations, either one-off or recurring. Even if you only get a single donation. Even if you get no donation. If you don't try, you won't get anything anyway. 