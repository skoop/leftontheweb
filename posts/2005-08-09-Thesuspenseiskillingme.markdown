---
layout: post
title: "The suspense is killing me"
date: 2005-08-09 4:43
comments: true
categories: 
  - audioscrobbler 
---
Today, the new <a rel="tag" href="http://www.audioscrobbler.com/">AudioScrobbler</a> will be launched. The temporary page they have up at the moment doesn&#39;t really say anything yet. I&#39;m really wondering... will the new site be as good as I hope it to be? Will the technical trouble that have been bothering AudioScrobbler be over?
