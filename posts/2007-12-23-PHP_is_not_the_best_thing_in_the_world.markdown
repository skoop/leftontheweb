---
layout: post
title: "PHP is not the best thing in the world"
date: 2007-12-23 2:40
comments: true
categories: 
  - technology 
---
<p>There are a lot of PHP developers bashing, for instance, Ruby, or Java, or any other language, really. And then there are the inter-framework wars. CakePHP advocates bashing symfony, symfony advocates bashing Zend Framework, Zend Framework advocates bashing you name it. Everyone bashing everyone, trying to &quot;protect&quot; their own thing.</p><p>But really, let&#39;s learn from each other. Fabien Potencier, lead developer of symfony, told us at SymfonyCamp that Ruby on Rails is, in his opinion not a good framework, but he still looks at it and borrows some concepts he feels are good. He actually looks at a lot of different frameworks and if he finds a good concept or implementation, uses that to his (or &quot;our&quot;) advantage in symfony. If you like DRY, if you want to work according to Design Patterns, then this is the way you should work. Why reinvent the wheel if someone else already has made a great implementation or thought of a great way to solve a problem you&#39;re facing? Just borrow the concept or even the implementation. You can, especially in the open source world</p><p>Let&#39;s become eachothers friends. We all try to solve problems with software. If we face the same problem, let&#39;s help eachother, and not tell eachother to use another language/framework/whatever.&nbsp;</p><p>Since Ruby on Rails if everyone&#39;s favorite bashing target, I&#39;d like to link you guys to this great <a href="http://reinholdweber.com/?p=6" target="_blank">list of things PHP developers should learn from Ruby on Rails</a> .&nbsp;</p>
