---
layout: post
title: "PHP Abstract: The PHP Podcast"
date: 2007-12-02 14:25
comments: true
categories: 
  - technology 
---
<p>Since starting to work on a Mac, and be able to use iTunes, I finally found a good application to actually subscribe to podcasts, and since I&#39;m deep into PHP, PHP Abstract sounded like a logical step to try. I really like it! :)</p><p>Funny enough though, of all the episodes, I&#39;ve listened to so far, it&#39;s the only not-really-technical episode that I like best so far. The <a href="http://devzone.zend.com/article/2718-PHP-Abstract-Podcast-Episode-25-An-Interview-With-Sara-Golemon" target="_blank">Interview with Sara Golemon</a>  so far was the best. I guess I&#39;m not really a geek after all, as I like to hear the personal experience and story more than the technologically interesting stories. No, that&#39;s stupid, I&#39;m geek enough, yet I still like to hear that personal story. Anyway, to everyone doing PHP, check out <a href="http://devzone.zend.com/article/2046-Announcing-PHP-Abstract-DevZones-new-PodCast-for-PHP-Developers" target="_blank">PHP Abstract</a> , it&#39;s a really nice podcast.&nbsp;</p>
