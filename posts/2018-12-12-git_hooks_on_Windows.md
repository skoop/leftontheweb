---
layout: post
title: "Git hooks on Windows"
date: 2018-12-12 8:00:00:00
comments: true
categories: 
  - git
  - hooks
  - windows
social:
    highlight_image: /_posts/images/hook.jpg
    summary: Git hooks on Windows are easier than you think
    
---

I recently was asked to add a git hook to our main repository to add the Jira issue number to the commit message in an automated way. We were so far handling this really inconsistently, with many commits not having the ticket number at all, while others would have them either at the start or the end of the commit message. Since our branches all contain the ticket number (naming is like `feature/APP-123-this-new-feature) this should be easy, right?

Searching around I found that Andy Carter has [already published a hook written in Python to do just this](https://andy-carter.com/blog/automating-git-commit-messages-with-git-hooks). I copied the script and put it in the prepare-commit-msg file. Because I work on Windows these days I expected `#!/usr/bin/env python` not  to work so I updated it to be `#!C:\Program Files\Python\Python.exe`.

I started testing my new hook, but it wouldn't work. I would constantly run into an error when committing: `error: cannot spawn .git/hooks/prepare-commit-msg: No such file or directory`.

After trying many different things including adding quotes to the shebang, just using `python.exe` (since Python was added to the PATH) and a few other things, I found out that I had simply been too excited to change the script. It turns out Windows does support `#!/usr/bin/env python`. So after simply changing the shebang back to it's original value the commit hook worked like a charm!