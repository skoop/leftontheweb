---
layout: post
title: "We Are All Artists"
date: 2015-01-06 21:30
comments: true
categories: 
  - php 
  - theartofasking 
  - artists 
  - pride
---

I've [written on this topic before](http://books.stuartherbert.com/if-i-knew-then/take-pride-in-what-you-do.html), but as [Amanda Palmer touched the topic in her book](http://www.amazon.com/The-Art-Asking-Learned-Worrying/dp/1455581089) I felt it was a good idea to write about it again, this time on my blog. Especially since the angle is slightly different.

In my contribution to Stuart Herbert's book I talk about the need to be proud of your work with the angle of ensuring a certain quality. However, there is also the other way of looking at it. I've talked to a lot of people who at some point in their career (often early on in their career) had a hard time being proud of their work because their work can not be seen. Designers and frontend developers can very easily show their work. "*Hey, look at this cool site I've designed!*" For backend developers, this is very hard. "*Look at this site, we've worked hard for 3 months building it!*" "*It's just a webshop, so what?*"

In her book, Amanda Palmer speaks about a phonecall she had with her mother who had been a computer programmer for 40 years. Amanda Palmer, being a pretty clear and proven artist, at some point gets confronted by her mother with something she said when she was 13:

  > MOM, I'm a REAL ARTIST. You're NOT
  
  Not long after, however, her mother says exactly the thing I've wanted to say to many of the developers I mentioned above that didn't know how to take pride from their work:
  
  > You know, Amanda, it always bothered me. You can't see my art, but... I'm one of the best artists I know. It's just... nobody could ever see the beautiful things I made. Because you couldn't hang them in a gallery.
    
This is the message I've tried to get across to many developers in the past years. Sure, you can't show non-programmers your work of art. Even if people can appreciate the amount of effort that goes into building software, many a mother, brother, nephew or neighbour would not be able to understand the difference between your building a high-end high-performance architecture and someone that "just makes it work". But that doesn't mean you stop being an artist. **YOU ARE AN ARTIST**. You need to solve your problems, implement your features, you **need to be creative**. Being a software programmer is creative, just as being a musician is creative. It's just a different problem you're solving.

So please, PLEASE be proud of what you do. We are all artists.

**Update**: [Bart de Boer](https://www.facebook.com/startswithab?fref=ufi) pointed me to [a 1995 interview with Steve Jobs](http://youtu.be/M6Oxl5dAnR0?t=33m30s) in which he mentioned the exact same thing. If even Steve says so, do you believe it now? ;)
