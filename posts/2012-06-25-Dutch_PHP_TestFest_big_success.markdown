---
layout: post
title: "Dutch PHP TestFest big success"
date: 2012-06-25 21:32
comments: true
categories: 
  - php 
  - testfest 
  - qa 
  - pfz.nl 
  - amsterdamphp 
---
<p>At the start of the day the group was split up into three smaller groups. Together with Felix De Vliegher the first group sat down to work on the Gearman extension. Together with Derick Rethans the second group started working on tests for the MongoDB extension, and the rest of the group joined Rafael Dohms to write tests for the core of PHP. In each group, the mentor started with an introduction and some pointers on how to write tests, and then everyone started work.</p>

<p>This morning, as we started checking the results of the TestFest, we were amazed at the results. One of the reasons for this was that some people actually continued writing tests when they got home, which is awesome! The total amount of tests so far is 54 tests, with a clear focus on the core of PHP (over 40 tests were written by that group!)</p>

<p>It was amazing to see so much interest in writing tests for PHP, and even better to see the result of this day of testing. Thanks everyone for your work!</p>

<p>Disclaimer: This post was posted before in Dutch at <a href="http://www.pfz.nl/nieuws/berichten/59-testfest-groot-succes/">PFZ.nl</a></p>
