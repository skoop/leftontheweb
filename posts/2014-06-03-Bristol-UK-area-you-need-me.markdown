---
layout: post
title: "Bristol UK area, you need me"
date: 2014-05-29 12:00
comments: true
categories: 
  - php 
  - communities 
  - phpsw 
  - bristol 
  - consulting
---
Yesterday, [@phpsw](http://twitter.com/phpsw) tweeted:

  > also, still looking for people to speak about testing/CI in July/August, if anyone wants to volunteer before we start searching/goading :)
  
I, sort of jokingly, responded with:

  > [@phpsw](http://twitter.com/phpsw) if someone can give me a good reason to travel there I could ;)

I say "sort of jokingly" because:

  * I'd love to speak on the topic
  * I'd love to visit a usergroup I've never been to
  * I realize just travelling to the UK for a usergroup is expensive and a lot of effort
  
The discussion went on a bit more and [Phil Sturgeon](http://twitter.com/philsturgeon) entered the discussion, inviting me for cider and noting we hadn't met in way too long. I agree, and I've not really tasted many ciders yet, so an expert on cider could definitely teach me a thing or two. So I asked him:

  > Nobody in your region in need of a 1-day consulting?
  
This prompted Phil to tweet out a call to anyone that may need something:

  > Dear the West Country: Who would like @skoop to come to their office and do some PHP/Symfony consulting on July 1st/2nd? @phpsw #php
  
This started out as a joke, but I seriously want to go and travel to speak at the phpsw usergroup. So here goes nothing:

## GET ME OVERTHERE! SPECIAL PRICE, JUST FOR YOU

I'm offering a discounted price for the company that flies me out for a single day of consulting or training. I'll happily brainstorm about your application, help you fix issues with your PHP application, give some training on PHP-related topics, review or audit your code, or just hang out at your office. My normal single day rate is ~1000 euro, but the company that agrees to fly me over to speak at phpsw will get a **50% discount**. I will work for you for the day for 500 euro + whatever the cost of the flight is. Let's make this work! Interested? Tweet @skoop and @phpsw, or e-mail me: stefan@ingewikkeld.net. 
