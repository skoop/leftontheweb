---
layout: post
title: "Why I will pay more attention to game nights"
date: 2017-08-30 22:10:00
comments: true
categories: 
  - php 
  - wecamp17
  - wecamp
  - games
  - conference
social:
    highlight_image: /_posts/images/gamenight.png
    highlight_image_credits:
        name: Stefan Koopmanschap
    summary: I need to admit a mistake. I made a mistake in judgement when selecting which games to bring to the WeCamp Game Night.
    
---

[WeCamp](https://weca.mp/) was last week and a lot was learned, and a lot of fun was had. Most lessons learned were good and nice, but I'm going to state here and now that I learned about something I should've known before WeCamp, but that I did not pay attention to: Which games to bring to game night.

## Game night

Game night at WeCamp is all about leaving your electronics behind: It is meant as a moment of leaving work behind and play board- and card games together with other attendees. It is for having fun and relaxing.

## Cards against humanity

During the first year of WeCamp, Cards Against Humanity was very popular in the PHP community at events. As such, I brought it to WeCamp. As the game night progressed, however, I started wondering whether this was a good choice. A majority of attendees were playing it, causing some people to be left out. The game can be quite offensive and not everyone enjoys playing such a potentially offensive game (even if all offensiveness is done in an atmosphere of joking, and is not meant seriously). After the first WeCamp I decided not to bring the game anymore. I wouldn't block it from being brought by other people, but would myself actively pursue playing other games.

## Making the same mistake all over again

Earlier this year I received a new game I backed through Kickstarter: Secret Hitler. While the name feels offensive, the gameplay looked really good, and within the right context this should not be a problem. Excited as I was about this new game, I packed it into the box of games to bring to WeCamp, not thinking about the effects it might have on others. I was just really excited about this fun new party game I had purchased.

## Context is important

What I had not considered is that with games like Cards Against Humanity and Secret Hitler, the context is very important. When played among friends it is clear to everyone that the game is not serious, that everyone is just having fun. When playing Cards Against Humanity, the purpose is to make jokes as offensive as possible without actually meaning offense. When playing Secret Hitler it is clear that nobody actually supports Hitler, that accusing someone of being a fascist is done jokingly and not serious.

However, when at an event such as a conference, or WeCamp, the context is different. While you may consider people friends, they are not good friends. And even when people are not playing the game, they may still be confronted with terms such as Hitler or fascist. I did not consider this when packing the game or setting it up, but even the usage of these terms, in whichever context, can be highly offensive to a lot of people.

Both during WeCamp and in the evaluation questionnaire we have received several comments about the fact that Secret Hitler was being played. This is what made me realize that I made a mistake. It was a big mistake in judgement on which games I could bring to the island, and for that I apologize to anyone who was offended by that.

## There is so much to play

I have a huge stack of games that I could bring to any event, including a lot of fun (party) games. At WeCamp, for instance, [Dixit](https://boardgamegeek.com/boardgame/39856/dixit) was a very popular choice. We had a lot of fun playing it. Another popular choice was [Bang! The Dice Game](https://boardgamegeek.com/boardgame/143741/bang-dice-game), [Saboteur](https://boardgamegeek.com/boardgame/9220/saboteur) and [7 Wonders](https://boardgamegeek.com/boardgame/68448/7-wonders) were also very popular. Other good choices could have been [Exploding Kittens](https://boardgamegeek.com/boardgame/172225/exploding-kittens) or the game I got recommended by friends: [Bunny Bunny Moose Moose](https://boardgamegeek.com/boardgame/59149/bunny-bunny-moose-moose).

Considering it now, one of my other favorite games, the historically accurate World War II boardgame [Escape From Colditz](https://boardgamegeek.com/boardgame/715/escape-colditz) would be another good example of a game I should *not* bring to an event like WeCamp.

## TIL

Looking back at WeCamp, I can say: Today I Learned that even though games may be associated with fun and may seem innocent, picking the right games for the right context is still important. I made a mistake by bringing Secret Hitler to WeCamp, and will give more thought to my choice of games for any future event that I attend.

Should we all stop playing these games? No. But we should remember that there is a time and a place for them. And this was not it.

