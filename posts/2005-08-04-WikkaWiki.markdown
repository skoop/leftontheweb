---
layout: post
title: "WikkaWiki"
date: 2005-08-04 3:07
comments: true
categories: 
  - php 
  - software 
  - wiki 
---
For a while, I&#39;ve been looking at several implementations of Wiki software. <a href="http://www.mediawiki.org/">MediaWiki</a> is nice, but very heavy on the server. Not weird, considering the software is loaded with features. Of course, a lot of those features are hardly needed, so I chose to stay away from MediaWiki.  After trying various other Wiki implementations, I came by <a href="http://wikka.jsnx.com/">WikkaWiki</a>. It has all the usual features of a wiki, but isn&#39;t too loaded with features that I don&#39;t need. I just need some wiki functionality. Looking at the code, it was quite easy to customize. And fully XHTML and CSS-based, which is also a big plus.  And so I now have a <a href="http://wiki.electronicmusicworld.com/">fully functional wiki</a> for my <a href="http://www.electronicmusicworld.com/">Electronic Music World</a> website. I also immediately made a slight change to the code.The one thing I did not like was the fact that when you linked to another page within the wiki, the link text would be the tag of the page. Linking to MyWikiPage would mean the link text would be &#39;MyWikiPage&#39;, and not the page title of that page, which could be &#39;My Wiki Page&#39;. And so I made a <a href="http://wikka.jsnx.com/LinkUsingPageTitle">contribution</a> to WikkaWiki. It&#39;s two simple changes to existing Wikka methods. It works like a charm now, so I&#39;m a happy man :)  Now... to find the time to actually fill the wiki with useful information ;)
