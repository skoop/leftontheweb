---
layout: post
title: "The Inter-team Standup"
date: 2014-11-07 14:00
comments: true
categories: 
  - scrum 
  - teams 
  - standup
---
For [WeCamp 2014](http://weca.mp/), we were creating multiple teams that would all work on their own project. But since we wanted to maximize the exposure of all delegates to the lessons learned by all teams, we needed to create a moment where people could exchange their ideas, their lessons, their work. 

Since our schedule already contained a 15-minute standup meeting for each team, it was not hard to come up with the idea of organizing a central stand-up meeting with all teams.

Our Inter-team standup was simple: All members of all teams would be meeting together. Each team would appoint a single representative that would tell everyone what the team had been working on, what challenges they encountered and what solutions they'd chosen. For us, not just the what was important but also the why of decisions. Since we want people to not just learn about the approach but also why this approach was taken, we tried to put some focus on that as well.

## Taking this outside the camp

Recently at a customer, we were having a discussion about how to improve the communication between the different development teams. Both teams work (partially) on the same codebase, each with their own responsibility. While doing their work, they may (try to) solve the same problems. That seems inefficient and may result in inconsistent solutions to the same problem.

During this discussion I thought back of WeCamp and proposed we would try this approach. This week we had our first standup, which was quite interesting. It's good to hear, in just a couple of minutes, what the other team is working on and what problems they are solving. We did notice that the meetup needs a bit more preparation from the teams to make it more efficient, but the concept of having a (in our case) weekly meetup of 10-15 minutes seems to work well.

## Don't replace normal communications
It is important to realize that this meeting is not meant to replace all other forms of communication between teams. Aside from this, it is important that individual team members can visit the other team when they have a question, that they have Skype/Slack/HipChat/IRC to communicate, that they talk to eachother by the watercooler or coffeemachine and that they go to lunch together. The inter-team standup is simply an organized and structured way of exchanging challenges and possible solutions. So far it works, I'm curious what more this will bring.
