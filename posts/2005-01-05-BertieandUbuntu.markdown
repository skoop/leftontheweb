---
layout: post
title: "Bertie and Ubuntu"
date: 2005-01-05 7:35
comments: true
categories: 
  - phpBB 
---
I made this picture today for a friend, and couldn't just let it go at that. I just needed to share this with you. It seems that <a href="http://www.phpbb.com/">phpBB</a>'s Bertie the Bear is good friends with the people of <a href="http://www.ubuntulinux.org/">Ubuntu</a>.

<txp:image id="8" />
