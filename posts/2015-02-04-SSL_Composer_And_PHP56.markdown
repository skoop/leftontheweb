---
layout: post
title: "SSL, Composer and PHP 5.6"
date: 2015-02-04 13:05
comments: true
categories: 
  - php 
  - ssl 
  - composer
---

A quick technical note: I ran into the same issue as [Rob Allen did](http://akrabat.com/php/ssl-certificate-verification-on-php-5-6/). Using his solution I came to understand the problem, but contrary to Rob, I was using an Ubuntu system in a Vagrant-box.

The solution is the same, the path is different. In your php.ini, you add/update the openssl.cafile parameter:

  > openssl.cafile=/etc/ssl/certs/ca-certificates.crt

Don't forget to also restart Zend Server if you're going to use this through the webserver as well. Thanks Rob for your blogpost and [Cees-Jan](http://twitter.com/wyrihaximus) for finding the right path to this file.
