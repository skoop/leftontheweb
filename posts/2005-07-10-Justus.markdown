---
layout: post
title: "Justus"
date: 2005-07-10 8:52
comments: true
categories: 
  - leftontheweb 
---
So, for the past few days, <a href="http://www.flickr.com/photos/skoop/24680044/">Justus</a> was at our place. He's <a href="http://www.livejournal.com/users/justusontour/">travelling through Europe</a> for months and we planned on meeting eachother while he was in The Netherlands. He ended up accepting our offer of a place to sleep for two nights. It was quite nice. Justus is a really cool guy. On friday, I picked him up at Amersfoort trainstation, and basically we just stayed at our place for the day. On saturday we went to Utrecht, to walk around, for Justus to <a href="http://www.flickr.com/photos/skoop/24680044/">take some pics</a>, and to arrange a bed for him. He's gonna stay in Utrecht for a few days before travelling further. It was quite nice. 

I'm gonna meet up with Justus in Amsterdam in a few days. On thursday, we'll be going to <a href="http://www.5daysoff.nl/">5 days off</a> to see Amon Tobin, maybe a bit of Funckarma, Speedy J (in 5.1 surround), Richard Devine (in 5.1 surround), Autechre, and then maybe check Luke Vibert for a bit. It's gonna be fun, that's for sure.
