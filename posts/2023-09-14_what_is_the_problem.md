---
layout: post
title: "What is the problem?"
date: 2023-09-14 15:30:00
comments: true
categories: 
  - php
  - problemsolving
  - debugging
  - performance
social:
    highlight_image: /_posts/images/debug.jpg
    highlight_image_credits:
        name: Sigmund
        url: https://unsplash.com/photos/TVxYoWzqdjs
    summary: Sometimes, taking a step back is the best solution. Especially when you are stuck. Here's some experiences of what can help when stuck on some code.
    
---

When you run into a problem with your code, either you're stuck in trying to implement a feature or a bug occurs and you can't quickly find what the cause of that bug is, it is very tempting to really dive into the code. It makes sense to do that, because you're working on that code and it isn't doing what you want it to. We also have great tools such as [Xdebug](https://xdebug.org) that can help us trace the path of the code. Or if the problem is performance, there's tools such as [Blackfire](https://www.blackfire.io) that can give you a lot of information about the when, where and why an issue occurs. So please, use tools like these to give you a lot of information.

But sometimes this is not enough to immediately spot the cause.

In that situation, please don't dive deeper into the code. There is a risk that you'll drown in the code, have no idea anymore what is happening where and eventually you'll be frustrated, tired and completely lost.

## Take a step back

Instead of digging into the code, take a step back. Gather as much data as you can about what is happening using all the tools that you can think of, and then step back from the code. Make an overview for yourself of what you expect to happen. Use pencil and paper or a whiteboard to make a map of the flow. Then, compare that to the data you've gotten from Xdebug, Blackfire and other tools. See if you can spot the flaw. See where your idea of what is supposed to happen diverges from what is actually happening.

## Get help

If you're still stuck, don't hesitate to get help. You can first start rubberducking by trying to explain it to a rubberduck or an elePHPant. Sometimes this already helps: By thinking about how to explain the situation, your brain will look at the problem from a different angle ("how do I describe this so someone else explains what I already know?"), and this sometimes explains to you "ah, this is why it's not working as I want it to".

If that doesn't work, get a second pair of human eyes on all the information you have. A fresh pair of eyes will have a different look on all the data and might help. Especially when you've been stuck on something for a while you may have become blind to the actual cause. Having someone else have a look may just bring that fresh set of eyes that is needed to quickly spot the cause of the issue.

And don't feel bad if your colleague spots the cause of the issue quickly. This is very normal. As mentioned before, when you've been stuck on something for a while you may have become blind. They have not been staring at this for a long time already, so they may quickly see what's going on.

## No really, step away

Another way to prevent becoming blind to the issue is to really step away from your computer for a while. Take a 15 minute walk, or perhaps even a 30 minute walk, to clear your head. Don't keep thinking about the bug, but instead think of anything else. Truly anything else: That concert you're going to next week, which laundry is next on your list, what's for dinner tonight. As long as it is NOT your code, you're good. Once you come back, you'll be the fresh pair of eyes, and you might spot the issue soon after returning to your computer.

Being a developer is not just about writing code, so don't feel guilty for taking 15-30 minutes for a walk. This _is_ part of your work. As someone who works with their brain, you need to give that head some rest every once in a while. Nobody can do full focus work for hours on end without needing a break every once in a while.

## Good luck!

I hope you won't run into many issues like these, but if you do: Good luck! I'm hoping you'll find the solution soon!
