---
layout: post
title: "PHPBenelux Conference 2010"
date: 2009-11-30 23:15
comments: true
categories: 
  - phpbenelux 
  - conference 
  - phpbnl10 
  - php 
---
<p>This is the first time we&#39;re doing a conference of this size, but definitely not the last time. And it&#39;s exciting already. We have already announced our first speaker: David Zuelke will be speaking about the state of SOAP in PHP. Aside from that, we have 9 other excellent speakers that will be announced in the coming days, plus two top-notch keynote speakers, also to be announced in the coming days.</p><p>Tickets are also on sale now in early bird! Registration is only 75 euro in the early bird, and you can easily pay using Paypal. There&#39;s limited tickets available to get yours as soon as you can!</p><p>I am also quite excited to let you know we already have two excellent sponsors for the event: Microsoft and Ibuildings are helping us pull off the whole show as Gold sponsors. We are still looking for some more sponsors though, including a single premier sponsor and some silver sponsors. We also have some custom sponsor packages available. So please do&nbsp;<a href="http://www.phpbenelux.eu/en/contact" target="_blank">contact us</a> &nbsp;for more information.&nbsp;</p>
