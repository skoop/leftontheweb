---
layout: post
title: "A good announcement"
date: 2005-01-07 9:03
comments: true
categories: 
  - leftontheweb 
---
Paul decided that <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=253930">it was time to call back some people</a>. As the Support Team Leader, I also get regular messages through PM, e-mail or even in the comments of my weblog of people asking me for support. That is not what is supposed to happen. It's clear from the <a href="http://www.phpbb.com/phpBB/rules.php">forum rules</a>, the <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=19745">README</a> and my signature that I (and all other team members) should not be contacted with support questions in any other way than through the <a href="http://www.phpbb.com/phpBB/viewforum.php?f=1">Support Forum</a>.

OK, that's all now. Walk on through. :)
