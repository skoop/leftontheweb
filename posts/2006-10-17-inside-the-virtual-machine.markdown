---
layout: post
title: "Inside The Virtual Machine"
date: 2006-10-17 21:16
comments: true
categories: 
  - music 
---
A while ago, I announced here the download of <a href="https://skoop.dev/article/265/back2buzzics">Back2Buzzics</a>, which was the first track I'd written in years, literally. I've not been doing nothing since then. I've actually finished yet another tune!!!!

This one is called 'Inside The Virtual Machine', is again quite housey in nature, with some bassline, dreamy strings and ... there is an amen break! ;)

Download now includes an mp3, by popular request :)

<a href="https://skoop.dev/insidethevirtualmachine.ogg">Ogg download</a>
<a href="https://skoop.dev/insidethevirtualmachine.mp3">MP3 download</a>
