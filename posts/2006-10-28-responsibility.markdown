---
layout: post
title: "Responsibility"
date: 2006-10-28 13:23
comments: true
categories: 
  - technology 
---
Earlier this week, the biggest dutch consumer organization declared that it felt not consumers but computer producers and software developers are responsible for the safety of computers worldwide. They felt virusscan programs, software writers and computer manufacturers should do more to prevent viruses. Basically, their statement said "people don't know how to handle computers, so you should be responsible for their lack of knowledge".

Now, I wonder, what were they thinking? If I buy a car from, say, Renault, is Renault responsible for the fact that I drive like a madman and kill 10 people? Is the gasoline computer responsible for me driving 200 km/h on a 50 km/h road? I don't think so. When you buy a car, and drive that car, you are responsible for what happens with it. And of course, to drive a car, you need a drivers license.

Maybe it *is* time to come up with some kind of computer users license. To get it, one would have to learn how to operate a computer, but also how to prevent viruses, phishing attacks, and other dangerous parts of computer usage. And please, do not limit this knowledge to Windows, because an incorrectly secured Mac or Linux machine can also be responsible for sending out spam, being used as a relay machine.

So who is responsible for viruses being able to get in to computers? Clearly not the manufacturers of those computers. Maybe partially the people who build the OS, but even the worst OS can be secured if you configure it correctly and install the correct software. Don't move responsibility off regular consumers: That way they'll never learn.
