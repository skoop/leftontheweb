---
layout: post
title: "Philip K. Dick - Do Androids Dream of Electric Sheep?"
date: 2006-02-27 15:27
comments: true
categories: 
  - books 
---
The book that was turned into Blade Runner, that sounds interesting. And I've read a lot of positive remarks about this book. I was curious to read it.

Of course, as with most "legendary" books that are already quite old, it turned out not to be the book I expected. Don't get me wrong, it's a very entertaining book and I definitely enjoyed reading it, but I had expected a bit more out of it. The way it ends is a complete turn-off for instance, in my humble opinion. 

Still, the story line is nice, the characters are well-written, there is definitely suspense about whether or not some of the characters are "andies". It's definitely a nice book. Just not one of the best ever written :)
