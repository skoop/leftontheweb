---
layout: post
title: "Stop Fighting"
date: 2014-07-23 23:15
comments: true
categories: 
  - personal 
  - violence 
  - war 
  - death
---
I posted this on Facebook about an hour ago, but this is not limited to Facebook. It should not be. So I'm reposting it here.

Today was a day like any other, except that it wasn't. It was an official day of mourning in The Netherlands. This morning as I woke up, it was still a day like any other. When I got into my car to drive to work was the first time I noticed a difference: Other music, special requests. As I got to work, nothing was different and actually work was no different. A full day of work, a meeting that lasted until just after 4 o'clock so I completely missed the minute of silence. A day like no other day. Since I went home a bit early on monday and I wanted to finish a couple of tasks that I had left, I stayed a bit longer. That "bit" became quite a bit longer as I ran into a colleague and talked a bit.

I started my way back home in the car. Turned on the radio. Again, the radio felt "odd" yet also comforting because of the special programme. Aside from some tweets and pictures, I had not seen anything of the return of the first victims, I'd not followed it yet. While on the highway, I heard the report of the motorcade of hearses travelling from the airport to the army base where they will try to identify the bodies. Bit by bit, the music, the reports, the tweets, it kicked in.

As I neared the exit of the highway that I need to take to go home, I heard where the motorcade was. A quick calculation later I realized that if I'd stay on the highway, I could reach the route they'd be taking easily. I wasn't sure. Should I? Isn't that just sensationalism. I decided it wasn't. I just wanted to pay my respect, especially since I wasn't able to join the minute of silence.

I am so glad I did decide to go on. It was a surreal experience. First of all, as I turned onto the highway the motorcade was going to use a bit later, there were thousands and thousands of people standing on the side of the road. Even in the opposite direction, cars were stopping on the side of the road. So many people. Police was trying to ensure safety of people, but they weren't enforcing the law. Obviously it's usually not allowed to stop by the side of the road like this. Much respect to the police for the way they handled everything, giving only focus to safety and nothing else.

The motorcade came. That was odd. Strange. Impressive. Fourty hearses, one after the other, several of the drivers with tears in their eyes. I could do nothing but stand there and watch. Silent. Tears in my eyes.

I usually have very little faith in humanity. We're destroying our world, we're destroying eachother. What happened is proof of that. But what I felt today, while standing there, the enormous amount of people all there to pay respect, to welcome the dead bodies home, the love, respect and sadness mixed together, shared with everyone there. What a crazy experience. It restored a bit of my faith in humanity. It is possible to have this, unfortunately it will never last long.

I had tears in my eyes for the whole trip home. And as I write this, the tears are again forming. Never, ever in my life will I forget this. But never ever in my life do I ever want to see this many hearses drive by. Ever. Can we please, please all stop fighting eachother?
