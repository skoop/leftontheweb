---
layout: post
title: "My Tools: Keeping up with the world: NewsFire and Zite"
date: 2011-07-21 5:47
comments: true
categories: 
  - blogs 
  - tools 
  - newsfire 
  - zite 
---
<h2>NewsFire</h2>
<p>I've tried many feed readers in the past. Online readers like Google Reader and Bloglines, and native software (of which I strange enough can not remember a single one). NewsFire isn't special in the RSS reader space, and that's what I like about it. It does one thing, and it does that well: It allows me to read my feeds. It supports categories of feeds, and browser through the posts in a certain feed. It keeps track of which items you've read already. There's some smart stuff (such as if you've copied a blog or feed URL and choose the option to add a feed, it will automatically take the URL from the clipboard and fill in the blogname and feed URL), but that's about it. And you know what, that's enough. Nothing to fancy, just something that works.</p>

<h2>Zite</h2>
<p>Perhaps slightly more fancy is Zite. Zite is an iPad app that finds blogs/articles/links you might enjoy. Where NewsFire is just for keeping up with what's being blogged about, Zite is about inspiration, about finding the new stuff, the things I have missed. Obviously, stuff pops up in Zite that I have read already or at least have seen come by, but Zite is relatively good at guessing what I think is interesting. It does this based on some information I gave it, of course, but the result is a custom "newspaper" full of mostly interesting information.</p>

<p>Zite also allows you to tell it whether you liked something or not (and I'm guessing it uses this in the algorhythm that determines what it should show you). It also integrates with Twitter, Facebook and a bunch of other services, so if you read something really good you can directly share it with the rest of the world. I've found Zite to be quite smart in how it determines what you might like, as I've rarely gotten stuff in my "personalized magazine" that I thought was really bad.</p>

