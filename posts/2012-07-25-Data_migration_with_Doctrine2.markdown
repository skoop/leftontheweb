---
layout: post
title: "Data migration with Doctrine2"
date: 2012-07-25 11:47
comments: true
categories: 
  - php 
  - symfony 
  - symfony2 
  - doctrine2 
  - datamigration 
---
<h2>The cause</h2>
<p>As it turns out, the cause of the problem is the fact that when you specify strategy="AUTO" for your ID-field, Doctrine2 will ignore any existing ID you've set in the entity. It will generate one because, well, you've specified it should be doing this. And usually, this is correct behaviour, but with the migration of existing data, to retain correct relations etc, it is much easier to keep the old IDs.</p>

<h2>The solution</h2>
<p>The solution is easy and, as usual, I found <a href="http://stackoverflow.com/questions/5301285/explicitly-set-id-with-doctrine-when-using-auto-strategy">it</a> thanks to StackOverflow. In your migrationscript, for each of the entities you're migrating, you just temporarily turn off the strategy:

<script src="https://gist.github.com/3175359.js?file=gistfile1.aw"></script>

As soon as I had added the above snippet for all the entities I'm migrating, it kept the old IDs and migration all of a sudden went flawlessly.</p>

<p><strong>Update:</strong> The above method assumes you have a way of setting the ID in the entity, for instance a setId() method.</p>
