---
layout: post
title: "Testing: Do it with your customer"
date: 2023-03-22 21:40:00
comments: true
categories: 
  - php
  - testing
  - customer
social:
    highlight_image: /_posts/images/crashtest.jpg
    highlight_image_credits:
        name: Danjo Paluska
        url: https://flickr.com/photos/sixmilliondollardan/353885622/
    summary: While automated testing is important and manual testing by the developer is essential, let's not forget the joy and importance of testing with your customer
    
---

I will be the last one to tell you not to write unit tests. I will be the last one to tell you not to test your own work (or work done by your team) manually. However, I was today reminded of another form of testing that is possibly equally important or perhaps even more important: Sitting down with your customer to test.

A full day. We scheduled a full day to sit down with a customer. The application we're working on is quite extensive and the project so far has not been very iterative. Yes, there has been feedback along the way, but really sitting down and testing is something that has not been done that often so far.

So today was the day: We sat down with three employees of the customer and 4 developers. Our goals:

- Being able to show the customer all the cool stuff we've done
- Being able to see how the customer would use our application
- Understanding what the customer expects to happen on certain key actions

If you're working in an iterative way, for instance when using scrum, you're used to being able to show the customer all the cool stuff you've made. But in some projects, that doesn't really happen. The project for which we gathered today is one of those, and so it was really great to be able to show the customer how we'd build certain things. Seeing and hearing the response of a customer is really useful: Their response, whether with what they say or even with their facial expression, really tells you whether you did a good job, and which parts of the application you may need to improve on. 

Another very useful piece of feedback is to actually see your customer navigate the software you built. It gives a lot of insight into whether you made the right choices, and gives good pointers on where you can improve the usability or the functionality. Today, we had someone always connected to a big screen so we could see exactly what they were doing on the screen. Combine what happens on the screen with the body language of the person controlling the computer at that point and you'll get a ton of very useful feedback.

At some points during the day, there may be confusion. The user will do something and will look surprised, confused, or perhaps both. This is where you really have to pay attention. This is where you can learn how you perhaps interpreted the wishes of your customer different from how the customer actually wanted it. There's a good chance they didn't even know yet how they wanted it. Whichever it is, now is the time to learn how you can truly improve the application to better fit how your customer works. 

## 4 developers

We were there with 4 developers for a reason: While the customer was testing, the developers were also simultaneously fixing issues that had arisen. By making this feedback cycle very small we made it a lot easier for the customer to get exactly what they needed. Another important goal of this was also to give developers a morale boost: To see a customer really happy with the stuff that was built is a very rewarding experience.

## Shorten the feedback cycle

In this project, while there were regular meetings, the feedback cycle was sometimes very long. During today's meeting, the cycle was extremely short. Today was a day of constantly running. This is not something you can do on the long term. But there is also no need to constantly walk very slowly. You have to find what works for you and for your customer. In my experience, 2-4 week cycles give you a good sustainable pace: There is enough room for the customer to give feedback and improve what you are building, but you also have enough time as developers to focus on building the coolest features. So don't do 6-month+ projects with little to no feedback moments. Shorten that cycle to something that works for everyone.

## We can help

Are you a development company or team and you have no idea where to start to improve this? Are you a customer of a development company and are you unhappy with how things are working out? Get in touch with us at [Ingewikkeld](https://ingewikkeld.dev/). We'd be happy to help you improve the development process to make it work for everyone involved. 
