---
layout: post
title: "The Latter Days At Colditz"
date: 2006-02-16 15:06
comments: true
categories: 
  - books 
---
No movie this time but this book was turned into a BBC TV Series. <a href="http://www.amazon.com/exec/obidos/redirect?link_code=as2&path=ASIN/0304364320&tag=electronicmus-20&camp=1789&creative=9325">The Latter Days at Colditz</a><img src="http://www.assoc-amazon.com/e/ir?t=electronicmus-20&l=as2&o=1&a=0304364320" width="0" height="0" border="0" alt="" style="border:none !important; margin:0px !important;display:none;" /> is the sequel to The Colditz Story, which I read earlier after finding a BookCrosser willing to part with both that and this sequel. BookCrossers are a rare breed of friendly people, that's evident ;)

In The Colditz Story, author P.R. Reid tells the story of how he came to be in the Colditz castle during the second world war, how his stay was there and how he escaped and made it back home. In this sequel, he tells the story of Colditz castle after his escape. For this he uses the same enjoyable combination of humor and seriousness, telling the good parts of people escaping and making it home, and the 'bad' parts of people being recaptured or even shot.

One thing that you can't help but notice when reading the book is that the author does feel he has to name every single person that is in one way or the other related to what he is saying. Quite often does he list every single person related to, for instance, an escape, in full with all military titles. Sometimes, this can be a bit too much, though of course the author probably just wanted to give credits where credits are due, give full honor to those who deserve it.

Though obviously this book was much harder to write since the author didn't experience it himself but had to use stories told by others, you often would think he was there himself. Reid writes with such ease about the happenings at the castle that it really doesn't matter if he was there or not. Here and there, one might suspect a bit of dramatization, of slightly twisting the story, but that can only be expected. A book, even though it is a book about historic facts, needs to be readable, and the author clearly made the choice to write the book not just by stating facts, but more in a novel-style with a clear story to it.

All I know is that I highly enjoyed it and after reading these two books more than ever understand why the boardgame '<a href="http://www.boardgamecompany.co.uk/ColditzParker(VG135).htm">Escape from Colditz</a>' is as it is. The game, also developed by Reid, was my first encounter with Colditz and triggered me to hunt down the books. I am glad I did. For anyone with a healthy interest in the second world war, I'd definitely recommend both books.
