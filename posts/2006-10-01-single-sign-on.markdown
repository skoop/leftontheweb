---
layout: post
title: "Single Sign-On"
date: 2006-10-01 3:56
comments: true
categories: 
  - technology 
---
Through <a href="http://www.wormus.com/aaron/stories/2006/10/01/yahoo-opens-up-signup-system.html">Aaron</a> I found out about Yahoo! opening up their user authentication to third parties. The ease of having just a single log in for various sites worldwide is very userfriendly. I applaud the idea. However, not the implementation.

As Aaron also notes, it can be confusing for users to be able to sign in with a Yahoo! account, but then if your site needs more information from the user, having to go through some kind of "registration" process. Single sign-on would be much better if it can be done in the background, using webservices. I can imagine though that Yahoo! would not like this too much, because that would mean the username and password would pass through the third-party application. Not good either.

The effort from Yahoo! is good, but again I don't like the implementation. I don't think I'll support the Yahoo! login any time soon on any of my websites.
