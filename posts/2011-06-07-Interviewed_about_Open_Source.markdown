---
layout: post
title: "Interviewed about Open Source"
date: 2011-06-07 6:35
comments: true
categories: 
  - interview 
  - opensource 
  - php 
  - symfony 
  - community 
  - meta 
---
<p>In the <a href="http://www.webcentralstation.ca/2011/06/06/community-and-open-source-potential-interview-with-stefan-koopmanschap/">interview</a>, I talk about what I like about open source, the community, and how I feel it can impact businesses that use it. If you're interested, go <a href="http://www.webcentralstation.ca/2011/06/06/community-and-open-source-potential-interview-with-stefan-koopmanschap/">check it out</a>.</p>
