---
layout: post
title: "Learn what you can handle"
date: 2008-03-24 20:20
comments: true
categories: 
  - development 
  - testing 
  - worldofwarcraft 
---
<p>In the World of Warcraft Battle Chest Guide, I found the following passage:</p><blockquote><p>The issue here is that intentionally testing your class makes the process more analytic; you can prepare new strategies, test them out, and really see the effects. It&#39;s MUCH harder to keep a keen eye out when you are surprised</p></blockquote><p>To me, you could apply this to development without even having to swap a single word, due to the usage of words like class (though in WoW, this has a different meaning). This quote fits very well in any development of software I guess. It&#39;s funny how a computer game can even remind you of such things ;) </p>
