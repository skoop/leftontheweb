---
layout: post
title: "Dan Brown - Digital Fortress"
date: 2006-09-21 15:15
comments: true
categories: 
  - books 
---
After reading the Da Vinci Code and Angels & Demons, I knew Dan Brown was able to write exciting books, thrilling books, books that dragged you into the story and didn't let go. But all that didn't prepare me for Digital Fortress. Wow. Reading books usually takes some time for me, I am not a fast reader, but I finished this one in a few hours, spread out over 3 days. I couldn't stop reading.

Of course, the topic of this book is for me even more interesting as the church-bashing of the Da Vinci Code. Technology has my interest, quite a lot of course. So for me stories like this come closer to my own little private world than the happenings inside the Catholic church. There is a good chance that for others, it is exactly opposite.

Still, there were also small let-downs. Certain 
