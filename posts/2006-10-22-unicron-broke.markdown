---
layout: post
title: "Unicron broke"
date: 2006-10-22 14:12
comments: true
categories: 
  - technology 
---
This morning, I got the shock of my life when I opened my laptop. I was surprised enough that for some reason, the laptop was turned off, I had used it 30 minutes before that moment, and had only closed the laptop. My wife had heard a weird sound but thought it came from her PC. Unfortunately, she was wrong.

I opened the laptop again to find my screen having some weird spots on it. So I turned on the laptop to find that these spots where actually some kind of cracks in the screen. The soft outside was not damaged at all, but something on the inside must be really damaged, because the screen now looks like broken glass. I let out a quite loud curse when I saw this.

My "luck" in this is that I also have my work laptop at home, so I can at least do some basic stuff. But all my data is also in there. I'll have to connect it to a regular screen soon I guess, so I can at least get some important data from the machine.

Right now, I'm looking at either finding a similar used laptop, or find a similar but broken laptop so I can switch the screens. I don't want to/can't spend a lot of money on this, so let's hope I can find something affordable.

<strong>update</strong>: <a href="/images/37.jpg">See what it looks like</a>
