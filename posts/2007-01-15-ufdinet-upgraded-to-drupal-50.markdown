---
layout: post
title: "Ufdi.net upgraded to Drupal 5.0"
date: 2007-01-15 12:35
comments: true
categories: 
  - technology 
---
I have just upgraded <a href="http://ufdi.net/">Ufdi.net</a> to the brand spanking new <a href="http://drupal.org/drupal-5.0">Drupal 5.0</a>! A yay for Drupal, who did a great job writing a flawless update script. It worked like a charm and everything seems to be working great.

Now that we're on 5.0, I guess <a href="http://www.zerokspot.com/">zeroK</a> and me can finally start working on getting the new Ufdi.net up. The new site will have a great new design, and some nice features the current site still lacks.
