---
layout: post
title: "April Fools anyone?"
date: 2007-04-01 10:52
comments: true
categories: 
  - world 
---
I really <a href="https://skoop.dev/article/325/ive-had-it-with-the-music-industry">had it with the music industry</a>. <a href="http://en.wikipedia.org/wiki/April_Fools">Think again</a>. It's been quite a  bit of fun. I mentioned my "problem" to people I know, I posted it in quite a few places, and got many responses. <a href="http://p2pnet.net/story/11828">P2Pnet</a> even wrote about it in a newsitem.

Sorry guys, you'd been fooled, you've been had, it was a hoax.

There were no lawyers threathening me. I have a good relationship with a few promotion agencies from the USA, who offer me information, the occasional audio stream and the occasional promo cd for review. It works fine, and I've had no trouble with them or the big labels, at least from this point of view.

Happy April Fools!
