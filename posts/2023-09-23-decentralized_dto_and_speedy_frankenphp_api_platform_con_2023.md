---
layout: post
title: "Decentralized DTO and speedy FrankenPHP: API Platform Con 2023"
date: 2023-09-23 09:20:00
comments: true
categories: 
  - php
  - apiplatform
  - decentralized web
  - conference
  - lille
  - frankenphp
social:
    highlight_image: /_posts/images/apiplatformcon2023.png
    summary: API Platform Con was an amazing conference. I can not remember the last time a conference left me so incredibly inspired. Here's some things that stood out.
    
---

As it can not surprise people that know me: I love conferences. I love learning about new things and I love meeting people. It might be friends I've known for years in the PHP community, it might be new friends. And this was what API Platform Con was all about: Learninng about new things and meeting people. All that in the beautiful city of Lille, in France.

## FrankenPHP

When [FrankenPHP](https://frankenphp.dev) was announced some time ago I was quite interested. But since it was still in development I didn't really pay much attention to it yet. During the very first talk of the very first day, Kevin Dunglas announced in his opening keynote that FrankenPHP is now in beta and that it is considered stable enough to use in production. With all the information he shared about Docker setups becoming less complex but more importantly: FrankenPHP being a lot faster than a standard Nginx+PHP-FPM setup, I was intrigued. So I sat down during the talk after and the morning break to try it on a local development environment for one of my projects. I had my local environment up and running very quickly. As in: It was almost as easy as removing the Nginx and PHP containers I had, and adding FrankenPHP. Nice! I've also done the preparation work for pushing this into production as well but did not dare merge the merge request for it yet, since I was at a conference and didn't want production to go down in case I had made a mistake. But this is to be continued, because so far, so good.

## The Need For Speed

The most popular sponsor stand by far was the stand of [Commerce Weavers](https://www.commerceweavers.com). They had brought a remote control race car track, which was a lot of fun. But that wasn't the only thing with speed. Had I talked already earlier about that FrankenPHP is a lot faster than Nginx and PHP-FPM, Lukasz Chrusciel of Commerce Weavers also did a talk about speed optimizations in API Platform, which contained a lot of useful tips and tricks to improve the speed of your API Platform project. Now, I must admit that I've not really worked on a lot of projects yet that do API Platform so I have not encountered speed issues yet, but next time I do run into issues, I must find the recordings of this talk because there's a good chance Lukasz has given the solution already.

## DTO

I started off day 2 with the keynote by Ryan Weaver, who presented some new options to make working with DTO's in API Platform a breeze. I must admit... at some point Ryan had completely lost me and I did not understand why you would do it like that. But he knew it didn't make sense, as he then moved on to explain why it didn't make sense to do it like that, and what the proper way was to actually make it work well. I must say, these new features to work with DTO's make working with them a breeze, and the way Ryan used a very simple mapper to create a generic provider and processor... how little code do you need to create a fully functional API these days? Wow! 

## Decentralized web

All of the above talks really inspired me to sit down at a computer and try out things, but the talk that inspired me the most was Pauline Vos and her talk about the decentralized web. After a depressing introduction of the past and the present of the web (the web currently is in a horrible state and definitely nowhere near what Tim Berners-Lee originally wanted), Pauline showed us what the options are for the future and how `The bright and exciting future of the decentralized web` is indeed bright and exciting. I registered for an account on Mastodon years ago but it wasn't really until earlier this year that I started digging a bit more into the fediverse and all the options it has available and that excited me a lot. It's inspired me to not really use Twitter/X anymore, to use Facebook a lot less, to use Instagram a lot less. Instead, I am now on Mastodon and Pixelfed, and I really want to dig into the various other federated replacements it has for big, corporate, centralized platforms that I still use such as Goodreads. 

But Pauline showed me that aside from the Fediverse there is a lot more out there that focusses on decentralizing the web, including an initiative by Tim Berners-Lee himself called the [Solid Project](https://solidproject.org) that aims to give everyone their own "pod" that controls their private data and that allows finegrained control over what data you share with other sites. I *LOVE* this idea. 

## Inspiration

So now I want to look more deeply into FrankenPHP, and I really want to start looking more into all those decentralized options that are available. And for my next API Platform project, I really want to try out the new DTO stuff and I know where to look if I run into any performance issues. I can not remember the last time a conference left me so incredibly inspired. A big thank you to Kevin, Lukasz, Ryan and Pauline and the other speakers for sharing all this information. And of course a lot of love to the organizers of the conference for doing this all in the beautiful city of Lille.