---
layout: post
title: "NP_ASrecentlist"
date: 2004-12-26 6:19
comments: true
categories: 
  - leftontheweb 
---
My christmas present to the <a href="http://www.nucleuscms.org/">Nucleus</a> community: <a href="http://www.stefankoopmanschap.nl/php/NP_ASrecentlist.tar.gz">NP_ASrecentlist</a>: A Nucleus plugin that fetches the last 10 tracks off your <a href="http://www.audioscrobbler.com/">AudioScrobbler</a> account and displays them in your weblog.
