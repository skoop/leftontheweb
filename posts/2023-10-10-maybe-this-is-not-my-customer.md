---
layout: post
title: "Maybe this is not my customer?"
date: 2023-10-10 14:00:00
comments: true
categories: 
  - php
  - business
  - customers
social:
    highlight_image: /_posts/images/customer.png
    highlight_image_credits:
        name: Leonardo.ai
        url: https://leonardo.ai/
    summary: Sometimes when talking to a potential customer you have to stop and think if you are the right fit for that customer. And sometimes, the answer is "no".

---

As an entrepreneur you want to help your customers as well as you can. But sometimes you have to stop and think: 

> Am I really helping this person or company by taking them on as a client?

I was reminded of this when I had a meeting with a potential client earlier this week. They approached me with a question about a specific piece of technology I had worked with in the past, and what they were trying to do sounded like something I could help with. I was excited to talk to the to see if I could help them.

As the meeting progressed I got the feeling more and more that what they wanted was indeed a potential solution to their issue, but the way they wanted it was something I had no experience with and it was so far out of my comfort zone that while I could probably do it, it would cost me way too much time and because of that it would cost them way more money. Especially considering there was a pretty hard deadline, and I was unsure if I would be able to make that deadline. 

No. I was not the right person to solve this issue.

So what is the next best thing I could do? Help them find a knowledgeable person in this area, and get them in touch with those people. So that is what I did. I simply told them "I'm not the right person for this, but I know someone who can help you". And that was appreciated. 

I see too many examples out there of people using _tool x_ to solve _issue y_ even if _tool x_ is not the right solution to that issue. A CMS, an e-commerce solution and a CRM all do some form of content management, but that doesn't mean they're interchangeable. I also see too many examples out there of people thinking _as long as I can sell myself to this customer, I can probably learn along the way_. And while that is not necessarily a bad thing, if the customer has a strict deadline or explains that there is no room for mistakes on this project, they still take that project on.

Build a network around you of experts. If you can't do it, pass the project on to someone who can. They will do the same with projects they can't do, but you can do. The customer should be the most important, at all times. Their issue needs to be solved. Whether you do it, or someone else, that is the most important. 

So next time you sit with a potential customer, and you get the feeling that this might not be a good fit, be honest about it. See if there's room for you to learn. If not, find someone in your network that can do the job. Your not-costumer will thank you for it.