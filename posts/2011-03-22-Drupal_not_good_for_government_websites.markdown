---
layout: post
title: "&quot;Drupal not good for government websites&quot;"
date: 2011-03-22 13:24
comments: true
categories: 
  - drupal 
  - opensource 
  - cms 
  - framework 
  - php 
  - symfony 
  - opinion 
  - therighttool 
  - zendframework 
---
<p>I am not a fan of Drupal. Nor am I a big fan of Joomla. But both have their place within the PHP and CMS ecosystem. And there are many situations where clearly one of these tools is the best for the job. For big custom projects, obviously my choice would be either symfony (or Symfony2), or Zend Framework. Or well, any web application framework for that matter.</p>

<p>And this is also a (valid) point that B
