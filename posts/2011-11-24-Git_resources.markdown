---
layout: post
title: "Git resources"
date: 2011-11-24 22:31
comments: true
categories: 
  - git 
  - documentation 
  - blogs 
  - training 
  - drupal 
  - joomla 
---
<h2>Think Like A Git</h2>
<p><a href="http://think-like-a-git.net/">This one</a> I came by first through <a href="http://linktuesday.com/">#linktuesday</a>. The site was started to make learning Git a lot easier, and I'm thinking they're succeeding. If you're new to Git (and even if you're not), this one might well be very useful to you.</p>

<h2>Pro Git</h2>
<p>I guess everyone who ever looked into Git has already seen <a href="http://progit.org/">Pro Git</a>, but I need to share this one if I'm talking about Git resources. It's still my default Git resource when I need to look something up.</p>

<h2>Starting With Git: Cheat Sheet</h2>
<p>Cheat sheets are always good, but this one is special. It's not just a listing of the Git commands, but actually contains some explanations as well, making it extra useful. I refer to <a href="http://thinkvitamin.com/code/starting-with-git-cheat-sheet/">this one</a> in every single training I deliver on Git.</p>

<h2>Building a Drupal site with Git</h2>
<p>The company I deliver most of my Git sessions for is one of the leading development and maintenance shops for Drupal and Joomla! projects in The Netherlands, so I've been digging into specific documentation for working with Git in combination with those projects. It turns out that Drupal already has <a href="http://drupal.org/node/803746">excellent documentation</a> on the topic!</p>

<h2>How to track your Joomla! project with Git</h2>
<p>It looks like Joomla! is a bit behind on the Git bandwagon, but I learned today from one of my students that Joomla! has actually finalized the decision to fully move to Git, so I'm guessing more documentation will follow soon for Joomla!, right now there is already some third-party blogs that have published some stuff, like the <a href="http://www.howtojoomla.net/how-tos/development/how-to-track-your-joomla-project-with-git">article by howtojoomla.net</a> on how to manage your Joomla! project with Git.</a>

<h2>Let me know!</h2>
<p>If you know of any other resources on using Git, let me know, I'm always happy to include more links in my training sessions!</p>
