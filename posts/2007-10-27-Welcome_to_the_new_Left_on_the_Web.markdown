---
layout: post
title: "Welcome to the new Left on the Web"
date: 2007-10-27 6:28
comments: true
categories: 
  - leftontheweb 
---
<p>It&#39;s not that Textpattern is bad or anything, but it just doesn&#39;t use the coding structure that I like and am used to, and so adapting it to my wishes was a bit hard. And since finding out about symfony, the problem maintaining software is gone, since at least in my experience maintaining a symfony project is simple (at least for applications like this site ;) )</p><p>I&#39;ve made some changes to the site as well. I&#39;ve implemented Flickr integration using the <a href="http://drewish.com/projects/phlickr/" target="_blank">Phlickr</a>  library (with my own custom caching... which was a bit hard to get worked into the site... I might write another post on this topic somewhere in the future) and am now also using <a href="http://ma.gnolia.com/" target="_blank">Ma.gnolia</a>  for link management, you can see my 5 most recent links on the homepage and a slightly bigger archive in the <a href="/links" target="_blank">links section</a>  of this site. It&#39;s currently used as a sort of linkdump, but the links page might end up becoming a complete archive. I&#39;m not sure yet about this.</p><p>So yeah, there already is a bit of doubt about the site now. I&#39;ve been changing quite a bit of stuff and that usually means I have 100&#39;s more ideas. But for now, I wanted to deploy this version of the site because I feel what I have here is stable enough for that. There might be some future changes/extensions though, I&#39;m just getting started! ;) </p><p>&nbsp;</p>
