---
layout: post
title: "Deployment"
date: 2006-09-06 11:12
comments: true
categories: 
  - technology 
---
One of the things where Java is way ahead of PHP at the moment is the deployment of projects. There are projects, such as <a href="http://phing.info/trac/">Phing</a>, that are already a good effort, and will make deploying your project easier. Yet, it's no <a href="http://maven.apache.org/">Maven</a> for instance.

In a <a href="http://www.sebastian-bergmann.de/blog/archives/622-PHP-Deployment-Model.html">recent weblog post</a>, Sebastian Bergmann, author of PHPUnit, asks for someone to step up to write a paper on Deployment in PHP, especially of large scale PHP applications. I would also be very interested in this. In my previous work, we've been struggling often with the deployment of our applications, missing the correct tools for easy deployment. It would be very good if such a paper were written.
