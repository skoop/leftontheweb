---
layout: post
title: "A Dream Come True"
date: 2006-02-06 2:13
comments: true
categories: 
  - music 
---
A dream come true. Something I've said should be happening for years on end is finally going to happen.

<a href="http://www.friendsoflive.com/">Live</a> will again be playing in <a href="http://www.paradiso.nl/">Paradiso</a>. I've seen them there once before, in 1997. Paradiso is the perfect place for a show by Live. And they're doing it again!!!!!!

<a href="http://www.livefanclub.nl/">Fanclub members</a> have a good chance of getting tickets. Boy, am I glad I'm a fanclub member.

<strong>update</strong> Live is doing another concert, in the worst venue of The Netherlands: Heineken Music Hall. Ah well, there's a good chance we'll still go.
