---
layout: post
title: "Silverfall"
date: 2005-10-29 11:16
comments: true
categories: 
  - books 
---
Ever since it was first published, I've been wanting to read <a href="http://www.fantasylibrary.net/book.php/593">Silverfall</a>. However, it was at first only available as hardcover. Though I do have quite a few hardcovers, I buy most books as paperback, simply because I'd have to be filthy rich to have em all in hardcover version, and also of course because reading a paperback is much easier, especially when reading on the train and such.

Anyway, today, we were in Utrecht, and visited the <a href="http://www.elffantasy.nl">Elf Fantasy</a> store. And wow, there it is. A single paperback copy of Silverfall. I of course immediately bought it. Finally I can read this book :)
