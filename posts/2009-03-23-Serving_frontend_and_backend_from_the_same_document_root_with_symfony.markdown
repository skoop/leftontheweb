---
layout: post
title: "Serving frontend and backend from the same document root with symfony"
date: 2009-03-23 21:28
comments: true
categories: 
  - symfony 
  - php 
  - howto 
  - apache 
  - vhost 
  - rewrite 
  - directoryindex 
---
<p>Quite often have I been staring at my virtualhost setup for the admin subdomain of this application, wondering why the hell it kept serving the frontend. I&#39;d set the DirectoryIndex to backend.php, so why did it keep serving the frontend? There would be no reason for it to do that, it seemed to ignore my DirectoryIndex setting!</p><p>Today, for something unrelated, I opened my project&#39;s .htaccess file and realised my mistake. Apache had been configured perfectly well for all this time. Because of course, the rewrite rules in the .htaccess were configured to point to index.php!</p><p>So, I moved all the rewrite rules into both my frontend and my backend vhost, changed the backend vhost to rewrite into backend.php, and lo and behold: it worked! All this time I was not considering the .htaccess!</p><p>Now, having moved the settings into my virtualhost setup has another advantage: performance. Apache now doesn&#39;t need to parse my .htaccess anymore (which it has to do on every request) but instead parses the virtualhost at startup and then remembers the settings. So the application should be performing better as well! Double WIN! </p>
