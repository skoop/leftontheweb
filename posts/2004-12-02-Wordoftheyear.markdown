---
layout: post
title: "Word of the year"
date: 2004-12-02 4:10
comments: true
categories: 
  - leftontheweb 
---
'Blog' is now <a href="http://news.bbc.co.uk/1/hi/technology/4059291.stm">word of the year</a>. For some reason, a lot of people didn't know what the word 'blog' meant this year. Isn't that strange? I mean, everyone and their grandma have at least one weblog, and yet nobody knows what it means.

Personally, I hate the term 'blog'. I'd rather speak of a 'weblog'. 'Blog' sounds like a scary disease.
