---
layout: post
title: "R.A. Salvatore - The Two Swords"
date: 2006-04-02 4:50
comments: true
categories: 
  - books 
---
This book has been on my stack of books to be read for way too long. I'm a huge fan of <a href="http://www.rasalvatore.com/">Salvatore</a>'s books about <a href="http://en.wikipedia.org/wiki/Drizzt">Drizzt Do'Urden</a>, and The Two Swords has been out for quite a while, has actually been standing on one of my bookshelves for quite some time. I am glad I finally started reading the book.

It's been a while since I read the previous book in the series, and so it took me a short while to remember what happened in the previous book. Funny enough, Salvatore didn't even pause to look back and just went on with the story. This is good of course, because you don't want to linger in what you know already, it just proved to be a slight difficulty for me since I needed to remember the previous storyline a bit. There were enough references though for me to pick up on the story. 

As all books about Drizzt Do'Urden, this one also brings a lot of excitement, a great storyline, wonderful descriptions of the surrounding (The Shining White sounds wonderful!) and great characters. With Drizzt being seperated from his friends, believing them dead, for the biggest part of the book, you actually follow two completely seperate storylines, until finally the storylines come together and the friends are finally re-united. The death of Wulfgar's wife was a bit shocking, though I suspect this paves the way for another romance, that might cause Drizzt's heart to be broken. 

I suspect by now at least one new book in this series is available, so I'll soon have to order that one to get my fix of Salvatore. I definitely again am fully into Salvatore's books, and am looking forward to reading the next book.
