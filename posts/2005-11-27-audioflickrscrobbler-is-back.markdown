---
layout: post
title: "AudioFlickrScrobbler is back!"
date: 2005-11-27 11:37
comments: true
categories: 
  - music 
---
It's been broken for way too long, so I just took some time to fix my <a href="https://skoop.dev/audioflickrscrobbler">AudioFlickrScrobbler</a>, one very nice script (that I <a href="http://interface-7.net/20050418/">didn't fully write myself</a>) that combines the data from AudioScrobbler/<a href="http://www.last.fm/">Last.fm</a> and <a href="http://www.flickr.com/">Flickr</a>.

<a href="https://skoop.dev/audioflickrscrobbler">Enjoy</a>!
