---
layout: post
title: "Recent symfony presentations"
date: 2008-04-12 20:49
comments: true
categories: 
  - symfony 
  - presentations 
  - nllgg 
  - pfcongrez 
  - php 
---
<p>Last week I did a presentation for the Dutch Linux Usergroup on symfony. It was an introductory talk as the audience there would probably not be very php-oriented. The presentation there went very well, even though there were only 4 people attending the talk. The presentation went very smoothly, and the fact that there so little people made a nice discussion possible with the attendees.</p><p>This week, I did a very similar presentation at an event that has a much larger scale. The pfCongrez had over 250 attendees for whom I presented the symfony framework. The slides (<a href="http://www.slideshare.net/skoop/symfony-framework-pfcongrez" target="_blank">here</a>) were nearly the same, the &quot;practical&quot; part of the presentation was nearly the same, the audience was just slightly different. Bigger and more experienced in PHP. Mostly the presentation went fine, but some technical trouble threw me off my story slightly, so I&#39;m not as happy with how this one went. But by the (very) positive response from the audience afterwards I can judge that the message came across anyhow. </p><p>Aside from my own presentation at pfCongrez, the other presentations were also very nice. <a href="http://www.quirksmode.org/" target="_blank">Peter Paul Koch</a>  did a very nice presentation on unobtrusive javascript, Jilles Oldenbeuving from <a href="http://www.marktplaats.nl/" target="_blank">Marktplaats.nl</a>  gave us a sneak view into the systems that run Marktplaats, and <a href="http://www.naarvoren.nl/" target="_blank">Robert Jan Verkade</a>  and <a href="http://cinnamon.nl/" target="_blank">Stephen Hay</a>  did a nice session on making sites more accessible by optimizing CSS and Javascript usage. Some excellent examples in that presentation. It was a great day, a great experience and I met some really nice people. </p>
