---
layout: post
title: "Being lead by mixed emotions"
date: 2006-11-29 12:16
comments: true
categories: 
  - personal 
---
For a long time already, Marjoleins grandpa is having trouble with his health. He has been in and out of hospitals quite a few times over the past years. On multiple occasions we visited him and expected this to be his last hospital visit. And every time, the man again was able to get out of the hospital and live his life.

Last weekend, he was again taken to the hospital. When Marjolein came back from visiting him last sunday evening, she told me this was worse than before. And indeed, the hospital also thought so. On monday morning, his kidney and heart activity was lessening to dangerous levels. They called that if we wanted to visit him, we better do it quick. So I took off from work expecting the worst.

Arriving at the hospital in the <acronym title="Intensive Care Unit">ICU</acronym>, in the hallway already we heard him joking to the nurses. He definitely did not look as bad as we expected him to be. He joked and seemed very much alive. When asked, he did mention having pain even though he was on morphine, but he definitely was much better when compared to sunday evening.

It's amazing. Because since then, he's getting stronger again. If he keeps up like this, he'll be moved out of the ICU into a regular room again. He's almost off the morphine, and even though he still does not feel very good, definitely does not feel as bad as before either.

The emotions are kicking you around though, sadness expecting to lose him, then happiness at him seeming to survive again. It's very hard to cope with such sudden mixtures of emotions. Whenever it is my time to go, let me go suddenly. It's a single emotional blow, but in the end much better than this.
