---
layout: post
title: "My Tools: Alfred"
date: 2011-06-29 12:05
comments: true
categories: 
  - tools 
  - alfred 
  - launcher 
  - launchy 
  - gnome-do 
---
<p>When I first got a MacBook Pro when I started at <a href="http://ibuildings.com/">Ibuildings</a>, I asked around which apps I would really need in my day to day work. One of the suggestions I got was Textmate, which was definitely a good choice. Another suggestion I got was Quicksilver, a launcher app. For years I used Quicksilver, but no new updates were being released for a long time and I had some (admittedly small) issues with it. While working for <a href="http://angrybytes.nl/">AngryBytes</a> last year, a developer there pointed me towards Alfred. Alfred is another launcher, very similar to Quicksilver, but with active development as some additional features.</p>

<p>What I like about working with a launcher app is that I don't have to open the Applications folder, browse through a long list of installed applications then select the one I want and launch that. Instead, I just press alt-space (this is the hotkey I've configured for Alfred), type the first couple of letters of the application that I want to start, then type enter. Alfred will autocomplete the title. If there are multiple applications that match the name, I can select the one I want. This saves me a lot of clicks and some time</p>

<p>But Alfred can do more. In the same way I can start applications, I can also open documents. Alfred will index the documents on my Mac and allow me to open a document directly from the Alfred interface. In a similar way, if you configure this, Alfred can give access to your e-mails, contacts and a lot of other information on your computer.</p>

<p>To prevent pollution of the results Alfred returns, you have the option to configure a certain search scope. Alfred won't index files that are not in the search scope, so that you don't get a huge list of results when you search for generic words.</p>

<p>For me, Alfred (and Quicksilver before that) is a huge timesaver. When working on Windows, I usually install Launchy immediately, and when working on Linux I use Gnome-do for this. I can not do without a launcher anymore.</p> 
