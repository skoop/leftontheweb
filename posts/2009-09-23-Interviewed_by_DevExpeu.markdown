---
layout: post
title: "Interviewed by DevExp.eu"
date: 2009-09-23 5:18
comments: true
categories: 
  - php 
  - symfony 
  - personal 
  - meta 
---
The questions range from projects to how I got to where I am today. So do <a href="http://www.devexp.eu/2009/09/22/interview-stefan-koopmanschap/" target="_blank">check out the interview</a>. It seems the guys of DevExp are working on getting more interviews up on their site in the coming period, so add their feed to your reader :)
