---
layout: post
title: "My schedule for PHPNW08"
date: 2008-11-20 21:19
comments: true
categories: 
  - phpnw08 
  - phpnw 
  - schedule 
  - conference 
---
<p>PHPNW08 will differ a bit from a lot of other conferences I&#39;ve been to, in that it&#39;s not a big commercial conference but a community-driven one. Though one could argue that the Dutch PHP Conference has a very community-feel to it, it is still only organized by one commercial organization, where PHPNW is a true usergroup conference such as we hope to have in the Netherlands one day with the PHP UserGroup here.</p><p>Anyway, so, what&#39;s going to happen at the conference. It will all start on friday night with the pre-conference social. I&#39;ve heard some strong rumours that there will be some Mario Kart there, which should be fun. But as fun as Mario Kart will be, I am very much looking forward to meeting such people as Rob Allen, Ciaran Walsh, Stuart Herbert and Steph Fox as well as re-meeting Derick, Zoe, Johannes, Arpad, Ivo, Scott, Felix, Lorna, and everyone else that I&#39;m forgetting right now ;) To me, meeting the community is probably even more fun than attending sessions and speaking. I know, I&#39;m a community addict.</p><p>As for the sessions, my plans are as follows:</p><p>Derick Rethans&#39; KISS keynote<br />MySQL EXPLAIN Explained by Adrian Hardy<br />Regular Expression Basics by Ciaran Walsh<br />What&#39;s new, what&#39;s hot in PHP5.3 by Johannes Schluter<br />obviously - my own talk<br />Arpad Ray&#39;s Exploiting PHP with PHP<br />Panel Discussion: State of the Community</p><p>I am already sure that it will be an excellent weekend, and am very much looking forward to it. I hope to see a lot of people there, have interesting discussions, convince people that refactoring is a good thing, and mainly: have fun! </p>
