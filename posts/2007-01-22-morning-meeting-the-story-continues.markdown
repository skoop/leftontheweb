---
layout: post
title: "Morning meeting: The story continues..."
date: 2007-01-22 3:17
comments: true
categories: 
  - technology 
---
While working for <a href="http://www.tomtom.com/">TomTom</a>, I got introduced into the world of <a href="http://en.wikipedia.org/wiki/Agile_development">Agile Development</a>. One of the most useful things I got to learn, aside from firmly planned iterations, was the morning stand-up meeting: A short meeting to go through what every has done, what everyone will be doing, and where people are stuck. These meetings serve multiple purposes:

* Team building: People know what the other team members are working on
* Problem solving: People don't get stuck on things for too long, help is only one question away. And since there is a platform for these questions, people don't delay asking someone else
* Project management knows the status of projects
* It also gives clarity for the developer: he/she has to think about where they are and what they are doing

Today we had the first of these meetings here at <a href="http://www.dop.nu/">DOP</a>. It is my first measure in trying to get the development more structured. And it has been useful already. I now know a bit more about what everyone else is working on. Someone was stuck on something, someone else immediately volunteered to have a look at it together. So even though it was only the first meeting, I think it was already a success.

I also asked about Agile/<a href="http://www.extremeprogramming.org/index.html">Extreme Programming</a> and <a href="http://www.controlchaos.com/about/">Scrum</a> on <a href="http://www.linkedin.com/">Linkedin</a> Answers, and got some very useful response there as well. The general message seems to be that you should not implement any of these methodologies blindly, but simply pick those parts that seem the most useful to your organization. Also, it should be implemented gradually, not all at once. Since I'm going to write a plan for the whole structure of development at DOP anyway, that'll give me some time to look at this and see what can be useful and what can't.

For my own project, I've already decided to also start working in iterations. Instead of 30 days or 2 weeks, as seems to be the most common, I've decided my iterations will be one week. This seems long enough for the project I'm working on, and gives an easier planning for me, and more clarity for sales and account management.
