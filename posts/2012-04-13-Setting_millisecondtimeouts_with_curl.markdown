---
layout: post
title: "Setting millisecond-timeouts with curl"
date: 2012-04-13 14:34
comments: true
categories: 
  - php 
  - curl 
  - timeout 
  - libcurl 
---
<p>So how did I found out about this? Thanks to Steve Kamerman, who took the time to <a href="http://nl3.php.net/manual/en/function.curl-setopt.php#104597">comment to the PHP documentation</a>. It turns out this issue is caused by the way libcurl behaves on linux and unix systems, where a SIGALRM is raised during the name resolution that is being caught by libcurl as a timeout alarm. Because of this, the request times out immediately instead of after the actual timeout.</p>

<p>The solution is to tell curl to ignore signals alltogether. This is not a perfect solution of course, because you're now suppressing something that can be useful in other situations, but in this case we decided the timeouts to be too important for the software to not use timeouts. So now it looks like this:</p>

<pre>
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_TIMEOUT_MS, 250);
curl_setopt($curl, CURLOPT_NOSIGNAL, 1);
</pre>

<p>It now works like a charm!</p>
