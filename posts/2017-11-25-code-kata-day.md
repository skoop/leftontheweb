---
layout: post
title: "Code Kata Day"
date: 2017-11-25 21:00:00
comments: true
categories: 
  - php 
  - codekata
  - domcode
  - python
  - kotlin
  - elixir
social:
    summary: Code katas are a great way to practice your programming skills, but usually you do so in the language of your own choice. DomCode, the polyglot user group in Utrecht, organized an event to break that habit. 
    
---

Today I was at the [DomCode Code Kata Day](https://www.meetup.com/DomCode/events/244171361/) in Utrecht. Over the course of the day, we were given 4 different code katas with about an hour each to solve the problem. You could either pick a programming language you wanted to learn more about or use the [wheel of languages](http://www.domcode.org/katas/) to get a random language. Here's a summary of the day and how it helped me become a better programmer and why I think it worked so well.

## Kata 1: 99 bottles of Kotlin

The first kata we got to do was a relatively simple one (with some minor but nasty details): [99 bottles](https://github.com/domcode/kataday/blob/master/99_Bottles/README.md). The idea is to write a piece of code that would "sing" the 99 botles of beer song. I spun the wheel and got [Kotlin](https://kotlinlang.org/) as the language to use for this kata. Given my experience is about 99.999% PHP, any venture outside of PHP would be an interesting exercise I was quite curious about whether I could solve this. But the problem was relatively easy, so I set out to try it.

Kotlin is a relatively easy language to get started in, especially when you're used to languages such as PHP and Python. The syntax is quite similar and [the documentation](https://kotlinlang.org/docs/reference/) is pretty good. It didn't take long until I had my [first attempt](https://gist.github.com/skoop/7e6ba4169ea3ee4951c494e02377daa3) working. Well, almost working. As it turned out I had not taken into account that when there are 2 bottles of beer on the wall and you take one down, there is not `1 bottles` of beer on the wall but there is `1 bottle` of beer (thanks Ross for pointing that one out). After quickly fixing that, it worked like a charm. Ross gently nudged me to look at the `when` syntax, and since I had some time left I decided to refactor my first attempt into using `when`. [Success!](https://gist.github.com/skoop/216ac805148b917d55a1850b46747d75) It looked a lot more readable and it worked like a charm.

## Kata 2: OCR

In the [second kata](https://github.com/domcode/kataday/blob/master/OCR/README.md) we went back to the days of yore when printing fancy stuff required ASCII art. In this case, we get [a file with numbers in fancy ASCII](https://gist.github.com/skoop/e9f6cbb61aceacb8fcacade69d390567#file-file1-txt) and we need to parse it to PHP. I spun the wheel and was told to use [Python](https://www.python.org/) for this, but to get my head around how to parse stuff like this I started out with a proof of concept in PHP. Unfortunately even getting this to work with PHP took me way too long, so I ended up not being able to finish a Python version. The [PHP version](https://gist.github.com/skoop/e9f6cbb61aceacb8fcacade69d390567#file-parse-php) works like a charm though. It turns out one of the biggest challenges I had was the fact that ASCII also relies heavily on spaces and my PHP IDE of choice (PHPStorm) strips spaces at the end of a file automatically. My code was working for a long time and I didn't realize the problem was with the file I was trying to parse!

Despite only having done this kata with my regular language of choice this was still a very good exercise in parsing unconvential data structures. Having to think about how to parse characters that are actually 3 lines high is pretty interesting, and I think I found a decent solution given the time constraints I had.

## Kata 3: Pig Latin

After lunch it was time for the third kata of the day: [Pig Latin](https://github.com/domcode/kataday/blob/master/Pig_Latin/README.md). I had never heard of this one before so it was very interesting to first brainstorm about the best way to actually do this. An added difficulty was the fact that the wheel of languages gave me [Elixir](http://elixir-lang.github.io/), a functional language in which everything is immutable. It was quite a paradigm shift for me, since I'm not used to functional programming, so it was quite interesting to combine these two unknowns.

Getting started with Elixer was quite hard. Having to think in such a different way made it extremely hard to get started by searching the web and reading the documentation I eventually got some code up and (nearly) running. Unfortunately I ran out of time before having a fully functional application, so this is one that I need to finish at a later date. I have saved my progress, so I can try and finish it.

Is it a problem that I didn't finish in time? Nope! Failure is the best way of learning, and I surely bumped my head a couple of times trying to implement this in Elixir. But I learned a lot from the experience, so it was definitely worth it.

## Kata 4: Blackjack

After lunch Clara, whom I met at [WeCamp 2017](https://weca.mp/), sat down next to me and as Blackjack was presented we decided to do pair programming on the last kata of the day. And that last kata turned out to be [Blackjack](https://github.com/domcode/kataday/blob/master/Blackjack/README.md), which has some interesting challenges to solve. Clara spun [Python](https://www.python.org/), which I quite liked since I never got to solving kata 2 in Python, and we started implementing Blackjack in Python. 

The idea was to write a piece of code that would get a set of "hands" and determine the winner of that round. For instance:

Clara: Q, J
Skoop: 9, K, 5
Jopie: 7, 5

The code should determine that Clara has 20 points, Skoop has 24 points and Jopie has 12 points, leaving Clara the winner since she has the highest score that does not exceed 21.

We had a basic script up and running without too much hassle but also without the main challenge (how to handle the Ace that can be worth 1 or 11, depending on the choice of the player). Handling the Ace turned out to be a pretty big challenge. As with kata 3, we were very close to actually solving the problem but at the end of the give timeframe we didn't have a working solution yet. We've both got the file on our laptops and will solve the problem at a later date. 

This kata gave us some nice challenges involving sorting and recursion. Python was pretty easy to pick up and thinking about how to solve this kata taught me a couple of things on how to work with datasets like these. Pairing on this problem was another good exercise which I quite enjoyed. 

## Why would one do a code kata?

There are many sites on the Internet that list code katas (coding exercises), but why would you actually do such a kata. Well, one of the most important reasons is to try your hand at a problem of a different type than the problems you try to solve in your day job. The challenge with katas however is not just in the type of problem, but also in the restrictions you place upon yourself when doing it. Restrictions you can think of are:

* Use a different programming language
* Limit the amount of time you can spend on the kata
* Limit the amount of lines you should use in your solution
* Determine the minimum speed of your code, i.e. your code needs to be finished in 100ms
* Determine a maximum amount of memory your code can use

Restrictions like these will allow you to think in creative ways to not just get a working solution but also a solution that requires you to care about specific elements of your code. It forces you to think out of the box.

## Why would I attend a code kata event?

Code katas are fun and useful, but attending a code kata event is even more useful. If you do code katas by yourself, you may set the restrictions with a bit of a bias on what you think you can solve. At an event, the restrictions are not set by yourself but by the organizers, causing you to be forced out of your comfort zone and having to think in ways you wouldn't otherwise do.

Another great aspect is the presence of other people, talking to them about their solutions and sometimes even pairing up with them to create a solution together. 

It is for these reasons that I would highly recommend to visit a local code kata event if there is one close to you. I would like to thank the great people of [DomCode](http://domcode.org) and [Infi](https://infi.nl/) for hosting the code kata event today. It was a great event!