---
layout: post
title: "Code analogy"
date: 2008-07-21 18:24
comments: true
categories: 
  - atk 
  - php 
  - atkMetaNode 
---
<blockquote>anything you want to change can be done in postMeta<br />only rarely you need to manually add stuff in the meta function<br />the policy is named policy because it enforces a standard on how to map database stuff to the application<br />in the meta function, this policy can be influenced<br />compare it to pregnancy<br />meta() is what happens inside the female<br />based on dna, policies, whatever food she happens to be eating etc, the baby is constructed<br />once it&#39;s born, you give it clothing etc.<br />sometimes however, you need to do surgery on the pre-born<br />if you wait until postmeta, it is either too late, or inefficient<br />which is why some stuff is done in meta()<br />e.g. if the womb is constructing a boy but you want a girl, you could let it be born first and then do a gender change<br />but that&#39;s a lot of overhead<br />so for performance reasons, you might change the gender before the utensils are appended to the unborn<br />(which is basically what things like $policy-&gt;setTypeAndParams do<br />(to make it a little more complete, even metanodes have a constructor, but that would then be comparable to conception, and you don&#39;t want to fiddle with stuff there yet)</blockquote>also published on the <a href="http://www.atk-framework.com/wiki/ATK/meta_vs_postMeta" target="_blank">ATK wiki</a> . 
