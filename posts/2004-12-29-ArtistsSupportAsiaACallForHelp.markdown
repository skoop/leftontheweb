---
layout: post
title: "Artists Support Asia - A Call For Help"
date: 2004-12-29 5:01
comments: true
categories: 
  - leftontheweb 
---
Artists Support Asia - A Call For Help

By now, everyone will know of what happened in Asia. An earthquake followed by tsunamis has resulted in tens of thousands of deaths, tens of thousands of wounded people and millions of homeless. Now, two days after it happening, help is getting started. And those organisations involved in this need our help. They need money to buy food and whatever other material that is necessary to help people.

Artists Support Asia is calling out for artists to help in their own way. Donate a track, one of your old tracks, one of your unreleased new tracks, or a track especially written for this project, to Artists Support Asia. We will be compiling these tracks into compilation cd-r's and sell them. All proceeds will be donated to the International Red Cross.

Artists! Want to help out? Join us now! Go to http://www.artistssupportasia.org/forum/ to show your interest in joining this project. Let's make a statement! Let's tell the world that independant artists can also show they care! Let's raise money to help all those affected by this terrible disaster.
