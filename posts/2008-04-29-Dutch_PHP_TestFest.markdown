---
layout: post
title: "Dutch PHP TestFest"
date: 2008-04-29 6:21
comments: true
categories: 
  - php 
  - phpgg 
  - event 
  - testfest 
  - usergroup 
---
<p>Aside from the fact that we&#39;re organizing the PHP TestFest, I am especially proud that we&#39;ve been able to get <a href="http://www.sebastian-bergmann.de/" target="_blank">Sebastian Bergmann</a>  over. He will be talking about testing PHP, and will also be available while attendees are writing the tests, to assist in the test writing and answer questions.</p><p>&nbsp;This event marks the re-launch of the Dutch PHP UserGroup as well. With the addition of two great Belgians, Michelangelo van Dam and Felix de Vliegher, I think the phpGG board has strengthened to allow for many great happenings in the future. </p>
