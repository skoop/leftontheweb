---
layout: post
title: "Installing Bolt extensions on Docker"
date: 2018-02-23 13:30:00
comments: true
categories: 
  - bolt
  - php
  - cms
  - docker
  - rancher
social:
    highlight_image: /_posts/images/bolts.jpg
    highlight_image_credits:
        name: frankieleon
        url: https://www.flickr.com/photos/armydre2008/5643873256/
    summary: I'm currently working on a website with the Bolt CMS. I am using an extension for that site, but since Bolt is using composer under the hood to install those and the extension is installed in vendor/, when I deploy this to production, I'm missing the extension. The solution was simple.
    
---

I'm currently working on a website with the [Bolt CMS](https://bolt.cm). For this website, I am using an extension. Now, the "problem" with extensions is that they are installed using Composer by Bolt, and end up in the .gitignore'd `vendor/` directory. Which is OK while developing, because the extension will just be in my local codebase, but once I commit my changes and push them, I run into a little problem.

## Some context

Let's start with a bit of context: Our current hosting platform is a bunch of [Digital Ocean](https://digitalocean.com/) droplets managed by [Rancher](https://rancher.com/). We use [Gitlab](https://gitlab.com) for our Git hosting, and use Gitlab Pipelines for building our docker containers and deploying them to production.

## The single line solution

In Slack, I checked with [Bob](https://twitter.com/bopp) to see what the easiest way was of getting the extensions installed when they're in the configuration but not in `vendor/`, and the solution was so simple I had not thought of it:

> Run `composer install --no-dev` in your `extensions/` directory

So I adapted my `Dockerfile` to include a single line:

```
RUN cd /var/www/extensions && composer install --no-dev
```

I committed the changes, pushed them, Gitlab picked them up and built the new container, Rancher pulled the new container and switched it on, and lo and behold, the extension was there!

![](/_posts/images/bolt-extension-installed.png)

Sometimes the simple solutions are actually the best solutions