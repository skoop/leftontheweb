---
layout: post
title: "ReviewMe: Another way to earn money with your website"
date: 2007-01-03 13:46
comments: true
categories: 
  - technology 
---
A while ago, I signed up for <a href="http://www.reviewme.com/">ReviewMe</a>, just to see what would happen. I added both this website, and <a href="http://www.electronicmusicworld.com/">Electronic Music World</a>. And yesterday, I found out it works.

An e-mail was waiting in my inbox that someone wanted something reviewed. I logged in to ReviewMe to find <a href="http://www.djdossiers.com/">DJDossiers</a> wanted me to review their site on Electronic Music World. I checked out the site, and felt I could write a review, so I accepted the review offer.

I just <a href="http://www.electronicmusicworld.com/2007/01/03/djdossiers-hosting-for-djs">Published the review</a>. I had quite a lot of fun. Usually, I review music on Electronic Music World, and so reviewing a site was a different experience, but it was fun to do.

So my conclusion: ReviewMe works. I get a bit of money, learned about a new site, played with the site to see how it worked, and wrote about how I felt. Luckily, there was no need for forced positiveness or to give them a harsh negative review, I could simply be enthousiastic about the site by checking it out: It's a nice thing they've set up for DJ's.
