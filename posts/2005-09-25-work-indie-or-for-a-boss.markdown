---
layout: post
title: "Work, indie or for a boss?"
date: 2005-09-25 4:24
comments: true
categories: 
  - personal 
---
Last week I made a hard and important decision: I would start looking again for a regular employment. Even though I love running my own business, now that our son is born, the incertainty, especially the financial risk, is harder to handle. I want to have some financial security again. That, combined with the fact that right now I'm working on two projects that have become a financial disaster for me, have made me make this decision.

So far though, I have not really had any regrets. After publishing my Curriculum Vitae on <a href="http://www.monsterboard.nl/">Monsterboard</a>, I've already been contacted by several people, mostly headhunters, to see if I was interested in one job or another. Some really interesting jobs as well. Of course, I haven't been hired yet so I'll have to wait with really positive remarks until something comes from it, but I'm definately optimistic about this.

Download my CV: <txp:file_download_link id="1"><txp:file_download_name /></txp:file_download_link>


