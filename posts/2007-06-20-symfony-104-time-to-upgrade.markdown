---
layout: post
title: "Symfony 1.0.4: time to upgrade"
date: 2007-06-20 13:59
comments: true
categories: 
  - technology 
---
The new version of Symfony has been <a href="http://www.symfony-project.com/weblog/2007/06/20/symfony-1-0-4-released.html">released</a>. It's a bugfix release, unfortunately still with no fixes for the bugs I've reported.

Those that saw my presentation at the Dutch PHP Conference may have noted that I was still running 1.0.1. I've now upgraded ;)
