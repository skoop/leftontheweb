---
layout: post
title: "Zend Studio &amp;#38; Symfony"
date: 2007-05-08 4:08
comments: true
categories: 
  - technology 
---
There is a little trick that <a href="http://www.vak18.com/">Peter</a> told me yesterday to get a better support for the code hinting/tooltips and code completion in Zend Studio when using <a href="http://www.symfony-project.com/">Symfony</a> with <a href="http://propel.phpdb.org/">Propel</a>:

In config/propel.ini, near the bottom of the file, you will find the setting:

<pre>propel.builder.addComments</pre>

This is set to false by default. What this basically means is that no phpDoc comments are added to your Base models. If you change this setting to true, and re-build your model, all of a sudden, when using for instance the retrieveByPK() method, the returned value all of a sudden also has all the code hinting/tooltips and code completion that is so great about Zend Studio.
