---
layout: post
title: "My plans for the Dutch PHP Conference"
date: 2007-06-15 8:20
comments: true
categories: 
  - technology 
---
Tomorrow is the Dutch PHP Conference in the RAI in Amsterdam. This is my plan for which sessions to attend:

* Welcome
* Key-note: My first Mash-up (English)
* Objects of Desire (English)
* Zend Framework (English)
* PHP Data Objects (English)
* Symfony framework (duh! I'm presenting!)
* Key-note: Test-driven Development (English)

It's gonna be a very cool conference, I'm sure of that. I'm very much looking forward to meeting the other speakers and attending the sessions.
