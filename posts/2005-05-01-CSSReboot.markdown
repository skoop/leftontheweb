---
layout: post
title: "CSS Reboot"
date: 2005-05-01 9:59
comments: true
categories: 
  - leftontheweb 
---
Well, we've just <a href="http://www.cssreboot.com/">Reboot</a>ed the <a href="http://www.webkoffer.org/">WebKoffer website</a>. After a few months of black-on-white standard design, we now have a real design up! Over the next week's, we'll still be tweaking it a bit, but this'll give you a good impression of how it's going to work!
